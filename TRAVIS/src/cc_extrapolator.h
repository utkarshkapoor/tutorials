/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_EXTRAPOLATOR_H
#define CC_EXTRAPOLATOR_H


// This must always be the first include directive
#include "config.h"

#include "cc_cubeframe.h"
#include "cc_bitset.h"
#include <vector>



//#define CHECK_EXTRAPOLATOR



void TestExtrapolator();



class CExtraPattern {
public:
	std::vector<int> m_iaSpatialIndex;
	std::vector<int> m_iaTemporalIndex;
	std::vector<double> m_faCoeff;
};




class CExtrapolator {
public:

	CExtrapolator() {
	}


	~CExtrapolator() {
	}


	void Initialize(
		int resx,
		int resy,
		int resz,
		int srangex,
		int srangey,
		int srangez,
		int trange,
		int sorder,
		int torder,
		int offsetx,
		int offsety,
		int offsetz,
		bool crosss,
		bool crosst,
		bool wrap,
		bool crossranges,
		bool crossranget,
		double distexpo,
		double timeexpo,
		double corrfactor,
		bool verbose,
		bool silent
	);


	void PushCubeFrame(const CCubeFrame *frame);

	void PushAtomFrame(const CAtomSet *frame);


	void InitializeXYZ(int trange, int torder, double timeexpo, bool verbose, bool silent) {

		Initialize(
			0,
			0,
			0,
			1,
			1,
			1,
			trange,
			0,
			torder,
			0,
			0,
			0,
			false,
			false,
			false,
			false,
			false,
			0.0,
			timeexpo,
			1.0,
			verbose,
			silent
		);
	}


	const CCubeFrame* GetCubeFrame(int depth) const {

		int i;

		if (depth >= m_iCubeFrameCount) {
			eprintf("CExtrapolator::GetCubeFrame(): Error: %d >= %d.\n",depth,m_iCubeFrameCount);
			abort();
		}
		i = m_iCubeFramePos - depth;
		if (i < 0)
			i += m_iTRange;
		return m_oaCubeFrames[i];
	}


	const CAtomSet* GetAtomFrame(int depth) const {

		int i;

		if (depth >= m_iCubeFrameCount) {
			eprintf("CExtrapolator::GetAtomFrame(): Error: %d >= %d.\n",depth,m_iCubeFrameCount);
			abort();
		}
		i = m_iCubeFramePos - depth;
		if (i < 0)
			i += m_iTRange;
		return m_oaAtomFrames[i];
	}


	double Extrapolate(int x, int y, int z) const {

		return ExtrapolateKnown( FindPattern(m_iTRange-m_iCubeFrameCount,x,y,z), x*m_iRes12 + y*m_iRes[2] + z );
	}


	double Extrapolate(int x, int y, int z, int index) const {

		return ExtrapolateKnown( FindPattern(m_iTRange-m_iCubeFrameCount,x,y,z), index );
	}


	double ExtrapolatePred(int index) const {

		return ExtrapolateKnownPred( m_iTRange-m_iCubeFrameCount, index );
	}


	double ExtrapolateCorr(const std::vector<double> &fa, int x, int y, int z, int index) const {

		return ExtrapolateKnownCorr( fa, FindPatternCorr(x,y,z), index );
	}


	double ExtrapolateXYZ(int index, int index2) const {

		return ExtrapolateKnownXYZ( m_iTRange-m_iCubeFrameCount, index, index2 );
	}


	double ExtrapolateKnown(int pattern, int index) const {

		double tf;
		unsigned int z;

		const CExtraPattern &p = m_oaPatterns[pattern];
		tf = 0;
//		if (pattern == 12)
//			mprintf("P12 Idx %u\n",index);
		for (z=0;z<p.m_iaSpatialIndex.size();z++) {
//			if (pattern == 12)
//				mprintf("    %d + %d = %d / %d\n",index,p.m_iaSpatialIndex[z],index+p.m_iaSpatialIndex[z],(int)160*160*160);
			const int ti = index + p.m_iaSpatialIndex[z];

			#ifdef CHECK_EXTRAPOLATOR
				if (ti < 0)
					eprintf("CExtrapolator::ExtrapolateKnown(): Pattern %d, Term %u/%lu: Fail1 %d + %d = %d\n",pattern,z,p.m_iaSpatialIndex.size(),index,p.m_iaSpatialIndex[z],ti);
				if (ti >= m_iRes012/*m_iRes[0]*m_iRes[1]*m_iRes[2]*/)
					eprintf("CExtrapolator::ExtrapolateKnown(): Pattern %d, Term %u/%lu: Fail2 %d + %d = %d\n",pattern,z,p.m_iaSpatialIndex.size(),index,p.m_iaSpatialIndex[z],ti);
				if ((p.m_iaSpatialIndex[z] >= 0) && (p.m_iaTemporalIndex[z] == 0))
					eprintf("CExtrapolator::ExtrapolateKnown(): Pattern %d, Term %u/%lu: Fail3.\n",pattern,z,p.m_iaSpatialIndex.size());
			#endif

	//		if (p.m_iaTemporalIndex[z] == 0)
	//			mprintf("@ Pattern %d, Index %d, z %d/%lu\n",pattern,index,z,p.m_iaSpatialIndex.size());
			const CCubeFrame *cfr = GetCubeFrame(p.m_iaTemporalIndex[z]);
		//	tf += p.m_faCoeff[z] * cfr->m_faBin[index+p.m_iaSpatialIndex[z]];
		//	tf += p.m_faCoeff[z] * cfr->m_iaMantis[ti] * pow10(cfr->m_iaExpo[ti]);
			tf += p.m_faCoeff[z] * cfr->m_faBin[ti];
		}
		return tf;
	}


	double ExtrapolateKnownPred(int pattern, int index) const {

		double tf;
		unsigned int z;

		const CExtraPattern &p = m_oaPatterns[pattern];
		tf = 0;
		for (z=0;z<p.m_iaSpatialIndex.size();z++)
			tf += p.m_faCoeff[z] * GetCubeFrame(p.m_iaTemporalIndex[z])->m_faBin[index];
		return tf;
	}


	double ExtrapolateKnownCorr(const std::vector<double> &fa, int pattern, int index) const {

		double tf;
		unsigned int z;

		const CExtraPattern &p = m_oaPatterns[pattern];
		tf = 0;
		for (z=0;z<p.m_iaSpatialIndex.size();z++) {
			const int ti = index + p.m_iaSpatialIndex[z];

			#ifdef CHECK_EXTRAPOLATOR
				if (ti < 0)
					eprintf("CExtrapolator::ExtrapolateKnownCorr(): Pattern %d, Term %u/%lu: Fail1 %d + %d = %d\n",pattern,z,p.m_iaSpatialIndex.size(),index,p.m_iaSpatialIndex[z],ti);
				if (ti >= m_iRes012)
					eprintf("CExtrapolator::ExtrapolateKnownCorr(): Pattern %d, Term %u/%lu: Fail2 %d + %d = %d\n",pattern,z,p.m_iaSpatialIndex.size(),index,p.m_iaSpatialIndex[z],ti);
				if (p.m_iaSpatialIndex[z] >= 0)
					eprintf("CExtrapolator::ExtrapolateKnownCorr(): Pattern %d, Term %u/%lu: Fail3.\n",pattern,z,p.m_iaSpatialIndex.size());
			#endif

			tf += p.m_faCoeff[z] * fa[ti];
		}
		return tf;
	}


	double ExtrapolateKnownXYZ(int pattern, int index, int index2) const {

		double tf;
		unsigned int z;

		const CExtraPattern &p = m_oaPatterns[pattern];
		tf = 0;
		for (z=0;z<p.m_iaSpatialIndex.size();z++) {
			const CAtomSet *afr = GetAtomFrame(p.m_iaTemporalIndex[z]);
			if (index < (int)afr->m_oaAtoms.size())
				tf += p.m_faCoeff[z] * afr->m_oaAtoms[index]->m_fRelCoord[index2];
			else
				tf += p.m_faCoeff[z] * afr->m_faCOM[index2];
		}
		return tf;
	}


	int FindPattern(int t, int x, int y, int z) const {

		int ix, iy, iz;

		if (x < m_iSOffset[0])
			ix = m_iSOffset[0] - x;
		else if (x > m_iTempVal[0]/*m_iRes[0]-m_iSRange[0]+m_iSOffset[0]*/)
			ix = m_iSOffset[0] + x - m_iTempVal[0]/*m_iRes[0]+m_iSRange[0]-m_iSOffset[0]*/;
		else
			ix = 0;

		if (y < m_iSOffset[1])
			iy = m_iSOffset[1] - y;
		else if (y > m_iTempVal[1]/*m_iRes[1]-m_iSRange[1]+m_iSOffset[1]*/)
			iy = m_iSOffset[1] + y - m_iTempVal[1]/*m_iRes[1]+m_iSRange[1]-m_iSOffset[1]*/;
		else
			iy = 0;

		if (z < m_iSOffset[2])
			iz = m_iSOffset[2] - z;
		else if (z > m_iTempVal[2]/*m_iRes[2]-m_iSRange[2]+m_iSOffset[2]*/)
			iz = m_iSOffset[2] + z - m_iTempVal[2]/*m_iRes[2]+m_iSRange[2]-m_iSOffset[2]*/;
		else
			iz = 0;

		return t * m_iSRange012/*m_iSRange[0]*m_iSRange[1]*m_iSRange[2]*/
			+ iz * m_iSRange01/*m_iSRange[0]*m_iSRange[1]*/ + iy * m_iSRange[0] + ix;

	//	mprintf("Pos (%d|%d|%d|%d) --> Pattern (%d|%d|%d|%d) Idx %d/%lu\n",t,x,y,z,it,ix,iy,iz,i,m_oaPatterns.size());
	}


	int FindPatternCorr(int x, int y, int z) const {

		int ix, iy, iz;

		if (x < m_iSOffset[0])
			ix = m_iSOffset[0] - x;
		else if (x > m_iTempVal[0]/*m_iRes[0]-m_iSRange[0]+m_iSOffset[0]*/)
			ix = m_iSOffset[0] + x - m_iTempVal[0]/*m_iRes[0]+m_iSRange[0]-m_iSOffset[0]*/;
		else
			ix = 0;

		if (y < m_iSOffset[1])
			iy = m_iSOffset[1] - y;
		else if (y > m_iTempVal[1]/*m_iRes[1]-m_iSRange[1]+m_iSOffset[1]*/)
			iy = m_iSOffset[1] + y - m_iTempVal[1]/*m_iRes[1]+m_iSRange[1]-m_iSOffset[1]*/;
		else
			iy = 0;

		if (z < m_iSOffset[2])
			iz = m_iSOffset[2] - z;
		else if (z > m_iTempVal[2]/*m_iRes[2]-m_iSRange[2]+m_iSOffset[2]*/)
			iz = m_iSOffset[2] + z - m_iTempVal[2]/*m_iRes[2]+m_iSRange[2]-m_iSOffset[2]*/;
		else
			iz = 0;

		return iz * m_iSRange01/*m_iSRange[0]*m_iSRange[1]*/ + iy * m_iSRange[0] + ix;
	}


	int m_iRes[3];
	int m_iSRange[3];
	int m_iSOffset[3];
	int m_iSOrder;
	int m_iTRange;
	int m_iTOrder;
	bool m_bCrossS;
	bool m_bCrossRangeS;
	bool m_bWrap;
	bool m_bCrossT;
	bool m_bCrossRangeT;
	double m_fDistExpo;
	double m_fTimeExpo;
	double m_fCorrFactor;

	double m_fDistExpoAdd;
	double m_fTimeExpoAdd;

	int m_iCubeFramePos;
	int m_iCubeFrameCount;

	int m_iSRange01;
	int m_iSRange012;
	int m_iTempVal[3];
	int m_iRes012;
	int m_iRes12;


	std::vector<const CCubeFrame*> m_oaCubeFrames;
	std::vector<const CAtomSet*> m_oaAtomFrames;
	std::vector<CExtraPattern> m_oaPatterns;
};



#endif



