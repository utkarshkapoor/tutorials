/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef BARBECUBE_H
#define BARBECUBE_H


// This must always be the first include directive
#include "config.h"

#include "tools.h"
#include "xdmatrix3.h"
#include "xstring.h"
#include <string>
#include "cc_bitset.h"


/* Frame Types:

	 0 - Reserved
	 1 - General Container Frame
	 2 - Index Frame
	 3 - Compressed Index Frame
	 4 - Trajectory Frame
	 5 - Compressed Trajectory Frame
	 6 - Volumetric Data Frame
	 7 - Compressed Volumetric Data Frame

*/

#define BARBECUBE_FRAMETYPE_GENERAL         (1)
#define BARBECUBE_FRAMETYPE_IDX             (2)
#define BARBECUBE_FRAMETYPE_COMPIDX         (3)
#define BARBECUBE_FRAMETYPE_TRAJ            (4)
#define BARBECUBE_FRAMETYPE_COMPTRAJSTART   (5)
#define BARBECUBE_FRAMETYPE_COMPTRAJ        (6)
#define BARBECUBE_FRAMETYPE_CUBE            (7)
#define BARBECUBE_FRAMETYPE_COMPCUBESTART   (8)
#define BARBECUBE_FRAMETYPE_COMPCUBE        (9)
#define BARBECUBE_FRAMETYPE_FILE           (10)
#define BARBECUBE_FRAMETYPE_COMPFILE       (11)


#define BARBECUBE_TYPE_STRING   (1)
#define BARBECUBE_TYPE_FLOAT    (2)
#define BARBECUBE_TYPE_DOUBLE   (3)
#define BARBECUBE_TYPE_INT8     (4)
#define BARBECUBE_TYPE_INT16    (5)
#define BARBECUBE_TYPE_INT32    (6)
#define BARBECUBE_TYPE_UINT8    (7)
#define BARBECUBE_TYPE_UINT16   (8)
#define BARBECUBE_TYPE_UINT32   (9)



extern bool g_bBarbecubeVerbose;


void SetBarbecubeVerbose(bool v);

bool WriteArrayToFile(FILE *a, const std::vector<unsigned char> &ia);

bool ReadArrayFromFile(FILE *a, std::vector<unsigned char> &ia, int length);

void ReadArrayFromFile(FILE *a, std::vector<unsigned char> &ia);


class CBarbecubeFile;
class CBarbecubeListEntry;


const char *GetFrameTypeString(int type);


class CBarbecubeIndex {
public:

	CBarbecubeIndex() {
	}

	CBarbecubeIndex(const CBarbecubeIndex &bi)
		: m_iaFrameLengths(bi.m_iaFrameLengths), m_iaFrameTypes(bi.m_iaFrameTypes) {
	}

	~CBarbecubeIndex() {
	}

	bool ImportFromArray(bool compressed, const std::vector<unsigned char> &ia);
	bool ExportToArray(bool compressed, std::vector<unsigned char> &ia);

	void Dump();

	std::vector<int> m_iaFrameLengths;
	std::vector<int> m_iaFrameTypes;
};


class CBarbecubeShortFrame {
public:

	CBarbecubeShortFrame()
		: m_iFrameType(0), m_iFrameTypeVersion(0), m_iIndex(0), m_iCRC32(0) {
	}

	~CBarbecubeShortFrame() {
	}

	int m_iFrameType;
	int m_iFrameTypeVersion;
	int m_iIndex;
	unsigned long m_iCRC32;

	std::vector<unsigned char> m_iaPayload;

};


class CBarbecubeListEntry {
public:

	CBarbecubeListEntry()
		: m_iFrameCount(-1), m_iFullFrameCount(-1), m_pIndex(NULL), m_pFile(NULL), m_iFrameStart(-1), m_iFrameEnd(-1) {
	}

	~CBarbecubeListEntry();

	std::string m_sFileName;
	int m_iFrameCount;
	int m_iFullFrameCount;
	CBarbecubeIndex *m_pIndex;
	CBarbecubeFile *m_pFile;
	int m_iFrameStart;
	int m_iFrameEnd;
};


class CBarbecubeFile {
public:

	CBarbecubeFile()
		: m_bEOF(false), m_bShortFrame(false), m_bListFile(false), m_pFile(NULL), m_bOpenRead(false), m_bOpenWrite(false), m_pShortFrame(NULL), m_pIndex(NULL), m_iTotalFrameCount(-1) {
	}

	~CBarbecubeFile();

	bool OpenRead(std::string s);
	bool OpenWriteAppend(std::string s);
	bool OpenWriteReplace(std::string s);
	bool Close();
	bool Rewind();

	bool SeekFrame(int i);
	bool SkipFrames(int i);

	bool IsEOF();

	bool OpenListFile(FILE *a);

	// General Frame Functions
	bool CreateGeneralFrame();

	// Short Frame Functions
	bool CreateShortFrame(int type, int version, int index);
	bool PushPayload(const std::vector<unsigned char> &ia);

	bool FinalizeFrame();

	bool WriteIndexFrame(bool compressed);

	int GetFrameIndex() const;
	int GetFrameType() const;
	int GetFrameTypeVersion() const;
	const std::vector<unsigned char>* GetFramePayload() const;

	bool ReadFrame();

	void DumpIndex();

	bool CheckIntegrity(std::string s, bool verbose);

	bool CompareCoords(std::string infile, std::string reffile, bool verbose);

	int GetTotalFrameCount() const {
		return m_iTotalFrameCount;
	}


private:
	int m_iFrameCounter;
	bool m_bEOF;
	int m_iListIndex;
	std::vector<CBarbecubeListEntry*> m_oaBarbecubeList;
	bool m_bShortFrame;
	bool m_bListFile;
	std::string m_sFileName;
	FILE *m_pFile;
	bool m_bOpenRead;
	bool m_bOpenWrite;
	CBarbecubeShortFrame *m_pShortFrame;
	CBarbecubeIndex *m_pIndex;
	int m_iTotalFrameCount;
};


class CBarbecubeTrajectoryFrameColumn {
public:

	CBarbecubeTrajectoryFrameColumn() : m_iType(255) {
	}

	CBarbecubeTrajectoryFrameColumn(unsigned char type, std::string label) : m_iType(type), m_sLabel(label) {
	}

	~CBarbecubeTrajectoryFrameColumn() {
	}

	bool ReadColumn(int ac, CBitSet *bs);
	void WriteColumn(int ac, CBitSet *bs);

	unsigned char m_iType;
	std::string m_sLabel;

	std::vector<CxString> m_aString;
	std::vector<double> m_aReal;
	std::vector<long> m_aSignedInt;
	std::vector<unsigned long> m_aUnsignedInt;
};


class CBarbecubeTrajectoryFrame {
public:

	CBarbecubeTrajectoryFrame() : m_iAtomCount(0), m_pCellMatrix(NULL) {
	}

	~CBarbecubeTrajectoryFrame() {
		int z;
		for (z=0;z<(int)m_oaColumns.size();z++)
			delete m_oaColumns[z];
		if (m_pCellMatrix != NULL) {
			delete m_pCellMatrix;
			m_pCellMatrix = NULL;
		}
	}

	bool ReadFrame(const std::vector<unsigned char> *data);
	void WriteFrame(std::vector<unsigned char> *data);

	CBarbecubeTrajectoryFrameColumn* GetColumn(std::string label);
	CBarbecubeTrajectoryFrameColumn* AddColumn(int type, std::string label);


	unsigned long m_iAtomCount;
	std::vector<CBarbecubeTrajectoryFrameColumn*> m_oaColumns;
	CxDMatrix3 *m_pCellMatrix;
};



#endif

