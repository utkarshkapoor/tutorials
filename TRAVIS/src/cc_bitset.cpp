/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "cc_bitset.h"


void CBitSet::Dump() const {
	
	int z, z2, ti;

	mprintf("\nBitSet Dump:\n");
	if (m_iExcessBits == 0)
		ti = (int)m_iaData.size();
	else
		ti = (int)m_iaData.size()-1;
	for (z=0;z<ti;z++) {
		mprintf("%6d: ",z*8);
		for (z2=0;z2<8;z2++)
			mprintf("%d ",(m_iaData[z]&(1<<(7-z2)))!=0?1:0);
		mprintf("   (%3d)\n",m_iaData[z]);
	}
	if (m_iExcessBits != 0) {
		mprintf("%6d: ",z*8);
		for (z2=0;z2<8-m_iExcessBits;z2++)
			mprintf("  ");
		for (z2=0;z2<m_iExcessBits;z2++)
			mprintf("%d ",(m_iaData[z]&(1<<(m_iExcessBits-z2-1)))!=0?1:0);
		mprintf("   (%3d)\n",m_iaData[z]);
	}
	mprintf("\n");
}


void CBitSet::DumpPlain() const {
	
	int z, z2, ti;

	if (m_iExcessBits == 0)
		ti = (int)m_iaData.size();
	else
		ti = (int)m_iaData.size()-1;
	for (z=0;z<ti;z++)
		for (z2=7;z2>=0;z2--)
			mprintf("%d",(m_iaData[z]&(1<<(7-z2)))!=0?1:0);
	for (z2=m_iExcessBits-1;z2>=0;z2--)
		mprintf("%d",(m_iaData[z]&(1<<(m_iExcessBits-z2-1)))!=0?1:0);
}


void CBitSet::ExportToFile(FILE *a) const {

	int z;
//	unsigned char c;

	for (z=0;z<(int)m_iaData.size()/4096;z++)
		fwrite(&m_iaData[z*4096],4096,1,a);
	if ((m_iaData.size()%4096) != 0)
		fwrite(&m_iaData[z*4096],m_iaData.size()%4096,1,a);

/*	for (z=0;z<(int)m_iaData.size();z++) {
		c = m_iaData[z];
		fwrite(&c,1,1,a);
	}*/
}


bool CBitSet::ImportFromFile(FILE *a, int bytes) {

	int z;
//	unsigned char c;

	m_iaData.resize(bytes);
//	m_iaData.clear();
	m_iExcessBits = 0;
	m_iReadPosBytes = 0;
	m_iReadPosExcess = 0;

	for (z=0;z<bytes/4096;z++) {

		fread(&m_iaData[z*4096],4096,1,a);

		if (feof(a))
			return false;
	}

	if ((bytes%4096) != 0)
		fread(&m_iaData[z*4096],bytes%4096,1,a); 

/*	for (z=0;z<bytes;z++) {
		fread(&c,1,1,a);
		m_iaData.push_back(c);
	}*/

	if (feof(a))
		return false;

	return true;
}


