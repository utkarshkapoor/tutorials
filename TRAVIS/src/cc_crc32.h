/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_CRC32_H
#define CC_CRC32_H


/* This code is taken from   http://www.networkdls.com/Software/View/CRC32   */


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <vector>
#include <stdlib.h>


class CCRC32 {
public:

	CCRC32();

	unsigned int ComputeCRC32(const std::vector<unsigned char> &data, int from=0, int to=-1) const;

	unsigned int ComputeCRC32_Begin(const std::vector<unsigned char> &data, int from=0, int to=-1) const;
	unsigned int ComputeCRC32_Continue(const std::vector<unsigned char> &data, unsigned int last, int from=0, int to=-1) const;
	unsigned int ComputeCRC32_Finish(unsigned int last) const;

private:
	unsigned int Reflect(unsigned int iReflect, const char cChar) const;
	unsigned int iTable[256]; // CRC lookup table array.
};


#endif

