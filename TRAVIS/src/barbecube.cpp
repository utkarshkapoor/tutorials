/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "barbecube.h"
#include "cc_crc32.h"
#include "cc_integerengine.h"
#include "timestep.h"
#include "xdvec3array.h"
#include "globalvar.h"
#include "conversion.h"


bool g_bBarbecubeVerbose = false;


CBarbecubeListEntry::~CBarbecubeListEntry() {

	if (m_pIndex != NULL) {
		delete m_pIndex;
		m_pIndex = NULL;
	}
	if (m_pFile != NULL) {
		delete m_pFile;
		m_pFile = NULL;
	}
}


CBarbecubeFile::~CBarbecubeFile() {

	int z;

	for (z=0;z<(int)m_oaBarbecubeList.size();z++)
		delete m_oaBarbecubeList[z];

	if (m_pShortFrame != NULL) {
		if (!m_bListFile) // Otherwise it is deleted elsewhere
			delete m_pShortFrame;
		m_pShortFrame = NULL;
	}

	if (m_pIndex != NULL) {
		delete m_pIndex;
		m_pIndex = NULL;
	}
}


void SetBarbecubeVerbose(bool v) {

	if (v)
		mprintf("\n    Switched on BQB verbosity.\n\n");

	g_bBarbecubeVerbose = v;
}


bool WriteArrayToFile(FILE *a, const std::vector<unsigned char> &ia) {

	unsigned int i;

	i = 0;
	while (i < ia.size())
		i += (unsigned int)fwrite(&ia[i],1,(i+4096<=ia.size())?4096:(ia.size()-i),a);

	return true;
}


bool ReadArrayFromFile(FILE *a, std::vector<unsigned char> &ia, int length) {

	unsigned int i, k;
	int z;
	static unsigned char buf[4096];

	i = (unsigned int)ia.size();
	ia.resize(ia.size()+length);
	while (i < ia.size()) {
		k = (unsigned int)fread(buf,1,(i+4096<=ia.size())?4096:(ia.size()-i),a);
		for (z=0;z<(int)k;z++)
			ia[i+z] = buf[z];
		i += k;
		if (feof(a))
			return false;
	}

	return true;
}


void ReadArrayFromFile(FILE *a, std::vector<unsigned char> &ia) {

	unsigned int k;
	int z;
	static unsigned char buf[4096];

	while (!feof(a)) {
		k = (unsigned int)fread(buf,1,4096,a);
		for (z=0;z<(int)k;z++)
			ia.push_back(buf[z]);
	}
}


long FindIndexPosition(FILE *a) {

	unsigned int i;
	unsigned char uc;
	long l;
	char buf[3];

	if (g_bBarbecubeVerbose)
		mprintf(">>> FindIndexPosition >>>\n");

	if (a == NULL) {
		if (g_bBarbecubeVerbose) {
			mprintf("    File pointer == NULL.\n");
			mprintf("<<< FindIndexPosition <<<\n\n");
		}
		return 0;
	}

	fseek(a,-3,SEEK_END);
	fread(buf,3,1,a);

//	if (memcmp(buf,"IDX",3) != 0) {
	if ((buf[0] != 'I') || (buf[1] != 'D') || (buf[2] != 'X')) { // To prevent Valgrind "uninitialized value" warning
		if (g_bBarbecubeVerbose) {
			mprintf("    Last three characters differ from \"IDX\".\n");
			mprintf("<<< FindIndexPosition <<<\n\n");
		}
		return 0;
	} else if (g_bBarbecubeVerbose)
		mprintf("    Found \"IDX\".\n");

	fseek(a,-7,SEEK_END);

	fread(&uc,1,1,a);
	i = uc << 24;
	fread(&uc,1,1,a);
	i += uc << 16;
	fread(&uc,1,1,a);
	i += uc << 8;
	fread(&uc,1,1,a);
	i += uc;

	if (g_bBarbecubeVerbose)
		mprintf("    Found index frame length %u.\n",i);

	fseek(a,-((int)i),SEEK_END);

	l = ftell(a);

	if (g_bBarbecubeVerbose) {
		mprintf("    Found index frame offset %ld.\n",l);
		mprintf("<<< FindIndexPosition <<<\n\n");
	}

	return l;
}


bool CBarbecubeIndex::ImportFromArray(bool compressed, const std::vector<unsigned char> &ia) {

	CIntegerEngine ie;
	CBitSet bs;
	int z;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeIndex::ImportFromArray >>>\n");

	if (compressed) {

		if (g_bBarbecubeVerbose)
			mprintf("    Importing compressed index frame...\n");

		m_iaFrameTypes.clear();
		m_iaFrameLengths.clear();

		bs.m_iaData.assign( ia.begin(), ia.end() );

		ie.DecompressSingle( &bs, m_iaFrameTypes, false );

		if (g_bBarbecubeVerbose)
			mprintf("    Decompressed %lu frame types.\n",m_iaFrameTypes.size());

	//	for (z=0;z<(int)m_iaFrameTypes.size();z++)
	//		mprintf("@@@ %d\n",m_iaFrameTypes[z]);

		ie.DecompressSingle( &bs, m_iaFrameLengths, false );

		if (g_bBarbecubeVerbose)
			mprintf("    Decompressed %lu frame lengths.\n",m_iaFrameLengths.size());

		for (z=0;z<(int)m_iaFrameTypes.size();z++)
			if ((m_iaFrameTypes[z] == 2) || (m_iaFrameTypes[z] == 3))
				eprintf("CBarbecubeIndex::ImportFromArray(): Warning: Index frame is on the index (%d/%lu).\n",z+1,m_iaFrameTypes.size());

	} else {

		eprintf("CBarbecubeIndex::ImportFromArray(): Uncompressed index not yet implemented.\n");
		abort();

	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeIndex::ImportFromArray <<<\n\n");

	return true;
}


bool CBarbecubeIndex::ExportToArray(bool compressed, std::vector<unsigned char> &ia) {

	CIntegerEngine ie;
	CBitSet bs;
	int i;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeIndex::ExportToArray >>>\n");

	PushStatistics();

	if (compressed) {

	//	for (i=0;i<(int)m_iaFrameTypes.size();i++)
	//		mprintf("@@@ %d\n",m_iaFrameTypes[i]);

		ie.CompressSingle(m_iaFrameTypes,&bs,false,true,true,50,1,false,false,false,10,false);

		i = bs.GetByteLength();
		if (g_bBarbecubeVerbose)
			mprintf("    Compressed %lu frame types to %d bytes.\n",m_iaFrameTypes.size(),i);

		ie.CompressSingle(m_iaFrameLengths,&bs,false,false,true,50,1,false,false,false,10,false);

		if (g_bBarbecubeVerbose)
			mprintf("    Compressed %lu frame lengths to %d bytes.\n",m_iaFrameLengths.size(),bs.GetByteLength()-i);

		ia.insert(ia.end(),bs.m_iaData.begin(),bs.m_iaData.end());

	} else {

		eprintf("CBarbecubeIndex::ExportToArray(): Uncompressed index not yet implemented.\n");
		abort();

	}

	PopStatistics();

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeIndex::ExportToArray <<<\n\n");

	return true;
}


void CBarbecubeIndex::Dump() {

	int z;
	unsigned long u;

	mprintf("\n");
	mprintf("    Offset    Length    Type    Ver\n");
	mprintf("----------------------------------------\n");

	u = 0;
	for (z=0;z<(int)m_iaFrameTypes.size();z++) {
		mprintf("%10lu  %8d     %3d    %3d\n",u,m_iaFrameLengths[z],m_iaFrameTypes[z]>>3,m_iaFrameTypes[z]%7);
		u += m_iaFrameLengths[z];
	}

	mprintf("\n");
}


bool CBarbecubeFile::OpenRead(std::string s) {

	long ip;
	unsigned char uc[5];

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::OpenRead >>>\n");

	if (m_bOpenRead || m_bOpenWrite) {
		eprintf("CBarbecubeFile::OpenRead(): Error: A file is already open.\n");
		return false;
	}

	m_pFile = fopen(s.c_str(),"rb");
	if (m_pFile == NULL) {
		eprintf("CBarbecubeFile::OpenRead(): Error: Could not open \"%s\" for reading.\n",s.c_str());
		return false;
	}
	m_sFileName = s;
	m_bOpenRead = true;
	m_bEOF = false;
	m_iFrameCounter = 0;

	fread(uc,5,1,m_pFile);
	if (memcmp(uc,"BQ",2) == 0) {
		if (g_bBarbecubeVerbose)
			mprintf("    File header is \"BQ\" --> Single File.\n");
		m_bListFile = false;
	} else if (memcmp(uc,"BLIST",5) == 0) {
		if (g_bBarbecubeVerbose)
			mprintf("    File header is \"BLIST\" --> List File.\n");
		m_bListFile = true;
		if (!OpenListFile(m_pFile)) {
			eprintf("CBarbecubeFile::OpenRead(): Error: Failed to open list file.\n");
			return false;
		}
		goto _end;
	} else {
		eprintf("CBarbecubeFile::OpenRead(): Error: File does not start with \"BQ\" or \"BLIST\".\n");
		return false;
	}

	ip = FindIndexPosition(m_pFile);

	if (ip != 0) {

		m_pIndex = new CBarbecubeIndex();

		if (g_bBarbecubeVerbose)
			mprintf("    Found index frame at offset %ld, reading...\n",ip);

		fseek(m_pFile,ip,SEEK_SET);

		if (!ReadFrame()) {
			eprintf("CBarbecubeFile::OpenRead(): Error: Could not read index frame.\n");
			return false;
		}

		if (m_pShortFrame->m_iFrameType == BARBECUBE_FRAMETYPE_COMPIDX) {

			if (m_pShortFrame->m_iFrameTypeVersion != 0) {
				eprintf("CBarbecubeFile::OpenRead(): Error: Unexpected frame version %d of compressed index frame.\n",m_pShortFrame->m_iFrameTypeVersion);
				return false;
			}

			m_pIndex->ImportFromArray(true,m_pShortFrame->m_iaPayload);

		} else if (m_pShortFrame->m_iFrameType == BARBECUBE_FRAMETYPE_IDX) {

			if (m_pShortFrame->m_iFrameTypeVersion != 0) {
				eprintf("CBarbecubeFile::OpenRead(): Error: Unexpected frame version %d of uncompressed index frame.\n",m_pShortFrame->m_iFrameTypeVersion);
				return false;
			}

			m_pIndex->ImportFromArray(false,m_pShortFrame->m_iaPayload);

		} else {

			eprintf("CBarbecubeFile::OpenRead(): Error: Unexpected frame type %d of index frame.\n",m_pShortFrame->m_iFrameType);
			return false;

		}

		m_iTotalFrameCount = (int)m_pIndex->m_iaFrameLengths.size();

		if (g_bBarbecubeVerbose)
			mprintf("    Positioning file pointer to start of file.\n");

	} else if (g_bBarbecubeVerbose)
		mprintf("    Found no index frame.\n");

	fseek(m_pFile,0,SEEK_SET);

	if (m_pShortFrame != NULL) {
		delete m_pShortFrame;
		m_pShortFrame = NULL;
	}

_end:

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::OpenRead <<<\n\n");

	return true;
}


bool CBarbecubeFile::ReadFrame() {

	unsigned char uc;
	unsigned char uci[2];
	unsigned int idx, len, crc;
	int ty, ve;
	CCRC32 ecrc;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::ReadFrame >>>\n");

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::ReadFrame(): Error: File not open for reading.\n");
		abort();
	}

	if (m_bListFile) {

		if (m_oaBarbecubeList.size() == 0) {
			if (g_bBarbecubeVerbose)
				mprintf("    LIST: Error: No entrys in list.\n");
			m_bEOF = true;
			return false;
		}

		if (m_iListIndex == -1) {
			if (g_bBarbecubeVerbose)
				mprintf("    LIST: Opening first entry from list...\n");
			m_iListIndex = 0;
			if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->OpenRead(m_oaBarbecubeList[m_iListIndex]->m_sFileName)) {
				if (g_bBarbecubeVerbose)
					mprintf("    LIST: Failed.\n");
				return false;
			}
			if (m_oaBarbecubeList[m_iListIndex]->m_iFrameStart > 0) {
				if (g_bBarbecubeVerbose)
					mprintf("    LIST: Skipping first %d entries...\n",m_oaBarbecubeList[m_iListIndex]->m_iFrameStart);
				if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->SeekFrame(m_oaBarbecubeList[m_iListIndex]->m_iFrameStart))
					return false;
			}
		}

_again:
		if (g_bBarbecubeVerbose)
			mprintf("    LIST: Reading frame from list entry %d...\n",m_iListIndex+1);
		if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->ReadFrame()) {
			if (g_bBarbecubeVerbose)
				mprintf("    LIST: Could not read further frame from list entry %d. Closing entry.\n",m_iListIndex+1);
			if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->Close()) {
				if (g_bBarbecubeVerbose)
					mprintf("    LIST: Failed to close entry.\n");
				return false;
			}
			if (m_iListIndex+1 == (int)m_oaBarbecubeList.size()) {
				if (g_bBarbecubeVerbose)
					mprintf("    LIST: No more list entries; leaving.\n");
				m_bEOF = true;
				m_iListIndex = -1;
				return false;
			}
			m_iListIndex++;
			if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->OpenRead(m_oaBarbecubeList[m_iListIndex]->m_sFileName)) {
				if (g_bBarbecubeVerbose)
					mprintf("    LIST: Error: Could not open list entry %d; leaving.\n",m_iListIndex+1);
				return false;
			}
			if (g_bBarbecubeVerbose)
				mprintf("    LIST: Opened list entry %d. Trying to read frame...\n",m_iListIndex+1);
			goto _again;
		}

		if ((m_oaBarbecubeList[m_iListIndex]->m_pFile->m_pShortFrame->m_iFrameType == 2) ||
			(m_oaBarbecubeList[m_iListIndex]->m_pFile->m_pShortFrame->m_iFrameType == 3)) {
			if (g_bBarbecubeVerbose)
				mprintf("    LIST: Is an index frame, skipping.\n");
			goto _again;
		}

		m_pShortFrame = m_oaBarbecubeList[m_iListIndex]->m_pFile->m_pShortFrame;

		if (g_bBarbecubeVerbose)
			mprintf("    LIST: Frame successfully read.\n");

	} else {

		if (m_pFile == NULL) {
			eprintf("CBarbecubeFile::ReadFrame(): Error: File pointer is NULL.\n");
			abort();
		}

		fread(uci,2,1,m_pFile);

		if (g_bBarbecubeVerbose)
			mprintf("    Offset is now %ld.\n",ftell(m_pFile));

		if (feof(m_pFile)) {
			m_bEOF = true;
			if (g_bBarbecubeVerbose) {
				mprintf("    Found EOF while reading frame header.\n");
				mprintf("<<< CBarbecubeFile::ReadFrame <<<\n\n");
			}
			return false;
		}

		if (memcmp(uci,"BQ",2) != 0) {
			eprintf("CBarbecubeFile::ReadFrame(): Error: Frame does not start with \"BQ\".\n");
			abort();
		}

		fread(&uc,1,1,m_pFile);

		ty = uc >> 3;
		ve = uc & 7;

		if (g_bBarbecubeVerbose)
			mprintf("    Found frame type %d, version %d.\n",ty,ve);

		if ((ty < 2) || (ty > 11)) {
			eprintf("CBarbecubeFile::ReadFrame(): Error: Frame type %d is not a valid short frame.\n",ty);
			abort();
		}

		if (m_pShortFrame != NULL)
			delete m_pShortFrame;
		m_pShortFrame = new CBarbecubeShortFrame();

		m_pShortFrame->m_iFrameType = ty;
		m_pShortFrame->m_iFrameTypeVersion = ve;

		fread(&idx,4,1,m_pFile);
		fread(&len,4,1,m_pFile);
		fread(&crc,4,1,m_pFile);

		m_pShortFrame->m_iIndex = idx;

		if (g_bBarbecubeVerbose)
			mprintf("    Found index %u, payload length %u, crc32 %u.\n",idx,len,crc);

		if (!ReadArrayFromFile(m_pFile,m_pShortFrame->m_iaPayload,len)) {
			eprintf("CBarbecubeFile::ReadFrame(): Error: Unexpected end of file while reading frame payload (%u bytes).\n",len);
			abort();
		}

		if (g_bBarbecubeVerbose)
			mprintf("    %u bytes of payload read.\n",len);

		if (g_bBarbecubeVerbose)
			mprintf("    Finished reading payload, CRC check...\n");

		m_pShortFrame->m_iCRC32 = ecrc.ComputeCRC32(m_pShortFrame->m_iaPayload);

		if (crc != m_pShortFrame->m_iCRC32) {
			eprintf("CBarbecubeFile::ReadFrame(): Error: CRC check failed for frame payload (computed %08lX, read %08X).\n",m_pShortFrame->m_iCRC32,crc);
			abort();
		}

		if (g_bBarbecubeVerbose)
			mprintf("    CRC matches. All done.\n");
	}

	m_iFrameCounter++;

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::ReadFrame <<<\n\n");

	return true;
}


bool CBarbecubeFile::OpenWriteAppend(std::string s) {

	long ip;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::OpenWriteAppend >>>\n");

	if (m_bOpenRead || m_bOpenWrite) {
		eprintf("CBarbecubeFile::OpenWriteAppend(): Error: A file is already open.\n");
		abort();
	}

	m_pIndex = new CBarbecubeIndex();

	m_pFile = fopen(s.c_str(),"r+b");

	if (m_pFile != NULL) { // File exists

		if (g_bBarbecubeVerbose)
			mprintf("    File already exists, looking for index frame...\n");

		ip = FindIndexPosition(m_pFile);

		if (ip != 0) {

			if (g_bBarbecubeVerbose)
				mprintf("    Found index frame at offset %ld, reading...\n",ip);

			fseek(m_pFile,ip,SEEK_SET);

			m_bOpenRead = true;
			ReadFrame();
			m_bOpenRead = false;

			if (m_pShortFrame->m_iFrameType == BARBECUBE_FRAMETYPE_COMPIDX) {

				if (m_pShortFrame->m_iFrameTypeVersion != 0) {
					eprintf("CBarbecubeFile::OpenWriteAppend(): Error: Unexpected frame version %d of compressed index frame.\n",m_pShortFrame->m_iFrameTypeVersion);
					abort();
				}

				m_pIndex->ImportFromArray(true,m_pShortFrame->m_iaPayload);

			} else if (m_pShortFrame->m_iFrameType == BARBECUBE_FRAMETYPE_IDX) {

				if (m_pShortFrame->m_iFrameTypeVersion != 0) {
					eprintf("CBarbecubeFile::OpenWriteAppend(): Error: Unexpected frame version %d of uncompressed index frame.\n",m_pShortFrame->m_iFrameTypeVersion);
					abort();
				}

				m_pIndex->ImportFromArray(false,m_pShortFrame->m_iaPayload);

			} else {

				eprintf("CBarbecubeFile::OpenWriteAppend(): Error: Unexpected frame type %d of index frame.\n",m_pShortFrame->m_iFrameType);
				abort();

			}

			if (g_bBarbecubeVerbose)
				mprintf("    Positioning file pointer to start of old index frame.\n");

			fseek(m_pFile,ip,SEEK_SET);

		} else if (g_bBarbecubeVerbose)
			mprintf("    Found no index frame.\n");

	} else { // File does not exist

		if (g_bBarbecubeVerbose)
			mprintf("    File does not exist, creating...\n");

		m_pFile = fopen(s.c_str(),"wb");
		if (m_pFile == NULL) {
			eprintf("CBarbecubeFile::OpenWriteAppend(): Error: Could not create \"%s\" for writing.\n",s.c_str());
			abort();
		}

	}

	m_sFileName = s;
	m_bOpenWrite = true;

	if (m_pShortFrame != NULL) {
		delete m_pShortFrame;
		m_pShortFrame = NULL;
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::OpenWriteAppend <<<\n\n");

	return true;
}


bool CBarbecubeFile::OpenWriteReplace(std::string s) {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::OpenWriteReplace >>>\n");

	if (m_bOpenRead || m_bOpenWrite) {
		eprintf("CBarbecubeFile::OpenWriteReplace(): Error: A file is already open.\n");
		abort();
	}

	m_pIndex = new CBarbecubeIndex();

	if (g_bBarbecubeVerbose)
		mprintf("    Creating file...\n");

	m_pFile = fopen(s.c_str(),"wb");
	if (m_pFile == NULL) {
		eprintf("CBarbecubeFile::OpenWriteReplace(): Error: Could not create \"%s\" for writing.\n",s.c_str());
		abort();
	}

	m_sFileName = s;
	m_bOpenWrite = true;

	if (m_pShortFrame != NULL) {
		delete m_pShortFrame;
		m_pShortFrame = NULL;
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::OpenWriteReplace <<<\n\n");

	return true;
}


bool CBarbecubeFile::Rewind() {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::Rewind() >>>\n");

	if (!m_bOpenRead && !m_bOpenWrite) {
		eprintf("CBarbecubeFile::Rewind(): Error: No file is open.\n");
		abort();
	}

	if (m_bListFile) {

		if (g_bBarbecubeVerbose)
			mprintf("    LIST: Rewinding list file...\n");
		if (m_iListIndex != -1)
			m_oaBarbecubeList[m_iListIndex]->m_pFile->Close();
		m_iListIndex = -1;

	} else {

		if (g_bBarbecubeVerbose)
			mprintf("    Rewinding bqb file...\n");

		if (m_pFile == NULL) {
			eprintf("CBarbecubeFile::Rewind(): Error: File pointer is NULL.\n");
			abort();
		}
		
		rewind(m_pFile);
	}

	m_bEOF = false;
	m_iFrameCounter = 0;

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::Rewind <<<\n\n");

	return true;
}


bool CBarbecubeFile::Close() {

	int z;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::Close >>>\n");

	if (!m_bOpenRead && !m_bOpenWrite) {
		eprintf("CBarbecubeFile::Close(): Error: No file is open.\n");
		abort();
	}

	if (m_bListFile) {

		if (g_bBarbecubeVerbose)
			mprintf("    LIST: Closing list file...\n");
		if (m_iListIndex != -1)
			m_oaBarbecubeList[m_iListIndex]->m_pFile->Close();
		m_iListIndex = -1;
		for (z=0;z<(int)m_oaBarbecubeList.size();z++)
			delete m_oaBarbecubeList[z];
		m_oaBarbecubeList.clear();
		m_pShortFrame = NULL; // Managed/deleted by subsequent instances

	} else {

		if (g_bBarbecubeVerbose)
			mprintf("    Closing BQB file...\n");

		if (m_pFile == NULL) {
			eprintf("CBarbecubeFile::Close(): Error: File pointer is NULL.\n");
			abort();
		}
		
		fclose(m_pFile);
		m_pFile = NULL;
	}

	m_sFileName = "";

	if (m_pIndex != NULL) {
		delete m_pIndex;
		m_pIndex = NULL;
	}

	if (m_pShortFrame != NULL) {
		delete m_pShortFrame;
		m_pShortFrame = NULL;
	}

	m_bOpenRead = false;
	m_bOpenWrite = false;
	m_iTotalFrameCount = -1;
	m_bListFile = false;

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::Close <<<\n\n");

	return true;
}


bool CBarbecubeFile::CreateGeneralFrame() {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::CreateGeneralFrame >>>\n");

	eprintf("CBarbecubeFile::CreateGeneralFrame(): Not yet implemented.\n");
	abort();

	return true;
}


bool CBarbecubeFile::CreateShortFrame(int type, int version, int index) {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::CreateShortFrame >>>\n");

	if (!m_bOpenWrite) {
		eprintf("CBarbecubeFile::CreateShortFrame(): Error: No file is open for writing.\n");
		abort();
	}

	if ((type < 2) || (type > 11)) {
		eprintf("CBarbecubeFile::CreateShortFrame(): Error: Short frame type must be in range 2..9 (specified: %d).\n",type);
		abort();
	}

	if ((version < 0) || (version > 7)) {
		eprintf("CBarbecubeFile::CreateShortFrame(): Error: Frame type version must be in range 0..7 (specified: %d).\n",version);
		abort();
	}

	m_bShortFrame = true;

	if (m_pShortFrame != NULL)
		delete m_pShortFrame;
	m_pShortFrame = new CBarbecubeShortFrame();
	m_pShortFrame->m_iFrameType = type;
	m_pShortFrame->m_iFrameTypeVersion = version;
	m_pShortFrame->m_iIndex = index;

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::CreateShortFrame <<<\n\n");

	return true;
}


bool CBarbecubeFile::PushPayload(const std::vector<unsigned char> &ia) {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::PushPayload >>>\n");

	if (!m_bOpenWrite) {
		eprintf("CBarbecubeFile::PushPayload(): Error: No file is open for writing.\n");
		abort();
	}

	if (!m_bShortFrame) {
		eprintf("CBarbecubeFile::PushPayload(): Error: Only applicable to short frames.\n");
		abort();
	}

	if (m_pShortFrame == NULL) {
		eprintf("CBarbecubeFile::PushPayload(): Error: No frame has been created before.\n");
		abort();
	}

	m_pShortFrame->m_iaPayload.insert( m_pShortFrame->m_iaPayload.end(), ia.begin(), ia.end() );

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::PushPayload <<<\n\n");

	return true;
}


bool CBarbecubeFile::FinalizeFrame() {

	unsigned char uc, tv;
	unsigned int ui;
	CCRC32 crc;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::FinalizeFrame >>>\n");

	if (!m_bOpenWrite) {
		eprintf("CBarbecubeFile::FinalizeFrame(): Error: No file is open for writing.\n");
		abort();
	}

	if (m_bShortFrame) {

		if (m_pShortFrame == NULL) {
			eprintf("CBarbecubeFile::FinalizeFrame(): Error: No frame has been created before.\n");
			abort();
		}

		if (m_pShortFrame->m_iaPayload.size() == 0) {
			eprintf("CBarbecubeFile::FinalizeFrame(): Error: Payload is empty.\n");
			abort();
		}

		uc = 'B';
		fwrite(&uc,1,1,m_pFile);
		uc = 'Q';
		fwrite(&uc,1,1,m_pFile);

		if (g_bBarbecubeVerbose)
			mprintf("    Offset %ld, writing frame type %d.\n",ftell(m_pFile),m_pShortFrame->m_iFrameType);

		tv = (unsigned char)((m_pShortFrame->m_iFrameType << 3) + m_pShortFrame->m_iFrameTypeVersion);
		fwrite(&tv,1,1,m_pFile);

		ui = m_pShortFrame->m_iIndex;
		fwrite(&ui,4,1,m_pFile);

		ui = (unsigned int)m_pShortFrame->m_iaPayload.size();
		fwrite(&ui,4,1,m_pFile);

		m_pShortFrame->m_iCRC32 = crc.ComputeCRC32(m_pShortFrame->m_iaPayload);

		ui = m_pShortFrame->m_iCRC32;
		fwrite(&ui,4,1,m_pFile);

		gc_oStatistics.m_lOverhead += 15 * 8;

		WriteArrayToFile(m_pFile,m_pShortFrame->m_iaPayload);

		fflush(m_pFile);

		if (g_bBarbecubeVerbose)
			mprintf("    %lu bytes of payload written, CRC32 is %08lX.\n",m_pShortFrame->m_iaPayload.size(),m_pShortFrame->m_iCRC32);

		m_pIndex->m_iaFrameLengths.push_back((int)m_pShortFrame->m_iaPayload.size()+15);
		m_pIndex->m_iaFrameTypes.push_back(tv);

		delete m_pShortFrame;
		m_pShortFrame = NULL;

	} else {

		eprintf("CBarbecubeFile::FinalizeFrame(): Error: Not yet implemented for general frames.\n");
		abort();

	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::FinalizeFrame <<<\n\n");

	return true;
}


bool CBarbecubeFile::WriteIndexFrame(bool compressed) {

	std::vector<unsigned char> uca;
	unsigned long ul;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::WriteIndexFrame >>>\n");

	if (!m_bOpenWrite) {
		eprintf("CBarbecubeFile::WriteIndexFrame(): Error: No file is open for writing.\n");
		abort();
	}

	if (compressed) {

		CreateShortFrame(BARBECUBE_FRAMETYPE_COMPIDX,0,0);

		m_pIndex->ExportToArray(compressed,uca);

		PushPayload(uca);

		gc_oStatistics.m_lOverhead += (int)(uca.size()*8);

		ul = (unsigned long)(uca.size() + 22);

		uca.clear();

		uca.push_back((ul>>24)&0xFF);
		uca.push_back((ul>>16)&0xFF);
		uca.push_back((ul>>8)&0xFF);
		uca.push_back(ul&0xFF);
		uca.push_back('I');
		uca.push_back('D');
		uca.push_back('X');

		PushPayload(uca);

		gc_oStatistics.m_lOverhead += (int)(uca.size()*8);

		FinalizeFrame();

	} else {

		eprintf("CBarbecubeFile::WriteIndexFrame(): Non-compressed index frames not yet implemented.\n");
		abort();

	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::WriteIndexFrame <<<\n\n");

	return true;
}


void CBarbecubeFile::DumpIndex() {

	m_pIndex->Dump();
}


bool CBarbecubeFile::CheckIntegrity(std::string s, bool verbose) {

	int i;
	unsigned long crc;
	CCRC32 ecrc;
	bool b;

	SetBarbecubeVerbose(verbose);

	mprintf("##########################################\n");
	mprintf("###   Barbecube File Integrity Check   ###\n");
	mprintf("##########################################\n");

	mprintf("Trying to open %s ...\n",s.c_str());

	if (!OpenRead(s)) {
		eprintf("CBarbecubeFile::CheckIntegrity(): Error: Failed to open %s\n",s.c_str());
		return false;
	}

	if (m_pIndex->m_iaFrameLengths.size() != 0) {

		mprintf("File contains an index with %lu/%lu entries.\n",m_pIndex->m_iaFrameTypes.size(),m_pIndex->m_iaFrameLengths.size());

		i = 0;
		b = false;
		while (ReadFrame()) {
			mprintf("    Frame %6d,  type %2d v%d (%s),  index %6d,  payload size %8lu bytes.\n",i+1,m_pShortFrame->m_iFrameType,m_pShortFrame->m_iFrameTypeVersion,GetFrameTypeString(m_pShortFrame->m_iFrameType),m_pShortFrame->m_iIndex,m_pShortFrame->m_iaPayload.size());
			if (b) {
				eprintf("CBarbecubeFile::CheckIntegrity(): Error: Frame %d not on the index, but was not the last frame.\n",i);
				Close();
				return false;
			}
			crc = ecrc.ComputeCRC32(m_pShortFrame->m_iaPayload);
			if (crc != m_pShortFrame->m_iCRC32) {
				eprintf("CBarbecubeFile::CheckIntegrity(): Error in frame %d: CRC mismatch (computed %08lX, read %08lX).\n",i+1,crc,m_pShortFrame->m_iCRC32);
				Close();
				return false;
			}
			if ((int)m_pIndex->m_iaFrameTypes.size() > i) {
				if ((m_pShortFrame->m_iFrameType != (int)(m_pIndex->m_iaFrameTypes[i]>>3)) || (m_pShortFrame->m_iFrameTypeVersion != (int)(m_pIndex->m_iaFrameTypes[i]&7)) || ((int)m_pShortFrame->m_iaPayload.size()+15 != m_pIndex->m_iaFrameLengths[i])) {
					eprintf("CBarbecubeFile::CheckIntegrity(): Error in frame %d: Mismatch between frame header and index: Type %d<-->%d, Version %d<-->%d, Length %lu<-->%d.\n",i+1,m_pShortFrame->m_iFrameType,m_pIndex->m_iaFrameTypes[i]>>3,m_pShortFrame->m_iFrameTypeVersion,m_pIndex->m_iaFrameTypes[i]&7,m_pShortFrame->m_iaPayload.size()+15,m_pIndex->m_iaFrameLengths[i]);
					Close();
					return false;
				}
			} else {
				b = true;
				mprintf("      This frame is not on the index...\n");
			}
			i++;
		}

	} else {

		mprintf("File does not contain an index.\n");

		i = 0;
		while (ReadFrame()) {
			mprintf("    Frame %6d,  type %2d v%d (%s),  index %6d,  payload size %8lu bytes.\n",i+1,m_pShortFrame->m_iFrameType,m_pShortFrame->m_iFrameTypeVersion,GetFrameTypeString(m_pShortFrame->m_iFrameType),m_pShortFrame->m_iIndex,m_pShortFrame->m_iaPayload.size());
			crc = ecrc.ComputeCRC32(m_pShortFrame->m_iaPayload);
			if (crc != m_pShortFrame->m_iCRC32) {
				eprintf("CBarbecubeFile::CheckIntegrity(): Error in frame %d: CRC mismatch (computed %08lX, read %08lX).\n",i+1,crc,m_pShortFrame->m_iCRC32);
				Close();
				return false;
			}
			i++;
		}

	}

	Close();

	mprintf("Integrity check passed.\n\n");

	return true;
}


int CBarbecubeFile::GetFrameIndex() const {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::GetFrameIndex >>>\n");

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::GetFrameIndex(): Error: File not open for reading.\n");
		abort();
	}

	if (m_pShortFrame == NULL) {
		eprintf("CBarbecubeFile::GetFrameIndex(): Error: No current frame.\n");
		abort();
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::GetFrameIndex <<<\n\n");

	return m_pShortFrame->m_iIndex;
}


int CBarbecubeFile::GetFrameType() const {

	if (g_bBarbecubeVerbose)
		mprintf(">>> GetFrameType::GetFrameIndex >>>\n");

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::GetFrameType(): Error: File not open for reading.\n");
		abort();
	}

	if (m_pShortFrame == NULL) {
		eprintf("CBarbecubeFile::GetFrameType(): Error: No current frame.\n");
		abort();
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::GetFrameType <<<\n\n");

	return m_pShortFrame->m_iFrameType;
}


int CBarbecubeFile::GetFrameTypeVersion() const {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::GetFrameTypeVersion >>>\n");

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::GetFrameTypeVersion(): Error: File not open for reading.\n");
		abort();
	}

	if (m_pShortFrame == NULL) {
		eprintf("CBarbecubeFile::GetFrameTypeVersion(): Error: No current frame.\n");
		abort();
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::GetFrameTypeVersion <<<\n\n");

	return m_pShortFrame->m_iFrameTypeVersion;
}


const std::vector<unsigned char>* CBarbecubeFile::GetFramePayload() const {

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::GetFramePayload >>>\n");

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::GetFramePayload(): Error: File not open for reading.\n");
		abort();
	}

	if (m_pShortFrame == NULL) {
		eprintf("CBarbecubeFile::GetFramePayload(): Error: No current frame.\n");
		abort();
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::GetFramePayload <<<\n\n");

	return &m_pShortFrame->m_iaPayload;
}


const char *GetFrameTypeString(int type) {

	const char* names[] = {
		"RESERVED     ",
		"GENERAL      ",
		"IDX          ",
		"COMPIDX      ",
		"TRAJ         ",
		"COMPTRAJSTART",
		"COMPTRAJ     ",
		"CUBE         ",
		"COMPCUBESTART",
		"COMPCUBE     ",
		"FILE         ",
		"COMPFILE     " };

	if ((type >= 0) && (type < 12))
		return names[type];
	else
		return "UNKNOWN      ";
}


bool CBarbecubeFile::OpenListFile(FILE *a) {

	char buf[1024], buf2[32], bstart[16], bend[16], *p, *q, *r;
	CBarbecubeListEntry *le;
	bool b;
	int z, ti;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::OpenListFile >>>\n");

	fseek(a,0,SEEK_SET);

	fgets(buf,1024,a);
	while ((buf[strlen(buf)-1] == '\r') || (buf[strlen(buf)-1] == '\n'))
		buf[strlen(buf)-1] = 0;
	if (strcmp(buf,"BLIST") != 0) {
		eprintf("CBarbecubeFile::OpenListFile(): Error: File header mismatch (\"%s\" != \"BLIST\").\n",buf);
		return false;
	}

	b = false;
	m_iTotalFrameCount = 0;
	m_iListIndex = -1;
	while (!feof(a)) {
		if (fgets(buf,1024,a) == NULL) {
			if (!feof(a)) {
				eprintf("CBarbecubeFile::OpenListFile(): Error while reading line %lu from BQB list file.\n",m_oaBarbecubeList.size()+1);
				return false;
			} else
				break;
		}
		while ((buf[strlen(buf)-1] == '\r') || (buf[strlen(buf)-1] == '\n'))
			buf[strlen(buf)-1] = 0;
		if (strlen(buf) == 0)
			continue;
		if (g_bBarbecubeVerbose)
			mprintf("    LIST: Checking list entry %3d: \"%s\"...\n",(int)m_oaBarbecubeList.size()+1,buf);
		le = new CBarbecubeListEntry();

		p = buf;
		while (*p == ' ')
			p++;
		if (*p == '[') {
			q = p+1;
			while ((*q != ']') && (*q != 0))
				q++;
			if (*q == 0) {
				eprintf("CBarbecubeFile::OpenListFile(): Missing \"[\" in line %lu: \"%s\".\n",m_oaBarbecubeList.size()+1,buf);
				return false;
			}
			r = p+1;
			while ((r < q) && (*r != '-'))
				r++;
			if (r == q) {
				eprintf("CBarbecubeFile::OpenListFile(): Missing \"-\" in line %lu: \"%s\".\n",m_oaBarbecubeList.size()+1,buf);
				return false;
			}
			if (p+1 < r)
				memcpy(buf2,p+1,r-p-1);
			buf2[r-p-1] = 0;
			le->m_iFrameStart = atoi(buf2)-1;
			if (r+1 < q)
				memcpy(buf2,r+1,q-r-1);
			buf2[q-r-1] = 0;
			le->m_iFrameEnd = atoi(buf2)-1;
			le->m_sFileName = q+1;
		} else
			le->m_sFileName = buf;

		le->m_pFile = new CBarbecubeFile();
		if (!le->m_pFile->OpenRead(le->m_sFileName)) {
			eprintf("CBarbecubeFile::OpenListFile(): Error while opening entry %d: \"%s\".\n",(int)m_oaBarbecubeList.size()+1,buf);
			return false;
		}

		if (le->m_pFile->m_pIndex != NULL) {
			le->m_pIndex = new CBarbecubeIndex(*le->m_pFile->m_pIndex);
			ti = 0;
			for (z=0;z<(int)le->m_pIndex->m_iaFrameLengths.size();z++)
				if ((le->m_pIndex->m_iaFrameTypes[z] != 2) && (le->m_pIndex->m_iaFrameTypes[z] != 3))
					ti++;
			le->m_iFullFrameCount = ti/*le->m_pIndex->m_iaFrameLengths.size()*/;
			if (g_bBarbecubeVerbose)
				mprintf("      LIST: Found index frame: %d frames, %d payload frames.\n",(int)le->m_pIndex->m_iaFrameLengths.size(),le->m_iFullFrameCount);
		} else if (g_bBarbecubeVerbose) {
			b = true;
			if (g_bBarbecubeVerbose)
				mprintf("      LIST: Found no index.\n");
		}

		if ((le->m_iFrameStart != -1) && (le->m_iFrameEnd != -1)) {
			le->m_iFrameCount = le->m_iFrameEnd - le->m_iFrameStart + 1;
			if ((le->m_iFullFrameCount != -1) && (le->m_iFrameCount > le->m_iFullFrameCount)) {
				eprintf("CBarbecubeFile::OpenListFile(): Error in entry %d: Frame range %d - %d is more than total frame count (%d).\n",(int)m_oaBarbecubeList.size()+1,le->m_iFrameStart+1,le->m_iFrameEnd+1,le->m_iFullFrameCount);
				return false;
			}
		} else if ((le->m_iFrameStart != -1) && (le->m_iFullFrameCount != -1)) {
			le->m_iFrameCount = le->m_iFullFrameCount - le->m_iFrameStart;
			if (le->m_iFrameStart+1 > le->m_iFullFrameCount) {
				eprintf("CBarbecubeFile::OpenListFile(): Error in entry %d: Starting frame number %d is larger than total frame count (%d).\n",(int)m_oaBarbecubeList.size()+1,le->m_iFrameStart+1,le->m_iFullFrameCount);
				return false;
			}
		} else if ((le->m_iFrameEnd != -1) && (le->m_iFullFrameCount != -1)) {
			le->m_iFrameCount = le->m_iFrameEnd + 1;
			if (le->m_iFrameEnd+1 > le->m_iFullFrameCount) {
				eprintf("CBarbecubeFile::OpenListFile(): Error in entry %d: Ending frame number %d is larger than total frame count (%d).\n",(int)m_oaBarbecubeList.size()+1,le->m_iFrameEnd+1,le->m_iFullFrameCount);
				return false;
			}
		} else
			le->m_iFrameCount = le->m_iFullFrameCount;

		if (le->m_iFrameCount != -1)
			m_iTotalFrameCount += le->m_iFrameCount;

		le->m_pFile->Close();
		m_oaBarbecubeList.push_back(le);
	}

	if (!b) {
		if (g_bBarbecubeVerbose)
			mprintf("LIST: Building overall index...\n");
		m_pIndex = new CBarbecubeIndex();
		for (z=0;z<(int)m_oaBarbecubeList.size();z++) {
			m_pIndex->m_iaFrameLengths.insert(m_pIndex->m_iaFrameLengths.end(),m_oaBarbecubeList[z]->m_pIndex->m_iaFrameLengths.begin(),m_oaBarbecubeList[z]->m_pIndex->m_iaFrameLengths.end());
			m_pIndex->m_iaFrameTypes.insert(m_pIndex->m_iaFrameTypes.end(),m_oaBarbecubeList[z]->m_pIndex->m_iaFrameTypes.begin(),m_oaBarbecubeList[z]->m_pIndex->m_iaFrameTypes.end());
		}
	} else {
		if (g_bBarbecubeVerbose)
			mprintf("LIST: Some list entries have no index; not building overall index.\n");
		m_iTotalFrameCount = -1;
	}

	if (g_bBarbecubeVerbose)
		mprintf("LIST: Added %lu entrys to list.\n",m_oaBarbecubeList.size());

	if (g_bBarbecubeVerbose) {
		if (m_iTotalFrameCount != -1)
			mprintf("LIST: Found %d frames in total.\n",m_iTotalFrameCount);
		else
			mprintf("LIST: Total frame count is unknown.\n");
	}

	fclose(a);

	mprintf("\n");
	mprintf(WHITE,"    Contents of BQB list file %s:\n",m_sFileName.c_str());
	for (z=0;z<(int)m_oaBarbecubeList.size();z++) {

		if (m_oaBarbecubeList[z]->m_iFrameStart != -1)
			sprintf(bstart,"%5d",m_oaBarbecubeList[z]->m_iFrameStart+1);
		else
			sprintf(bstart,"start");

		if (m_oaBarbecubeList[z]->m_iFrameEnd != -1)
			sprintf(bend,"%5d",m_oaBarbecubeList[z]->m_iFrameEnd+1);
		else
			sprintf(bend,"  end");

		if (m_oaBarbecubeList[z]->m_pIndex != NULL)
			mprintf("    %3d.) %5d payload frames, using %5d (%5s - %5s), %s\n",z+1,m_oaBarbecubeList[z]->m_iFullFrameCount,m_oaBarbecubeList[z]->m_iFrameCount,bstart,bend,m_oaBarbecubeList[z]->m_sFileName.c_str());
		else if (m_oaBarbecubeList[z]->m_iFrameCount != -1)
			mprintf("    %3d.) (no index),           using %5d (%5s - %5s), %s\n",z+1,m_oaBarbecubeList[z]->m_iFrameCount,bstart,bend,m_oaBarbecubeList[z]->m_sFileName.c_str());
		else
			mprintf("    %3d.) (no index),                       (%5s - %5s), %s\n",z+1,bstart,bend,m_oaBarbecubeList[z]->m_sFileName.c_str());
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::OpenListFile <<<\n");

	return true;
}


bool CBarbecubeFile::IsEOF() {

	return m_bEOF;
}


bool CBarbecubeFile::CompareCoords(std::string infile, std::string reffile, bool verbose) {

	FILE *a;
	int i, z, z2, ty, ver;
	CTimeStep ts;
	CxDVec3Array da;
	double cv[3], m, t1, t2, tv[3];
	bool wrap;
	CCubeFrame *cfr;
	CAtomSet *afr;
	bool fail;

	mprintf("\n");
	mprintf(WHITE,"    *********************************\n");
	mprintf(WHITE,"    ***   Coordinate Comparison   ***\n");
	mprintf(WHITE,"    *********************************\n\n");

	mprintf("    Will compare the coordinates in BQB file %s\n",infile.c_str());
	mprintf("    to the reference coordinates in XYZ file %s.\n",reffile.c_str());
	mprintf("\n");

	SetBarbecubeVerbose(verbose);

	mprintf("    Opening BQB file...\n");
	if (!OpenRead(infile))
		return false;

	mprintf("\n");
	mprintf("    Opening XYZ file...\n");
	a = fopen(reffile.c_str(),"rb");
	if (a == NULL) {
		eprintf("Error: Could not open file for reading.\n");
		return false;
	}

	g_pCCEngine = new CCCEngine();
	g_pCCEngine->PrepareDecompressCube(false);

	mprintf("\n");
	if (AskYesNo("    Allow wrapping (y) or compare absolute coordinates (n)? [yes] ",true)) {

		wrap = true;

		mprintf("\n");
		mprintf("    Reading first frame...\n");
		if (!ReadFrame()) {
			fclose(a);
			return false;
		}

		ty = GetFrameType();
		ver = GetFrameTypeVersion();

		mprintf("    First frame is of type %d v%d.\n",ty,ver);

		if ((ty != 8) || (ver > 1)) {
			eprintf("Error: Frame type not yet supported.\n");
			fclose(a);
			return false;
		}

		if (!g_pCCEngine->DecompressCubeStep(GetFramePayload(),ver,verbose)) {
			fclose(a);
			return false;
		}

		cfr = g_pCCEngine->GetOutputCubeFrame(0);

		mprintf("\n    *** Input of cell vector (in pm) ***\n\n");
		cv[0] = AskFloat("    Enter X component: [%11.4f] ",cfr->m_fStrideA[0]*cfr->m_iRes[0]*LEN_AU2PM,cfr->m_fStrideA[0]*cfr->m_iRes[0]*LEN_AU2PM);
		cv[1] = AskFloat("    Enter Y component: [%11.4f] ",cfr->m_fStrideB[1]*cfr->m_iRes[1]*LEN_AU2PM,cfr->m_fStrideB[1]*cfr->m_iRes[1]*LEN_AU2PM);
		cv[2] = AskFloat("    Enter Z component: [%11.4f] ",cfr->m_fStrideC[2]*cfr->m_iRes[2]*LEN_AU2PM,cfr->m_fStrideC[2]*cfr->m_iRes[2]*LEN_AU2PM);

		mprintf("\n");
		mprintf("    Rewinding BQB file...\n");

		if (!Rewind()) {
			fclose(a);
			return false;
		}

	} else
		wrap = false;

	i = 0;
	fail = false;
	mprintf("\n");
	mprintf("Starting comparison process.\n");
	while (true) {

		if (!ReadFrame())
			break;

		mprintf("  Frame %4d ...  ",i+1);

		mprintf("Type %2dv%1d,  ",GetFrameType(),GetFrameTypeVersion());

		if ((GetFrameType() == 2) || (GetFrameType() == 3)) {
			mprintf("Skipping index frame.\n");
			continue;
		} else if (((GetFrameType() != 8) && (GetFrameType() != 9)) || (GetFrameTypeVersion() != 0)) {
			eprintf("\nError: Frame type %d v%d not yet supported.\n",GetFrameType(),GetFrameTypeVersion());
			fclose(a);
			return false;
		}

		if (!g_pCCEngine->DecompressCubeStep(GetFramePayload(),verbose,true))
			return false;

		afr = g_pCCEngine->GetOutputAtomFrame(0);

		if (!ts.ReadXYZ(a,true,&da)) {
			eprintf("\nError: Could not read frame %d from XYZ trajectory.\n",i+1);
			fclose(a);
			return false;
		}
		m = 0;
		for (z=0;z<(int)afr->m_oaAtoms.size();z++) {
//			mprintf("@  %10.4f  %10.4f  %10.4f  vs  %10.4f  %10.4f  %10.4f\n",afr->m_oaAtoms[z]->m_fCoord[0]*LEN_AU2PM,afr->m_oaAtoms[z]->m_fCoord[1]*LEN_AU2PM,afr->m_oaAtoms[z]->m_fCoord[2]*LEN_AU2PM,da[z][0],da[z][1],da[z][2]);
			for (z2=0;z2<3;z2++) {
				t1 = da[z][z2];
				t2 = afr->m_oaAtoms[z]->m_fCoord[z2]*LEN_AU2PM;
				if (wrap) {
					while (t1 < 0)
						t1 += cv[z2];
					while (t1 >= cv[z2])
						t1 -= cv[z2];
					while (t2 < 0)
						t2 += cv[z2];
					while (t2 >= cv[z2])
						t2 -= cv[z2];
				}
				tv[z2] = t2 - t1;
			}
			t1 = sqrt(tv[0]*tv[0] + tv[1]*tv[1] + tv[2]*tv[2]);
			if (t1 > m)
				m = t1;
		}

		mprintf("Max. dev. %11.5f pm  -->  ",m);

		if (m >= 0.001) {
			fail = true;
			mprintf(RED,"fail\n");
		} else
			mprintf(GREEN,"match\n");

		i++;
	}

	mprintf("Compared %d frames.\n",i);
	mprintf("Result: ");
	if (fail)
		mprintf(RED,"Fail.\n");
	else
		mprintf(GREEN,"Success.\n");

	fclose(a);
	Close();

	delete g_pCCEngine;

	return (!fail);
}


bool CBarbecubeTrajectoryFrameColumn::ReadColumn(int ac, CBitSet *bs) {

	unsigned char uc;
	int z, z2;

	m_iType = (unsigned char)bs->ReadBitsInteger(8);

	uc = (unsigned char)bs->ReadBitsInteger(8);
	m_sLabel.resize(uc);
	for (z=0;z<(int)uc;z++)
		m_sLabel[z] = (char)bs->ReadBitsInteger(8);

	switch(m_iType) {
		case BARBECUBE_TYPE_STRING:
			m_aString.resize(ac);
			for (z=0;z<ac;z++) {
				uc = (unsigned char)bs->ReadBitsInteger(8);
				m_aString[z].SetBufSize(uc+1);
				for (z2=0;z2<(int)uc;z2++)
					m_aString[z](z2) = (char)bs->ReadBitsInteger(8);
				m_aString[z](uc) = 0;
			}
			break;
		case BARBECUBE_TYPE_FLOAT:
			m_aReal.resize(ac);
			for (z=0;z<ac;z++)
				m_aReal[z] = bs->ReadBitsFloat();
			break;
		case BARBECUBE_TYPE_DOUBLE:
			m_aReal.resize(ac);
			for (z=0;z<ac;z++)
				m_aReal[z] = bs->ReadBitsDouble();
			break;
		case BARBECUBE_TYPE_UINT8:
			m_aUnsignedInt.resize(ac);
			for (z=0;z<ac;z++)
				m_aUnsignedInt[z] = bs->ReadBitsInteger(8);
			break;
		case BARBECUBE_TYPE_UINT16:
			for (z=0;z<ac;z++)
				m_aUnsignedInt[z] = bs->ReadBitsInteger(16);
			break;
		case BARBECUBE_TYPE_UINT32:
			for (z=0;z<ac;z++)
				m_aUnsignedInt[z] = bs->ReadBitsInteger(32);
			break;
		default:
			eprintf("CBarbecubeTrajectoryFrameColumn::ReadColumn(): Error: Type %u not yet implemented.\n",m_iType);
			abort();
	}

	return true;
}


void CBarbecubeTrajectoryFrameColumn::WriteColumn(int ac, CBitSet *bs) {

	unsigned char uc;
	int z, z2;

	bs->WriteBits(m_iType,8);

	uc = (unsigned char)m_sLabel.length();
	bs->WriteBits(uc,8);
	for (z=0;z<(int)m_sLabel.length();z++) {
		uc = m_sLabel[z];
		bs->WriteBits(uc,8);
	}

	switch(m_iType) {
		case BARBECUBE_TYPE_STRING:
			for (z=0;z<ac;z++) {
				uc = (unsigned char)m_aString[z].GetLength();
				bs->WriteBits(uc,8);
				for (z2=0;z2<(int)m_aString[z].GetLength();z2++) {
					uc = m_aString[z][z2];
					bs->WriteBits(uc,8);
				}
			}
			break;
		case BARBECUBE_TYPE_FLOAT:
			for (z=0;z<ac;z++)
				bs->WriteBitsFloat((float)m_aReal[z]);
			break;
		case BARBECUBE_TYPE_DOUBLE:
			for (z=0;z<ac;z++)
				bs->WriteBitsDouble(m_aReal[z]);
			break;
		case BARBECUBE_TYPE_UINT8:
			for (z=0;z<ac;z++)
				bs->WriteBits(m_aUnsignedInt[z],8);
			break;
		case BARBECUBE_TYPE_UINT16:
			for (z=0;z<ac;z++)
				bs->WriteBits(m_aUnsignedInt[z],16);
			break;
		case BARBECUBE_TYPE_UINT32:
			for (z=0;z<ac;z++)
				bs->WriteBits(m_aUnsignedInt[z],32);
			break;
		default:
			eprintf("CBarbecubeTrajectoryFrameColumn::WriteColumn(): Error: Type %u not yet implemented.\n",m_iType);
			abort();
	}
}


bool CBarbecubeTrajectoryFrame::ReadFrame(const std::vector<unsigned char> *data) {

	CBitSet bs;
	int i, z;
	CBarbecubeTrajectoryFrameColumn *col;

	bs.m_iaData.assign(data->begin(),data->end());

	m_iAtomCount = bs.ReadBitsInteger(32);

	if (bs.ReadBitsInteger(8) != 0) {
		if (m_pCellMatrix == NULL)
			m_pCellMatrix = new CxDMatrix3();
		for (z=0;z<9;z++)
			m_pCellMatrix->GetAt(z) = bs.ReadBitsDouble();
	} else {
		if (m_pCellMatrix != NULL) {
			delete m_pCellMatrix;
			m_pCellMatrix = NULL;
		}
	}

	i = bs.ReadBitsInteger(32);

	for (z=0;z<(int)m_oaColumns.size();z++)
		if (m_oaColumns[z] != NULL)
			delete m_oaColumns[z];

	m_oaColumns.resize(i);
	for (z=0;z<i;z++) {
		col = new CBarbecubeTrajectoryFrameColumn();
		m_oaColumns[z] = col;
		col->ReadColumn(m_iAtomCount,&bs);
	}

	return true;
}


void CBarbecubeTrajectoryFrame::WriteFrame(std::vector<unsigned char> *data) {

	CBitSet bs;
	unsigned long ul;
	int z;

	bs.WriteBits(m_iAtomCount,32);

	if (m_pCellMatrix != NULL) {
		bs.WriteBits(1,8);
		for (z=0;z<9;z++)
			bs.WriteBitsDouble(m_pCellMatrix->GetAt(z));
	} else
		bs.WriteBits(0,8);

	ul = (unsigned long)m_oaColumns.size();

	bs.WriteBits(ul,32);

	for (z=0;z<(int)m_oaColumns.size();z++)
		m_oaColumns[z]->WriteColumn(m_iAtomCount,&bs);

	data->insert(data->end(),bs.m_iaData.begin(),bs.m_iaData.end());
}


CBarbecubeTrajectoryFrameColumn* CBarbecubeTrajectoryFrame::GetColumn(std::string label) {

	int z;

	for (z=0;z<(int)m_oaColumns.size();z++)
		if (mystricmp(label.c_str(),m_oaColumns[z]->m_sLabel.c_str()) == 0)
			return m_oaColumns[z];

	return NULL;
}


CBarbecubeTrajectoryFrameColumn* CBarbecubeTrajectoryFrame::AddColumn(int type, std::string label) {

	CBarbecubeTrajectoryFrameColumn *col;

	col = new CBarbecubeTrajectoryFrameColumn((unsigned char)type,label);
	m_oaColumns.push_back(col);

	return col;
}


bool CBarbecubeFile::SeekFrame(int i) {

	int z, k;
	unsigned long ul;
	CBarbecubeListEntry *le;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::SeekFrame() i=%d >>>\n",i);

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::SeekFrame(): Error: File not open for reading.\n");
		abort();
	}

	if (m_bListFile) {

		if (g_bBarbecubeVerbose)
			mprintf("    Is a list file.\n");

		if (m_oaBarbecubeList.size() == 0) {
			if (g_bBarbecubeVerbose)
				eprintf("CBarbecubeFile::SeekFrame(): Error: No entrys in list.\n");
			return false;
		}

		if (m_iListIndex != -1) {
			if (g_bBarbecubeVerbose)
				mprintf("    List entry %d is currently open; closing.\n",m_iListIndex+1);
			if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->Close()) {
				eprintf("CBarbecubeFile::SeekFrame(): Error: Failed to close list entry %d.\n",m_iListIndex+1);
				return false;
			}
		} else if (g_bBarbecubeVerbose)
			mprintf("    No list entry is currently open.\n");

		m_pShortFrame = NULL; // Deleted inside of the corresponding list entry

		k = 0;
		for (z=0;z<(int)m_oaBarbecubeList.size();z++) {
			if (g_bBarbecubeVerbose)
				mprintf("    Processing list entry %d...\n",z+1);
			le = m_oaBarbecubeList[z];
			if (le->m_pIndex == NULL) {
				eprintf("CBarbecubeFile::SeekFrame(): Error: List entry %d does not contain an index.\n",z+1);
				return false;
			}
			if (i >= (int)le->m_pIndex->m_iaFrameLengths.size()+k) {
				k += (int)le->m_pIndex->m_iaFrameLengths.size();
				if (g_bBarbecubeVerbose)
					mprintf("      Entry has %lu frames, so end of entry is frame %d, which is < %d.\n",le->m_pIndex->m_iaFrameLengths.size(),k,i);
				continue;
			} else {
				if (g_bBarbecubeVerbose)
					mprintf("      Entry has %lu frames, so end of entry is frame %d, which is >= %d --> Found.\n",le->m_pIndex->m_iaFrameLengths.size(),(int)le->m_pIndex->m_iaFrameLengths.size()+k,i);
				m_iListIndex = z;
				if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->OpenRead(m_oaBarbecubeList[m_iListIndex]->m_sFileName)) {
					eprintf("CBarbecubeFile::SeekFrame(): Error: Could not open list entry %d for reading.\n",m_iListIndex+1);
					return false;
				}
				if (i-k != 0) {
					if (g_bBarbecubeVerbose)
						mprintf("      Seeking to frame %d of list entry %d...\n",i-k+1,m_iListIndex+1);
					if (!m_oaBarbecubeList[m_iListIndex]->m_pFile->SeekFrame(i-k)) {
						eprintf("CBarbecubeFile::SeekFrame(): Error: Could not seek to frame %d within list entry %d.\n",i-k,m_iListIndex+1);
						return false;
					}
				} else if (g_bBarbecubeVerbose)
					mprintf("      Required frame is first frame of list entry %d, not seeking.\n",m_iListIndex+1);

				break;
			}
		}

	} else {

		if (g_bBarbecubeVerbose)
			mprintf("    Is a simple file.\n");

		if (m_pFile == NULL) {
			eprintf("CBarbecubeFile::SeekFrame(): Error: File pointer is NULL.\n");
			abort();
		}

		if (m_pIndex == NULL) {
			eprintf("CBarbecubeFile::SeekFrame(): Error: File does not contain an index.\n");
			abort();
		}

		if (i >= (int)m_pIndex->m_iaFrameLengths.size()) {
			eprintf("CBarbecubeFile::SeekFrame(): Error: Tried to seek frame beyond index (%d/%lu).\n",i,m_pIndex->m_iaFrameLengths.size());
			abort();
		}

		ul = 0;
		for (z=0;z<i;z++)
			ul += (unsigned long)m_pIndex->m_iaFrameLengths[z];

		if (g_bBarbecubeVerbose)
			mprintf("    Offset of frame %d is %lu.\n",i,ul);

		fseek(m_pFile,ul,SEEK_SET);
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::SeekFrame() i=%d <<<\n",i);

	return true;
}


bool CBarbecubeFile::SkipFrames(int i) {

	int z;

	if (g_bBarbecubeVerbose)
		mprintf(">>> CBarbecubeFile::SkipFrames() i=%d >>>\n",i);

	if (!m_bOpenRead) {
		eprintf("CBarbecubeFile::SkipFrames(): Error: File not open for reading.\n");
		abort();
	}

	for (z=0;z<i;z++) {
		if (!ReadFrame()) {
			eprintf("CBarbecubeFile::SkipFrames(): Error while skipping frame %d/%d.\n",z+1,i);
			return false;
		}
	}

	if (g_bBarbecubeVerbose)
		mprintf("<<< CBarbecubeFile::SkipFrames() i=%d <<<\n",i);

	return true;
}


