/******************************************************************************/
/*

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    09 December 20080

  Author:

    John Burkardt

  Reference:

    George Marsaglia, Wai Wan Tsang,
    The Ziggurat Method for Generating Random Variables,
    Journal of Statistical Software,
    Volume 5, Number 8, October 2000, seven pages.
*/	 
		
#ifndef ZIGGURAT_H
#define ZIGGURAT_H


// This must always be the first include directive
#include "config.h"



double r4_exp ( unsigned long int *jsr, int ke[256], double fe[256], 
  double we[256] );
void r4_exp_setup ( int ke[256], double fe[256], double we[256] );
double r4_nor ( unsigned long int *jsr, int kn[128], double fn[128], 
  double wn[128] );
void r4_nor_setup ( int kn[128], double fn[128], double wn[128] );
double r4_uni ( unsigned long int *jsr );
unsigned long int shr3 ( unsigned long int *jsr );
void timestamp ( void );


#endif
