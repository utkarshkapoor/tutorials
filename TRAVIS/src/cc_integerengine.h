/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_INTEGERENGINE_H
#define CC_INTEGERENGINE_H


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <vector>
#include "cc_bitset.h"
#include "cc_hufftree.h"
#include "cc_alphabet.h"


//class CHuffmanTable;


class CBWPair {
public:
	CBWPair(int symbol, int index) : m_iSymbol(symbol), m_iIndex(index) { }
	int m_iSymbol;
	int m_iIndex;
};


class CIntegerEngine {
public:

	bool Compress(
		std::vector<int> &inp,
		CBitSet *outp,
		bool bw,
		bool mtf,
		bool coderun,
		int blocklength,
		int tables,
		bool opttables,
		bool chr,
		bool preopt,
		int maxiter,
		int maxchunk,
		bool verbose
		);

	bool Decompress(
		CBitSet *inp,
		std::vector<int> &outp,
		bool verbose
		);

	bool CompressSingle(
		std::vector<int> &inp,
		CBitSet *outp,
		bool bw,
		bool mtf,
		bool coderun,
		int blocklength,
		int tables,
		bool opttables,
		bool chr,
		bool preopt,
		int maxiter,
		bool verbose
		);

	bool DecompressSingle(
		CBitSet *inp,
		std::vector<int> &outp,
		bool verbose
		);

private:

	void MultiHuffmanOptimize(int tables, std::vector<CHuffmanTree*> &hta, CAlphabet *alp, std::vector<int> &tia, std::vector<int> &asi, std::vector<int> &iasi, int blocklength, bool verbose);

};


#endif


