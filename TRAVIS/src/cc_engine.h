/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_ENGINE_H
#define CC_ENGINE_H


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <stdio.h>
#include "cc_cubeframe.h"
#include "cc_hufftree.h"
#include "cc_extrapolator.h"


extern std::vector<int> gc_iaAtomOrd;

//extern FILE *gc_fResiCorrFrame;
//extern FILE *gc_fResiCorrTraj;


template<class FT> class CCCReadCache {
public:

	CCCReadCache() : m_iHistoryDepth(0), m_iCachePos(0), m_iReadPos(0), m_fRef(NULL), m_fInput(NULL), m_bStartLock(false) {
	}

	virtual ~CCCReadCache() {
		if (m_fInput != NULL)
			CloseFile();
		for (int z=0;z<(int)m_oaFrames.size();z++)
			if (m_oaFrames[z] != NULL)
				delete m_oaFrames[z];
		m_oaFrames.clear();
	}

	void SetStartLock() {
		if (m_bStartLock) {
			eprintf("CCCReadCache::SetStartLock(): Error: Start lock already set.\n");
			abort();
		}
		m_bStartLock = true;
	}

	void LiftStartLock() {
		if (!m_bStartLock) {
			eprintf("CCCReadCache::LiftStartLock(): Error: Start lock not in place.\n");
			abort();
		}
		m_bStartLock = false;
	}

	void SetHistoryDepth(int i) {
		m_oaFrames.resize(i,NULL);
	}

	int GetCachedSteps() const {
		if (m_iCachePos >= m_iReadPos)
			return m_iCachePos - m_iReadPos;
		else
			return m_iCachePos - m_iReadPos + (int)m_oaFrames.size();
	}

	const FT* GetFrameHistory(int i) const {
		if (i > m_iHistoryDepth) {
			eprintf("CCCReadCache::GetFrameHistory(): Error: Index out of range (%d/%d).\n",i,m_iHistoryDepth);
			abort();
		}
		int t = m_iReadPos - i;
		if (t < 0)
			t += (int)m_oaFrames.size();
		return m_oaFrames[t];
	}

	const FT* GetNextFrame() {
		if (m_iReadPos == m_iCachePos) {
			m_iCachePos++;
			if (m_iCachePos >= (int)m_oaFrames.size()) {
				if (m_bStartLock) {
					eprintf("CCCReadCache::GetNextFrame(): Error: Violation of start lock.\n");
					abort();
				}
				m_iCachePos = 0;
			}
			if (!CacheOneStep())
				return NULL;
		}
		m_iReadPos++;
		if (m_iReadPos >= (int)m_oaFrames.size())
			m_iReadPos = 0;
		if (m_iHistoryDepth+1 < (int)m_oaFrames.size())
			m_iHistoryDepth++;
		return m_oaFrames[m_iReadPos];
	}

	bool CacheSteps(int i, bool progress) {
		if (progress) {
			mprintf("    Caching %d frames...\n",i);
			mprintf("      [");
		}
		double tfs = i / 60.0;
		int k=0;
		for (int z=0;z<i;z++) {
			m_iCachePos++;
			if (m_iCachePos >= (int)m_oaFrames.size()) {
				if (m_bStartLock) {
					eprintf("CCCReadCache::CacheSteps(): Error: Violation of start lock.\n");
					abort();
				}
				m_iCachePos = 0;
			}
			if (!CacheOneStep()) {
				m_iCachePos--;
				if (m_iCachePos < 0)
					m_iCachePos = (int)m_oaFrames.size()-1;
				mprintf("] Only %d frames read.\n",k);
				return false;
			}
			k++;
			if (progress && (fmod(z,tfs) < 1.0))
				mprintf("#");
		}
		if (progress)
			mprintf("] Done.\n");
		return true;
	}

	void RewindReadPos() {
		if (!m_bStartLock) {
			eprintf("CCCReadCache::RewindReadPos(): Error: Requires start lock.\n");
			abort();
		}
		m_iReadPos = -1;
	}

	bool FileOpenRead(const char *inp, const char *ref = NULL) {
		m_iCachePos = -1;
		m_iReadPos = -1;
		m_fInput = fopen(inp,"rb");
		if (m_fInput == NULL) {
			eprintf("CCCReadCache::FileOpenRead(): Error: Could not open input file for reading.\n");
			return false;
		}
		if (ref != NULL) {
			m_fRef = fopen(inp,"wb");
			if (m_fRef == NULL) {
				eprintf("CCCReadCache::FileOpenRead(): Error: Could not open reference file for writing.\n");
				fclose(m_fInput);
				m_fInput = NULL;
				return false;
			}
		}
		return true;
	}

	void CloseFile() {
		fclose(m_fInput);
		m_fInput = NULL;
		if (m_fRef != NULL) {
			fclose(m_fRef);
			m_fRef = NULL;
		}
	}

	long ftell() {
		if (m_fInput == NULL)
			return 0;
		return ::ftell(m_fInput);
	}

	virtual bool SkipOneStep() = 0;


protected:
	virtual bool CacheOneStep() = 0;

	int m_iHistoryDepth;
	int m_iCachePos; // Frame which has been cached last
	int m_iReadPos; // Frame which was read last
	std::vector<FT*> m_oaFrames;
	FILE *m_fRef;
	FILE *m_fInput;
	bool m_bStartLock;
};



class CCCReadXYZCache : public CCCReadCache<CAtomSet> {
public:

	CCCReadXYZCache() : CCCReadCache<CAtomSet>(), m_iSigni(-1) {
	}

	~CCCReadXYZCache() {
	}

	void SetReadParameters(int signi) {
		m_iSigni = signi;
	}

	bool SkipOneStep() {
		return CAtomSet().SkipXYZ(m_fInput);
	}

private:
	bool CacheOneStep() {
		if ((m_iCachePos < 0) || (m_iCachePos >= (int)m_oaFrames.size())) {
			eprintf("CCCReadXYZCache::CacheOneStep(): Error: m_iCachePos out of range (%d/%lu).\n",m_iCachePos,m_oaFrames.size());
			abort();
		}
		if (m_iSigni < 0) {
			eprintf("CCCReadXYZCache::CacheOneStep(): Error: m_iSigni out of range (%d).\n",m_iSigni);
			abort();
		}
		if (m_oaFrames[m_iCachePos] != NULL)
			delete m_oaFrames[m_iCachePos];
		m_oaFrames[m_iCachePos] = new CAtomSet();
		if (m_fInput == NULL) {
			eprintf("CCCReadXYZCache::CacheOneStep(): Error: m_fInput == NULL.\n");
			abort();
		}
		if (!m_oaFrames[m_iCachePos]->ReadXYZ(m_fInput,m_iSigni,m_fRef))
			return false;
		return true;
	}

	int m_iSigni;
};



class CCCReadCubeCache : public CCCReadCache<CCubeFrame> {
public:

	CCCReadCubeCache()
		: CCCReadCache<CCubeFrame>(), m_iEps(-1), m_iCSigni(-1), m_iASigni(-1) {
	}

	~CCCReadCubeCache() {
	}

	void SetReadParameters(int eps, int csigni, int asigni) {
		m_iEps = eps;
		m_iCSigni = csigni;
		m_iASigni = asigni;
	}

	bool SkipOneStep() {
		return CCubeFrame().SkipFrame(m_fInput);
	}

private:
	bool CacheOneStep() {
		if ((m_iCachePos < 0) || (m_iCachePos >= (int)m_oaFrames.size())) {
			eprintf("CCCReadCubeCache::CacheOneStep(): Error: m_iCachePos out of range (%d/%lu).\n",m_iCachePos,m_oaFrames.size());
			abort();
		}
		if (m_oaFrames[m_iCachePos] != NULL)
			delete m_oaFrames[m_iCachePos];
		m_oaFrames[m_iCachePos] = new CCubeFrame();
		if (m_fInput == NULL) {
			eprintf("CCCReadCubeCache::CacheOneStep(): Error: m_fInput == NULL.\n");
			abort();
		}
		if (!m_oaFrames[m_iCachePos]->ReadFrame(m_fInput,m_iEps,m_iCSigni,m_iASigni,false))
			return false;
		if (m_fRef != NULL)
			m_oaFrames[m_iCachePos]->WriteFrame(m_fRef,false);
		return true;
	}

	int m_iEps;
	int m_iCSigni;
	int m_iASigni;
};



class CCCEngine {
public:

	CCCEngine();

	~CCCEngine();

	bool MergeBQB(const char *outfile, const std::vector<const char*> &infile, bool verbose);

//	bool SplitBQB(const char *infile, const char *outbase, int splitlength, int steps, bool verbose);

	void ResetStatistics();

	void BeginStatistics();

	void EndStatistics(bool n, bool c);


	bool CompressFile(
		const char *inp,
		const char *outp,
		int block,
		int tables,
		bool opttables,
		bool coderun,
		bool bw,
		bool mtf,
		bool preopt,
		int maxiter,
		int maxchunk,
		bool verbose
	);
	

	bool DecompressFile(const char *inp, const char *outp, const char *ref, bool verbose);

	void CompressString(const char *s, CBitSet *bs, bool verbose);

	void DecompressString(CBitSet *bs, char **s, bool verbose);


	bool CompressCube(
		const char *inp,
		const char *outp,
		const char *ref,
		int start,
		int steps,
		int stride,
		int corder,
		bool optcorder,
		int aorder,
		bool optaorder,
		int eps,
		int csigni,
		int asigni,
		int csplit,
		int asplit,
		int cblock,
		int ablock,
		int ctables,
		bool optctables,
		bool cpreopt,
		int cmaxiter,
		int cmaxchunk,
		int atables,
		bool optatables,
		bool hilbert,
		double nbhfac,
		bool ccoderun,
		bool cbw,
		bool cmtf,
		bool acoderun,
		bool abw,
		bool amtf,
		bool apreopt,
		int amaxiter,
		bool asortatom,
		int amaxchunk,
		bool usecextra,
		bool useaextra,
		int aextratrange,
		int aextratorder,
		double aextratimeexpo,
		int cextrasrangex,
		int cextrasrangey,
		int cextrasrangez,
		int cextratrange,
		int cextrasorder,
		int cextratorder,
		int cextraoffsetx,
		int cextraoffsety,
		int cextraoffsetz,
		bool cextracrosss,
		bool cextracrosst,
		bool cextrawrap,
		bool cextracrossranges,
		bool cextracrossranget,
		double cextradistexpo,
		double cextratimeexpo,
		bool cextrapredcorr,
		double cextracorrfactor,
		int optimize,
		int optsteps,
		bool onlyopt,
		bool comment,
		bool compare,
		bool dummyread,
		bool verbose
	);


	double CompressCubeBenchmark(
		int corder,
		int eps,
		int csigni,
		int csplit,
		int cblock,
		int ctables,
		bool optctables,
		bool cpreopt,
		int cmaxiter,
		int cmaxchunk,
		bool hilbert,
		double nbhfac,
		bool ccoderun,
		bool cbw,
		bool cmtf,
		bool usecextra,
		int cextrasrangex,
		int cextrasrangey,
		int cextrasrangez,
		int cextratrange,
		int cextrasorder,
		int cextratorder,
		int cextraoffsetx,
		int cextraoffsety,
		int cextraoffsetz,
		bool cextracrosss,
		bool cextracrosst,
		bool cextrawrap,
		bool cextracrossranges,
		bool cextracrossranget,
		double cextradistexpo,
		double cextratimeexpo,
		bool cextrapredcorr,
		double cextracorrfactor,
		bool verbose1,
		bool verbose2,
		bool realsize,
		double *presi,
		std::vector<std::vector<int> > *tciaa
	);


	double OptimizeCube_ObjectiveTExpo(double texpo);

	double OptimizeCube_ObjectiveSExpo(double sexpo);


	bool OptimizeCubeParameters(
		int     level,
		int    &corder,
		int     eps,
		int     csigni,
		int    &csplit,
		int    &cblock,
		int    &ctables,
		bool   &optctables,
		bool   &cpreopt,
		int    &cmaxiter,
		int    &cmaxchunk,
		bool   &hilbert,
		double &nbhfac,
		bool   &ccoderun,
		bool   &cbw,
		bool   &cmtf,
		bool   &usecextra,
		int    &cextrasrangex,
		int    &cextrasrangey,
		int    &cextrasrangez,
		int    &cextratrange,
		int    &cextrasorder,
		int    &cextratorder,
		int    &cextraoffsetx,
		int    &cextraoffsety,
		int    &cextraoffsetz,
		bool   &cextracrosss,
		bool   &cextracrosst,
		bool   &cextrawrap,
		bool   &cextracrossranges,
		bool   &cextracrossranget,
		double &cextradistexpo,
		double &cextratimeexpo,
		bool   &cextrapredcorr,
		double &cextracorrfactor,
		bool    verbose
	);

	
	bool DecompressCube(const char *inp, const char *outp, const char *readref, int steps, int stride, bool verbose);


	bool CompressXYZ(
		const char *inp, 
		const char *outp, 
		const char *ref,
		int start,
		int steps, 
		int stride, 
		int order, 
		bool optorder,
		int signi, 
		int split, 
		int block, 
		int tables, 
		bool opttables, 
		bool coderun, 
		bool bw, 
		bool mtf,
		bool preopt,
		int maxiter,
		bool sortatom,
		bool useextra,
		int extratrange,
		int extratorder,
		double extratimeexpo,
		bool comment, 
		bool compare,
		int maxchunk, 
		int optimize,
		int optsteps,
		bool onlyopt,
		bool verbose
	);


	double CompressXYZBenchmark(
		int order, 
		int signi, 
		int split, 
		int block, 
		int tables, 
		bool opttables, 
		bool coderun, 
		bool bw, 
		bool mtf,
		bool preopt,
		int maxiter,
		bool sortatom,
		bool useextra,
		int extratrange,
		int extratorder,
		double extratimeexpo,
		bool comment, 
		int maxchunk, 
		bool verbose1,
		bool verbose2,
		bool realsize,
		double *presi,
		std::vector<std::vector<int> > *tciaa
	);


	double GoldenSectionSearch(double maxl, double eps, double (CCCEngine::*fobj)(double));


	double REC_GoldenSectionSearch(
		double a,
		double b,
		double c,
		double fa,
		double fb,
		double fc,
		double (CCCEngine::*fobj)(double),
		double eps,
		int depth
	);

		
	double OptimizeXYZ_ObjectiveTExpo(double texpo);


	bool OptimizeXYZParameters(
		int     level,
		int    &order, 
		int     signi, 
		int    &split, 
		int    &block, 
		int    &tables, 
		bool   &opttables, 
		bool   &coderun, 
		bool   &bw, 
		bool   &mtf,
		bool   &preopt,
		int    &maxiter,
		bool   &sortatom,
		bool   &useextra,
		int    &extratrange,
		int    &extratorder,
		double &extratimeexpo,
		bool    comment, 
		int    &maxchunk, 
		bool    verbose,
		bool    cubepart
	);


	bool DecompressXYZ(const char *inp, const char *outp, const char *readref, int steps, int stride, bool verbose);

	double PredictFrame(int order, int eps, bool hilbert, bool coderun, bool bwmtf, int blocklength, int tables, bool verbose);


	bool CubeToIntegerArray(
		std::vector<int> &outp,
		int order,
		int eps,
		int signi,
		int esplit,
		bool hilbert,
		double nbhfac,
		bool verbose,
		bool skipcomp=false,
		double *resi=NULL
	);


	bool IntegerArrayToCube(
		std::vector<int> &inp,
		int order,
		int eps,
		int signi,
		int esplit,
		bool hilbert,
		double nbhfac,
		int ver,
		bool verbose,
		bool second
	);


	bool CompressCubeFrame(
		CBitSet *bs,
		int corder,
		int eps,
		int csigni,
		int csplit,
		int cblock,
		int ctables,
		bool opttables,
		bool hilbert,
		double nbhfac,
		bool coderun,
		bool bw,
		bool mtf,
		bool preopt,
		int maxiter,
		bool atominfo,
		int maxchunk,
		bool verbose,
		bool skipcomp=false,
		double *resi=NULL,
		std::vector<std::vector<int> > *tciaa=NULL
	);


	bool DecompressCubeFrame(CBitSet *bs, int ver, bool verbose, bool second, bool check);


	bool CompressAtomFrame(
		CBitSet *bs,
		int order,
		int signi,
		int split,
		int block,
		int tables,
		bool opttables,
		bool coderun,
		bool bw,
		bool mtf,
		bool preopt,
		int maxiter,
		bool sortatom,
		bool atominfo,
		bool comment,
		int maxchunk,
		bool verbose,
		bool skipcomp=false,
		double *resi=NULL,
		std::vector<std::vector<int> > *tciaa=NULL
	);


	bool DecompressAtomFrame(CBitSet *bs, int ver, bool verbose, bool second, bool check);

	bool PrepareDecompressCube(bool verbose);

	bool DecompressCubeStep(const std::vector<unsigned char> *inp, int ver, bool verbose, bool skipvol=false);

	void ExportCubeHeader(CBitSet *bs, int order, bool verbose);

	void ImportCubeHeader(CBitSet *bs, bool verbose, bool second);

	void ExportAtomLabels(const CAtomSet *as, CBitSet *bs, bool verbose);

	void ImportAtomLabels(CAtomSet *as, CBitSet *bs, bool verbose);


	void AtomsToIntegerArray(
		std::vector<int> &outp,
		int order,
		int asigni,
		int esplit,
		bool verbose,
		bool skipcomp=false,
		double *resi=NULL
	);


	void IntegerArrayToAtoms(
		const std::vector<int> &inp,
		int order,
		int asigni,
		int esplit,
		bool verbose,
		bool second
	);


	void ExportExtrapolatorSettings(CBitSet *bs, bool volumetric, bool verbose);

	void ImportExtrapolatorSettings(CBitSet *bs, bool volumetric, bool verbose);


//	void PushInputCubeFrame(CCubeFrame *cfr);
//	CCubeFrame* GetInputCubeFrame(int depth);

	void PushOutputCubeFrame(CCubeFrame *cfr);
	CCubeFrame* GetOutputCubeFrame(int depth);

	void PushOutput2CubeFrame(CCubeFrame *cfr);
	CCubeFrame* GetOutput2CubeFrame(int depth);

//	void PushInputAtomFrame(CAtomSet *cfr);
//	CAtomSet* GetInputAtomFrame(int depth);

	void PushOutputAtomFrame(CAtomSet *cfr);
	CAtomSet* GetOutputAtomFrame(int depth);

	void PushOutput2AtomFrame(CAtomSet *cfr);
	CAtomSet* GetOutput2AtomFrame(int depth);

	void PushInputAtomFrame_NoDelete(CAtomSet *cfr);
	void PushInputCubeFrame_NoDelete(CCubeFrame *cfr);

	void BuildHilbertIdx(int resx, int resy, int resz, bool verbose);

	double BlockMultiHuffman(std::vector<int> &inp, int blocksize, int multi, bool bwmtf, bool verbose=false);

	void BuildAtomSort(const CAtomSet *as);

	void BuildIdentityAtomSort(const CAtomSet *as);

//	int m_iInputCubeBufPos;
//	std::vector<CCubeFrame*> m_oaInputCubeBuf;

	int m_iOutputCubeBufPos;
	std::vector<CCubeFrame*> m_oaOutputCubeBuf;

	int m_iOutput2CubeBufPos;
	std::vector<CCubeFrame*> m_oaOutput2CubeBuf;

//	int m_iInputAtomBufPos;
//	std::vector<CAtomSet*> m_oaInputAtomBuf;

	int m_iOutputAtomBufPos;
	std::vector<CAtomSet*> m_oaOutputAtomBuf;

	int m_iOutput2AtomBufPos;
	std::vector<CAtomSet*> m_oaOutput2AtomBuf;

	std::vector<int> m_iaHilbertIdx;

	std::vector<double> m_faTempPred;
	std::vector<double> m_faTempPred2;

	std::vector<int> m_iaAtomSort;

	std::vector<int> m_iaTempCompressCube;
	std::vector<int> m_iaTempCompressXYZ;

	CExtrapolator *m_pExtrapolator;
	CExtrapolator *m_pExtrapolatorCorr;
	CExtrapolator *m_pExtrapolatorXYZ;

	CCCReadXYZCache *m_pReadCacheXYZ;
	CCCReadCubeCache *m_pReadCacheCube;

	//double m_fEntropyExpoXYZ;
	//double m_fEntropyExpoCube;

	int m_iOptIncludeFirst;

};


#endif


