/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"


unsigned long gc_iInputSize;
unsigned long gc_iOutputSize;
unsigned long gc_iOutputSizeAlphabet;
unsigned long gc_iOutputSizeHuffmanTables;
unsigned long gc_iOutputSizeTableSwitch;
unsigned long gc_iOutputSizeHuffmanData;
unsigned long gc_iCount;

unsigned long gc_iCOutputSize;
unsigned long gc_iCOutputSizeAlphabet;
unsigned long gc_iCOutputSizeHuffmanTables;
unsigned long gc_iCOutputSizeTableSwitch;
unsigned long gc_iCOutputSizeHuffmanData;
unsigned long gc_iCCount;

unsigned long gc_iTempOutputSize;
unsigned long gc_iTempOutputSizeAlphabet;
unsigned long gc_iTempOutputSizeHuffmanTables;
unsigned long gc_iTempOutputSizeTableSwitch;
unsigned long gc_iTempOutputSizeHuffmanData;


CCCStatistics gc_oStatistics;
std::vector<CCCStatistics> gc_oaStatisticsStack;


void PushStatistics() {

	gc_oaStatisticsStack.push_back(gc_oStatistics);
}


void PopStatistics() {

	if (gc_oaStatisticsStack.size() == 0) {
		eprintf("PopStatistics(): Error: Statistics stack is empty.\n");
		abort();
	}

	gc_oStatistics.m_lSize = gc_oaStatisticsStack.back().m_lSize;
	gc_oStatistics.m_lOverhead = gc_oaStatisticsStack.back().m_lOverhead;
	gc_oStatistics.m_lAlphabet = gc_oaStatisticsStack.back().m_lAlphabet;
	gc_oStatistics.m_lHuffmanTables = gc_oaStatisticsStack.back().m_lHuffmanTables;
	gc_oStatistics.m_lTableSwitch = gc_oaStatisticsStack.back().m_lTableSwitch;
	gc_oStatistics.m_lHuffmanData = gc_oaStatisticsStack.back().m_lHuffmanData;

	gc_oaStatisticsStack.pop_back();
}


void PopDiffStatistics() {

	if (gc_oaStatisticsStack.size() < 2) {
		eprintf("PopDiffStatistics(): Error: Statistics stack has size of %lu < 2.\n",gc_oaStatisticsStack.size());
		abort();
	}

	gc_oStatistics.m_lSize += gc_oaStatisticsStack[gc_oaStatisticsStack.size()-2].m_lSize - gc_oaStatisticsStack.back().m_lSize;
	gc_oStatistics.m_lOverhead += gc_oaStatisticsStack[gc_oaStatisticsStack.size()-2].m_lOverhead - gc_oaStatisticsStack.back().m_lOverhead;
	gc_oStatistics.m_lAlphabet += gc_oaStatisticsStack[gc_oaStatisticsStack.size()-2].m_lAlphabet - gc_oaStatisticsStack.back().m_lAlphabet;
	gc_oStatistics.m_lHuffmanTables += gc_oaStatisticsStack[gc_oaStatisticsStack.size()-2].m_lHuffmanTables - gc_oaStatisticsStack.back().m_lHuffmanTables;
	gc_oStatistics.m_lTableSwitch += gc_oaStatisticsStack[gc_oaStatisticsStack.size()-2].m_lTableSwitch - gc_oaStatisticsStack.back().m_lTableSwitch;
	gc_oStatistics.m_lHuffmanData += gc_oaStatisticsStack[gc_oaStatisticsStack.size()-2].m_lHuffmanData - gc_oaStatisticsStack.back().m_lHuffmanData;

	gc_oaStatisticsStack.pop_back();
}


void PopIgnoreStatistics() {

	if (gc_oaStatisticsStack.size() == 0) {
		eprintf("PopStatistics(): Error: Statistics stack is empty.\n");
		abort();
	}

	gc_oaStatisticsStack.pop_back();
}


void PushNumeration(std::vector<int> &ia, int n, int s1, int s2) {

/*	int t, i, z, a;

//	printf("Push %d ...\n",n);
	t = (int)(floor(mylog2(n+1))+0.5);
//	printf("t=%d\n",t);
	i = n;
	for (z=0;z<t;z++) {
		a = i - 2*(((int)(ceil(i/2.0)+0.5)) - 1);
//		printf("%d\n",a);
		if (a == 1)
			ia.push_back(s1);
		else
			ia.push_back(s2);
		i = ((int)(ceil(i/2.0)+0.5)) - 1;
	}*/

	int z2;

	z2 = n-1;
	while (true) {
		if (z2 & 1)
			ia.push_back(s2);
		else
			ia.push_back(s1);
		if (z2 < 2)
			break;
		z2 = (z2 - 2) / 2;
	}
}


int GetAtomOrd(const char *s) {

	if (mystricmp(s, "H"  ) == 0) return   1;
	if (mystricmp(s, "D"  ) == 0) return   1;
	if (mystricmp(s, "B"  ) == 0) return   5;
	if (mystricmp(s, "C"  ) == 0) return   6;
	if (mystricmp(s, "N"  ) == 0) return   7;
	if (mystricmp(s, "O"  ) == 0) return   8;
	if (mystricmp(s, "F"  ) == 0) return   9;
	if (mystricmp(s, "P"  ) == 0) return  15;
	if (mystricmp(s, "S"  ) == 0) return  16;
	if (mystricmp(s, "K"  ) == 0) return  19;
	if (mystricmp(s, "V"  ) == 0) return  23;
	if (mystricmp(s, "Y"  ) == 0) return  39;
	if (mystricmp(s, "I"  ) == 0) return  53;
	if (mystricmp(s, "W"  ) == 0) return  74;
	if (mystricmp(s, "U"  ) == 0) return  92;

	if (mystricmp(s, "He" ) == 0) return   2;
	if (mystricmp(s, "Li" ) == 0) return   3;
	if (mystricmp(s, "Be" ) == 0) return   4;
	if (mystricmp(s, "Ne" ) == 0) return  10;
	if (mystricmp(s, "Na" ) == 0) return  11;
	if (mystricmp(s, "Mg" ) == 0) return  12;
	if (mystricmp(s, "Al" ) == 0) return  13;
	if (mystricmp(s, "Si" ) == 0) return  14;
	if (mystricmp(s, "Cl" ) == 0) return  17;
	if (mystricmp(s, "Ar" ) == 0) return  18;
	if (mystricmp(s, "Ca" ) == 0) return  20;
	if (mystricmp(s, "Sc" ) == 0) return  21;
	if (mystricmp(s, "Ti" ) == 0) return  22;
	if (mystricmp(s, "Cr" ) == 0) return  24;
	if (mystricmp(s, "Mn" ) == 0) return  25;
	if (mystricmp(s, "Fe" ) == 0) return  26;
	if (mystricmp(s, "Co" ) == 0) return  27;
	if (mystricmp(s, "Ni" ) == 0) return  28;
	if (mystricmp(s, "Cu" ) == 0) return  29;
	if (mystricmp(s, "Zn" ) == 0) return  30;
	if (mystricmp(s, "Ga" ) == 0) return  31;
	if (mystricmp(s, "Ge" ) == 0) return  32;
	if (mystricmp(s, "As" ) == 0) return  33;
	if (mystricmp(s, "Se" ) == 0) return  34;
	if (mystricmp(s, "Br" ) == 0) return  35;
	if (mystricmp(s, "Kr" ) == 0) return  36;
	if (mystricmp(s, "Rb" ) == 0) return  37;
	if (mystricmp(s, "Sr" ) == 0) return  38;
	if (mystricmp(s, "Zr" ) == 0) return  40;
	if (mystricmp(s, "Nb" ) == 0) return  41;
	if (mystricmp(s, "Mo" ) == 0) return  42;
	if (mystricmp(s, "Tc" ) == 0) return  43;
	if (mystricmp(s, "Ru" ) == 0) return  44;
	if (mystricmp(s, "Rh" ) == 0) return  45;
	if (mystricmp(s, "Pd" ) == 0) return  46;
	if (mystricmp(s, "Ag" ) == 0) return  47;
	if (mystricmp(s, "Cd" ) == 0) return  48;
	if (mystricmp(s, "In" ) == 0) return  49;
	if (mystricmp(s, "Sn" ) == 0) return  50;
	if (mystricmp(s, "Sb" ) == 0) return  51;
	if (mystricmp(s, "Te" ) == 0) return  52;
	if (mystricmp(s, "Xe" ) == 0) return  54;
	if (mystricmp(s, "Cs" ) == 0) return  55;
	if (mystricmp(s, "Ba" ) == 0) return  56;
	if (mystricmp(s, "La" ) == 0) return  57;
	if (mystricmp(s, "Ce" ) == 0) return  58;
	if (mystricmp(s, "Pr" ) == 0) return  59;
	if (mystricmp(s, "Nd" ) == 0) return  60;
	if (mystricmp(s, "Pm" ) == 0) return  61;
	if (mystricmp(s, "Sm" ) == 0) return  62;
	if (mystricmp(s, "Eu" ) == 0) return  63;
	if (mystricmp(s, "Gd" ) == 0) return  64;
	if (mystricmp(s, "Tb" ) == 0) return  65;
	if (mystricmp(s, "Dy" ) == 0) return  66;
	if (mystricmp(s, "Ho" ) == 0) return  67;
	if (mystricmp(s, "Er" ) == 0) return  68;
	if (mystricmp(s, "Tm" ) == 0) return  69;
	if (mystricmp(s, "Yb" ) == 0) return  70;
	if (mystricmp(s, "Lu" ) == 0) return  71;
	if (mystricmp(s, "Hf" ) == 0) return  72;
	if (mystricmp(s, "Ta" ) == 0) return  73;
	if (mystricmp(s, "Re" ) == 0) return  75;
	if (mystricmp(s, "Os" ) == 0) return  76;
	if (mystricmp(s, "Ir" ) == 0) return  77;
	if (mystricmp(s, "Pt" ) == 0) return  78;
	if (mystricmp(s, "Au" ) == 0) return  79;
	if (mystricmp(s, "Hg" ) == 0) return  80;
	if (mystricmp(s, "Tl" ) == 0) return  81;
	if (mystricmp(s, "Pb" ) == 0) return  82;
	if (mystricmp(s, "Bi" ) == 0) return  83;
	if (mystricmp(s, "Po" ) == 0) return  84;
	if (mystricmp(s, "At" ) == 0) return  85;
	if (mystricmp(s, "Rn" ) == 0) return  86;
	if (mystricmp(s, "Fr" ) == 0) return  87;
	if (mystricmp(s, "Ra" ) == 0) return  88;
	if (mystricmp(s, "Ac" ) == 0) return  89;
	if (mystricmp(s, "Th" ) == 0) return  90;
	if (mystricmp(s, "Pa" ) == 0) return  91;
	if (mystricmp(s, "Np" ) == 0) return  93;
	if (mystricmp(s, "Pu" ) == 0) return  94;
	if (mystricmp(s, "Am" ) == 0) return  95;
	if (mystricmp(s, "Cm" ) == 0) return  96;
	if (mystricmp(s, "Bk" ) == 0) return  97;
	if (mystricmp(s, "Cf" ) == 0) return  98;
	if (mystricmp(s, "Es" ) == 0) return  99;
	if (mystricmp(s, "Fm" ) == 0) return 100;
	if (mystricmp(s, "Md" ) == 0) return 101;
	if (mystricmp(s, "No" ) == 0) return 102;
	if (mystricmp(s, "Lr" ) == 0) return 103;

	return 200;
}

