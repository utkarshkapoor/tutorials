/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "cc_extrapolator.h"
#include "linalg.h"
#include <set>



void CExtrapolator::Initialize(
		int resx,
		int resy,
		int resz,
		int srangex,
		int srangey,
		int srangez,
		int trange,
		int sorder,
		int torder,
		int offsetx,
		int offsety,
		int offsetz,
		bool crosss,
		bool crosst,
		bool wrap,
		bool crossranges,
		bool crossranget,
		double distexpo,
		double timeexpo,
		double corrfactor,
		bool verbose,
		bool silent
	) {

	int ix, iy, iz, it, rx, ry, rz, rt, dp, i, j2, morder;
	int jx, jy, jz, jt, kx, ky, kz, kt;
	int dx, dy, dz, dt;
	int maxdp, maxcoeff;
	double tf;
	CxDMatrixMN minp, mout;
	std::vector<int> iax, iay, iaz, iat, iaind;
	std::set<int> setx, sety, setz, sett;
	std::set<int>::iterator setit;


	if ((offsetx < 0) || (offsetx >= srangex)) {
		eprintf("CExtrapolator::Initialize(): Error: Invalid value for offsetx (found %d, allowed 0 .. %d).\n",offsetx,srangex-1);
		abort();
	}
	if ((offsety < 0) || (offsety >= srangey)) {
		eprintf("CExtrapolator::Initialize(): Error: Invalid value for offsety (found %d, allowed 0 .. %d).\n",offsety,srangey-1);
		abort();
	}
	if ((offsetz < 0) || (offsetz >= srangez)) {
		eprintf("CExtrapolator::Initialize(): Error: Invalid value for offsetz (found %d, allowed 0 .. %d).\n",offsetz,srangez-1);
		abort();
	}

	m_fDistExpoAdd = 1.0;
	m_fTimeExpoAdd = 1.0;

	if (sorder < 0) { // Order -1 means no extrapolation, which is less than order 0 (constant extrapolation)
		sorder = 0;
		srangex = 1;
		srangey = 1;
		srangez = 1;
	}
	
	if (torder < 0) {
		torder = 0;
		trange = 1;
	}

	m_iSRange[0] = srangex;
	m_iSRange[1] = srangey;
	m_iSRange[2] = srangez;
	m_iTRange = trange;
	m_iSOffset[0] = offsetx;
	m_iSOffset[1] = offsety;
	m_iSOffset[2] = offsetz;
	m_iSOrder = sorder;
	m_iTOrder = torder;

	m_bCrossS = crosss;
	m_bCrossRangeS = crossranges;
	m_bWrap = wrap;
	m_bCrossT = crosst;
	m_bCrossRangeT = crossranget;
	m_fDistExpo = distexpo;
	m_fTimeExpo = timeexpo;
	m_fCorrFactor = corrfactor;

	m_iRes[0] = resx;
	m_iRes[1] = resy;
	m_iRes[2] = resz;

	m_iSRange01 = m_iSRange[0] * m_iSRange[1];
	m_iSRange012 = m_iSRange[0] * m_iSRange[1] * m_iSRange[2];
	m_iTempVal[0] = m_iRes[0] - m_iSRange[0] + m_iSOffset[0];
	m_iTempVal[1] = m_iRes[1] - m_iSRange[1] + m_iSOffset[1];
	m_iTempVal[2] = m_iRes[2] - m_iSRange[2] + m_iSOffset[2];
	m_iRes012 = m_iRes[0] * m_iRes[1] * m_iRes[2];
	m_iRes12 = m_iRes[1] * m_iRes[2];

	maxdp = 0;
	maxcoeff = 0;


	m_oaCubeFrames.resize(m_iTRange);
	m_oaAtomFrames.resize(m_iTRange);
	m_iCubeFramePos = 0;
	m_iCubeFrameCount = 0;
	for (ix=0;ix<m_iTRange;ix++) {
		m_oaCubeFrames[ix] = NULL;
		m_oaAtomFrames[ix] = NULL;
	}

	if (verbose)
		mprintf("Initializing Extrapolator...\n");

	for (it=0;it<m_iTRange;it++) {

		for (iz=0;iz<m_iSRange[2];iz++) {

			for (iy=0;iy<m_iSRange[1];iy++) {

				for (ix=0;ix<m_iSRange[0];ix++) {

					if (verbose)
						mprintf("\n*** Pattern %lu: Preparing Matrix for t=%d x=%d y=%d z=%d ...\n",m_oaPatterns.size(),it,ix,iy,iz);

					m_oaPatterns.push_back(CExtraPattern());

					dp = 0;

					iax.clear();
					iay.clear();
					iaz.clear();
					iat.clear();
					iaind.clear();

					setx.clear();
					sety.clear();
					setz.clear();
					sett.clear();

					for (jt=0;jt<m_iTRange-it;jt++) {

						for (jx=-m_iSOffset[0];jx<m_iSRange[0]-m_iSOffset[0];jx++) {

							if (!wrap) {
								if (ix > m_iSOffset[0]) {
									if (jx >= m_iSRange[0]-ix)
										continue;
								} else if (ix <= m_iSOffset[0]) {
									if (jx < -m_iSOffset[0]+ix)
										continue;
								}
							}

							if ((ix <= m_iSOffset[0]) && (jx < -m_iSOffset[0]+ix) && wrap)
								kx = jx+resx;
							else if ((ix > m_iSOffset[0]) && (jx > m_iSRange[0]-1-ix) && wrap)
								kx = jx-resx;
							else
								kx = jx;

							for (jy=-m_iSOffset[1];jy<m_iSRange[1]-m_iSOffset[1];jy++) {

								if (!wrap) {
									if (iy > m_iSOffset[1]) {
										if (jy >= m_iSRange[1]-iy)
											continue;
									} else if (iy <= m_iSOffset[1]) {
										if (jy < -m_iSOffset[1]+iy)
											continue;
									}
								}

								if ((iy <= m_iSOffset[1]) && (jy < -m_iSOffset[1]+iy) && wrap)
									ky = jy+resy;
								else if ((iy > m_iSOffset[1]) && (jy > m_iSRange[1]-1-iy) && wrap)
									ky = jy-resy;
								else
									ky = jy;

								for (jz=-m_iSOffset[2];jz<m_iSRange[2]-m_iSOffset[2];jz++) {

									if (!wrap) {
										if (iz > m_iSOffset[2]) {
											if (jz >= m_iSRange[2]-iz)
												continue;
										} else if (iz <= m_iSOffset[2]) {
											if (jz < -m_iSOffset[2]+iz)
												continue;
										}
									}

									if ((iz <= m_iSOffset[2]) && (jz < -m_iSOffset[2]+iz) && wrap)
										kz = jz+resz;
									else if ((iz > m_iSOffset[2]) && (jz > m_iSRange[2]-1-iz) && wrap)
										kz = jz-resz;
									else
										kz = jz;

									if (!crossranges && (((jx != 0) && (jy != 0)) || ((jx != 0) && (jz != 0)) || ((jy != 0) && (jz != 0))))
										continue;

									if (!crossranget && (jt != 0) && ((jx != 0) || (jy != 0) || (jz != 0)))
										continue;

									if ((jt == 0) && ((long)kx*resy*resz+(long)ky*resz+(long)kz >= 0))
										continue;

									if (verbose) {
										mprintf("%3d:  ( ",dp+1);
										mprintf("%2d",jt);
										mprintf(" | ");
										mprintf("%2d:%4d",jx,kx);
										mprintf(" | ");
										mprintf("%2d:%4d",jy,ky);
										mprintf(" | ");
										mprintf("%2d:%4d",jz,kz);
										mprintf(" ) ");
										mprintf("DIdx %10ld",(long)kx*resy*resz+(long)ky*resz+(long)kz);
										mprintf("\n");
									}

									iax.push_back(jx);
									iay.push_back(jy);
									iaz.push_back(jz);
									iat.push_back(jt);
									setx.insert(jx);
									sety.insert(jy);
									setz.insert(jz);
									sett.insert(jt);
									iaind.push_back((long)kx*resy*resz+(long)ky*resz+(long)kz);
									dp++;
								}
							}
						}
					}

					if (dp == 0) {
						if (verbose)
							mprintf("  Skipping.\n");
						continue;
					}


					rx = (int)setx.size();
					ry = (int)sety.size();
					rz = (int)setz.size();
					rt = (int)sett.size();

					if (verbose) {

						mprintf("  %lu different X values:",setx.size());
						for (setit=setx.begin();setit!=setx.end();++setit)
							mprintf(" %d",*setit);
						mprintf("\n");

						mprintf("  %lu different Y values:",sety.size());
						for (setit=sety.begin();setit!=sety.end();++setit)
							mprintf(" %d",*setit);
						mprintf("\n");

						mprintf("  %lu different Z values:",setz.size());
						for (setit=setz.begin();setit!=setz.end();++setit)
							mprintf(" %d",*setit);
						mprintf("\n");

						mprintf("  %lu different T values:",sett.size());
						for (setit=sett.begin();setit!=sett.end();++setit)
							mprintf(" %d",*setit);
						mprintf("\n");
					}

					morder = MIN(sorder,MAX3(rx-1,ry-1,rz-1));
_norder:
					if (rx-1 < morder) dx = rx-1; else dx = morder;
					if (ry-1 < morder) dy = ry-1; else dy = morder;
					if (rz-1 < morder) dz = rz-1; else dz = morder;
					if (rt-1 < torder) dt = rt-1; else dt = torder;
					if (verbose)
						mprintf("  Using MaxOrder=%d, degree X=%d, Y=%d, Z=%d, T=%d\n",morder,dx,dy,dz,dt);
					i = 0;
					for (jx=0;jx<=dx;jx++) {
						for (jy=0;jy<=dy;jy++) {
							if (jx+jy > morder)
								break;
							for (jz=0;jz<=dz;jz++) {
								if (jx+jy+jz > morder)
									break;
								if (!crosss && (((jx != 0) && (jy != 0)) || ((jx != 0) && (jz != 0)) || ((jy != 0) && (jz != 0))))
									continue;
								for (jt=0;jt<=dt;jt++) {
									if (!crosst && (jt != 0) && ((jx != 0) || (jy != 0) || (jz != 0)))
										break;
									i++;
								}
							}
						}
					}
					if (i > dp) {
						morder--;
						goto _norder;
					}

					if (verbose) {

						mprintf("  These are %d coefficients.\n",i);
						mprintf("  Constructing %dx%d matrix...\n",dp,i);
					}

					if (i > maxcoeff)
						maxcoeff = i;

					minp = CxDMatrixMN(dp,i);

					if (verbose)
						mprintf("    ");
					i = 0;
					for (jt=0;jt<=dt;jt++) {
						for (jx=0;jx<=dx;jx++) {
							for (jy=0;jy<=dy;jy++) {
								if (jx+jy > morder)
									break;
								for (jz=0;jz<=dz;jz++) {
									if (jx+jy+jz > morder)
										break;
									if (!crosss && (((jx != 0) && (jy != 0)) || ((jx != 0) && (jz != 0)) || ((jy != 0) && (jz != 0))))
										continue;
									if (!crosst && (jt != 0) && ((jx != 0) || (jy != 0) || (jz != 0)))
										break;

									if (verbose) {

										if ((jx==0) && (jy==0) && (jz==0) && (jt==0))
											mprintf("  1");
										if (jx > 1)
											mprintf("  x^%d",jx);
										else if (jx == 1)
											mprintf("  x");
										if (jy > 1)
											mprintf("  y^%d",jy);
										else if (jy == 1)
											mprintf("  y");
										if (jz > 1)
											mprintf("  z^%d",jz);
										else if (jz == 1)
											mprintf("  z");
										if (jt > 1)
											mprintf("  t^%d",jt);
										else if (jt == 1)
											mprintf("  t");
									}

									for (kt=0;kt<dp;kt++)
										minp(kt,i) = (double)(mypow(iat[kt],jt) * mypow(iax[kt],jx) * mypow(iay[kt],jy) * mypow(iaz[kt],jz)) *
													1.0/mypow((double)iax[kt]*iax[kt]+iay[kt]*iay[kt]+iaz[kt]*iaz[kt]+m_fDistExpoAdd,0.5*distexpo) *
													1.0/mypow((double)iat[kt]+m_fTimeExpoAdd,timeexpo);

									i++;
								}
							}
						}
					}
					if (verbose)
						mprintf("\n");

					mout = ComputePseudoInverse(minp);

					j2 = 0;
					for (kt=0;kt<dp;kt++)
						if (fabs(mout(0,kt)) > 1.0E-13)
							j2++;

					if (verbose)
						mprintf("  Have %d zero coefficients, %d data points remain.\n",dp-j2,j2);

					if (j2 > maxdp)
						maxdp = j2;

					m_oaPatterns.back().m_iaSpatialIndex.resize(j2);
					m_oaPatterns.back().m_iaTemporalIndex.resize(j2);
					m_oaPatterns.back().m_faCoeff.resize(j2);

					if (verbose) {
						mprintf("  Matrix:\n");
						mprintf("  ");
					}
					i = 0;
					for (kt=0;kt<dp;kt++) {
						if (fabs(mout(0,kt)) <= 1.0E-13)
							continue;
						m_oaPatterns.back().m_iaSpatialIndex[i] = iaind[kt];
						m_oaPatterns.back().m_iaTemporalIndex[i] = iat[kt];
						if (verbose)
							mprintf("%2d|%2d|%2d|%2d  ",iat[kt],iax[kt],iay[kt],iaz[kt]);
						i++;
					}
					if (verbose)
						mprintf("\n");

					if (verbose) {

						if ((minp.GetCols() < 20) && (minp.GetRows() < 20))
							minp.Dump();
						else
							mprintf("[too large]\n");

						mprintf("  Pseudo Inverse:\n");

						if ((minp.GetCols() < 20) && (minp.GetRows() < 20))
							mout.Dump();
						else
							mprintf("[too large]\n");
					}

					i = 0;
					tf = 0;
					for (kt=0;kt<dp;kt++) {
						if (fabs(mout(0,kt)) <= 1.0E-13)
							continue;
						m_oaPatterns.back().m_faCoeff[i] = mout(0,kt) *
							1.0/mypow((double)iax[kt]*iax[kt]+iay[kt]*iay[kt]+iaz[kt]*iaz[kt]+m_fDistExpoAdd,0.5*distexpo) *
							1.0/mypow((double)iat[kt]+m_fTimeExpoAdd,timeexpo);
						tf += m_oaPatterns.back().m_faCoeff[i];

				//		m_oaPatterns.back().m_faCoeff[i] = floor(m_oaPatterns.back().m_faCoeff[i]*10000.0+0.5) / 10000.0;

						i++;
					}
					if (verbose)
						mprintf("  Vector: ");
					for (kt=0;kt<(int)m_oaPatterns.back().m_faCoeff.size();kt++) {
						m_oaPatterns.back().m_faCoeff[kt] *= corrfactor / tf;
						if (verbose)
							mprintf("  %.16f",m_oaPatterns.back().m_faCoeff[kt]);
					}
					if (verbose)
						mprintf("\n");

				}
			}
		}
	}

	if (verbose) {
		mprintf("Extrapolator initialized.\n");
		mprintf("%d data points, %d coefficients, %lu patterns.\n\n",maxdp,maxcoeff,m_oaPatterns.size());
	} else if (!silent)
		mprintf("        %d data points, %d coefficients, %lu patterns.\n",maxdp,maxcoeff,m_oaPatterns.size());
}


void CExtrapolator::PushCubeFrame(const CCubeFrame *frame) {

	if (m_iCubeFrameCount < m_iTRange)
		m_iCubeFrameCount++;

	m_iCubeFramePos++;
	if (m_iCubeFramePos >= m_iTRange)
		m_iCubeFramePos -= m_iTRange;

	m_oaCubeFrames[m_iCubeFramePos] = frame;
}


void CExtrapolator::PushAtomFrame(const CAtomSet *frame) {

	if (m_iCubeFrameCount < m_iTRange)
		m_iCubeFrameCount++;

	m_iCubeFramePos++;
	if (m_iCubeFramePos >= m_iTRange)
		m_iCubeFramePos -= m_iTRange;

	m_oaAtomFrames[m_iCubeFramePos] = frame;
}



#include "df.h"


// Momentan beste Settings:
// travis compress cube -check no -cextra -cextrange 4 -cextorder 3 -cexsrange 5 -cexoffset 2 -cexsorder 1
//        -cexscross -cexdistexpo 2.5 -cexscrossrange -cexpredcorr -cexcorrfac 0.97 -cexwrap /home/brehm/cubefill/first10.cube new.bqb


void TestExtrapolator() {

	CExtrapolator extra, extra2;
	CCubeFrame cfr;
	int ix, iy, iz;
	double tf, tf2/*, tf3*/;
	FILE *a;
	CDF dfabs, dfrel;


/*	extra.Initialize(
		100,    // resx,
		100,    // resy,
		100,    // resz,
		4,      // srangex,
		4,      // srangey,
		4,      // srangez,
		1,      // trange,
		3,      // sorder,
		0,      // torder,
		2,      // offsetx,
		2,      // offsety,
		2,      // offsetz,
		true,   // crosss,
		true,   // crosst,
		true,   // wrap,
		true,   // crossranges,
		false   // crossranget
		);*/

	extra.Initialize(
		160,    // resx,
		160,    // resy,
		160,    // resz,
		3,      // srangex,
		3,      // srangey,
		3,      // srangez,
		5,      // trange,
		1,      // sorder,
		4,      // torder,
		1,      // offsetx,
		1,      // offsety,
		1,      // offsetz,
		false,   // crosss,
		false,  // crosst,
		false,   // wrap,
		false,   // crossranges,
		true,  // crossranget
		0.0,      // distexpo
		0.0,     // timeexpo
		1.0,  // corrfactor
		true,    // verbose
		false
		);

	return;

	extra.Initialize(
		160,    // resx,
		160,    // resy,
		160,    // resz,
		5,      // srangex,
		5,      // srangey,
		5,      // srangez,
		1,      // trange,
		2,      // sorder,
		0,      // torder,
		2,      // offsetx,
		2,      // offsety,
		2,      // offsetz,
		true,   // crosss,
		false,  // crosst,
		true,   // wrap,
		true,   // crossranges,
		false,  // crossranget
		6.0,      // distexpo
		0.0,    // timeexpo
		1.0, // corrfactor
		true,    // verbose
		false
		);

/*	for (z=0;z<extra.m_oaPatterns.size();z++)
		if (extra.m_oaPatterns[z].m_iaSpatialIndex.size() != 0)
			mprintf("Pattern %d: %d\n",z,extra.m_oaPatterns[z].m_iaSpatialIndex[0]);

	return;*/

	a = fopen("first10.cube","rt");

	if (!cfr.ReadFrame(a,12,5,6,true)) {
		mprintf("Error reading cube frame.\n");
		return;
	}

	fclose(a);

	extra.PushCubeFrame(&cfr);

/*	ix = 100;
	iy = 100;
	iz = 0;

	tf = extra.Extrapolate(ix,iy,iz);
	tf2 = cfr.m_faBin[ix*160*160+iy*160+iz];

	mprintf(" (%d|%d|%d): Pattern %d/%lu, Orig %.10G Extrap %.10G\n",ix,iy,iz,extra.FindPattern(0,ix,iy,iz),extra.m_oaPatterns.size(),tf2,tf);

	return;*/

	dfabs.m_iResolution = 100;
	dfabs.m_fMinVal = 0;
	dfabs.m_fMaxVal = 1.0;
	dfabs.Create();
	dfabs.SetLabelX("Absolute Deviation");
	dfabs.SetLabelY("Occurrence");

	dfrel.m_iResolution = 100;
	dfrel.m_fMinVal = 1.0;
	dfrel.m_fMaxVal = 2.0;
	dfrel.Create();
	dfrel.SetLabelX("Relative Deviation");
	dfrel.SetLabelY("Occurrence");

	mprintf("Extrapolating:");

	std::vector<double> tda;

	tda.resize(160*160*160);

	for (ix=0;ix<160;ix++) {
		if ((ix%2) == 0)
			mprintf(".");
		for (iy=0;iy<160;iy++) {
			for (iz=0;iz<160;iz++) {
		//		mprintf("%d|%d|%d --> %d/%lu\n",ix,iy,iz,extra.FindPattern(0,ix,iy,iz),extra.m_oaPatterns.size());
				tf = extra.Extrapolate(ix,iy,iz);
				tf2 = cfr.m_faBin[ix*160*160+iy*160+iz];
				dfabs.AddToBin(fabs(tf - tf2));
				if (tf != 0) {
					if (tf > tf2)
						dfrel.AddToBin(tf/tf2);
					else
						dfrel.AddToBin(tf2/tf);
				}
	/*			tf3 = extra2.Extrapolate(ix,iy,iz);
				if (tf > tf2)
					tf = tf / tf2;
				else
					tf = tf2 / tf;
				if (tf3 > tf2)
					tf3 = tf3 / tf2;
				else
					tf3 = tf2 / tf3;
				if (_finite(tf) && _finite(tf3))
					tda[ix*160*160+iy*160+iz] = tf3 - tf;
				else
					tda[ix*160*160+iy*160+iz] = 0;*/
			}
		}
	}

/*	for (ix=0;ix<160*160*160;ix++)
		cfr.m_faBin[ix] = tda[ix];

	a = fopen("diff.cube","wt");
	cfr.WriteFrame_Double(a,true);
	fclose(a);*/

	mprintf("Done.\n");

	dfabs.NormBinIntegral(1000.0);
	dfabs.Write("","absf.csv","",false);

	dfrel.NormBinIntegral(1000.0);
	dfrel.Write("","relf.csv","",false);
}


