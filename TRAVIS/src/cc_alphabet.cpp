/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <algorithm>
#include <set>
#include <math.h>
#include "cc_alphabet.h"
#include "cc_hufftree.h"


bool SORT_Alphabet_Freq(const CAlphabetEntry *e1, const CAlphabetEntry *e2) {

	return (e1->m_iFrequency > e2->m_iFrequency);
}



bool SORT_Alphabet_Symbol(const CAlphabetEntry *e1, const CAlphabetEntry *e2) {

	return (e1->m_iSymbol < e2->m_iSymbol);
}



void CAlphabet::BuildAlphabet(const std::vector<int> &inp, bool verbose) {

/*	std::set<int> ts;
	std::vector<int> tia;
	CAlphabetEntry *ae;
	int z, ti;


	if (verbose)
		mprintf("Building alphabet (new method, %lu symbols)...\n",inp.size());

	for (z=C_MIN_RESERVED;z;z++) {
		ts.insert(z);
		if (z == C_MAX_RESERVED)
			break;
	}

	for (z=0;z<(int)inp.size();z++)
		ts.insert(inp[z]);

	tia.resize(ts.size());
	std::copy(ts.begin(), ts.end(), tia.begin());

	for (z=0;z<(int)m_oaAlphabet.size();z++)
		delete m_oaAlphabet[z];
	m_oaAlphabet.resize(tia.size());

	for (z=0;z<(int)tia.size();z++) {
		ae = new CAlphabetEntry();
		ae->m_iSymbol = tia[z];
		ae->m_iIndex = z;
		ae->m_iFrequency = 0;
		m_oaAlphabet[z] = ae;
	}

	m_iaIndices.resize(inp.size());

	for (z=0;z<(int)inp.size();z++) {
		ti = std::lower_bound(tia.begin(),tia.end(),inp[z]) - tia.begin();
		m_oaAlphabet[ti]->m_iFrequency++;
		m_iaIndices[z] = ti;
	}

	m_oaAlphabetSortFreq.assign(m_oaAlphabet.begin(),m_oaAlphabet.end());

	std::sort(m_oaAlphabetSortFreq.begin(),m_oaAlphabetSortFreq.end(),SORT_Alphabet_Freq);

	if (verbose) {
		mprintf("]\n");
		mprintf("Finished building alphabet (new method).\n");
	}*/


	int z, z2;
	CAlphabetEntry *ae;
	std::vector<std::vector<CAlphabetEntry*> > ta;
	int ti2;
	const unsigned int hash_size = 1024;


	if (verbose) {
		mprintf("Building alphabet (%lu symbols)...\n",inp.size());
		mprintf("    [");
	}

	for (z=0;z<(int)m_oaAlphabet.size();z++)
		delete m_oaAlphabet[z];
	m_oaAlphabet.clear();
	m_oaAlphabetSortFreq.clear();

	ta.resize(hash_size);

	for (z=C_MIN_RESERVED;true;z++) {
		ti2 = positive_modulo(z, hash_size);
		ae = new CAlphabetEntry();
		ae->m_iSymbol = z;
		ae->m_iFrequency = 0;
		m_oaAlphabet.push_back(ae);
		ta[ti2].push_back(ae);
		if (z == C_MAX_RESERVED)
			break;
	}

	for (z=0;z<(int)inp.size();z++) {
		if (verbose) {
			if (fmod(z,inp.size()/60.0) < 1.0) {
				mprintf("#");
				fflush(stdout);
			}
		}
		ti2 = positive_modulo(inp[z], hash_size);
		for (z2=0;z2<(int)ta[ti2].size();z2++)
			if (ta[ti2][z2]->m_iSymbol == inp[z])
				goto _found2;
		z2 = (int)ta[ti2].size();
		ae = new CAlphabetEntry();
		ae->m_iSymbol = inp[z];
		ae->m_iFrequency = 0;
		m_oaAlphabet.push_back(ae);
		ta[ti2].push_back(ae);
_found2:
		ta[ti2][z2]->m_iFrequency++;
	}
	if (verbose) {
		mprintf("]\n");
		mprintf("Found %lu symbol types.\n",m_oaAlphabet.size());
		mprintf("Sorting alphabet by symbols...\n");
	}

	m_oaAlphabetSortFreq.assign(m_oaAlphabet.begin(),m_oaAlphabet.end());

	std::sort(m_oaAlphabet.begin(),m_oaAlphabet.end(),SORT_Alphabet_Symbol);

	if (verbose)
		mprintf("Sorting alphabet by frequencies...\n");

	std::sort(m_oaAlphabetSortFreq.begin(),m_oaAlphabetSortFreq.end(),SORT_Alphabet_Freq);

	for (z=0;z<(int)m_oaAlphabet.size();z++)
		m_oaAlphabet[z]->m_iIndex = z;

	if (verbose)  {
		mprintf("Creating alphabet indices...\n");
		mprintf("    [");
	}

//	mprintf("Assigning output alphabet...\n");
	m_iaIndices.resize(inp.size());
	for (z=0;z<(int)inp.size();z++) {
		if (verbose) {
			if (fmod(z,inp.size()/60.0) < 1.0) {
				mprintf("#");
				fflush(stdout);
			}
		}

		//ti2 = inp[z] % hash_size;
		ti2 = positive_modulo(inp[z], hash_size);
		for (z2=0;z2<(int)ta[ti2].size();z2++)
			if (ta[ti2][z2]->m_iSymbol == inp[z])
				break;
		m_iaIndices[z] = ta[ti2][z2]->m_iIndex;
	}

	if (verbose) {
		mprintf("]\n");
		mprintf("Finished building alphabet.\n");
	}
}


void CAlphabet::Export(CBitSet *bs, bool huffman, bool chr, bool verbose) const {

	int z, bits, ti, border, bborder, bsize, bold;
	long mis, mas, s, m, cht;
	std::vector<int> ia, ia2;
	CHuffmanTree *ht, *ht2;

	if (verbose) {
		if (chr)
			mprintf("Exporting alphabet (char flag)...\n");
		else
			mprintf("Exporting alphabet (no char flag)...\n");
	}

	mis = 0x7FFFFFF0;
	mas = -0x7FFFFFF0;
	for (z=0;z<(int)m_oaAlphabet.size();z++) {
		if (m_oaAlphabet[z]->m_iSymbol >= C_MIN_RESERVED)
			break;
		if (m_oaAlphabet[z]->m_iSymbol > mas)
			mas = m_oaAlphabet[z]->m_iSymbol;
		if (m_oaAlphabet[z]->m_iSymbol < mis)
			mis = m_oaAlphabet[z]->m_iSymbol;
	}

	if (verbose)
		mprintf("Will write %d symbol types.\n",z);

	if (!chr) {
		bits = (int)ceil(mylog2(z+1));
		if (bits <= 8) {
			bs->WriteBit(0);
			if (verbose)
				mprintf("Using short format (8 bits) for symbol type count.\n");
			bs->WriteBits(z,8);
		} else {
			bs->WriteBit(1);
			bs->WriteBits(bits,6);
			if (verbose)
				mprintf("Using %d bits for symbol type count.\n",bits);
			bs->WriteBits(z,bits);
		}
	} else
		bs->WriteBits(z,8);

	if (verbose)
		mprintf("Symbols range from %ld to %ld.\n",mis,mas);

	if (huffman) {

		if (verbose)
			mprintf(">>> Huffman coding of alphabet >>>\n");

		bs->WriteBit(1);

		if (!chr) {
			bits = (int)ceil(mylog2(abs(m_oaAlphabet[0]->m_iSymbol)+1));

			if (verbose)
				mprintf("First alphabet entry is %d, using %d bits.\n",m_oaAlphabet[0]->m_iSymbol,bits);

			bs->WriteBits(bits,6);

			if (m_oaAlphabet[0]->m_iSymbol < 0)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			bs->WriteBits(abs(m_oaAlphabet[0]->m_iSymbol),bits);
		} else
			bs->WriteBits(m_oaAlphabet[0]->m_iSymbol,8);

		m = 0;
		for (z=0;z<(int)m_oaAlphabet.size()-1;z++) {
			if (m_oaAlphabet[z+1]->m_iSymbol >= C_MIN_RESERVED)
				break;
			if (m_oaAlphabet[z+1]->m_iSymbol-m_oaAlphabet[z]->m_iSymbol > m)
				m = m_oaAlphabet[z+1]->m_iSymbol-m_oaAlphabet[z]->m_iSymbol;
			ia.push_back(m_oaAlphabet[z+1]->m_iSymbol-m_oaAlphabet[z]->m_iSymbol);
		}

		bits = (int)ceil(mylog2(m+1));

		if (verbose)
			mprintf("Largest difference is %ld, using %d bits.\n",m,bits);

		ia2.resize(bits+1);

		for (z=0;z<(int)ia2.size();z++)
			ia2[z] = 0;

		for (z=0;z<(int)ia.size();z++)
			ia2[(int)ceil(mylog2(ia[z]+1))]++;

		if (verbose)
			for (z=0;z<(int)ia2.size();z++)
				mprintf("      %2d bits: %6d\n",z,ia2[z]);

		bborder = -1;
		bsize = 1000000000;
		for (border=0;border<bits;border++) {
			if (chr)
				ti = 0;
			else
				ti = 10;
			for (z=0;z<(int)ia.size();z++)
				if ((int)ceil(mylog2(ia[z]+1)) <= border)
					ti += border+1;
				else
					ti += bits+1;
			if (ti < bsize) {
				bsize = ti;
				bborder = border;
			}
			if (verbose)
				mprintf("      Border=%2d: %.2f Bytes.\n",border,ti/8.0);
		}
		if (verbose)
			mprintf("Best border storage would require %.2f bytes (border=%d).\n",bsize/8.0,bborder);

		if (verbose)
			mprintf("Creating Normal Huffman tree...\n");

		ht = new CHuffmanTree();
		ht->Init(m+1);
		for (z=0;z<(int)ia.size();z++)
			ht->m_iaFrequencies[ia[z]]++;
		ht->BuildTree(false);
		ti = ht->ExportTree(NULL,chr,verbose);
		for (z=0;z<(int)ia.size();z++)
			ti += ht->m_oaBitStrings[ia[z]]->GetLength();

		if (verbose)
			mprintf("Creating Canonical Huffman tree...\n");

		ht2 = new CHuffmanTree();
		ht2->Init(m+1);
		for (z=0;z<(int)ia.size();z++)
			ht2->m_iaFrequencies[ia[z]]++;
		ht2->BuildTree(true);
		cht = ht2->ExportTree(NULL,chr,verbose);
		for (z=0;z<(int)ia.size();z++)
			cht += ht2->m_oaBitStrings[ia[z]]->GetLength();

		if (verbose) {
			mprintf("Best border storage would require %8.2f bytes.\n",bsize/8.0);
			mprintf("Huffman alphabet storage requires %8.2f Bytes.\n",ti/8.0);
			mprintf("Canonical Huffman would require   %8.2f Bytes.\n",cht/8.0);
		}

		if ((bsize <= ti) && (bsize <= cht)) {

			if (verbose)
				mprintf("Falling back to more efficient border algorithm.\n");

			bs->WriteBit(0);

			if (chr) {
				bs->WriteBits(bits,4);
				bs->WriteBits(bborder,3);
			} else {
				bs->WriteBits(bits,6);
				bs->WriteBits(bborder,5);
			}

			for (z=0;z<(int)ia.size();z++) {
				if ((int)ceil(mylog2(ia[z]+1)) <= bborder) {
					bs->WriteBit(0);
					bs->WriteBits(ia[z],bborder);
				} else {
					bs->WriteBit(1);
					bs->WriteBits(ia[z],bits);
				}
			}

		} else if (ti <= cht) {

			if (verbose)
				mprintf("Exporting Normal Huffman tree...\n");

			bs->WriteBit(1);

			bold = bs->GetLength();

			ht->ExportTree(bs,chr,verbose);

			if (verbose)
				mprintf("%d bits written to Huffman tree.\n",bs->GetLength()-bold);

			if (verbose)
				mprintf("Exporting Normal Huffman data (count %lu)...\n",ia.size());

			bold = bs->GetLength();

			for (z=0;z<(int)ia.size();z++)
				bs->WriteBits(ht->m_oaBitStrings[ia[z]]);

			if (verbose)
				mprintf("%d bits written to Huffman data.\n",bs->GetLength()-bold);

		} else {

			if (verbose)
				mprintf("Exporting Canonical Huffman tree...\n");

			bs->WriteBit(1);

			ht2->ExportTree(bs,chr,verbose);

			if (verbose)
				mprintf("Exporting Canonical Huffman data...\n");

			for (z=0;z<(int)ia.size();z++)
				bs->WriteBits(ht2->m_oaBitStrings[ia[z]]);
		}

		delete ht;
		delete ht2;

		if (verbose)
			mprintf("Done.\n");

	} else { // Not Huffman

		bs->WriteBit(0);

		mis = iabs(mis);
		mas = iabs(mas);
		if (mis > mas)
			mas = mis;
		bits = (int)ceil(mylog2(mas+1))+1;
		s = 1<<(bits-1);

		if (verbose)
			mprintf("Requires %d bits per symbol, s=%ld.\n",bits,s);

		bs->WriteBits(bits,6);

		for (z=0;z<(int)m_oaAlphabet.size();z++) {
			if (m_oaAlphabet[z]->m_iSymbol >= C_MIN_RESERVED)
				break;
			bs->WriteBits(m_oaAlphabet[z]->m_iSymbol+s,bits);
		}
	}

	if (verbose)
		mprintf("Exporting alphabet done.\n");
}


void CAlphabet::Import(CBitSet *bs, bool chr, bool verbose) {

	int z, ti, bits, border, bold;
	unsigned long i;
	long l;
	long s;
	CAlphabetEntry *ae;
	CHuffmanTree *ht;

	if (verbose) {
		if (chr)
			mprintf("Importing alphabet (char flag)...\n");
		else
			mprintf("Importing alphabet (no char flag)...\n");
	}

	if (!chr) {
		if (bs->ReadBit()) {
			bits = bs->ReadBitsInteger(6);
			if (verbose)
				mprintf("Symbol type count is stored with %d bits.\n",bits);
			i = bs->ReadBitsInteger(bits);
		} else {
			if (verbose)
				mprintf("Symbol type count short format (8 bits).\n");
			i = bs->ReadBitsInteger(8);
		}
	} else
		i = bs->ReadBitsInteger(8);

	if (chr && (i == 0)) {
		if (verbose)
			mprintf("Warning: Symbol type count was 0, assuming 256.\n");
		i = 256;
	}

	if (verbose)
		mprintf("Will read %lu symbol types.\n",i);

	m_oaAlphabet.reserve(i);

	if (bs->ReadBit()) { // Huffman

		if (verbose)
			mprintf("Reading Huffman-coded alphabet.\n");

		if (!chr) {
			bits = bs->ReadBitsInteger(6);

			if (verbose)
				mprintf("First alphabet symbol has %d bits.\n",bits);

			if (bs->ReadBit())
				l = -((long)bs->ReadBitsInteger(bits));
			else
				l = bs->ReadBitsInteger(bits);
		} else
			l = bs->ReadBitsInteger(8);

		if (verbose)
			mprintf("First alphabet symbol is %ld.\n",l);

		ae = new CAlphabetEntry();
		ae->m_iFrequency = 0;
		ae->m_iIndex = 0;
		ae->m_iSymbol = l;
		m_oaAlphabet.push_back(ae);

		if (bs->ReadBit()) { // Huffman

			ht = new CHuffmanTree();

			if (verbose)
				mprintf("Reading Huffman table...\n");

			bold = bs->GetReadPos();

			ht->ImportTree(bs,chr,verbose);

			if (verbose)
				mprintf("Read %d bits from Huffman table.\n",bs->GetReadPos()-bold);

			if (verbose)
				mprintf("Tree root element has %lu children.\n",ht->m_pTree->m_oaChildren.size());

			if (verbose)
				mprintf("Reading Huffman data (%ld symbols)...\n",((long)i)-1);

			bold = bs->GetReadPos();

			for (z=0;z<((long)i)-1;z++) {
				ae = new CAlphabetEntry();
				ae->m_iFrequency = 0;
				ae->m_iIndex = (int)m_oaAlphabet.size();
				ae->m_iSymbol = m_oaAlphabet[m_oaAlphabet.size()-1]->m_iSymbol + ht->DecodeSymbol(bs);
				m_oaAlphabet.push_back(ae);
			}

			if (verbose)
				mprintf("Read %d bits from Huffman data.\n",bs->GetReadPos()-bold);

			delete ht;

		} else { // Fallback Border Storage

			if (verbose)
				mprintf("Fallback border storage.\n");

			if (chr) {
				bits = bs->ReadBitsInteger(4);
				border = bs->ReadBitsInteger(3);
			} else {
				bits = bs->ReadBitsInteger(6);
				border = bs->ReadBitsInteger(5);
			}

			if (verbose)
				mprintf("%d bits maximum, %d bits border.\n",bits,border);

			for (z=0;z<((long)i)-1;z++) {

				if (bs->ReadBit())
					ti = bs->ReadBitsInteger(bits);
				else
					ti = bs->ReadBitsInteger(border);

				ae = new CAlphabetEntry();
				ae->m_iFrequency = 0;
				ae->m_iIndex = (int)m_oaAlphabet.size();
				ae->m_iSymbol = m_oaAlphabet[m_oaAlphabet.size()-1]->m_iSymbol + ti;
				m_oaAlphabet.push_back(ae);
			}

		}

		if (verbose)
			mprintf("Done.\n");

	} else { // Not Huffman

		bits = bs->ReadBitsInteger(6);
		s = 1<<(bits-1);

		if (verbose)
			mprintf("%lu symbol types, %d bits per symbol type, s=%ld.\n",i,bits,s);

		for (z=0;z<(long)i;z++) {
			ae = new CAlphabetEntry();
			ae->m_iIndex = z;
			m_oaAlphabet.push_back(ae);
			ae->m_iSymbol = bs->ReadBitsInteger(bits) - s;
		}
	}

	for (z=C_MIN_RESERVED;true;z++) {
		ae = new CAlphabetEntry();
		ae->m_iSymbol = z;
		ae->m_iFrequency = 0;
		m_oaAlphabet.push_back(ae);
		if (z == C_MAX_RESERVED)
			break;
	}

	if (verbose)
		mprintf("Importing alphabet done.\n");
}


int CAlphabet::FindIndex(int symbol) const {

	int z;

	for (z=0;z<(int)m_oaAlphabet.size();z++)
		if (m_oaAlphabet[z]->m_iSymbol == symbol)
			return z;

	return -1;
}


void CAlphabet::RecalcFrequencies(const std::vector<int> &ia) {

	int z;

	for (z=0;z<(int)m_oaAlphabet.size();z++)
		m_oaAlphabet[z]->m_iFrequency = 0;

	for (z=0;z<(int)ia.size();z++)
		m_oaAlphabet[ia[z]]->m_iFrequency++;

	std::sort(m_oaAlphabetSortFreq.begin(),m_oaAlphabetSortFreq.end(),SORT_Alphabet_Freq);
}
