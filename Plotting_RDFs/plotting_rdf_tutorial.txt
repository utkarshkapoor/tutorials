******************************************************************************************
*********************************Tutorial for Plotting RDFs*******************************
******************************************************************************************

Name: Ionic liquid [bmim+][cl-][ntf2-] 50 : 50 mixture

******************************************************************************************
*********************************Required Materials***************************************
******************************************************************************************

Required files: trajectories, TRAVIS, XMGRACE

Required software can also be downloaded from:
(1) TRAVIS: http://travis-analyzer.de

(Choose MAC or WINDOWS based on your requirement)

For WINDOWS
(1) gitbash : https://gitforwindows.org       (For getting a terminal)

Files Provided: cl_ntf2_5050.pdb (containing 100 snapshots), pre-compiled TRAVIS


******************************************************************************************
****************************Computing SDFs using TRAVIS***********************************
******************************************************************************************

> MAKE SURE YOU HAVE A COMPILER TO COMPILE TRAVIS
> THE SPECIFIC COMPILER SETTINGS FOR TRAVIS CAN BE CHANGED BY EDITING Makefile.
> EDIT g++ to any compiler of your choice

Steps for computing SDFs

1. After cloning the directory. Move to TRAVIS directory.

> make clean
> make

- This is required to create a path, to execute the executable file.

2. Supply the trajectory file to TRAVIS

> /PATH/TRAVIS/exe/travis -nocolor -p cl_ntf2_5050.pdb

- travis is the executable file.
-nocolor helps to see working of TRAVIS better if we have white background for the terminal.
-p *.pdb allows TRAVIS to read in the pdb file.

3. Follow the questions asked by TRAVIS.

> Use the advanced mode until the analysis selection menu (y/n)? [no] y

- [no] represents the default answers -- if you press enter
- Its a good practice to say YES to "advanced mode" for more options

> Use these values (y) or enter different values (n)? [yes] y

> Update cell geometry in every time step (i.e., NPT ensemble) (y) or use fixed cell (n)? [yes] y

> Should the periodic box be multiplied (y/n)? [no]

> Execute molecule recognition for which time step (-1 = disable)? [0]

- Always a good practice to have TRAVIS execute molecule recognition

>  Use spatial domain decomposition for faster mol. rec. (disable if there are problems) (y/n)? [yes] n

- Its better to say NO to this, although the process is slower the recognition of molecules is accurate in this way.
- You can say yes, but then its better to make sure that the molecule is rightly recognized. 

> Show bond matrices only for first representant of each molecule type (y/n)? [yes] y
> Show only bonds in the bond matrices (y/n)? [yes] y

- For this particular example you will see that F atoms (CF3) are not recognized as part of [NTf2-] molecule. 
- One way to fix this is change/increase the covalent radii of F atoms that will allow TRAVIS to connect bonds between F and S atoms.
- To do that, do not accept the molecules when asked in the next question.
- Remember, in a similar manner we can also break all or specific bonds to create different species.

>  Create images of the structural formulas (y/n)? [no]
> Accept these molecules (y) or change something (n)? [yes] n

- - 

*** Modify Molecules ***

    Your choices:

    1.) Change covalent atom radii used for bond recognition
    2.) Break specific bonds
    3.) Rename elements

Please select: [done] 1

> Which radius do you want to change (RETURN=done)? F
> Please enter new bond radius for F in pm: [83.9] 150

> Which radius do you want to change (RETURN=done)?
> Please select: [done]

- This will ask you to re-run the molecule recognition. Do that and make sure the molecules are recognized as expected.

> Accept these molecules (y) or change something (n)? [yes]
> Change the atom ordering in some molecule (y/n)? [no]

> Define additional virtual atoms (y/n)? [no]
> Do you want to define pseudo-molecules (y/n)? [no]

- Always good to read the description provided by TRAVIS software of what it is doing with the molecules.

> Which functions to compute (comma separated)? rdf

- safe to always perform only one kind of analysis at a time.

> Use the advanced mode for the main part (y/n)? [no] y (ALWAYS SAY YES in general)

>  Which of the molecules should be the reference molecule (F2NO4S2=1, C8H3N2=2, Cl=3)? 2

- Choose the molecule you wish to put at the center, as a reference.
    
> Please enter the atom to put into the box center (e.g. C3): [center of mass]

- Any particular atom can be selected. But, in general center-of-mass works good. 


> Perform this observation intramolecular (within the reference molecule) (0) or intermolecular (1)? [1]

- If we have long molecules, computing intramolecular SDFs might be of interest.
- But here we like to perform intermolecular between cation and anion so we choose that.

> Which molecule should be observed (F2NO4S2=1, C8H3N2=2, Cl=3)? 3

- Choose the molecule you want to observe with respect to the molecule put as the reference in the center of the box.

> Observe only certain molecules of C8H3N2 / Cl (y/n)? [no]

- This comes in handy if we want to look at movement of a particular molecule rather than average spatial densities.
   
> Decompose this observation into contributions from different elements (y/n)? [no]

- This comes in handy if we want to look at neighbor distributions; how 1st neighbor, 2nd neighbor (defined by the distance minimum condition) contributes to the average RDF

> Take reference atom(s) from RM C8H3N2 (0) or from OM Cl (1)? [0]
> Take observed atom(s) from RM C8H3N2 (0) or from OM Cl (1)? [1]

> Which atom(s) to take from RM C8H3N2 (e.g. "C1,C3-5,H", "*"=all)? [#2]

- Here #2 is defined as a virtual atom (center-of-mass) of the molecule. See the description when TRAVIS generates information after molecule recognition.
   
> Add another set of atoms to this (!) RDF (y/n)? [no]

- Can add another set of atoms to this RDF too.
- Can come in handy if we are trying to generate one single RDF for calculating total structure factor. As structure factor is calculated by taking fourier transform of the RDF

> Use values (0), 1st time derivative (1), or 2nd time derivative (2) of the values? [0]

> Enter the minimal radius of this RDF in pm: [0]

> Enter the maximal radius of this RDF in pm: [2300.0] 

- 2300.0 is automatically chosen by TRAVIS as half of the box length. (Box length is 4618 pm)

> Enter the resolution (bin count) for this RDF: [300]

- Can increase the number of bins! But usually internal number serves well.

> Please enter histogram resolution (0=no histogram): [0]

- This is a very specific thing, can be considered if it is of interest, otherwise no should be selected. It will create histograms for all types of binning.

> Draw a line in the agr file at g(r) = 1 (y/n)? [no] y

- Always a good practice to see, as g(r) should go to 1 at longer distances.

> Correct radial distribution for this RDF (y/n)? [yes]

> Compute occurrence in nm^(-3) (y) or rel. to uniform density (n)? [no]

- Since, by definition, RDF is local density vs. bulk density; choose calculation of values relative to uniform density


> Save temporal development for this observation (y/n)? [no]
> Create a temporal difference plot for this observation (y/n)? [no]

- These are very specific things, can be considered if it is of interest, otherwise no should be selected. See TRAVIS document to know more. 

> Add a condition to this observation (y/n)? [no]

- This is a very important option. If we say YES, we can define constraints like (1) distance criteria (2) Angle criteria; and the values will be binned only when the condition is met. This comes in handy when computing something within the first solvation shell. For eg: Angular distribution of anions around cations within first coordination sphere.

> Add another observation (y/n)? [no]

- We can compute multiples RDFs at the same time. if we say yes to this, we have to answer similar questions once again to tell TRAVIS which RDF we want to calculate.

> Perform a multi-interval analysis (y/n)? [no]

- You can divide the full trajectory into multiple intervals.

> In which time step to start processing the trajectory? [1]

- This allows us to skip some configurations, initially, if needed.

> How many time steps to use (from this position on)? [all]

- This is total number of snapshots you want to use.

> Use every n-th time step from the trajectory? [1]

- This allows us to skip some configurations, in-between, if needed.




4. Once the execution finishes, this will create multiple files.
> input.txt -> this can be re-used to re-run the same calculation
	       /PATH/TRAVIS/exe/travis -nocolor -p cl_ntf2_5050.pdb - i input.txt
> travis.log -> this records every step that we undergo.

> rdf_***.agr file -> this is an xmgrace format file that contains r vs. g(r) [RDF] data, that can be plotted using XMGRACE.

> Similarly, rdf_***.csv -> is a simple format file that has data in the tabular format. Can also be opened using XMGRACE

******************************************************************************************
*************** Visualizing the RDFs using XMGRACE and generating GRAPHS *****************
******************************************************************************************

Required files:
1. rdf***.agr, file created by TRAVIS and 2. XMGRACE (installed on your local machine or supercomputing cluster)


USING XMGRACE

1. xmgrace rdf***.agr
2. Autoscale - if required
3. See if g(r) -> 1 at longer distances
4. Can change picometer to angstrom. For that, go to Data -> Transformation -> Evaluate Expression. 
5. Select the data and write x = x / 100 and click apply.
6. Autoscale - to see the changed x-axis
7. For changing Axis properties. Go to Plot -> Axis properties and change the relevant information
8. For displaying legends. Go to Plot -> Graph appearance and select "display legends" 
9. To save the graph. (i) Go to File -> Print setup. (ii) choose the appropriate format (pdf, png, or jpeg) and click apply. (iii) Go to File -> Print This will save the graph within the directory.

********* This completes the RDF computation *******************

Happy Computing!







