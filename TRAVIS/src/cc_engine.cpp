/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <math.h>
#include <algorithm>
#include "cc_engine.h"
#include "cc_hilbert.h"
#include "cc_integerengine.h"
#include "cc_bitset.h"
#include "cc_crc32.h"
#include "barbecube.h"
#include "maintools.h"


std::vector<int> gc_iaAtomOrd;

//FILE *gc_fResiCorrFrame = NULL;
//FILE *gc_fResiCorrTraj = NULL;

int gc_iHistogramCounter = 0;


int gc_iOptCubeOrder;
int gc_iOptCubeEps;
int gc_iOptCubeSigni;
bool gc_bOptCubeHilbert;
double gc_fOptCubeNbhFac;
bool gc_bOptCubeUseExtra;
int gc_iOptCubeSRangeX;
int gc_iOptCubeSRangeY;
int gc_iOptCubeSRangeZ;
int gc_iOptCubeTRange;
int gc_iOptCubeSOrder;
int gc_iOptCubeTOrder;
int gc_iOptCubeOffsetX;
int gc_iOptCubeOffsetY;
int gc_iOptCubeOffsetZ;
bool gc_bOptCubeCrossS;
bool gc_bOptCubeCrossT;
bool gc_bOptCubeWrap;
bool gc_bOptCubeCrossRangeS;
bool gc_bOptCubeCrossRangeT;
double gc_fOptCubeDistExpo;
double gc_fOptCubeTimeExpo;
bool gc_bOptCubePredCorr;
double gc_fOptCubeCorrFactor;

int gc_iOptCubeSplit;
int gc_iOptCubeBlock;
int gc_iOptCubeTables;
bool gc_bOptCubeOptTables;
bool gc_bOptCubePreOpt;
int gc_iOptCubeMaxIter;
int gc_iOptCubeMaxChunk;
bool gc_bOptCubeCoderun;
bool gc_bOptCubeBW;
bool gc_bOptCubeMTF;
bool gc_bOptCubeRealSize;

int gc_iOptXYZOrder;
int gc_iOptXYZSigni;
bool gc_bOptXYZUseExtra;
int gc_iOptXYZExTRange;
int gc_iOptXYZExTOrder;

int gc_iOptXYZSplit;
int gc_iOptXYZBlock;
int gc_iOptXYZTables;
bool gc_iOptXYZOptTables;
bool gc_bOptXYZCoderun;
bool gc_bOptXYZBW;
bool gc_bOptXYZMTF;
bool gc_bOptXYZPreOpt;
int gc_iOptXYZMaxIter;
bool gc_bOptXYZSortAtom;
bool gc_bOptXYZComment;
int gc_iOptXYZMaxChunk;
bool gc_bOptXYZRealSize;



CCCEngine::CCCEngine() {

	m_pExtrapolator = NULL;
	m_pExtrapolatorCorr = NULL;
	m_pExtrapolatorXYZ = NULL;
	m_pReadCacheXYZ = NULL;
	m_pReadCacheCube = NULL;
//	m_fEntropyExpoXYZ = 2.0;
//	m_fEntropyExpoCube = 0.5;
	m_iOptIncludeFirst = -1;
}


CCCEngine::~CCCEngine() {

	int z;

//	for (z=0;z<(int)m_oaInputAtomBuf.size();z++)
//		delete m_oaInputAtomBuf[z];
//	for (z=0;z<(int)m_oaInputCubeBuf.size();z++)
//		delete m_oaInputCubeBuf[z];
	for (z=0;z<(int)m_oaOutputAtomBuf.size();z++)
		delete m_oaOutputAtomBuf[z];
	for (z=0;z<(int)m_oaOutputCubeBuf.size();z++)
		delete m_oaOutputCubeBuf[z];
	for (z=0;z<(int)m_oaOutput2AtomBuf.size();z++)
		delete m_oaOutput2AtomBuf[z];
	for (z=0;z<(int)m_oaOutput2CubeBuf.size();z++)
		delete m_oaOutput2CubeBuf[z];

	if (m_pExtrapolator != NULL) {
		delete m_pExtrapolator;
		m_pExtrapolator = NULL;
	}
	if (m_pExtrapolatorCorr != NULL) {
		delete m_pExtrapolatorCorr;
		m_pExtrapolatorCorr = NULL;
	}
	if (m_pExtrapolatorXYZ != NULL) {
		delete m_pExtrapolatorXYZ;
		m_pExtrapolatorXYZ = NULL;
	}
	if (m_pReadCacheXYZ != NULL) {
		delete m_pReadCacheXYZ;
		m_pReadCacheXYZ = NULL;
	}
	if (m_pReadCacheCube != NULL) {
		delete m_pReadCacheCube;
		m_pReadCacheCube = NULL;
	}
}


#define AR_EPS (0.0001)
#define CR_EPS (0.0001)


void CCCEngine::ResetStatistics() {

	gc_iOutputSize = 0;
	gc_iOutputSizeAlphabet = 0;
	gc_iOutputSizeHuffmanTables = 0;
	gc_iOutputSizeTableSwitch = 0;
	gc_iOutputSizeHuffmanData = 0;
	gc_iCount = 0;

	gc_iCOutputSize = 0;
	gc_iCOutputSizeAlphabet = 0;
	gc_iCOutputSizeHuffmanTables = 0;
	gc_iCOutputSizeTableSwitch = 0;
	gc_iCOutputSizeHuffmanData = 0;
	gc_iCCount = 0;
}


void CCCEngine::BeginStatistics() {

	gc_iTempOutputSizeAlphabet = 0;
	gc_iTempOutputSizeHuffmanTables = 0;
	gc_iTempOutputSizeTableSwitch = 0;
	gc_iTempOutputSizeHuffmanData = 0;
}


void CCCEngine::EndStatistics(bool n, bool c) {

	gc_iOutputSizeAlphabet += gc_iTempOutputSizeAlphabet;
	gc_iOutputSizeHuffmanTables += gc_iTempOutputSizeHuffmanTables;
	gc_iOutputSizeTableSwitch += gc_iTempOutputSizeTableSwitch;
	gc_iOutputSizeHuffmanData += gc_iTempOutputSizeHuffmanData;
	if (n)
		gc_iCount++;

	if (c) {
		gc_iCOutputSizeAlphabet += gc_iTempOutputSizeAlphabet;
		gc_iCOutputSizeHuffmanTables += gc_iTempOutputSizeHuffmanTables;
		gc_iCOutputSizeTableSwitch += gc_iTempOutputSizeTableSwitch;
		gc_iCOutputSizeHuffmanData += gc_iTempOutputSizeHuffmanData;
		if (n)
			gc_iCCount++;
	}
}


typedef struct {
	unsigned long m_iN;
	unsigned long m_iH;
} THilbertPair;



int CompareHilbertPair( const void *arg1, const void *arg2 ) {
	
	if (((THilbertPair*)arg1)->m_iH > ((THilbertPair*)arg2)->m_iH)
		return 1;
	else if (((THilbertPair*)arg1)->m_iH < ((THilbertPair*)arg2)->m_iH)
		return -1;
	else
		return 0;
}


void CCCEngine::BuildHilbertIdx(int resx, int resy, int resz, bool verbose) {

	int b, bt;
	int resxyz, resyz;
	bitmask_t mi;
	unsigned long crd[3], /*crd2[3],*/ tul;
	FILE *a;
	unsigned char *uc;
	THilbertPair *hlist;


	if (verbose)
		mprintf("\nCreating Hilbert curve index table...\n");
	else
		mprintf("      Creating Hilbert curve index table (%dx%dx%d)...\n",resx,resy,resz);

	b = (int)(ceil(mylog2(resx))+0.5);
	bt = (int)(ceil(mylog2(resy))+0.5);
	if (bt > b)
		b = bt;
	bt = (int)(ceil(mylog2(resz))+0.5);
	if (bt > b)
		b = bt;
	mi = 2<<(3*b-1);

	if (b > 10) {
		eprintf("CCCEngine::BuildHilbertIdx(): Error: Cube file resolution is too large (%d x %d x %d).\n\n",resx,resy,resz);
		mprintf("Resolutions >= 1024 are not supported by Hilbert curve indexing.\n");
		mprintf("You can disable Hilbert curve indexing by specifying \"-chilbert no\".\n");
		abort();
	}

	resxyz = resx*resy*resz;
	resyz = resy*resz;

	m_iaHilbertIdx.resize(resxyz);

	if (verbose) {
		mprintf("Resolution %d | %d | %d --> Bits %d\n",resx,resy,resz,b);
		mprintf("Requires %lu indices, %.3f MiB of RAM.\n",mi,resxyz*sizeof(int)/1024.0/1024.0);
		mprintf("Running...\n");
	}
	mprintf("        [");
	fflush(stdout);

	uc = new unsigned char[resxyz];
	memset(uc,0,resxyz);

/*
	int ti;
	bitmask_t index, coords[3];
  
	ti = 0;
	for (index=0;index<mi;index++) {
		if ((index%100) == 0)
			if (fmod(index/100,mi/6000.0) < 1.0) {
				mprintf("#");
				fflush(stdout);
			}
		hilbert_i2c(3,b,index,coords);
		if (((long)coords[2] >= resx) || ((long)coords[1] >= resy) || ((long)coords[0] >= resz))
			continue;
		m_iaHilbertIdx[ti] = coords[2]*resyz+coords[1]*resz+coords[0];
		uc[m_iaHilbertIdx[ti]]++;
		ti++;
	}*/

	hlist = new THilbertPair[resxyz];

	for (crd[0]=0;(long)crd[0]<resz;crd[0]++) {
		for (crd[1]=0;(long)crd[1]<resy;crd[1]++) {
			if (fmod(crd[0]*resy+crd[1],resz*resy/60.0) < 1.0) {
				mprintf("#");
				fflush(stdout);
			}
			for (crd[2]=0;(long)crd[2]<resx;crd[2]++) {
				tul = crd[0]+crd[1]*resz+crd[2]*resyz;
				hlist[tul].m_iN = tul;
				hlist[tul].m_iH = hilbert_c2i(3,b,crd);
	//			hilbert_i2c(3,b,hlist[tul].m_iH,crd2);
	//			if ((crd[0] != crd2[0]) || (crd[1] != crd2[1]) || (crd[2] != crd2[2]))
	//				eprintf("Mismatch.\n");
			}
		}
	}

	qsort(hlist,resxyz,sizeof(THilbertPair),CompareHilbertPair);

	for (b=0;b<resxyz;b++) {
		m_iaHilbertIdx[b] = hlist[b].m_iN;
		uc[m_iaHilbertIdx[b]]++;
	}

	delete[] hlist;

	mprintf("]\n");
	if (verbose)
		mprintf("Sanity check...\n");
	for (b=0;b<resxyz;b++) {
		if (uc[b] > 1) {
			eprintf("CCCEngine::BuildHilbertIdx(): Error: Element %d touched multiple times (%u).\n",b,uc[b]);
			eprintf("Hilbert curve index creation failed.\n");
			mprintf("\nYou can disable Hilbert curve indexing by specifying \"-chilbert no\".\n");
			abort();
		}
		if (uc[b] == 0) {
			eprintf("CCCEngine::BuildHilbertIdx(): Error: Element %d never touched.\n",b);
			eprintf("Hilbert curve index creation failed.\n");
			mprintf("\nYou can disable Hilbert curve indexing by specifying \"-chilbert no\".\n");
			abort();
		}
	}
	delete[] uc;
	if (verbose) {
		mprintf("Finished. %lu entries.\n",m_iaHilbertIdx.size());
		mprintf("Dumping Hilbert index to file \"hilbert.csv\"...\n");
		a = OpenFileWrite("hilbert.csv",true);
		mprintf("  [");
		for (b=0;b<resxyz;b++) {
			if ((b%100) == 0)
				if (fmod(b/100,resxyz/6000.0) < 1.0) {
					mprintf("#");
					fflush(stdout);
				}
			fprintf(a,"%d;  %d\n",b,m_iaHilbertIdx[b]);
		}
		mprintf("]\n\n");
		fclose(a);
	}
}


//#define DEBUG_VOL_ELEMENT_FROM 0
//#define DEBUG_VOL_ELEMENT_TO 2


bool CCCEngine::CubeToIntegerArray(
		std::vector<int> &outp,
		int order,
		int eps,
		int signi,
		int esplit,
		bool hilbert,
		double nbhfac,
		bool verbose,
		bool skipcomp,
		double *resi
	) {

	int z, z2, ti, tiov, tiuf, tiuf2, zx, zy, zz, zi, ii, ex, res, mantis, split;
	const CCubeFrame *cfr;
	double tf, tf2, tf3, tfa2;
	int maxsymb;
	bool uf, of;


/*	CDF df;
	if (resi != NULL) {
		df.m_iResolution = 250;
		df.m_fMinVal = 0.0;
		df.m_fMaxVal = 8.0;
		df.Create();
	}*/

	if (verbose)
		mprintf("** CubeToIntegerArray order=%d ***\n",order);

	split = 1<<esplit;

	cfr = m_pReadCacheCube->GetFrameHistory(0);

	if (!skipcomp) // Reserve some space (lower boundary estimate)
		outp.reserve(outp.size()+cfr->m_iResXYZ);
	
	maxsymb = pow10i(signi)-1;

	if (m_pExtrapolator != NULL) {

		if (verbose) {
			mprintf("Extrapolator running...\n");
			mprintf("    [");
		}

		if (m_pExtrapolatorCorr != NULL) {

			m_faTempPred.resize(cfr->m_iResXYZ);
			m_faTempPred2.resize(cfr->m_iResXYZ);

			if (verbose)
				tf = cfr->m_iRes[0] / 60.0;

			zi = 0;
			for (zx=0;zx<cfr->m_iRes[0];zx++) {

				if (verbose)
					if (fmod(zx,tf) < 1.0)
						mprintf("#");

				for (zy=0;zy<cfr->m_iRes[1];zy++) {

					if ((zx >= m_pExtrapolatorCorr->m_iSOffset[0]) && 
						(zx <= m_pExtrapolatorCorr->m_iTempVal[0]) &&
						(zy >= m_pExtrapolatorCorr->m_iSOffset[1]) && 
						(zy <= m_pExtrapolatorCorr->m_iTempVal[1])) {

						for (zz=0;zz<m_pExtrapolatorCorr->m_iSOffset[2];zz++) {
							tf2 = m_pExtrapolator->ExtrapolatePred(zi);
							//m_faTempPred[zi] = ((double)cfr->m_iaMantis[zi]*pow10(cfr->m_iaExpo[zi])) - tf2;
							m_faTempPred[zi] = cfr->m_faBin[zi] - tf2;
							m_faTempPred2[zi] = tf2 + m_pExtrapolatorCorr->ExtrapolateCorr(m_faTempPred,zx,zy,zz,zi);
							zi++;
						}

						for (;zz<=m_pExtrapolatorCorr->m_iTempVal[2];zz++) {
							tf2 = m_pExtrapolator->ExtrapolatePred(zi);
							//m_faTempPred[zi] = ((double)cfr->m_iaMantis[zi]*pow10(cfr->m_iaExpo[zi])) - tf2;
							m_faTempPred[zi] = cfr->m_faBin[zi] - tf2;
							m_faTempPred2[zi] = tf2 + m_pExtrapolatorCorr->ExtrapolateKnownCorr(m_faTempPred,0,zi);
							zi++;
						}

						for (;zz<cfr->m_iRes[2];zz++) {
							tf2 = m_pExtrapolator->ExtrapolatePred(zi);
							//m_faTempPred[zi] = ((double)cfr->m_iaMantis[zi]*pow10(cfr->m_iaExpo[zi])) - tf2;
							m_faTempPred[zi] = cfr->m_faBin[zi] - tf2;
							m_faTempPred2[zi] = tf2 + m_pExtrapolatorCorr->ExtrapolateCorr(m_faTempPred,zx,zy,zz,zi);
							zi++;
						}

					} else {

						for (zz=0;zz<cfr->m_iRes[2];zz++) {
							tf2 = m_pExtrapolator->ExtrapolatePred(zi);
							//m_faTempPred[zi] = ((double)cfr->m_iaMantis[zi]*pow10(cfr->m_iaExpo[zi])) - tf2;
							m_faTempPred[zi] = cfr->m_faBin[zi] - tf2;
							m_faTempPred2[zi] = tf2 + m_pExtrapolatorCorr->ExtrapolateCorr(m_faTempPred,zx,zy,zz,zi);
							zi++;
						}
					}
				}
			}

		} else {

			m_faTempPred2.resize(cfr->m_iResXYZ);

			if (verbose)
				tf = cfr->m_iRes[0] / 60.0;

			zi = 0;
			for (zx=0;zx<cfr->m_iRes[0];zx++) {

				if (verbose)
					if (fmod(zx,tf) < 1.0)
						mprintf("#");

				for (zy=0;zy<cfr->m_iRes[1];zy++) {
					for (zz=0;zz<cfr->m_iRes[2];zz++) {
						m_faTempPred2[zi] = m_pExtrapolator->Extrapolate(zx,zy,zz,zi);
						zi++;
					}
				}
			}

		}


		if (verbose)
			mprintf("] Done.\n");

	} else { // No Extrapolator

		m_faTempPred.resize(cfr->m_iResXYZ);
		m_faTempPred2.resize(cfr->m_iResXYZ);
		for (z=0;z<cfr->m_iResXYZ;z++)
			m_faTempPred[z] = 0;

		if (verbose)
			mprintf("Polynomial extrapolation...\n");

		for (z=1;z<order+1;z++) {
			cfr = m_pReadCacheCube->GetFrameHistory(z);
			tf = 1.0;
			for (z2=1;z2<=order;z2++) { 
				if (z2 == z)
					continue;
				tf *= z2 / ((double)z2 - z);
			}
	//		mprintf("@%d: %.16f\n",z,tf);
			for (z2=0;z2<cfr->m_iResXYZ;z2++)
				m_faTempPred[z2] += tf * cfr->m_iaMantis[z2] * pow10(cfr->m_iaExpo[z2]);
		}

		cfr = m_pReadCacheCube->GetFrameHistory(0);

		if (nbhfac != 0) {

			if (verbose)
				mprintf("Neighborhood extrapolation...\n");

			zi = 0;
			for (zx=0;zx<cfr->m_iRes[0];zx++) {
				for (zy=0;zy<cfr->m_iRes[1];zy++) {
					for (zz=0;zz<cfr->m_iRes[2];zz++) {

						tf2 = 0;
						tf3 = 0;

						if (zz > 0) {
							tf2++;
							tf3 += ((double)cfr->m_iaMantis[zi-1]*pow10(cfr->m_iaExpo[zi-1])) - m_faTempPred[zi-1];
						}

						if (zy > 0) {
							tf2++;
							tf3 += ((double)cfr->m_iaMantis[zi-cfr->m_iRes[2]]*pow10(cfr->m_iaExpo[zi-cfr->m_iRes[2]])) - m_faTempPred[zi-cfr->m_iRes[2]];
						}

						if (zx > 0) {
							tf2++;
							tf3 += ((double)cfr->m_iaMantis[zi-cfr->m_iResYZ]*pow10(cfr->m_iaExpo[zi-cfr->m_iResYZ])) - m_faTempPred[zi-cfr->m_iResYZ];
						}

						if (tf2 != 0)
							tf3 /= tf2 * nbhfac;

						tf3 += m_faTempPred[zi];

						m_faTempPred2[zi] = tf3;

						zi++;
					}
				}
			}
		} else
			m_faTempPred2.assign(m_faTempPred.begin(),m_faTempPred.end());

	} // End No Extrapolator

	if (verbose)
		mprintf("Discretizing and serializing symbols...\n");

	tiov = 0;
	tiuf = 0;
	tiuf2 = 0;
	tfa2 = 0;
	for (zi=0;zi<cfr->m_iResXYZ;zi++) {

		if (hilbert)
			ii = m_iaHilbertIdx[zi];
		else
			ii = zi;

//		if (zi < 10)
//			mprintf("%d --> %d\n",zi,ii);

		if (m_faTempPred2[ii] == 0)
			ex = -eps;
		else
			ex = (int)floor(log10(fabs(m_faTempPred2[ii])))-signi+1;

		if (ex < -eps)
			ex = -eps;

		uf = false;
		of = false;

_again:
		mantis = (int)floor(cfr->m_faBin[ii]*pow10(-ex)+0.5);

		if (floor(m_faTempPred2[ii]*pow10(-ex)+0.5+CR_EPS) != floor(m_faTempPred2[ii]*pow10(-ex)+0.5-CR_EPS)) {
			ti = (int)floor(m_faTempPred2[ii]*pow10(-ex));
	//		mprintf("Sanitized rounding of %f.\n",m_faTempPred2[ii]*pow10(-ex));
		} else
			ti = (int)floor(m_faTempPred2[ii]*pow10(-ex)+0.5);

		res = mantis - ti;

		if (resi != NULL) {
			*resi += sqrt(fabs(res));
			//*resi += mypow(fabs(res),m_fEntropyExpoCube);
			//if (res != 0)
			//	df.AddToBin(log10(fabs(res)));
		}

		if (skipcomp)
			continue;

		if (ExpMantisEqual(cfr->m_iaExpo[ii],cfr->m_iaMantis[ii],ex,mantis) && (abs(res) <= maxsymb)) {
			if (uf)
				outp.push_back(C_UNDERFLOW);
			if (of)
				outp.push_back(C_OVERFLOW);
			if (abs(res) >= split) {
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@> %d: Equal with split (%d): %d = %d & %d\n",ii,split,res,res/split,abs(res)%split);
				#endif
				outp.push_back(C_SPLIT);
				outp.push_back(res/split);
				if (res < 0)
					outp.push_back(-(abs(res)%split));
				else
					outp.push_back(res%split);
			} else {
				outp.push_back(res);
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@> %d: Equal without split: %d\n",ii,res);
				#endif
			}
		} else if ((ex == cfr->m_iaExpo[ii]+1) && (ex > -eps)) {
			tiuf2++;
			ex--;
	//		tia.push_back(maxsymb+2);
			uf = true;
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@> %d: Soft Underflow 1.\n",ii);
			#endif
			goto _again;
		} else if (ex == cfr->m_iaExpo[ii]-1) {
			tiuf2++;
			ex++;
			of = true;
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@> %d: Soft Underflow 2.\n",ii);
			#endif
			goto _again;
		} else if (ex > cfr->m_iaExpo[ii]) {
			tiuf++;
			outp.push_back(C_FULLNUMBER);
			outp.push_back(cfr->m_iaExpo[ii]);
			outp.push_back(cfr->m_iaMantis[ii]/split);
			if (cfr->m_iaMantis[ii] < 0)
				outp.push_back(-(abs(cfr->m_iaMantis[ii])%split));
			else
				outp.push_back(cfr->m_iaMantis[ii]%split);
			if (verbose)
				mprintf("Underflow %d\n",ex-cfr->m_iaExpo[ii]);
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO) && (!verbose))
					mprintf("@> %d: Underflow %d\n",ii,ex-cfr->m_iaExpo[ii]);
			#endif
		} else if (abs(res) > maxsymb) {
			tiov++;
			outp.push_back(C_FULLNUMBER);
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@> %d: @0: %d\n",ii,outp[outp.size()-1]);
			#endif
			outp.push_back(cfr->m_iaExpo[ii]);
			outp.push_back(cfr->m_iaMantis[ii]/split);
			if (cfr->m_iaMantis[ii] < 0)
				outp.push_back(-(abs(cfr->m_iaMantis[ii])%split));
			else
				outp.push_back(cfr->m_iaMantis[ii]%split);
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@> %d: Full Number: %d %d %d\n",ii,cfr->m_iaExpo[ii],cfr->m_iaMantis[ii]/split,abs(cfr->m_iaMantis[ii])%split);
			#endif
		} else {
			if (verbose)
				mprintf("UE: idx=%d, val=%.10f, pred=%.10f, cexpo=%d, pexpo=%d, cmantis=%d, pmantis=%d, res=%d.\n",ii,cfr->m_faBin[ii],m_faTempPred2[ii],cfr->m_iaExpo[ii],ex,cfr->m_iaMantis[ii],mantis,res);
			outp.push_back(C_FULLNUMBER);
			outp.push_back(cfr->m_iaExpo[ii]);
			outp.push_back(cfr->m_iaMantis[ii]/split);
			if (cfr->m_iaMantis[ii] < 0)
				outp.push_back(-(abs(cfr->m_iaMantis[ii])%split));
			else
				outp.push_back(cfr->m_iaMantis[ii]%split);
		}

		#ifdef DEBUG_VOL_ELEMENT_FROM
			if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
				mprintf("@> UE: idx=%6d, val=%.10f, pred=%.22f, cexpo=%d, pexpo=%d, cmantis=%d, pmantis=%d, res=%d.\n",ii,cfr->m_faBin[ii],m_faTempPred2[ii],cfr->m_iaExpo[ii],ex,cfr->m_iaMantis[ii],mantis,res);
		#endif

		tfa2 += res;
	}

	if (verbose) {
		tfa2 /= cfr->m_iResXYZ;
		mprintf("      tfa2 = %8.6f\n",tfa2);
		mprintf("      Overflow: %d, Underflow: %d, Soft Underflow: %d\n",tiov,tiuf,tiuf2);
		mprintf("      Produced %lu symbols.\n",outp.size());
	}

/*	if (resi != 0) {
		df.NormBinIntegral(1000.0);
		CxString buf;
		buf.sprintf("histo%06d.csv",gc_iHistogramCounter++);
		df.Write("",(const char*)buf,"",false);
	}*/

	return true;
}


bool CCCEngine::IntegerArrayToCube(std::vector<int> &inp, int order, int eps, int signi, int esplit, bool hilbert, double nbhfac, int ver, bool verbose, bool second) {

	int z, z2, zx, zy, zz, zi, ii, ex, split;
	CCubeFrame *ofr, *cfr;
	double tf, tf2, tf3;
	std::vector<int> tresi, texpo, tmantis;


	if (verbose)
		mprintf("** IntegerArrayToCube order=%d ***\n",order);
	
	if (ver >= 1)
		split = 1<<esplit;
	else
		split = esplit; // Backward compatibility with bug

	if (second)
		ofr = GetOutput2CubeFrame(0);
	else
		ofr = GetOutputCubeFrame(0);

	m_faTempPred.resize(ofr->m_iResXYZ);

	if (m_pExtrapolator == NULL) {

		for (z=0;z<ofr->m_iResXYZ;z++)
			m_faTempPred[z] = 0;

		if (verbose)
			mprintf("Polynomial extrapolation...\n");

		for (z=1;z<order+1;z++) {
			if (second)
				cfr = GetOutput2CubeFrame(z);
			else
				cfr = GetOutputCubeFrame(z);
			if (cfr == NULL) {
				eprintf("CCCEngine::IntegerArrayToCube(): Error: Frame is of order %d, step history is too short (%d).\n",order,z);
				abort();
			}
			tf = 1.0;
			for (z2=1;z2<=order;z2++) { 
				if (z2 == z)
					continue;
				tf *= z2 / ((double)z2 - z);
			}
			for (z2=0;z2<ofr->m_iResXYZ;z2++)
				m_faTempPred[z2] += tf * cfr->m_iaMantis[z2] * pow10(cfr->m_iaExpo[z2]);
		}

	} else {

		if (m_pExtrapolatorCorr != NULL)
			for (z2=0;z2<ofr->m_iResXYZ;z2++)
				m_faTempPred[z2] = m_pExtrapolator->ExtrapolatePred(z2);
	}

	tresi.resize(ofr->m_iResXYZ);
	texpo.resize(ofr->m_iResXYZ);
	tmantis.resize(ofr->m_iResXYZ);

	if (verbose)
		mprintf("Decoding input...\n");

	zi = 0;
	z = 0;
	while (zi < (int)inp.size()) {
		if (hilbert)
			ii = m_iaHilbertIdx[z];
		else
			ii = z;
		if (inp[zi] == C_FULLNUMBER) { // Full Number
			texpo[ii] = inp[++zi];
			tmantis[ii] = inp[++zi]*split;
			tmantis[ii] += inp[zi+1];
	//		if (inp[zi+1] < 0)
	//			tmantis[ii] = -tmantis[ii];
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@< %d: Full Number: %d %d %d --> %d %d\n",ii,inp[zi-1],inp[zi],inp[zi+1],texpo[ii],tmantis[ii]);
			#endif
			zi++;
		} else if (inp[zi] == C_UNDERFLOW) { // Soft Underflow
			if (inp[++zi] == C_SPLIT) {
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d: Soft Underflow with split: %d & %d = %d\n",ii,inp[zi+1],inp[zi+2],inp[zi+1]*split+inp[zi+2]);
				#endif
				tresi[ii] = inp[++zi]*split;
				tresi[ii] += inp[zi+1];
	//			if (inp[zi+1] < 0)
	//				tresi[ii] = -tresi[ii];
				zi++;
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d:  --> %d\n",ii,tresi[ii]);
				#endif
			} else {
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d: Soft Underflow without split.\n",ii);
				#endif
				tresi[ii] = inp[zi];
			}
			texpo[ii] = -101;
		} else if (inp[zi] == C_OVERFLOW) { // Soft Overflow
			if (inp[++zi] == C_SPLIT) {
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d: Soft Underflow with split: %d & %d = %d\n",ii,inp[zi+1],inp[zi+2],inp[zi+1]*split+inp[zi+2]);
				#endif
				tresi[ii] = inp[++zi]*split;
				tresi[ii] += inp[zi+1];
	//			if (inp[zi+1] < 0)
	//				tresi[ii] = -tresi[ii];
				zi++;
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d:  --> %d\n",ii,tresi[ii]);
				#endif
			} else {
				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d: Soft Underflow without split.\n",ii);
				#endif
				tresi[ii] = inp[zi];
			}
			texpo[ii] = -102;
		} else if (inp[zi] == C_SPLIT) { // Splitted Residue
			tresi[ii] = inp[++zi]*split;
			tresi[ii] += inp[zi+1];
	//		if (inp[zi+1] < 0)
	//			tresi[ii] = -tresi[ii];
			zi++;
			texpo[ii] = -100;
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@< %d: Splitted residue.\n",ii);
			#endif
		} else { // Standard Residue
			tresi[ii] = inp[zi];
			texpo[ii] = -100;
			#ifdef DEBUG_VOL_ELEMENT_FROM
				if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
					mprintf("@< %d: Standard residue.\n",ii);
			#endif
		}
		#ifdef DEBUG_VOL_ELEMENT_FROM
			if ((ii >= DEBUG_VOL_ELEMENT_FROM) && (ii <= DEBUG_VOL_ELEMENT_TO))
				mprintf("@< %d: resi=%d, expo=%d, mantis=%d\n",ii,tresi[ii],texpo[ii],tmantis[ii]);
		#endif
		zi++;
		z++;
	}

	if (verbose) {
		mprintf("Read %d symbols and %d grid points.\n",zi,z);
		mprintf("Neighborhood extrapolation...\n");
	}

	zi = 0;
	for (zx=0;zx<ofr->m_iRes[0];zx++) {
//		mprintf("zx=%d...\n",zx);
		for (zy=0;zy<ofr->m_iRes[1];zy++) {
//			mprintf("zy=%d...\n",zy);
			for (zz=0;zz<ofr->m_iRes[2];zz++) {
//				mprintf("zz=%d...\n",zz);

				if (texpo[zi] > -100) {
					ofr->m_iaMantis[zi] = tmantis[zi];
					ofr->m_iaExpo[zi] = (char)texpo[zi];
					ofr->m_faBin[zi] = ofr->m_iaMantis[zi] * pow10(ofr->m_iaExpo[zi]);
					if (m_pExtrapolatorCorr != NULL) {
						#ifdef DEBUG_VOL_ELEMENT_FROM
							if ((zi >= DEBUG_VOL_ELEMENT_FROM) && (zi <= DEBUG_VOL_ELEMENT_TO))
								mprintf("@< %d: X-Updating TempPred %.10G with true %.10G: %.10G\n",zi,m_faTempPred[zi],ofr->m_faBin[zi],ofr->m_faBin[zi]-m_faTempPred[zi]);
						#endif
						m_faTempPred[zi] = ofr->m_faBin[zi] - m_faTempPred[zi];
					}
					zi++;
					continue;
				}

				if (m_pExtrapolator != NULL) {

					if (m_pExtrapolatorCorr != NULL) {
						tf3 = m_faTempPred[zi] + m_pExtrapolatorCorr->ExtrapolateCorr(m_faTempPred,zx,zy,zz,zi);
						#ifdef DEBUG_VOL_ELEMENT_FROM
							if ((zi >= DEBUG_VOL_ELEMENT_FROM) && (zi <= DEBUG_VOL_ELEMENT_TO))
								mprintf("@< %d: Pred %.10G, Corr %.10G --> %.10G\n",zi,m_faTempPred[zi],tf3-m_faTempPred[zi],tf3);
						#endif
					} else
						tf3 = m_pExtrapolator->Extrapolate(zx,zy,zz,zi);

				} else { // Classical Neighborhood correction

					tf2 = 0;
					tf3 = 0;

					if (nbhfac != 0) {

						if (zz > 0) {
							tf2++;
							tf3 += ((double)ofr->m_iaMantis[zi-1]*pow10(ofr->m_iaExpo[zi-1])) - m_faTempPred[zi-1];
						}

						if (zy > 0) {
							tf2++;
							tf3 += ((double)ofr->m_iaMantis[zi-ofr->m_iRes[2]]*pow10(ofr->m_iaExpo[zi-ofr->m_iRes[2]])) - m_faTempPred[zi-ofr->m_iRes[2]];
						}

						if (zx > 0) {
							tf2++;
							tf3 += ((double)ofr->m_iaMantis[zi-ofr->m_iResYZ]*pow10(ofr->m_iaExpo[zi-ofr->m_iResYZ])) - m_faTempPred[zi-ofr->m_iResYZ];
						}

						if (tf2 != 0)
							tf3 /= tf2 * nbhfac;
					}

					tf3 += m_faTempPred[zi];
				}


				if (tf3 == 0)
					ex = -eps;
				else
					ex = (int)floor(log10(fabs(tf3)))-signi+1;

				if (ex < -eps)
					ex = -eps;

				if (texpo[zi] == -101)
					ex--;
				else if (texpo[zi] == -102)
					ex++;

				if (floor(tf3*pow10(-ex)+0.5+CR_EPS) != floor(tf3*pow10(-ex)+0.5-CR_EPS)) {
					ofr->m_iaMantis[zi] = (int)floor(tf3*pow10(-ex));
	//				mprintf("Sanitized rounding of %f.\n",tf3*pow10(-ex));
				} else
					ofr->m_iaMantis[zi] = (int)floor(tf3*pow10(-ex)+0.5);

				ofr->m_iaMantis[zi] += tresi[zi];
				ofr->m_iaExpo[zi] = (char)ex;
				//while (ofr->m_iaMantis[zi] >= pow10i(signi)) {
				while (abs(ofr->m_iaMantis[zi]) >= pow10i(signi)) {
					#ifdef DEBUG_VOL_ELEMENT_FROM
						if ((zi >= DEBUG_VOL_ELEMENT_FROM) && (zi <= DEBUG_VOL_ELEMENT_TO))
							mprintf("@< %d: A %d %d %d\n",zi,ofr->m_iaMantis[zi],ofr->m_iaExpo[zi],signi);
					#endif
					ofr->m_iaMantis[zi] /= 10;
					ofr->m_iaExpo[zi]++;
				}
				//while ((ofr->m_iaMantis[zi] < pow10i(signi-1)) && (ofr->m_iaExpo[zi] > -eps)) {
				while ((abs(ofr->m_iaMantis[zi]) < pow10i(signi-1)) && (ofr->m_iaExpo[zi] > -eps)) {
					#ifdef DEBUG_VOL_ELEMENT_FROM
						if ((zi >= DEBUG_VOL_ELEMENT_FROM) && (zi <= DEBUG_VOL_ELEMENT_TO))
							mprintf("@< %d: B %d %d %d\n",zi,ofr->m_iaMantis[zi],ofr->m_iaExpo[zi],signi);
					#endif
					ofr->m_iaMantis[zi] *= 10;
					ofr->m_iaExpo[zi]--;
				}
				ofr->m_faBin[zi] = ofr->m_iaMantis[zi] * pow10(ofr->m_iaExpo[zi]);

				if (m_pExtrapolatorCorr != NULL) {
					#ifdef DEBUG_VOL_ELEMENT_FROM
						if ((zi >= DEBUG_VOL_ELEMENT_FROM) && (zi <= DEBUG_VOL_ELEMENT_TO))
							mprintf("@< %d: Updating TempPred %.10G with true %.10G: %.10G\n",zi,m_faTempPred[zi],ofr->m_faBin[zi],ofr->m_faBin[zi]-m_faTempPred[zi]);
					#endif
					m_faTempPred[zi] = ofr->m_faBin[zi] - m_faTempPred[zi];
				}

				#ifdef DEBUG_VOL_ELEMENT_FROM
					if ((zi >= DEBUG_VOL_ELEMENT_FROM) && (zi <= DEBUG_VOL_ELEMENT_TO))
						mprintf("@< %d: Pred=%.22f, expo=%d, predmantis=%d, resi=%d, mantis=%d, result=%.10f\n",zi,tf3,ex,(int)floor(tf3*pow10(-ex)+0.5),tresi[zi],ofr->m_iaMantis[zi],ofr->m_faBin[zi]);
				#endif

				zi++;
			}
		}
	}

	if (verbose)
		mprintf("All done.\n");

	return true;
}


void CCCEngine::ExportCubeHeader(CBitSet *bs, int order, bool verbose) {

	const CCubeFrame *cfr;
	int i;

	if (verbose)
		mprintf("Writing cube header...\n");

	if (order != 0) {
		if (verbose)
			mprintf("Cube header already written before.\n");
		bs->WriteBit(0);
		return;
	} else
		bs->WriteBit(1);

	i = bs->GetLength();

	cfr = m_pReadCacheCube->GetFrameHistory(0);

	bs->WriteBits(cfr->m_iRes[0],10);
	bs->WriteBits(cfr->m_iRes[1],10);
	bs->WriteBits(cfr->m_iRes[2],10);

	bs->WriteSignedBits(cfr->m_iCenter[0],32);
	bs->WriteSignedBits(cfr->m_iCenter[1],32);
	bs->WriteSignedBits(cfr->m_iCenter[2],32);

	bs->WriteSignedBits(cfr->m_iStrideA[0],32);
	bs->WriteSignedBits(cfr->m_iStrideA[1],32);
	bs->WriteSignedBits(cfr->m_iStrideA[2],32);

	bs->WriteSignedBits(cfr->m_iStrideB[0],32);
	bs->WriteSignedBits(cfr->m_iStrideB[1],32);
	bs->WriteSignedBits(cfr->m_iStrideB[2],32);

	bs->WriteSignedBits(cfr->m_iStrideC[0],32);
	bs->WriteSignedBits(cfr->m_iStrideC[1],32);
	bs->WriteSignedBits(cfr->m_iStrideC[2],32);

	if (verbose)
		mprintf("%d bytes written.\n",(bs->GetLength()-i)/8);
}


void CCCEngine::ImportCubeHeader(CBitSet *bs, bool verbose, bool second) {

	CCubeFrame *ofr, *ofr2;
	int z, i;

	if (second)
		ofr = GetOutput2CubeFrame(0);
	else
		ofr = GetOutputCubeFrame(0);

	if (verbose)
		mprintf("Reading cube header...\n");

	i = bs->GetReadPos();

	if (!bs->ReadBit()) { // Header already found in previous frame
		if (second)
			ofr2 = GetOutput2CubeFrame(1);
		else
			ofr2 = GetOutputCubeFrame(1);
		if (ofr2 == NULL) {
			mprintf("CEngine::ImportCubeHeader(): Error: First frame does not contain header.\n");
			return;
		}
		if (verbose)
			mprintf("Cube header already read before.\n");
		for (z=0;z<3;z++) {
			ofr->m_iRes[z] = ofr2->m_iRes[z];
			ofr->m_iCenter[z] = ofr2->m_iCenter[z];
			ofr->m_iStrideA[z] = ofr2->m_iStrideA[z];
			ofr->m_iStrideB[z] = ofr2->m_iStrideB[z];
			ofr->m_iStrideC[z] = ofr2->m_iStrideC[z];
			ofr->m_fCenter[z] = ofr2->m_fCenter[z];
			ofr->m_fStrideA[z] = ofr2->m_fStrideA[z];
			ofr->m_fStrideB[z] = ofr2->m_fStrideB[z];
			ofr->m_fStrideC[z] = ofr2->m_fStrideC[z];
		}
	} else { // Header here

		for (z=0;z<3;z++)
			ofr->m_iRes[z] = bs->ReadBitsInteger(10);

		for (z=0;z<3;z++) {
			ofr->m_iCenter[z] = bs->ReadBitsSignedInteger(32);
			ofr->m_fCenter[z] = FixedToFloat(ofr->m_iCenter[z],6);
		}

		for (z=0;z<3;z++) {
			ofr->m_iStrideA[z] = bs->ReadBitsSignedInteger(32);
			ofr->m_fStrideA[z] = FixedToFloat(ofr->m_iStrideA[z],6);
		}

		for (z=0;z<3;z++) {
			ofr->m_iStrideB[z] = bs->ReadBitsSignedInteger(32);
			ofr->m_fStrideB[z] = FixedToFloat(ofr->m_iStrideB[z],6);
		}

		for (z=0;z<3;z++) {
			ofr->m_iStrideC[z] = bs->ReadBitsSignedInteger(32);
			ofr->m_fStrideC[z] = FixedToFloat(ofr->m_iStrideC[z],6);
		}
	}

	ofr->m_fMinVal[0] = ofr->m_fCenter[0];
	ofr->m_fMaxVal[0] = ofr->m_fCenter[0] + ofr->m_fStrideA[0] * ofr->m_iRes[0];
	ofr->m_fMinVal[1] = ofr->m_fCenter[1];
	ofr->m_fMaxVal[1] = ofr->m_fCenter[1] + ofr->m_fStrideB[1] * ofr->m_iRes[1];
	ofr->m_fMinVal[2] = ofr->m_fCenter[2];
	ofr->m_fMaxVal[2] = ofr->m_fCenter[2] + ofr->m_fStrideC[2] * ofr->m_iRes[2];

	if (verbose) {
		mprintf("Resolution: %d x %d x %d\n",ofr->m_iRes[0],ofr->m_iRes[1],ofr->m_iRes[2]);
		mprintf("Center: %f | %f | %f\n",ofr->m_fCenter[0],ofr->m_fCenter[1],ofr->m_fCenter[2]);
		mprintf("Stride A: %f | %f | %f\n",ofr->m_fStrideA[0],ofr->m_fStrideA[1],ofr->m_fStrideA[2]);
		mprintf("Stride B: %f | %f | %f\n",ofr->m_fStrideB[0],ofr->m_fStrideB[1],ofr->m_fStrideB[2]);
		mprintf("Stride C: %f | %f | %f\n",ofr->m_fStrideC[0],ofr->m_fStrideC[1],ofr->m_fStrideC[2]);
		mprintf("Range: X %f - %f, Y %f - %f, Z %f - %f\n",ofr->m_fMinVal[0],ofr->m_fMaxVal[0],ofr->m_fMinVal[1],ofr->m_fMaxVal[1],ofr->m_fMinVal[2],ofr->m_fMaxVal[2]);
		mprintf("%d bytes read.\n",(bs->GetReadPos()-i)/8);
	}

	ofr->m_iResXY = ofr->m_iRes[0] * ofr->m_iRes[1];
	ofr->m_iResYZ = ofr->m_iRes[1] * ofr->m_iRes[2];
	ofr->m_iResXYZ = ofr->m_iRes[0] * ofr->m_iRes[1] * ofr->m_iRes[2];

	ofr->m_faBin.resize(ofr->m_iResXYZ);
	ofr->m_iaExpo.resize(ofr->m_iResXYZ);
	ofr->m_iaMantis.resize(ofr->m_iResXYZ);
}


//#define DEBUG_POS_ELEMENT 0


void CCCEngine::AtomsToIntegerArray(
		std::vector<int> &outp,
		int order,
		int asigni,
		int esplit,
		bool verbose,
		bool skipcomp,
		double *resi
	) {

	int z, z2, i, zc, zi;
	long mantis, ti, res;
	int split;
	double tf;
	const CAtomSet *cfr;


	if (verbose)
		mprintf("\n*** AtomsToIntegerArray order=%d ***\n",order);

	i = (int)outp.size();

	if (m_pReadCacheCube != NULL)
		cfr = m_pReadCacheCube->GetFrameHistory(0)->m_pAtoms;
	else
		cfr = m_pReadCacheXYZ->GetFrameHistory(0);

	if (verbose)
		mprintf("Processing %lu atoms...\n",cfr->m_oaAtoms.size());

	split = 1<<esplit;


/****** Version with X, Y, Z separated ************************************************/

	std::vector<double> tpred;
	double compred;

	tpred.resize(cfr->m_oaAtoms.size());

	for (zc=0;zc<3;zc++) {

		if (verbose)
			mprintf("*** Now coordinate %d/3...\n",zc+1);

		if (m_pExtrapolatorXYZ != NULL) {

			if (verbose)
				mprintf("Extrapolator...\n");

			for (z=0;z<(int)cfr->m_oaAtoms.size();z++)
				tpred[z] = m_pExtrapolatorXYZ->ExtrapolateXYZ(m_iaAtomSort[z],zc);

			compred = m_pExtrapolatorXYZ->ExtrapolateXYZ((int)cfr->m_oaAtoms.size(),zc);

		} else {
	
			if (verbose)
				mprintf("Polynomial extrapolation...\n");

			for (z=0;z<(int)cfr->m_oaAtoms.size();z++)
				tpred[z] = 0;
			compred = 0;

			for (z=1;z<order+1;z++) {

				if (m_pReadCacheCube != NULL)
					cfr = m_pReadCacheCube->GetFrameHistory(z)->m_pAtoms;
				else
					cfr = m_pReadCacheXYZ->GetFrameHistory(z);

				tf = 1.0;
				for (z2=1;z2<=order;z2++) { 
					if (z2 == z)
						continue;
					tf *= z2 / ((double)z2 - z);
				}
				for (z2=0;z2<(int)cfr->m_oaAtoms.size();z2++)
					tpred[z2] += tf * cfr->m_oaAtoms[m_iaAtomSort[z2]]->m_fRelCoord[zc];

				compred += tf * cfr->m_faCOM[zc];
			}
		}

		if (m_pReadCacheCube != NULL)
			cfr = m_pReadCacheCube->GetFrameHistory(0)->m_pAtoms;
		else
			cfr = m_pReadCacheXYZ->GetFrameHistory(0);

		if (verbose)
			mprintf("Discretizing and serializing symbols...\n");

		// COM component
		mantis = cfr->m_iaCOM[zc];
		if (floor(compred*pow10(asigni)+0.5+AR_EPS) != floor(compred*pow10(asigni)+0.5-AR_EPS)) {
			ti = (int)floor(compred*pow10(asigni));
//			mprintf("1A) Sanitized rounding of %f.\n",compred*pow10(asigni));
		} else
			ti = (int)floor(compred*pow10(asigni)+0.5);

		res = mantis - ti;

		#ifdef DEBUG_POS_ELEMENT
			mprintf("@> CoM zc=%d, true %.6f, pred %.6f, mantis %ld, ti %ld, res %ld.\n",
				zc,cfr->m_faCOM[zc],compred,mantis,ti,res);
		#endif

		if (resi != NULL)
			*resi += pow2((double)res);
			//*resi += mypow(fabs(res),m_fEntropyExpoXYZ);

		if (!skipcomp) {

			if (abs(res) >= split) {
				outp.push_back(C_SPLIT);
				outp.push_back(res/split);
				if (res < 0)
					outp.push_back(-(abs(res)%split));
				else
					outp.push_back(res%split);
			} else {
				outp.push_back(res);
			}
		}

		for (zi=0;zi<(int)cfr->m_oaAtoms.size();zi++) {

			mantis = cfr->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc];

			if (floor(tpred[zi]*pow10(asigni)+0.5+AR_EPS) != floor(tpred[zi]*pow10(asigni)+0.5-AR_EPS)) {
				ti = (int)floor(tpred[zi]*pow10(asigni));
//				mprintf("1B) %3d Sanitized rounding of %f.\n",zi,tpred[zi]*pow10(asigni));
			} else
				ti = (int)floor(tpred[zi]*pow10(asigni)+0.5);

			res = mantis - ti;

			#ifdef DEBUG_POS_ELEMENT
				if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
					mprintf("@> A%d zc=%d, true %.6f, pred %.6f, mantis %ld, ti %ld, res %ld.\n",
						m_iaAtomSort[zi],zc,cfr->m_oaAtoms[m_iaAtomSort[zi]]->m_fRelCoord[zc],tpred[zi],mantis,ti,res);
			#endif

			if (resi != NULL)
				*resi += pow2((double)res);
				//*resi += mypow(fabs(res),m_fEntropyExpoXYZ);

			if (skipcomp)
				continue;

			if (abs(res) >= split) {
				outp.push_back(C_SPLIT);
				outp.push_back(res/split);
				if (res < 0) {
					outp.push_back(-(abs(res)%split));
					#ifdef DEBUG_POS_ELEMENT
						if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
							mprintf("@> A%d zc=%d, Negative Split(%d): %ld | %ld = %ld.\n",
								m_iaAtomSort[zi],zc,split,res/split,-(abs(res)%split),res);
					#endif
				} else {
					outp.push_back(res%split);
					#ifdef DEBUG_POS_ELEMENT
						if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
							mprintf("@> A%d zc=%d, Positive Split(%d): %ld | %ld = %ld.\n",
								m_iaAtomSort[zi],zc,split,res/split,res%split,res);
					#endif
				}

			} else {
				outp.push_back(res);
				#ifdef DEBUG_POS_ELEMENT
					if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
						mprintf("@> A%d zc=%d, No Split: %ld.\n",
							m_iaAtomSort[zi],zc,res);
				#endif
			}
		}
	}

/***************************************************************************************************/


/****** Version with X, Y, Z combined ************************************************

	std::vector<double> tpred[3];
	double compred[3];

	for (zc=0;zc<3;zc++)
		tpred[zc].resize(cfr->m_oaAtoms.size());

	if (m_pExtrapolatorXYZ != NULL) {

		if (verbose)
			mprintf("Extrapolator...\n");

		for (zc=0;zc<3;zc++) {

			for (z=0;z<(int)cfr->m_oaAtoms.size();z++)
				tpred[zc][z] = m_pExtrapolatorXYZ->ExtrapolateXYZ(m_iaAtomSort[z],zc);

			compred[zc] = m_pExtrapolatorXYZ->ExtrapolateXYZ(cfr->m_oaAtoms.size(),zc);
		}

	} else {

		if (verbose)
			mprintf("Polynomial extrapolation...\n");

		for (zc=0;zc<3;zc++) {
			for (z=0;z<(int)cfr->m_oaAtoms.size();z++)
				tpred[zc][z] = 0;
			compred[zc] = 0;
		}

		for (z=1;z<order+1;z++) {

			if (m_pReadCacheCube != NULL)
				cfr = m_pReadCacheCube->GetFrameHistory(z)->m_pAtoms;
			else
				cfr = m_pReadCacheXYZ->GetFrameHistory(z);

			tf = 1.0;
			for (z2=1;z2<=order;z2++) { 
				if (z2 == z)
					continue;
				tf *= z2 / ((double)z2 - z);
			}

			for (zc=0;zc<3;zc++) {
				for (z2=0;z2<(int)cfr->m_oaAtoms.size();z2++)
					tpred[zc][z2] += tf * cfr->m_oaAtoms[m_iaAtomSort[z2]]->m_fRelCoord[zc];
				compred[zc] += tf * cfr->m_faCOM[zc];
			}
		}
	}

	if (m_pReadCacheCube != NULL)
		cfr = m_pReadCacheCube->GetFrameHistory(0)->m_pAtoms;
	else
		cfr = m_pReadCacheXYZ->GetFrameHistory(0);

	if (verbose)
		mprintf("Discretizing and serializing symbols...\n");

	// COM component
	for (zc=0;zc<3;zc++) {

		mantis = cfr->m_iaCOM[zc];
		if (floor(compred[zc]*pow10(asigni)+0.5+AR_EPS) != floor(compred[zc]*pow10(asigni)+0.5-AR_EPS)) {
			ti = (int)floor(compred[zc]*pow10(asigni));
	//			mprintf("1A) Sanitized rounding of %f.\n",compred*pow10(asigni));
		} else
			ti = (int)floor(compred[zc]*pow10(asigni)+0.5);

		res = mantis - ti;

		if (resi != NULL)
			*resi += mypow(fabs(res),m_fEntropyExpoXYZ);

		if (!skipcomp) {

			if (abs(res) >= split) {
				outp.push_back(C_SPLIT);
				outp.push_back(res/split);
				if (res < 0)
					outp.push_back(-(abs(res)%split));
				else
					outp.push_back(res%split);
			} else {
				outp.push_back(res);
			}
		}
	}

	for (zi=0;zi<(int)cfr->m_oaAtoms.size();zi++) {

		for (zc=0;zc<3;zc++) {

			mantis = cfr->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc];

			if (floor(tpred[zc][zi]*pow10(asigni)+0.5+AR_EPS) != floor(tpred[zc][zi]*pow10(asigni)+0.5-AR_EPS)) {
				ti = (int)floor(tpred[zc][zi]*pow10(asigni));
	//				mprintf("1B) %3d Sanitized rounding of %f.\n",zi,tpred[zi]*pow10(asigni));
			} else
				ti = (int)floor(tpred[zc][zi]*pow10(asigni)+0.5);

			res = mantis - ti;

			if (resi != NULL)
				*resi += mypow(fabs(res),m_fEntropyExpoXYZ);

			if (skipcomp)
				continue;

			if (abs(res) >= split) {
				outp.push_back(C_SPLIT);
				outp.push_back(res/split);
				if (res < 0) 
					outp.push_back(-(abs(res)%split));
				else
					outp.push_back(res%split);

			} else
				outp.push_back(res);
		}
	}

***************************************************************************************************/

	if (verbose)
		mprintf("*** AtomsToIntegerArray finished, %lu symbols written ***\n\n",outp.size()-i);
}


void CCCEngine::IntegerArrayToAtoms(const std::vector<int> &inp, int order, int asigni, int esplit, bool verbose, bool second) {

	int z, z2, i=0, zc, zi, ii;
	std::vector<double> tpred;
	double compred;
	long ti;
	int split;
	double tf;
	CAtomSet *ofr, *ofr0;


	ii = 0;

	if (verbose) {
		mprintf("\n*** IntegerArrayToAtoms ***\n");
		i = ii;
	}

	split = 1<<esplit;

	if (verbose)
		mprintf("Order=%d, asigni=%d, esplit=%d, split=%d\n",order,asigni,esplit,split);

	if (second)
		ofr0 = GetOutput2AtomFrame(0);
	else
		ofr0 = GetOutputAtomFrame(0);

	tpred.resize(ofr0->m_oaAtoms.size());

	for (zc=0;zc<3;zc++) {

		if (verbose)
			mprintf("*** Now coordinate %d/3...\n",zc+1);
	
		if (m_pExtrapolatorXYZ != NULL) {

			if (verbose)
				mprintf("Extrapolator...\n");

			for (z=0;z<(int)ofr0->m_oaAtoms.size();z++)
				tpred[z] = m_pExtrapolatorXYZ->ExtrapolateXYZ(m_iaAtomSort[z],zc);

			compred = m_pExtrapolatorXYZ->ExtrapolateXYZ((int)ofr0->m_oaAtoms.size(),zc);

		} else {

			if (verbose)
				mprintf("Polynomial extrapolation...\n");

			for (z=0;z<(int)ofr0->m_oaAtoms.size();z++)
				tpred[z] = 0;
			compred = 0;

			for (z=1;z<order+1;z++) {
				if (second)
					ofr = GetOutput2AtomFrame(z);
				else
					ofr = GetOutputAtomFrame(z);
				if (ofr == NULL) {
					eprintf("CCCEngine::IntegerArrayToAtoms(): Error: Frame is of order %d, step history is too short (%d).\n",order,z);
					abort();
				}
				tf = 1.0;
				for (z2=1;z2<=order;z2++) { 
					if (z2 == z)
						continue;
					tf *= z2 / ((double)z2 - z);
				}
		//		if (zc == 2)
		//			mprintf("@%d: %f\n",z,tf);
				for (z2=0;z2<(int)ofr0->m_oaAtoms.size();z2++)
					tpred[z2] += tf * ofr->m_oaAtoms[m_iaAtomSort[z2]]->m_fRelCoord[zc];
		//		if (zc == 2)
		//			mprintf("   + %.2f * %f\n",tf,ofr->m_oaAtoms[0]->m_fRelCoord[zc]);
				compred += tf * ofr->m_faCOM[zc];
			}
		//	if (zc == 2)
		//		mprintf("   = %f\n",tpred[0]);
		}

		if (second)
			ofr = GetOutput2AtomFrame(0);
		else
			ofr = GetOutputAtomFrame(0);

		if (verbose)
			mprintf("Decoding symbols...\n");

		// COM component
		if (floor(compred*pow10(asigni)+0.5+AR_EPS) != floor(compred*pow10(asigni)+0.5-AR_EPS)) {
			ti = (int)floor(compred*pow10(asigni));
//			mprintf("2A) Sanitized rounding of %f.\n",compred*pow10(asigni));
		} else
			ti = (int)floor(compred*pow10(asigni)+0.5);
		if (inp[ii] == C_SPLIT) {
			ii++;
			ofr0->m_iaCOM[zc] = ti + inp[ii++] * split;
			ofr0->m_iaCOM[zc] += inp[ii++];
		} else
			ofr0->m_iaCOM[zc] = ti + inp[ii++];
		ofr0->m_faCOM[zc] = FixedToFloat(ofr0->m_iaCOM[zc],asigni);

		#ifdef DEBUG_POS_ELEMENT
			mprintf("@< CoM zc=%d, pred %.6f, mantis %d, ti %ld, true %.6f.\n",zc,compred,ofr0->m_iaCOM[zc],ti,ofr0->m_faCOM[zc]);
		#endif

//		if ((order > 2) && (zc == 2))
//			mprintf("COM = %f\n",ofr0->m_faCOM[zc]);

//		if (zc == 2)
//			mprintf("COM[%d]: %.6f (%ld).\n",zc,ofr0->m_faCOM[zc],ofr0->m_iaCOM[zc]);

		for (zi=0;zi<(int)ofr0->m_oaAtoms.size();zi++) {

			if (floor(tpred[zi]*pow10(asigni)+0.5+AR_EPS) != floor(tpred[zi]*pow10(asigni)+0.5-AR_EPS)) {
				ti = (int)floor(tpred[zi]*pow10(asigni));
//				mprintf("2B) %3d Sanitized rounding of %f.\n",zi,tpred[zi]*pow10(asigni));
			} else
				ti = (int)floor(tpred[zi]*pow10(asigni)+0.5);

			if (inp[ii] == C_SPLIT) {
				ii++;
				ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc] = ti + inp[ii++] * split;
				ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc] += inp[ii++];
	//			if ((zc==2) && (m_iaAtomSort[zi] == 92))
	//				mprintf("pop %d %d --> %d\n",inp[ii-2],inp[ii-1],inp[ii-2]*split+inp[ii-1]);
				#ifdef DEBUG_POS_ELEMENT
					if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
						mprintf("@< A%d Split(%d): %ld + %d*%d + %d = %ld.\n",
							m_iaAtomSort[zi],split,ti,inp[ii-2],split,inp[ii-1],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc]);
				#endif
			} else {
				ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc] = ti + inp[ii++];
				#ifdef DEBUG_POS_ELEMENT
					if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
						mprintf("@< A%d No Split: %ld + %d = %ld.\n",
							m_iaAtomSort[zi],ti,inp[ii-1],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc]);
				#endif
	//			if ((zc==2) && (m_iaAtomSort[zi] == 92))
	//				mprintf("pop %d\n",inp[ii-1]);
			}

			ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iCoord[zc] = ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc] + ofr0->m_iaCOM[zc];

			ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_fRelCoord[zc] = FixedToFloat(ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc],asigni);
			ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_fCoord[zc] = FixedToFloat(ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iCoord[zc],asigni);

			#ifdef DEBUG_POS_ELEMENT
				if (m_iaAtomSort[zi] == DEBUG_POS_ELEMENT)
					mprintf("@< A%d zc=%d, pred %.6f, mantis %ld, ti %ld, true %.6f.\n",
						m_iaAtomSort[zi],zc,tpred[zi],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc],ti,ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_fRelCoord[zc]);
			#endif

	//		if ((zc==2) && (m_iaAtomSort[zi] == 92)) {
	//			mprintf("Atom %d[%d]: fa=%.6f, ia=%ld, fr=%.6f, ir=%ld.\n",m_iaAtomSort[zi],zc,ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_fCoord[zc],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iCoord[zc],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_fRelCoord[zc],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc]);
	//			mprintf("dec %2d: pred %d, mantis %d, relcoord %f, res %d\n",m_iaAtomSort[zi],ti,ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_iRelCoord[zc],ofr0->m_oaAtoms[m_iaAtomSort[zi]]->m_fRelCoord[zc],inp[ii-1]);
	//		}
		}
	}

	if (verbose)
		mprintf("*** IntegerArrayToAtoms finished, %d symbols read ***\n\n",ii-i);
}



void CCCEngine::CompressString(const char *s, CBitSet *bs, bool verbose) {

	CIntegerEngine ie;
	std::vector<int> ia;
	int z, mi, ma, bits, co;
	CBitSet bs2;
	unsigned long tpos;

	tpos = bs->GetLength();

	if (verbose)
		mprintf(">>> CompressString >>>\n");

	for (z=0;z<(int)strlen(s);z++)
		ia.push_back(s[z]);

	mi = 255;
	ma = 0;
	for (z=0;z<(int)ia.size();z++) {
		if (ia[z] > ma)
			ma = ia[z];
		if (ia[z] < mi)
			mi = ia[z];
	}
	if (ia.size() == 0) {
		mi = 0;
		ma = 0;
	}
	bits = (int)ceil(mylog2(ma-mi+1));
	co = (int)ceil((bits*ia.size()+4.0)/8.0)+2;

	if (verbose)
		mprintf("CompressString: Trying conventional storage, length %lu (%lu), range %d - %d, %d bits.\n",strlen(s),ia.size(),mi,ma,bits);

	PushStatistics();

	if (ia.size() > 1)
		ie.CompressSingle(ia,&bs2,true,true,true,50,1,false,true,false,10,verbose);

	if (verbose)
		mprintf("CompressString: Source %lu Bytes, IntegerEngine %d Bytes, Conventional %d Bytes.\n",ia.size(),bs2.GetByteLength(),co);

	if ((ia.size() <= 1) || ((co <= bs2.GetByteLength()) && (ia.size() < 256))) {
		if (verbose)
			mprintf("--> Using conventional storage.\n");
		bs->WriteBit(0);
		bs->WriteBits((unsigned long)strlen(s),8);
		bs->WriteBits(mi,8);
		bs->WriteBits(bits,4);
		for (z=0;z<(int)ia.size();z++)
			bs->WriteBits(ia[z]-mi,bits);
	} else {
		if (verbose)
			mprintf("--> Using IntegerEngine storage.\n");
		bs->WriteBit(1);
		bs->WriteBits(&bs2);
	}

	PopStatistics();
	gc_oStatistics.m_lOverhead += bs->GetLength() - tpos;

	if (verbose)
		mprintf("<<< CompressString <<<\n");
}


void CCCEngine::DecompressString(CBitSet *bs, char **s, bool verbose) {

	CIntegerEngine ie;
	std::vector<int> ia;
	int z, mi, c, bits;

	if (verbose)
		mprintf(">>> DecompressString >>>\n");

	if (!bs->ReadBit()) {

		if (verbose)
			mprintf("Classical storage.\n");

		c = bs->ReadBitsInteger(8);
		mi = bs->ReadBitsInteger(8);
		bits = bs->ReadBitsInteger(4);

		for (z=0;z<c;z++)
			ia.push_back(mi+bs->ReadBitsInteger(bits));

	} else {

		if (verbose)
			mprintf("IntegerEngine was used.\n");

		ie.DecompressSingle(bs,ia,verbose);
	}

	*s = new char[ia.size()+1];

	for (z=0;z<(int)ia.size();z++)
		(*s)[z] = (char)ia[z];

	(*s)[z] = 0;

	if (verbose)
		mprintf("<<< DecompressString <<<\n");
}


bool CCCEngine::CompressCubeFrame(
		CBitSet *bs,
		int corder,
		int eps,
		int csigni,
		int csplit,
		int cblock,
		int ctables,
		bool opttables,
		bool hilbert,
		double nbhfac,
		bool coderun,
		bool bw,
		bool mtf,
		bool preopt,
		int maxiter,
		bool atominfo,
		int maxchunk,
		bool verbose,
		bool skipcomp,
		double *resi,
		std::vector<std::vector<int> > *tciaa
	) {

	UNUSED(atominfo);

	CIntegerEngine ie;
	const CCubeFrame *cfr;
	unsigned long tpos;


	if (resi != NULL)
		*resi = 0.0;

	m_iaTempCompressCube.clear();

	cfr = m_pReadCacheCube->GetFrameHistory(0);

	if (hilbert && ((int)m_iaHilbertIdx.size() != cfr->m_iResXYZ))
		BuildHilbertIdx(cfr->m_iRes[0],cfr->m_iRes[1],cfr->m_iRes[2],verbose);

	if (tciaa != NULL) {
		tciaa->push_back(std::vector<int>());
		CubeToIntegerArray(tciaa->back(),corder,eps,csigni,csplit,hilbert,nbhfac,verbose,skipcomp,resi);
	} else
		CubeToIntegerArray(m_iaTempCompressCube,corder,eps,csigni,csplit,hilbert,nbhfac,verbose,skipcomp,resi);

	if (skipcomp || (tciaa != NULL))
		return true;

	tpos = bs->GetLength();

	bs->WriteBits(corder,4);
	bs->WriteBits(csigni,4);
	bs->WriteBits(eps,6);
	bs->WriteBits(csplit,6);
	bs->WriteBit(hilbert?1:0);

	// Additional fields in version 1
	if (m_pExtrapolator != NULL) {
		bs->WriteBit(1);
		ExportExtrapolatorSettings(bs,true,verbose);
	} else {
		bs->WriteBit(0);
		bs->WriteBits((unsigned long)floor(nbhfac*1000.0+0.5),11);
	}

	bs->WriteBit(0); // Reserved for future use, must be 0
	// End Additional fields

	ExportCubeHeader(bs,corder,verbose);

	gc_oStatistics.m_lOverhead += bs->GetLength() - tpos;

	ie.Compress(m_iaTempCompressCube,bs,bw,mtf,coderun,cblock,ctables,opttables,false,preopt,maxiter,maxchunk,verbose);

	return true;
}


bool CCCEngine::DecompressCubeFrame(CBitSet *bs, int ver, bool verbose, bool second, bool check) {

	CIntegerEngine ie;
	int tco, tcsigni, teps, tcsplit;
	double nbhfac;
	CCubeFrame *cfr;
	bool hilbert;


	nbhfac = 0;

	if (ver > 1) {
		eprintf("CCCEngine::DecompressCubeFrame(): Unknown Atom Frame Version: %d.\n",ver);
		mprintf("Either the BQB file is damaged, or was written with a more recent software version.\n\n");
		return false;
	}

	m_iaTempCompressCube.clear();

	tco = bs->ReadBitsInteger(4);
	tcsigni = bs->ReadBitsInteger(4);
	teps = bs->ReadBitsInteger(6);
	tcsplit = bs->ReadBitsInteger(6);
	hilbert = bs->ReadBit();

	// Additional fields in version 1
	if (ver >= 1) {
		if (bs->ReadBit())
			ImportExtrapolatorSettings(bs,true,verbose);
		else
			nbhfac = bs->ReadBitsInteger(11) / 1000.0;

		if (bs->ReadBit()) {
			eprintf("CCCEngine::DecompressCubeFrame(): Reserved bit expected to be 0, but was 1.\n");
			mprintf("Either the BQB file is damaged, or was written with a more recent software version.\n\n");
			return false;
		}
	} else
		nbhfac = 1.075;
	// End Additional fields

	if (second)
		cfr = GetOutput2CubeFrame(0);
	else
		cfr = GetOutputCubeFrame(0);

	cfr->m_iEps = teps;
	cfr->m_iSigni = tcsigni;

	ImportCubeHeader(bs,verbose,second);

	if (hilbert && ((int)m_iaHilbertIdx.size() != cfr->m_iResXYZ))
		BuildHilbertIdx(cfr->m_iRes[0],cfr->m_iRes[1],cfr->m_iRes[2],verbose);

	if (!check && (m_pExtrapolator != NULL))
		m_pExtrapolator->PushCubeFrame(cfr);

	ie.Decompress(bs,m_iaTempCompressCube,verbose);

	IntegerArrayToCube(m_iaTempCompressCube,tco,teps,tcsigni,tcsplit,hilbert,nbhfac,ver,verbose,second);

	return true;
}


void CCCEngine::ExportAtomLabels(const CAtomSet *as, CBitSet *bs, bool verbose) {

	std::vector<int> ia;
	char tch[5001];
	std::string tst;
	int z, z2, zt, rem;


	if ((as->m_oaAtoms.size()%5000) == 0)
		zt = (int)as->m_oaAtoms.size()/5000;
	else
		zt = (int)as->m_oaAtoms.size()/5000 + 1;

	if (zt > 255) {
		eprintf("CCCEngine::ExportAtomLabels(): Error: More than 1'275'000 atoms are not supported (%d).\n",zt*5000);
		abort();
	}

	bs->WriteBits(zt,8);

	for (z=0;z<zt;z++) {
	
		rem = (int)as->m_oaAtoms.size() - z*5000;
		if (rem > 5000)
			rem = 5000;

		tst = "";
		for (z2=0;z2<rem;z2++) {
			tch[z2] = (char)as->m_oaAtoms[z*5000+z2]->m_sLabel.length();
			tst += as->m_oaAtoms[z*5000+z2]->m_sLabel;
			if (as->m_oaAtoms[z*5000+z2]->m_sLabel.length() > 127) {
				eprintf("CCCEngine::ExportAtomLabels(): Error: Labels longer than 127 characters are not supported (%ld).\n",as->m_oaAtoms[z*5000+z2]->m_sLabel.length());
				abort();
			}
		}
		tch[rem] = 0;

		CompressString(tch,bs,verbose);
		CompressString(tst.c_str(),bs,verbose);
	}
}


void CCCEngine::ImportAtomLabels(CAtomSet *as, CBitSet *bs, bool verbose) {

	int z, z2, zt, i;
	char *tch, *tch2, *p, buf[128];
	unsigned char tuc;


	zt = bs->ReadBitsInteger(8);

	i = 0;
	for (z=0;z<zt;z++) {

		DecompressString(bs,&tch,verbose);
		DecompressString(bs,&tch2,verbose);

		z2 = 0;
		p = tch2;
		while (tch[z2] != 0) {
			if (tch[z2] < 0) {
				eprintf("CCCEngine::ImportAtomLabels(): Error: Label length < 0 (%d).\n",tch[z2]);
				abort();
			}
			tuc = (unsigned char)tch[z2];
			memcpy(buf,p,tuc);
			buf[tuc] = 0;
			as->m_oaAtoms[i]->m_sLabel = buf;
			p += tuc;
			z2++;
			i++;
		}

		delete[] tch;
		delete[] tch2;
	}

	if (i != (int)as->m_oaAtoms.size()) {
		eprintf("CCCEngine::ImportAtomLabels(): Error: Expected %lu atom labels, but found %d (%d).\n",as->m_oaAtoms.size(),i,zt);
		abort();
	}
}


bool CCCEngine::CompressAtomFrame(
		CBitSet *bs,
		int order,
		int signi, 
		int split,
		int block,
		int tables,
		bool opttables,
		bool coderun,
		bool bw,
		bool mtf,
		bool preopt,
		int maxiter,
		bool sortatom,
		bool atominfo,
		bool comment,
		int maxchunk,
		bool verbose,
		bool skipcomp,
		double *resi,
		std::vector<std::vector<int> > *tciaa
	) {

	CIntegerEngine ie;
	//std::vector<int> ia;
	std::string ts;
	int bits, z;
	const CAtomSet *as;
	unsigned long tpos;


/*	mprintf("@ CompressAtomFrame\n");
	mprintf("@ order %d\n",order);
	mprintf("@ signi %d\n",signi);
	mprintf("@ split %d\n",split);
	mprintf("@ block %d\n",block);
	mprintf("@ tables %d\n",tables);
	mprintf("@ opttables %s\n",opttables?"true":"false");
	mprintf("@ coderun %s\n",coderun?"true":"false");
	mprintf("@ bw %s\n",bw?"true":"false");
	mprintf("@ mtf %s\n",mtf?"true":"false");
	mprintf("@ preopt %s\n",preopt?"true":"false");
	mprintf("@ maxiter %d\n",maxiter);
	mprintf("@ sortatom %s\n",sortatom?"true":"false");
	mprintf("@ atominfo %s\n",atominfo?"true":"false");
	mprintf("@ comment %s\n",comment?"true":"false");
	mprintf("@ maxchunk %d\n",maxchunk);
	mprintf("@ initial bs length: %d bytes\n",bs->GetByteLength());*/

	if (resi != NULL)
		*resi = 0.0;

	m_iaTempCompressXYZ.clear();

	if (skipcomp) {
		AtomsToIntegerArray(m_iaTempCompressXYZ,order,signi,split,verbose,skipcomp,resi);
		return true;
	}

	if (m_pReadCacheCube != NULL)
		as = m_pReadCacheCube->GetFrameHistory(0)->m_pAtoms;
	else
		as = m_pReadCacheXYZ->GetFrameHistory(0);

	tpos = bs->GetLength();
	PushStatistics();

	if (atominfo) {

		bs->WriteBit(1); // Atom Type information present
		bits = (int)ceil(mylog2((double)as->m_oaAtoms.size()+1));
		bs->WriteBits(bits,5);
		bs->WriteBits((unsigned long)as->m_oaAtoms.size(),bits);

		if (as->m_bOrd) {
			bs->WriteBit(1); // Ord numbers
			for (z=0;z<(int)as->m_oaAtoms.size();z++)
				bs->WriteBits(as->m_oaAtoms[z]->m_iOrd,8);
		} else
			bs->WriteBit(0); // No Ord numbers
		
		if (as->m_bLabels) {
			bs->WriteBit(1); // Atom label strings
			ExportAtomLabels(as,bs,verbose);
		} else
			bs->WriteBit(0); // No Atom label strings

		// Additional fields in version 1
		if (sortatom) {
			bs->WriteBit(1);
			BuildAtomSort(as);
		} else {
			bs->WriteBit(0);
			BuildIdentityAtomSort(as);
		}
		// End Additional fields

	} else
		bs->WriteBit(0); // No atom Type information present

	if ((as->m_sComment != NULL) && comment) {
		bs->WriteBit(1);
		CompressString(as->m_sComment,bs,verbose);
	} else
		bs->WriteBit(0);

	bs->WriteBits(order,4);
	bs->WriteBits(signi,4);
	bs->WriteBits(split,6);

	// Additional fields in version 1
	if (m_pExtrapolatorXYZ != NULL) {
		bs->WriteBit(1);
		ExportExtrapolatorSettings(bs,false,verbose);
	} else
		bs->WriteBit(0);

	bs->WriteBit(0); // Reserved for future use, must be 0
	// End Additional fields

	if (tciaa != NULL) {
		tciaa->push_back(std::vector<int>());
		AtomsToIntegerArray(tciaa->back(),order,signi,split,verbose,skipcomp,resi);
	} else
		AtomsToIntegerArray(m_iaTempCompressXYZ,order,signi,split,verbose,skipcomp,resi);

	PopStatistics();
	gc_oStatistics.m_lOverhead += bs->GetLength() - tpos;

	if (tciaa == NULL)
		ie.Compress(m_iaTempCompressXYZ,bs,bw,mtf,coderun,block,tables,opttables,false,preopt,maxiter,maxchunk,verbose);

	return true;
}


bool CCCEngine::DecompressAtomFrame(CBitSet *bs, int ver, bool verbose, bool second, bool check) {

	CIntegerEngine ie;
	int z, bits, ti;
	int tao, tasigni, tasplit;
	CCCAtom *at;
	CAtomSet *as, *as2;


	if (ver > 1) {
		eprintf("CCCEngine::DecompressAtomFrame(): Unknown Atom Frame Version: %d.\n",ver);
		mprintf("Either the BQB file is damaged, or was written with a more recent software version.\n\n");
		return false;
	}

	m_iaTempCompressXYZ.clear();

	if (second)
		as = GetOutput2AtomFrame(0);
	else
		as = GetOutputAtomFrame(0);

	if (bs->ReadBit()) {

		if (verbose)
			mprintf("Reading atom type information...\n");
		bits = bs->ReadBitsInteger(5);
		ti = bs->ReadBitsInteger(bits);
		if (verbose)
			mprintf("Will read %d atoms.\n",ti);
		for (z=0;z<ti;z++) {
			at = new CCCAtom();
			as->m_oaAtoms.push_back(at);
		}

		if (bs->ReadBit()) { // Read Ord Numbers
			as->m_bOrd = true;
			for (z=0;z<ti;z++)
				as->m_oaAtoms[z]->m_iOrd = bs->ReadBitsInteger(8);
		}

		if (bs->ReadBit()) { // Read Atom label strings
			as->m_bLabels = true;
			ImportAtomLabels(as,bs,verbose);
			if (!as->m_bOrd)
				for (z=0;z<ti;z++) {
					as->m_oaAtoms[z]->m_iOrd = GetAtomOrd(as->m_oaAtoms[z]->m_sLabel.c_str());
					//mprintf("%d: \"%s\", %d\n",z,as->m_oaAtoms[z]->m_sLabel.c_str(),as->m_oaAtoms[z]->m_iOrd);
				}
		}

		// Additional fields in version 1
		if (ver >= 1) {
			if (bs->ReadBit()) // Use Atom Sorting
				BuildAtomSort(as);
			else
				BuildIdentityAtomSort(as);
		} else
			BuildAtomSort(as);
		// End Additional fields

	} else {

		if (verbose)
			mprintf("Atom type information already read before.\n");
		if (second)
			as2 = GetOutput2AtomFrame(1);
		else
			as2 = GetOutputAtomFrame(1);
		if (as2 == NULL) {
			mprintf("CEngine::DecompressAtomFrame(): Error: First frame does not contain atom type information.\n");
			return false;
		}
		for (z=0;z<(int)as2->m_oaAtoms.size();z++) {
			at = new CCCAtom();
			at->m_iOrd = as2->m_oaAtoms[z]->m_iOrd;
			at->m_sLabel = as2->m_oaAtoms[z]->m_sLabel;
			as->m_oaAtoms.push_back(at);
		}
		as->m_bLabels = as2->m_bLabels;
		as->m_bOrd = as2->m_bOrd;
	}

	if (bs->ReadBit())
		DecompressString(bs,&as->m_sComment,verbose);
	else
		as->m_sComment = NULL;

	tao = bs->ReadBitsInteger(4);
	tasigni = bs->ReadBitsInteger(4);
	tasplit = bs->ReadBitsInteger(6);

	// Additional fields in version 1
	if (ver >= 1) {
		if (bs->ReadBit())
			ImportExtrapolatorSettings(bs,false,verbose);
		if (bs->ReadBit()) {
			eprintf("CCCEngine::DecompressAtomFrame(): Reserved bit expected to be 0, but was 1.\n");
			mprintf("Either the BQB file is damaged, or was written with a more recent software version.\n\n");
		}
	}
	// End Additional fields

	as->m_iSigni = tasigni;

	if (!check && (m_pExtrapolatorXYZ != NULL))
		m_pExtrapolatorXYZ->PushAtomFrame(as);

	ie.Decompress(bs,m_iaTempCompressXYZ,verbose);

	IntegerArrayToAtoms(m_iaTempCompressXYZ,tao,tasigni,tasplit,verbose,second);

	return true;
}


bool CCCEngine::CompressFile(const char *inp, const char *outp, int block, int tables, bool opttables, bool coderun, bool bw, bool mtf, bool preopt, int maxiter, int maxchunk, bool verbose) {

	FILE *a;
	unsigned char buf[4096];
	std::vector<int> ia;
	CBitSet bs;
	CIntegerEngine ie;
	CBarbecubeFile bf;
	int i, z;


	if (verbose)
		mprintf(">>> CEngine::CompressFile >>>\n");

	gc_oStatistics.m_lSize = 0;
	gc_oStatistics.m_lOverhead = 0;
	gc_oStatistics.m_lAlphabet = 0;
	gc_oStatistics.m_lHuffmanTables = 0;
	gc_oStatistics.m_lTableSwitch = 0;
	gc_oStatistics.m_lHuffmanData = 0;

	a = fopen(inp,"rb");
	if (a == NULL) {
		mprintf("Error: Could not open file \"%s\" for reading.\n",inp);
		return false;
	}

	if (outp != NULL) {
		if (!bf.OpenWriteReplace(outp)) {
			mprintf("Error: Could not open file \"%s\" for writing.\n",outp);
			fclose(a);
			return false;
		}
	}

	while (!feof(a)) {
		i = (int)fread(buf,1,4096,a);
		for (z=0;z<i;z++)
			ia.push_back(buf[z]);
		if (i < 4096)
			break;
	}

	mprintf("%lu Bytes read from input file.\n",ia.size());

	if (!ie.Compress(ia,&bs,bw,mtf,coderun,block,tables,opttables,true,preopt,maxiter,maxchunk,verbose)) {
		mprintf("IntegerEngine returned an error.\n");
		return false;
	}

	fclose(a);

	if (outp != NULL) {
		bf.CreateShortFrame(BARBECUBE_FRAMETYPE_COMPFILE,0,0);
		bf.PushPayload(bs.m_iaData);
		bf.FinalizeFrame();
		bf.Close();
	}

	mprintf("Compressed to %d bytes (%.3f:1, %7.3f%%, %.3f bits/byte).\n\n",bs.GetByteLength(),(double)ia.size()/bs.GetByteLength(),(double)bs.GetByteLength()/ia.size()*100.0,(double)bs.GetLength()/ia.size());

	mprintf("%10.3f MiB (%12s Bytes) overhead.\n",double(gc_oStatistics.m_lOverhead)/1024.0/1024.0/8.0,(gc_oStatistics.m_lOverhead>>3).string());
	mprintf("%10.3f MiB (%12s Bytes) alphabet data.\n",double(gc_oStatistics.m_lAlphabet)/1024.0/1024.0/8.0,(gc_oStatistics.m_lAlphabet>>3).string());
	mprintf("%10.3f MiB (%12s Bytes) Huffman tables.\n",double(gc_oStatistics.m_lHuffmanTables)/1024.0/1024.0/8.0,(gc_oStatistics.m_lHuffmanTables>>3).string());
	mprintf("%10.3f MiB (%12s Bytes) table switching.\n",double(gc_oStatistics.m_lTableSwitch)/1024.0/1024.0/8.0,(gc_oStatistics.m_lTableSwitch>>3).string());
	mprintf("%10.3f MiB (%12s Bytes) payload data.\n",double(gc_oStatistics.m_lHuffmanData)/1024.0/1024.0/8.0,(gc_oStatistics.m_lHuffmanData>>3).string());
	mprintf("%10.3f MiB (%12s Bytes) in total.\n\n",double(gc_oStatistics.m_lOverhead+gc_oStatistics.m_lAlphabet+gc_oStatistics.m_lHuffmanTables+gc_oStatistics.m_lTableSwitch+gc_oStatistics.m_lHuffmanData)/1024.0/1024.0/8.0,((gc_oStatistics.m_lOverhead+gc_oStatistics.m_lAlphabet+gc_oStatistics.m_lHuffmanTables+gc_oStatistics.m_lTableSwitch+gc_oStatistics.m_lHuffmanData)>>3).string());

	if (verbose)
		mprintf("<<< CEngine::CompressFile <<<\n");

	return true;
}


bool CCCEngine::DecompressFile(const char *inp, const char *outp, const char *ref, bool verbose) {

	UNUSED(ref);

	FILE *b;
	unsigned char buf[4096];
	std::vector<int> ia;
	CBitSet bs;
	CIntegerEngine ie;
	int /*i,*/ z, z2;
	CBarbecubeFile bf;

	if (verbose)
		mprintf(">>> CEngine::DecompressFile >>>\n");

	if (!bf.OpenRead(inp)) {
		mprintf("Error: Could not open file \"%s\" for reading.\n",inp);
		return false;
	}

	b = fopen(outp,"wb");
	if (b == NULL) {
		mprintf("Error: Could not open file \"%s\" for writing.\n",outp);
		return false;
	}

	bf.ReadFrame();

	if ((bf.GetFrameType() != BARBECUBE_FRAMETYPE_COMPFILE) || (bf.GetFrameTypeVersion() != 0)) {
		mprintf("Error: Unexpected frame type (expected %d v0, found %d v%d).\n",BARBECUBE_FRAMETYPE_COMPFILE,bf.GetFrameType(),bf.GetFrameTypeVersion());
		fclose(b);
		return false;
	}

	bs.m_iaData.assign(bf.GetFramePayload()->begin(),bf.GetFramePayload()->end());

	bf.Close();

	mprintf("%lu bytes of payload read from input file.\n",bs.m_iaData.size());

	if (!ie.Decompress(&bs,ia,verbose)) {
		mprintf("IntegerEngine returned an error.\n");
		fclose(b);
		return false;
	}

	if (verbose)
		mprintf("Decompressed to %lu Bytes.\n",ia.size());

	for (z=0;z<(int)ia.size()/4096;z++) {
		for (z2=0;z2<4096;z2++)
			buf[z2] = (unsigned char)ia[z*4096+z2];
		fwrite(buf,4096,1,b);
	}

	if ((ia.size()%4096) != 0) {
		for (z2=0;z2<(int)ia.size()%4096;z2++)
			buf[z2] = (unsigned char)ia[z*4096+z2];
		fwrite(buf,ia.size()%4096,1,b);
	}

	fclose(b);

	if (verbose)
		mprintf("<<< CEngine::DecompressFile <<<\n");

	return true;
}


bool CCCEngine::CompressCube(
		const char *inp,
		const char *outp,
		const char *ref,
		int start,
		int steps,
		int stride,
		int corder,
		bool optcorder,
		int aorder,
		bool optaorder,
		int eps,
		int csigni,
		int asigni,
		int csplit,
		int asplit,
		int cblock,
		int ablock,
		int ctables,
		bool optctables,
		bool cpreopt,
		int cmaxiter,
		int cmaxchunk,
		int atables,
		bool optatables,
		bool hilbert,
		double nbhfac,
		bool ccoderun,
		bool cbw,
		bool cmtf,
		bool acoderun,
		bool abw,
		bool amtf,
		bool apreopt,
		int amaxiter,
		bool asortatom,
		int amaxchunk,
		bool usecextra,
		bool useaextra,
		int aextratrange,
		int aextratorder,
		double aextratimeexpo,
		int cextrasrangex,
		int cextrasrangey,
		int cextrasrangez,
		int cextratrange,
		int cextrasorder,
		int cextratorder,
		int cextraoffsetx,
		int cextraoffsety,
		int cextraoffsetz,
		bool cextracrosss,
		bool cextracrosst,
		bool cextrawrap,
		bool cextracrossranges,
		bool cextracrossranget,
		double cextradistexpo,
		double cextratimeexpo,
		bool cextrapredcorr,
		double cextracorrfactor,
		int optimize,
		int optsteps,
		bool onlyopt,
		bool comment,
		bool compare,
		bool dummyread,
		bool verbose
		) {

	const CCubeFrame *cfr;
	CCubeFrame *cfr2;
	int z, z2, i, k, co, ao, lcsize, lasize, morder;
	double tb, tb2, tf, tf2;
	long insize;
	std::vector<int> ia, ia2;
	CBitSet bsat, bscu, bshe, bstemp;
	bool converged, err, to;
	unsigned long t0, rxyz=1;
	CBarbecubeFile bf;


	if (usecextra)
		if (cextratrange > corder)
			corder = cextratrange;

	if (useaextra)
		if (aextratrange > aorder)
			aorder = aextratrange;

	mprintf("Opening cube file \"%s\" ...\n",inp);

	m_pReadCacheCube = new CCCReadCubeCache();
	m_pReadCacheCube->SetReadParameters(eps,csigni,asigni);
	if (!m_pReadCacheCube->FileOpenRead(inp,ref)) {
		mprintf("Error: Could not open file for reading.\n");
		return false;
	}

	if (optimize != 0) {

		if (MAX(aorder,corder)+1 > optsteps)
			m_pReadCacheCube->SetHistoryDepth(MAX(aorder,corder)+1);
		else
			m_pReadCacheCube->SetHistoryDepth(optsteps);

		mprintf("\n");
		m_pReadCacheCube->CacheSteps(optsteps,true);
		mprintf("\n");

		if (m_iOptIncludeFirst == -1) {
			if ((m_pReadCacheCube->GetCachedSteps() < optsteps) || (optsteps < 10)) {
				mprintf("        Low step count, switching on -optstart.\n\n");
				m_iOptIncludeFirst = 1;
			} else {
				mprintf("        Large step count, switching off -optstart.\n\n");
				m_iOptIncludeFirst = 0;
			}
		}

		m_pReadCacheCube->SetStartLock();
	
		if (!OptimizeXYZParameters(
				optimize, // int     level,
				aorder, // int    &order, 
				asigni, // int     signi, 
				asplit, // int    &split, 
				ablock, // int    &block, 
				atables, // int    &tables, 
				optatables, // bool   &opttables, 
				acoderun, // bool   &coderun, 
				abw, // bool   &bw, 
				amtf, // bool   &mtf,
				apreopt, // bool   &preopt,
				amaxiter, // int    &maxiter,
				asortatom, // bool   &sortatom,
				useaextra, // bool   &useextra,
				aextratrange, // int    &extratrange,
				aextratorder, // int    &extratorder,
				aextratimeexpo, // double &extratimeexpo,
				comment, // bool    comment, 
				amaxchunk, // int    &maxchunk, 
				verbose, // bool    verbose
				true // bool    cubepart
		)) {
			eprintf("Error: Position data parameter optimization failed.\n");
			delete m_pReadCacheCube;
			m_pReadCacheCube = NULL;
			return false;
		}

		mprintf("\n");
		
		if (!OptimizeCubeParameters(
				optimize, // int     level,
				corder, // int    &corder,
				eps, // int     eps,
				csigni, // int     csigni,
				csplit, // int    &csplit,
				cblock, // int    &cblock,
				ctables, // int    &ctables,
				optctables, // bool   &optctables,
				cpreopt, // bool   &cpreopt,
				cmaxiter, // int    &cmaxiter,
				cmaxchunk, // int    &cmaxchunk,
				hilbert, // bool   &hilbert,
				nbhfac, // double &nbhfac,
				ccoderun, // bool   &ccoderun,
				cbw, // bool   &cbw,
				cmtf, // bool   &cmtf,
				usecextra, // bool   &usecextra,
				cextrasrangex, // int    &cextrasrangex,
				cextrasrangey, // int    &cextrasrangey,
				cextrasrangez, // int    &cextrasrangez,
				cextratrange, // int    &cextratrange,
				cextrasorder, // int    &cextrasorder,
				cextratorder, // int    &cextratorder,
				cextraoffsetx, // int    &cextraoffsetx,
				cextraoffsety, // int    &cextraoffsety,
				cextraoffsetz, // int    &cextraoffsetz,
				cextracrosss, // bool   &cextracrosss,
				cextracrosst, // bool   &cextracrosst,
				cextrawrap, // bool   &cextrawrap,
				cextracrossranges, // bool   &cextracrossranges,
				cextracrossranget, // bool   &cextracrossranget,
				cextradistexpo, // double &cextradistexpo,
				cextratimeexpo, // double &cextratimeexpo,
				cextrapredcorr, // bool   &cextrapredcorr,
				cextracorrfactor, // double &cextracorrfactor,
				verbose // bool    verbose
		)) {
			eprintf("Error: Volumetric data parameter optimization failed.\n");
			delete m_pReadCacheCube;
			m_pReadCacheCube = NULL;
			return false;
		}

		mprintf("\n    The optimal parameters for this trajectory are:\n");
		mprintf("     ");
		if (useaextra) {
			if (aextratrange != aextratorder+2) {
				if (optimize >= 3)
					mprintf(" -aextra yes -aextrange %d -aextorder %d -aextimeexpo %.3f",aextratrange,aextratorder,aextratimeexpo);
				else
					mprintf(" -aextra yes -aextrange %d -aextorder %d -aextimeexpo %.2f",aextratrange,aextratorder,aextratimeexpo);
			} else
				mprintf(" -aextra yes -aextrange %d -aextorder %d",aextratrange,aextratorder);
		} else
			mprintf(" -aextra no -aorder %d",aorder);

		if (optimize >= 2) {
			mprintf(" -atables %d",atables);
			if (atables != 1)
				mprintf(" -ablock %d",ablock);
		}

		if (optimize >= 3) {

			if (abw)
				mprintf(" -abw yes");
			if (amtf)
				mprintf(" -amtf yes");
		}

		mprintf(" -cextra yes");

		if ((cextrasrangex == cextrasrangey) && (cextrasrangex == cextrasrangez))
			mprintf(" -cexsrange %d",cextrasrangex);
		else
			mprintf(" -cexsrangex %d -cexsrangey %d -cexsrangez %d",cextrasrangex,cextrasrangey,cextrasrangez);

		mprintf(" -cexsorder %d",cextrasorder);

		if ((cextraoffsetx == cextraoffsety) && (cextraoffsetx == cextraoffsetz))
			mprintf(" -cexoffset %d",cextraoffsetx);
		else
			mprintf(" -cexoffsetx %d -cexoffsety %d -cexoffsetz %d",cextraoffsetx,cextraoffsety,cextraoffsetz);

		mprintf(" -cextrange %d -cextorder %d",cextratrange,cextratorder);

		if (optimize >= 3) {
			if (cextratrange != cextratorder+2)
				mprintf(" -cexdistexpo %.3f -cextimeexpo %.3f",cextradistexpo,cextratimeexpo);
			else
				mprintf(" -cexdistexpo %.3f",cextradistexpo);
		} else {
			if (cextratrange != cextratorder+2)
				mprintf(" -cexdistexpo %.2f -cextimeexpo %.2f",cextradistexpo,cextratimeexpo);
			else
				mprintf(" -cexdistexpo %.2f",cextradistexpo);
		}

		if (optimize >= 2) {
			mprintf(" -ctables %d",ctables);
			if (ctables > 1)
				mprintf(" -cblock %d",cblock);
			if (optimize == 3) {
				if (cbw)
					mprintf(" -cbw yes");
				if (cmtf)
					mprintf(" -cmtf yes");
			}
		}

		mprintf("\n\n");

		if (onlyopt) {
			m_pReadCacheCube->CloseFile();
			delete m_pReadCacheCube;
			m_pReadCacheCube = NULL;
			return true;
		}

		m_pReadCacheCube->RewindReadPos();

		m_pReadCacheCube->LiftStartLock();

		if (usecextra)
			if (cextratrange > corder)
				corder = cextratrange;

		if (useaextra)
			if (aextratrange > aorder)
				aorder = aextratrange;

	} else {

		m_pReadCacheCube->SetHistoryDepth(MAX(aorder,corder)+1);
	}

	t0 = (unsigned long)time(NULL);

	gc_oStatistics.m_lSize = 0;
	gc_oStatistics.m_lOverhead = 0;
	gc_oStatistics.m_lAlphabet = 0;
	gc_oStatistics.m_lHuffmanTables = 0;
	gc_oStatistics.m_lTableSwitch = 0;
	gc_oStatistics.m_lHuffmanData = 0;

	if (!dummyread) {
		if (outp != NULL) {
			mprintf("Opening compressed file \"%s\" ...\n",outp);
			if (!bf.OpenWriteAppend(outp)) {
				mprintf("Error: Could not open file for writing.\n");
				m_pReadCacheCube->CloseFile();
				delete m_pReadCacheCube;
				m_pReadCacheCube = NULL;
				return false;
			}
		}
	} else {
		ref = NULL;
		outp = NULL;
		mprintf("Dummy Read Mode (no compression / output is performed).\n");
	}

	m_oaOutputCubeBuf.resize(corder+1);
	for (z=0;z<corder+1;z++)
		m_oaOutputCubeBuf[z] = NULL;
	m_iOutputCubeBufPos = 0;

	m_oaOutputAtomBuf.resize(aorder+1);
	for (z=0;z<aorder+1;z++)
		m_oaOutputAtomBuf[z] = NULL;
	m_iOutputAtomBufPos = 0;

	mprintf("Starting process...\n");

	if (start != 0) {
		mprintf("Fast-forwarding to step %d: ",start+1);
		for (z=0;z<start;z++) {
			if (!m_pReadCacheCube->SkipOneStep()) {
				m_pReadCacheCube->CloseFile();
				delete m_pReadCacheCube;
				m_pReadCacheCube = NULL;
				return false;
			}
			mprintf(".");
		}
		mprintf(" Done.\n");
	}

	morder = corder;
	if (aorder > morder)
		morder = aorder;

	ResetStatistics();

	converged = false;
	err = false;
	i = 0;
	k = 0;
	tb = 0;
	tb2 = 0;
	tf = 0;
	tf2 = 0;
	to = false;
	if (optcorder && !usecextra)
		lcsize = 1000000000;
	else
		lcsize = -1;
	if (optaorder && !useaextra)
		lasize = 1000000000;
	else
		lasize = -1;
	while (true) {

		if (verbose)
			mprintf("\n\n");

		if (i != 0)
			mprintf("    Step %6d ... (avg %5.1f s/step)\n",i+1,((double)(time(NULL)-t0))/i);
		else
			mprintf("    Step %6d ...\n",i+1);

		cfr = m_pReadCacheCube->GetNextFrame();

		if (cfr == NULL) {
			mprintf("      Reached end of input trajectory.\n");
			break;
		}

		if (i == 0) {
			rxyz = cfr->m_iResXYZ;

			if (usecextra) {

				if (cextrapredcorr) {

					mprintf("      Initializing Volumetric Predictor Extrapolator (%d/%d)...\n",cextratrange,cextratorder);
					m_pExtrapolator = new CExtrapolator();
					m_pExtrapolator->Initialize(
						cfr->m_iRes[0], // resx,
						cfr->m_iRes[1], // resy,
						cfr->m_iRes[2], // resz,
						1,              // srangex,
						1,              // srangey,
						1,              // srangez,
						cextratrange,   // trange,
						0,              // sorder,
						cextratorder,   // torder,
						0,              // offsetx,
						0,              // offsety,
						0,              // offsetz,
						false,          // crosss,
						false,          // crosst,
						false,          // wrap,
						false,          // crossranges,
						false,          // crossranget
						0,              // distexpo
						cextratimeexpo, // timeexpo
						1.0,            // corrfactor
						verbose,
						false
					);

					if ((cextrasrangex == cextrasrangey) && (cextrasrangex == cextrasrangez))
						mprintf("      Initializing Volumetric Corrector Extrapolator (%d/%d)...\n",cextrasrangex,cextrasorder);
					else
						mprintf("      Initializing Volumetric Corrector Extrapolator (%d|%d|%d/%d)...\n",cextrasrangex,cextrasrangey,cextrasrangez,cextrasorder);
					m_pExtrapolatorCorr = new CExtrapolator();
					m_pExtrapolatorCorr->Initialize(
						cfr->m_iRes[0],     // resx,
						cfr->m_iRes[1],     // resy,
						cfr->m_iRes[2],     // resz,
						cextrasrangex,      // srangex,
						cextrasrangey,      // srangey,
						cextrasrangez,      // srangez,
						1,                  // trange,
						cextrasorder,       // sorder,
						0,                  // torder,
						cextraoffsetx,      // offsetx,
						cextraoffsety,      // offsety,
						cextraoffsetz,      // offsetz,
						cextracrosss,       // crosss,
						false,              // crosst,
						cextrawrap,         // wrap,
						cextracrossranges,  // crossranges,
						false,              // crossranget
						cextradistexpo,     // distexpo
						0,                  // timeexpo
						cextracorrfactor,   // corrfactor
						verbose,
						false
					);

				} else {

					if ((cextrasrangex == cextrasrangey) && (cextrasrangex == cextrasrangez))
						mprintf("      Initializing Volumetric Extrapolator (%d/%d;%d/%d)...\n",cextrasrangex,cextrasorder,cextratrange,cextratorder);
					else
						mprintf("      Initializing Volumetric Extrapolator (%d|%d|%d/%d;%d|%d)...\n",cextrasrangex,cextrasrangey,cextrasrangez,cextrasorder,cextratrange,cextratorder);
					m_pExtrapolator = new CExtrapolator();
					m_pExtrapolator->Initialize(
						cfr->m_iRes[0],     // resx,
						cfr->m_iRes[1],     // resy,
						cfr->m_iRes[2],     // resz,
						cextrasrangex,      // srangex,
						cextrasrangey,      // srangey,
						cextrasrangez,      // srangez,
						cextratrange,       // trange,
						cextrasorder,       // sorder,
						cextratorder,       // torder,
						cextraoffsetx,      // offsetx,
						cextraoffsety,      // offsety,
						cextraoffsetz,      // offsetz,
						cextracrosss,       // crosss,
						cextracrosst,       // crosst,
						cextrawrap,         // wrap,
						cextracrossranges,  // crossranges,
						cextracrossranget,  // crossranget
						cextradistexpo,     // distexpo
						cextratimeexpo,     // timeexpo
						1.0,                // corrfactor
						verbose,
						false
					);
				}
			}

			if (useaextra) {

				mprintf("      Initializing Position Extrapolator (%d/%d)...\n",aextratrange,aextratorder);
				m_pExtrapolatorXYZ = new CExtrapolator();
				m_pExtrapolatorXYZ->InitializeXYZ(aextratrange,aextratorder,aextratimeexpo,verbose,false);
			}

		}

		for (z=0;z<stride-1;z++) {
			if (verbose)
				mprintf("Skipping frame...\n");
			if (!m_pReadCacheCube->SkipOneStep()) {
				mprintf("      Reached end of input trajectory.\n");
				break;
			}
		}

		if (dummyread) // Just benchmark the CUBE reading speed
			goto _dummy;

		if (m_pExtrapolator != NULL)
			m_pExtrapolator->PushCubeFrame(cfr);

		if (m_pExtrapolatorXYZ != NULL)
			m_pExtrapolatorXYZ->PushAtomFrame(cfr->m_pAtoms);

_again:
		if (i < corder)
			co = i;
		else if (k < corder)
			co = k;
		else
			co = corder;

		if (i < aorder)
			ao = i;
		else if (k < aorder)
			ao = k;
		else
			ao = aorder;

_aagain:
		bsat.Clear();
		BeginStatistics();
		PushStatistics();

		CompressAtomFrame(
			&bsat,
			ao,
			asigni,
			asplit,
			ablock,
			atables,
			optatables,
			acoderun,
			abw,
			amtf,
			apreopt&&(i>=2),
			amaxiter,
			asortatom,
			(i==0),
			comment,
			amaxchunk,
			verbose
		);

		if (m_pExtrapolatorXYZ == NULL)
			mprintf("      Atoms: Order %d, output size %9.3f KiB.\n",ao,bsat.GetByteLength()/1024.0);
		else if (m_pExtrapolator != NULL)
			mprintf("      Atoms: Output size %9.3f KiB.\n",bsat.GetByteLength()/1024.0);
		else
			mprintf("      Atoms:          Output size %9.3f KiB.\n",bsat.GetByteLength()/1024.0);

		if (lasize >= 0) {
			if (to) {
				to = false;
				if (bsat.GetByteLength() <= bstemp.GetByteLength()) {
					mprintf("      Size is indeed smaller with lower order. Limiting atom order to %d.\n",ao);
					PopDiffStatistics();
					aorder = ao;
					lasize = -1;
				} else {
					mprintf("      Size was smaller with higher order. Not limiting.\n");
					PopStatistics();
					bsat = CBitSet(bstemp);
					lasize = bsat.GetByteLength();
				}
			} else {
				if ((ao != 0) && ((double)bsat.GetByteLength() >= (double)lasize*0.97)) {
					mprintf("      Size did not decrease any further with order %d. Trying order %d...\n",ao,ao-1);
					bstemp = CBitSet(bsat);
					to = true;
					ao--;
					goto _aagain;
				} else if (ao == aorder)
					lasize = -1;
				else
					lasize = bsat.GetByteLength();
			}
		}

		PopIgnoreStatistics();
		EndStatistics(true,converged);

_cagain:
		bscu.Clear();
		BeginStatistics();
		PushStatistics();

		CompressCubeFrame(
			&bscu,
			co,
			eps,
			csigni,
			csplit,
			cblock,
			ctables,
			optctables,
			hilbert,
			nbhfac,
			ccoderun,
			cbw,
			cmtf,
			cpreopt&&(i>=2),
			cmaxiter,
			(i==0),
			cmaxchunk,
			verbose
		);

		if (m_pExtrapolator == NULL)
			mprintf("      Cube:  Order %d, output size %9.3f KiB.\n",co,bscu.GetByteLength()/1024.0);
		else if (m_pExtrapolatorXYZ != NULL)
			mprintf("      Cube:  Output size %9.3f KiB.\n",bscu.GetByteLength()/1024.0);
		else
			mprintf("      Cube:           Output size %9.3f KiB.\n",bscu.GetByteLength()/1024.0);

		if (lcsize >= 0) {
			if (to) {
				to = false;
				if (bscu.GetByteLength() <= bstemp.GetByteLength()) {
					mprintf("      Size is indeed smaller with lower order. Limiting cube order to %d.\n",co);
					PopDiffStatistics();
					corder = co;
					lcsize = -1;
				} else {
					mprintf("      Size was smaller with higher order. Not limiting.\n");
					PopStatistics();
					bscu = CBitSet(bstemp);
					lcsize = bscu.GetByteLength();
				}
			} else {
				if ((co != 0) && ((double)bscu.GetByteLength() >= (double)lcsize*0.97)) {
					mprintf("      Size did not decrease any further with order %d. Trying order %d...\n",co,co-1);
					bstemp = CBitSet(bscu);
					to = true;
					co--;
					goto _cagain;
				} else if (co == corder)
					lcsize = -1;
				else
					lcsize = bscu.GetByteLength();
			}
		}

		PopIgnoreStatistics();
		EndStatistics(false,converged);

		if (MAX(aorder,corder) < morder)
			morder = MAX(aorder,corder);

		if (compare) {

			cfr2 = new CCubeFrame();
			PushOutputCubeFrame(cfr2);
			cfr2->m_pAtoms = new CAtomSet();
			PushOutputAtomFrame(cfr2->m_pAtoms);

			DecompressAtomFrame(&bsat,1,verbose,false,true);

			DecompressCubeFrame(&bscu,1,verbose,false,true);

			if (verbose)
				mprintf("Comparing input and output...\n");

			for (z=0;z<(int)cfr2->m_pAtoms->m_oaAtoms.size();z++)
				for (z2=0;z2<3;z2++)
					if (cfr->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2] != cfr2->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2]) {
						eprintf("        Error in atom coordinate %d[%d]: %.6f (%ld) != %.6f (%ld)\n",z,z2,cfr->m_pAtoms->m_oaAtoms[z]->m_fCoord[z2],cfr->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2],cfr2->m_pAtoms->m_oaAtoms[z]->m_fCoord[z2],cfr2->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2]);
						err = true;
						goto _skerr;
					}

			for (z=0;z<cfr->m_iResXYZ;z++)
				if (!ExpMantisEqual(cfr->m_iaExpo[z],cfr->m_iaMantis[z],cfr2->m_iaExpo[z],cfr2->m_iaMantis[z])) {
					eprintf("        Error in volumetric data element %7d: %.10G vs %.10G\n",z,cfr->m_faBin[z],cfr2->m_faBin[z]);
					err = true;
					goto _skerr;
				}
_skerr:
			if (err) {
				if (k != 0) {
					eprintf("Errors occured. Compressing frame again with order zero.\n");
					err = false;
					k = 0;
					goto _again;
				} else {
					eprintf("Errors occured despite of order zero. Aborting.\n");
					goto _end;
				}
			}

			if (verbose)
				mprintf("Done.\n");
		}

		bshe.Clear();

		bshe.WriteBits(bsat.GetByteLength(),32);
		bshe.WriteBits(bscu.GetByteLength(),32);

		tb += bshe.GetByteLength();
		tb += bsat.GetByteLength();
		tb += bscu.GetByteLength();
		tf++;

		gc_oStatistics.m_lOverhead += bshe.GetLength();

		if ((bsat.GetLength()%8) != 0)
			gc_oStatistics.m_lOverhead += 8 - (bsat.GetLength()%8);

		if ((bscu.GetLength()%8) != 0)
			gc_oStatistics.m_lOverhead += 8 - (bscu.GetLength()%8);

		gc_iOutputSize += bshe.GetByteLength() + bsat.GetByteLength() + bscu.GetByteLength();
		if (converged)
			gc_iCOutputSize += bshe.GetByteLength() + bsat.GetByteLength() + bscu.GetByteLength();

		if (i >= morder) {
			tb2 += bshe.GetByteLength();
			tb2 += bsat.GetByteLength();
			tb2 += bscu.GetByteLength();
			tf2++;
		}

		if (outp != NULL) {
			bf.CreateShortFrame((k==0)?BARBECUBE_FRAMETYPE_COMPCUBESTART:BARBECUBE_FRAMETYPE_COMPCUBE,1,i+1);
			bf.PushPayload(bshe.m_iaData);
			bf.PushPayload(bsat.m_iaData);
			bf.PushPayload(bscu.m_iaData);
			bf.FinalizeFrame();
		}

		if ((ao == aorder) && (co == corder))
			converged = true;

_dummy:
		i++;
		k++;
		if (i == steps)
			break;
	}

	if (outp != NULL)
		bf.WriteIndexFrame(true);

_end:
	if (outp != NULL)
		bf.Close();

	insize = m_pReadCacheCube->ftell();

	m_pReadCacheCube->CloseFile();
	delete m_pReadCacheCube;
	m_pReadCacheCube = NULL;

	if (m_pExtrapolator != NULL) {
		delete m_pExtrapolator;
		m_pExtrapolator = NULL;
	}

	if (m_pExtrapolatorCorr != NULL) {
		delete m_pExtrapolatorCorr;
		m_pExtrapolatorCorr = NULL;
	}

	if (m_pExtrapolatorXYZ != NULL) {
		delete m_pExtrapolatorXYZ;
		m_pExtrapolatorXYZ = NULL;
	}

	if (err) {
		mprintf("Errors occurred while compressing the cube file.\n");
		return false;
	} else {
		mprintf("Finished compressing the cube file.\n\n");
		mprintf("%10.3f MiB (%12s Bytes) overhead.\n",double(gc_oStatistics.m_lOverhead)/1024.0/1024.0/8.0,(gc_oStatistics.m_lOverhead>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) alphabet data.\n",double(gc_oStatistics.m_lAlphabet)/1024.0/1024.0/8.0,(gc_oStatistics.m_lAlphabet>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) Huffman tables.\n",double(gc_oStatistics.m_lHuffmanTables)/1024.0/1024.0/8.0,(gc_oStatistics.m_lHuffmanTables>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) table switching.\n",double(gc_oStatistics.m_lTableSwitch)/1024.0/1024.0/8.0,(gc_oStatistics.m_lTableSwitch>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) payload data.\n",double(gc_oStatistics.m_lHuffmanData)/1024.0/1024.0/8.0,(gc_oStatistics.m_lHuffmanData>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) in total.\n\n",double(gc_oStatistics.m_lOverhead+gc_oStatistics.m_lAlphabet+gc_oStatistics.m_lHuffmanTables+gc_oStatistics.m_lTableSwitch+gc_oStatistics.m_lHuffmanData)/1024.0/1024.0/8.0,((gc_oStatistics.m_lOverhead+gc_oStatistics.m_lAlphabet+gc_oStatistics.m_lHuffmanTables+gc_oStatistics.m_lTableSwitch+gc_oStatistics.m_lHuffmanData)>>3).string());
		if (tf > 0) {
			mprintf("\n  * Totals:\n");
			mprintf("      %9.3f KiB per frame on average.\n",tb/1024.0/tf);
			mprintf("      %9.3f bits per bin entry on average.\n",tb/tf/rxyz*8.0);
			mprintf("      Compression ratio of %.3f : 1\n",(double)insize/tb);
		}
		if (tf2 > 0) {
			mprintf("\n  * Starting from step %d:\n",morder+1);
			mprintf("      %9.3f KiB per frame on average.\n",tb2/1024.0/tf2);
			mprintf("      %9.3f bits per bin entry on average.\n",(tb2/tf2)/rxyz*8.0);
			mprintf("      Compression ratio of %.3f : 1\n",((double)insize/tf*tf2)/tb2);
		}
		mprintf("\n");
	}

	return true;
}


double CCCEngine::CompressCubeBenchmark(
		int corder,
		int eps,
		int csigni,
		int csplit,
		int cblock,
		int ctables,
		bool optctables,
		bool cpreopt,
		int cmaxiter,
		int cmaxchunk,
		bool hilbert,
		double nbhfac,
		bool ccoderun,
		bool cbw,
		bool cmtf,
		bool usecextra,
		int cextrasrangex,
		int cextrasrangey,
		int cextrasrangez,
		int cextratrange,
		int cextrasorder,
		int cextratorder,
		int cextraoffsetx,
		int cextraoffsety,
		int cextraoffsetz,
		bool cextracrosss,
		bool cextracrosst,
		bool cextrawrap,
		bool cextracrossranges,
		bool cextracrossranget,
		double cextradistexpo,
		double cextratimeexpo,
		bool cextrapredcorr,
		double cextracorrfactor,
		bool verbose1,
		bool verbose2,
		bool realsize,
		double *presi,
		std::vector<std::vector<int> > *tciaa
	) {

	const CCubeFrame *cfr;
	int i, co;
	double tb, tf, resi, tresi;
	CBitSet bscu;
	unsigned long t0, rxyz=1;


	resi = 0;

	t0 = (unsigned long)time(NULL);

	if (usecextra)
		if (cextratrange > corder)
			corder = cextratrange;

	if (verbose1)
		mprintf("Starting process...\n");

	m_pReadCacheCube->RewindReadPos();

	i = 0;
	tb = 0;
	tf = 0;
	while (true) {

		if (verbose2)
			mprintf("\n\n");

		if (verbose1) {
			if (i != 0)
				mprintf("    Step %6d ... (avg %5.1f s/step)\n",i+1,((double)(time(NULL)-t0))/i);
			else
				mprintf("    Step %6d ...\n",i+1);
		}

		cfr = m_pReadCacheCube->GetNextFrame();

		if (cfr == NULL)
			break;

		if (i == 0) {
			rxyz = cfr->m_iResXYZ;

			if (usecextra) {

				if (cextrapredcorr) {

					if (verbose1)
						mprintf("      Initializing Predictor Extrapolator...\n");
					m_pExtrapolator = new CExtrapolator();
					m_pExtrapolator->Initialize(
						cfr->m_iRes[0],  // resx,
						cfr->m_iRes[1],  // resy,
						cfr->m_iRes[2],  // resz,
						1,               // srangex,
						1,               // srangey,
						1,               // srangez,
						cextratrange,    // trange,
						0,               // sorder,
						cextratorder,    // torder,
						0,               // offsetx,
						0,               // offsety,
						0,               // offsetz,
						false,           // crosss,
						false,           // crosst,
						false,           // wrap,
						false,           // crossranges,
						false,           // crossranget
						0,               // distexpo
						cextratimeexpo,  // timeexpo
						1.0,             // corrfactor
						verbose2,
						!verbose1
					);

					if (verbose1)
						mprintf("      Initializing Corrector Extrapolator...\n");
					m_pExtrapolatorCorr = new CExtrapolator();
					m_pExtrapolatorCorr->Initialize(
						cfr->m_iRes[0],     // resx,
						cfr->m_iRes[1],     // resy,
						cfr->m_iRes[2],     // resz,
						cextrasrangex,      // srangex,
						cextrasrangey,      // srangey,
						cextrasrangez,      // srangez,
						1,                  // trange,
						cextrasorder,       // sorder,
						0,                  // torder,
						cextraoffsetx,      // offsetx,
						cextraoffsety,      // offsety,
						cextraoffsetz,      // offsetz,
						cextracrosss,       // crosss,
						false,              // crosst,
						cextrawrap,         // wrap,
						cextracrossranges,  // crossranges,
						false,              // crossranget
						cextradistexpo,     // distexpo
						0,                  // timeexpo
						cextracorrfactor,   // corrfactor
						verbose2,
						!verbose1
					);

				} else {

					if (verbose1)
						mprintf("      Initializing Extrapolator...\n");
					m_pExtrapolator = new CExtrapolator();
					m_pExtrapolator->Initialize(
						cfr->m_iRes[0],     // resx,
						cfr->m_iRes[1],     // resy,
						cfr->m_iRes[2],     // resz,
						cextrasrangex,      // srangex,
						cextrasrangey,      // srangey,
						cextrasrangez,      // srangez,
						cextratrange,       // trange,
						cextrasorder,       // sorder,
						cextratorder,       // torder,
						cextraoffsetx,      // offsetx,
						cextraoffsety,      // offsety,
						cextraoffsetz,      // offsetz,
						cextracrosss,       // crosss,
						cextracrosst,       // crosst,
						cextrawrap,         // wrap,
						cextracrossranges,  // crossranges,
						cextracrossranget,  // crossranget
						cextradistexpo,     // distexpo
						cextratimeexpo,     // timeexpo
						1.0,                // corrfactor
						verbose2,
						!verbose1
					);
				}
			}
		}

		if (m_pExtrapolator != NULL)
			m_pExtrapolator->PushCubeFrame(cfr);

		if (i < corder)
			co = i;
		else
			co = corder;

		if ((i+1 > cextratrange) || (m_iOptIncludeFirst > 0)) {

			bscu.Clear();

			CompressCubeFrame(
				&bscu,
				co,
				eps,
				csigni,
				csplit,
				cblock,
				ctables,
				optctables,
				hilbert,
				nbhfac,
				ccoderun,
				cbw,
				cmtf,
				cpreopt&&(i>=2),
				cmaxiter,
				(i==0),
				cmaxchunk,
				verbose2,
				!realsize,
				&tresi,
				tciaa
			);

			resi += tresi;

			tf++;

			if (realsize) {
				tb += bscu.GetByteLength();
/*				if (gc_fResiCorrFrame != NULL) {
					mfprintf(gc_fResiCorrFrame,"%.3f;  %.3f; %d\n", bscu.GetByteLength()/1024.0, mypow( tresi / rxyz, 1.0/m_fEntropyExpoCube ), gc_iHistogramCounter-1 );
					fflush(gc_fResiCorrFrame);
				}*/
			}
		}

		i++;
		if (m_pReadCacheCube->GetCachedSteps() == 0)
			break;
	}

	if (m_pExtrapolator != NULL) {
		delete m_pExtrapolator;
		m_pExtrapolator = NULL;
	}

	if (m_pExtrapolatorCorr != NULL) {
		delete m_pExtrapolatorCorr;
		m_pExtrapolatorCorr = NULL;
	}

	if (m_pExtrapolatorXYZ != NULL) {
		delete m_pExtrapolatorXYZ;
		m_pExtrapolatorXYZ = NULL;
	}

	resi = pow2( resi / (tf * rxyz) );
	//resi = mypow( resi / (tf * rxyz), 1.0/m_fEntropyExpoCube );

	if (presi != NULL)
		*presi = resi;

	if (realsize) {
/*		if (gc_fResiCorrTraj != NULL) {
			mfprintf(gc_fResiCorrTraj,"%.4f;  %.4f\n", tb / tf / 1024.0, resi );
			fflush(gc_fResiCorrTraj);
		}*/
		return tb / tf / 1024.0;
	} else
		return resi;
}


double CCCEngine::OptimizeCube_ObjectiveTExpo(double texpo) {

	double entropy, resi;

	entropy = CompressCubeBenchmark(
		gc_iOptCubeOrder,        // int corder,
		gc_iOptCubeEps,          // int eps,
		gc_iOptCubeSigni,        // int csigni,
		gc_iOptCubeSplit,        // int csplit,
		gc_iOptCubeBlock,        // int cblock,
		gc_iOptCubeTables,       // int ctables,
		gc_bOptCubeOptTables,    // bool optctables,
		gc_bOptCubePreOpt,       // bool cpreopt,
		gc_iOptCubeMaxIter,      // int cmaxiter,
		gc_iOptCubeMaxChunk,     // int cmaxchunk,
		gc_bOptCubeHilbert,      // bool hilbert,
		gc_fOptCubeNbhFac,       // double nbhfac,
		gc_bOptCubeCoderun,      // bool ccoderun,
		gc_bOptCubeBW,           // bool cbw,
		gc_bOptCubeMTF,          // bool cmtf,
		gc_bOptCubeUseExtra,     // bool usecextra,
		gc_iOptCubeSRangeX,      // int cextrasrangex,
		gc_iOptCubeSRangeY,      // int cextrasrangey,
		gc_iOptCubeSRangeZ,      // int cextrasrangez,
		gc_iOptCubeTRange,       // int cextratrange,
		gc_iOptCubeSOrder,       // int cextrasorder,
		gc_iOptCubeTOrder,       // int cextratorder,
		gc_iOptCubeOffsetX,      // int cextraoffsetx,
		gc_iOptCubeOffsetY,      // int cextraoffsety,
		gc_iOptCubeOffsetZ,      // int cextraoffsetz,
		gc_bOptCubeCrossS,       // bool cextracrosss,
		gc_bOptCubeCrossT,       // bool cextracrosst,
		gc_bOptCubeWrap,         // bool cextrawrap,
		gc_bOptCubeCrossRangeS,  // bool cextracrossranges,
		gc_bOptCubeCrossRangeT,  // bool cextracrossranget,
		gc_fOptCubeDistExpo,     // double cextradistexpo,
		texpo,                   // double cextratimeexpo,
		gc_bOptCubePredCorr,     // bool cextrapredcorr,
		gc_fOptCubeCorrFactor,   // double cextracorrfactor,
		false,                   // bool verbose1,
		false,                   // bool verbose2,
		gc_bOptCubeRealSize,     // bool realsize
		&resi,                   // double *presi
		NULL                     // std::vector<std::vector<int> > *tciaa
	);

	if (gc_bOptCubeRealSize)
		mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f (%10.3f kiB)\n",
			gc_iOptCubeSRangeX,
			gc_iOptCubeSOrder,
			gc_iOptCubeTRange,
			gc_iOptCubeTOrder,
			gc_fOptCubeDistExpo,
			texpo,
			resi,
			entropy
		);
	else
		mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f\n",
			gc_iOptCubeSRangeX,
			gc_iOptCubeSOrder,
			gc_iOptCubeTRange,
			gc_iOptCubeTOrder,
			gc_fOptCubeDistExpo,
			texpo,
			entropy
		);

	return entropy;
}


double CCCEngine::OptimizeCube_ObjectiveSExpo(double sexpo) {

	double entropy, resi;

	entropy = CompressCubeBenchmark(
		gc_iOptCubeOrder,        // int corder,
		gc_iOptCubeEps,          // int eps,
		gc_iOptCubeSigni,        // int csigni,
		gc_iOptCubeSplit,        // int csplit,
		gc_iOptCubeBlock,        // int cblock,
		gc_iOptCubeTables,       // int ctables,
		gc_bOptCubeOptTables,    // bool optctables,
		gc_bOptCubePreOpt,       // bool cpreopt,
		gc_iOptCubeMaxIter,      // int cmaxiter,
		gc_iOptCubeMaxChunk,     // int cmaxchunk,
		gc_bOptCubeHilbert,      // bool hilbert,
		gc_fOptCubeNbhFac,       // double nbhfac,
		gc_bOptCubeCoderun,      // bool ccoderun,
		gc_bOptCubeBW,           // bool cbw,
		gc_bOptCubeMTF,          // bool cmtf,
		gc_bOptCubeUseExtra,     // bool usecextra,
		gc_iOptCubeSRangeX,      // int cextrasrangex,
		gc_iOptCubeSRangeY,      // int cextrasrangey,
		gc_iOptCubeSRangeZ,      // int cextrasrangez,
		gc_iOptCubeTRange,       // int cextratrange,
		gc_iOptCubeSOrder,       // int cextrasorder,
		gc_iOptCubeTOrder,       // int cextratorder,
		gc_iOptCubeOffsetX,      // int cextraoffsetx,
		gc_iOptCubeOffsetY,      // int cextraoffsety,
		gc_iOptCubeOffsetZ,      // int cextraoffsetz,
		gc_bOptCubeCrossS,       // bool cextracrosss,
		gc_bOptCubeCrossT,       // bool cextracrosst,
		gc_bOptCubeWrap,         // bool cextrawrap,
		gc_bOptCubeCrossRangeS,  // bool cextracrossranges,
		gc_bOptCubeCrossRangeT,  // bool cextracrossranget,
		sexpo,                   // double cextradistexpo,
		gc_fOptCubeTimeExpo,     // double cextratimeexpo,
		gc_bOptCubePredCorr,     // bool cextrapredcorr,
		gc_fOptCubeCorrFactor,   // double cextracorrfactor,
		false,                   // bool verbose1,
		false,                   // bool verbose2,
		gc_bOptCubeRealSize,     // bool realsize
		&resi,                   // double *presi
		NULL                     // std::vector<std::vector<int> > *tciaa
	);

	if (gc_bOptCubeRealSize)
		mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f (%10.3f kiB)\n",
			gc_iOptCubeSRangeX,
			gc_iOptCubeSOrder,
			gc_iOptCubeTRange,
			gc_iOptCubeTOrder,
			sexpo,
			gc_fOptCubeTimeExpo,
			resi,
			entropy
		);
	else
		mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f\n",
			gc_iOptCubeSRangeX,
			gc_iOptCubeSOrder,
			gc_iOptCubeTRange,
			gc_iOptCubeTOrder,
			sexpo,
			gc_fOptCubeTimeExpo,
			entropy
		);

	return entropy;
}


class VARPARAMS{
public:
	VARPARAMS(int srx, int sry, int srz, int so, int ofx, int ofy, int ofz, int tr, int to, double de, double te)
		: srangex(srx), srangey(sry), srangez(srz), sorder(so), offsetx(ofx), offsety(ofy), offsetz(ofz),
		  trange(tr), torder(to), distexp(de), timeexp(te) { }
	bool operator < (const VARPARAMS& v) const { UNUSED(v); return true; }
	int srangex;
	int srangey;
	int srangez;
	int sorder;
	int offsetx;
	int offsety;
	int offsetz;
	int trange;
	int torder;
	double distexp;
	double timeexp;
};


bool CCCEngine::OptimizeCubeParameters(
		int     level,
		int    &corder,
		int     eps,
		int     csigni,
		int    &csplit,
		int    &cblock,
		int    &ctables,
		bool   &optctables,
		bool   &cpreopt,
		int    &cmaxiter,
		int    &cmaxchunk,
		bool   &hilbert,
		double &nbhfac,
		bool   &ccoderun,
		bool   &cbw,
		bool   &cmtf,
		bool   &usecextra,
		int    &cextrasrangex,
		int    &cextrasrangey,
		int    &cextrasrangez,
		int    &cextratrange,
		int    &cextrasorder,
		int    &cextratorder,
		int    &cextraoffsetx,
		int    &cextraoffsety,
		int    &cextraoffsetz,
		bool   &cextracrosss,
		bool   &cextracrosst,
		bool   &cextrawrap,
		bool   &cextracrossranges,
		bool   &cextracrossranget,
		double &cextradistexpo,
		double &cextratimeexpo,
		bool   &cextrapredcorr,
		double &cextracorrfactor,
		bool    verbose
	) {

	int steps, z, z2, z3, ti, zsr,zso, btr, bto, bsrx, bsry, bsrz, bsofx, bsofy, bsofz, bso, ca, bi;
	int zsrx, zsry, zsrz, zsofx, zsofy, zsofz;
	int tia[12];
	double tf, tflast, btv, resi, tfa[12], tfa2[12], fac;
	bool realsize;
	unsigned long t0;
	const CCubeFrame *cfr;
	CxString buf;
	std::vector<std::pair<double,std::string> > rlist;
	std::vector<std::pair<double,VARPARAMS> > rlist2, rlist3;
	std::vector<std::vector<int> > tciaa;
	std::vector<int> iatc, iabl;
	CBitSet bs;
	CIntegerEngine *tie;


	mprintf(WHITE,"    >>> Volumetric Trajectory Parameter Optimization >>>\n");

/*	if (level == 3) {
		gc_fResiCorrFrame = OpenFileWrite("corrframecube.csv",true);
		gc_fResiCorrTraj = OpenFileWrite("corrtrajcube.csv",true);
	}*/

	if (level >= 3)
		realsize = true;
	else
		realsize = false;

	t0 = (unsigned long)time(NULL);

	m_pReadCacheCube->RewindReadPos();

	cfr = m_pReadCacheCube->GetNextFrame();

	if (hilbert)
		BuildHilbertIdx(cfr->m_iRes[0],cfr->m_iRes[1],cfr->m_iRes[2],verbose);

	m_pReadCacheCube->RewindReadPos();

	steps = m_pReadCacheCube->GetCachedSteps();

	mprintf("        Optimization level %d\n",level);
	//mprintf("        Entropy Exponent %.3f\n",m_fEntropyExpoCube);
	mprintf("        Have %d cached steps.\n",steps);

	mprintf("        SR  SO  TR  TO  DistExp  TimeExp        Residuum\n");

	tflast = 0;
	btv = 1.0e30;
	tf = 0;
	btr = -1;
	bto = -1;
	bsrx = -1;
	bsry = -1;
	bsrz = -1;
	bsofx = -1;
	bsofy = -1;
	bsofz = -1;
	bso = -1;

	usecextra = true;
	//cextratimeexpo = 1.0;
	//cextradistexpo = 3.0;
	cextrasorder = 0;
	cextrasrangex = 1;
	cextrasrangey = 1;
	cextrasrangez = 1;
	cextraoffsetx = 0;
	cextraoffsety = 0;
	cextraoffsetz = 0;

	if (level == 2)
		ctables = 1;

	/********************* Separate Code Block for Level 1 *************************************************/
	if (level == 1) {

		rlist2.clear();

		switch(steps) {
			case 1: //                                                       RX RY RZ SO OX OY OZ TR TO  SE   TE
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 6, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 5, 7, 7, 3, 2, 2, 2, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 5, 5, 5, 2, 2, 2, 2, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 3, 3, 3, 2, 1, 1, 1, 1, 0, 3.5, 1.5 ) ) );
				break;
			case 2: //                                                       RX RY RZ SO OX OY OZ TR TO  SE   TE
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 5, 5, 5, 3, 2, 2, 2, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 3, 3, 3, 2, 1, 1, 1, 2, 0, 3.5, 1.5 ) ) );
				break;
			case 3: //                                                       RX RY RZ SO OX OY OZ TR TO  SE   TE
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 3, 1, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 5, 5, 5, 3, 2, 2, 2, 3, 1, 3.5, 1.5 ) ) );
				break;
			case 4: //                                                       RX RY RZ SO OX OY OZ TR TO  SE   TE
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 4, 1, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 5, 5, 5, 3, 2, 2, 2, 4, 1, 3.5, 1.5 ) ) );
				break;
			default: //                                                      RX RY RZ SO OX OY OZ TR TO  SE   TE
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 5, 1, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 4, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 1, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 7, 7, 7, 5, 3, 3, 3, 2, 0, 3.5, 1.5 ) ) );
				rlist2.push_back( std::pair<double, VARPARAMS>( 0, VARPARAMS( 5, 5, 5, 3, 2, 2, 2, 4, 1, 3.5, 1.5 ) ) );
				break;
		}

		for (z=0;z<(int)rlist2.size();z++) {

			tf = CompressCubeBenchmark(
				corder,                    // int corder,
				eps,                       // int eps,
				csigni,                    // int csigni,
				csplit,                    // int csplit,
				cblock,                    // int cblock,
				ctables,                   // int ctables,
				optctables,                // bool optctables,
				cpreopt,                   // bool cpreopt,
				cmaxiter,                  // int cmaxiter,
				cmaxchunk,                 // int cmaxchunk,
				hilbert,                   // bool hilbert,
				nbhfac,                    // double nbhfac,
				ccoderun,                  // bool ccoderun,
				cbw,                       // bool cbw,
				cmtf,                      // bool cmtf,
				usecextra,                 // bool usecextra,
				rlist2[z].second.srangex,  // int cextrasrangex,
				rlist2[z].second.srangey,  // int cextrasrangey,
				rlist2[z].second.srangez,  // int cextrasrangez,
				rlist2[z].second.trange,   // int cextratrange,
				rlist2[z].second.sorder,   // int cextrasorder,
				rlist2[z].second.torder,   // int cextratorder,
				rlist2[z].second.offsetx,  // int cextraoffsetx,
				rlist2[z].second.offsety,  // int cextraoffsety,
				rlist2[z].second.offsetz,  // int cextraoffsetz,
				cextracrosss,              // bool cextracrosss,
				cextracrosst,              // bool cextracrosst,
				cextrawrap,                // bool cextrawrap,
				cextracrossranges,         // bool cextracrossranges,
				cextracrossranget,         // bool cextracrossranget,
				rlist2[z].second.distexp,  // double cextradistexpo,
				rlist2[z].second.timeexp,  // double cextratimeexpo,
				cextrapredcorr,            // bool cextrapredcorr,
				cextracorrfactor,          // double cextracorrfactor,
				verbose,                   // bool verbose1,
				false,                     // bool verbose2,
				false,                     // bool realsize
				&resi,                     // double *presi
				NULL                       // std::vector<std::vector<int> > *tciaa
			);

			mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f",
				rlist2[z].second.srangex,rlist2[z].second.sorder,rlist2[z].second.trange,
				rlist2[z].second.torder,rlist2[z].second.distexp,rlist2[z].second.timeexp,tf);

			if (tf < btv) {
				btv = tf;
				btr = z;
				mprintf("  <---");
			}

			mprintf("\n");
		}

		cextrasrangex = rlist2[btr].second.srangex;
		cextrasrangey = rlist2[btr].second.srangey;
		cextrasrangez = rlist2[btr].second.srangez;
		cextrasorder = rlist2[btr].second.sorder;
		cextratrange = rlist2[btr].second.trange;
		cextratorder = rlist2[btr].second.torder;
		cextraoffsetx = rlist2[btr].second.offsetx;
		cextraoffsety = rlist2[btr].second.offsety;
		cextraoffsetz = rlist2[btr].second.offsetz;
		cextratimeexpo = rlist2[btr].second.timeexp;
		cextradistexpo = rlist2[btr].second.distexp;
		tflast = cextratimeexpo;

		if (cextratrange != cextratorder+2) {

			tfa[0] = 0.5;
			tfa[1] = 1.0;

			for (z=0;z<2;z++) {

				tfa2[z] = CompressCubeBenchmark(
					corder,             // int corder,
					eps,                // int eps,
					csigni,             // int csigni,
					csplit,             // int csplit,
					cblock,             // int cblock,
					ctables,            // int ctables,
					optctables,         // bool optctables,
					cpreopt,            // bool cpreopt,
					cmaxiter,           // int cmaxiter,
					cmaxchunk,          // int cmaxchunk,
					hilbert,            // bool hilbert,
					nbhfac,             // double nbhfac,
					ccoderun,           // bool ccoderun,
					cbw,                // bool cbw,
					cmtf,               // bool cmtf,
					usecextra,          // bool usecextra,
					cextrasrangex,      // int cextrasrangex,
					cextrasrangey,      // int cextrasrangey,
					cextrasrangez,      // int cextrasrangez,
					cextratrange,       // int cextratrange,
					cextrasorder,       // int cextrasorder,
					cextratorder,       // int cextratorder,
					cextraoffsetx,      // int cextraoffsetx,
					cextraoffsety,      // int cextraoffsety,
					cextraoffsetz,      // int cextraoffsetz,
					cextracrosss,       // bool cextracrosss,
					cextracrosst,       // bool cextracrosst,
					cextrawrap,         // bool cextrawrap,
					cextracrossranges,  // bool cextracrossranges,
					cextracrossranget,  // bool cextracrossranget,
					cextradistexpo,     // double cextradistexpo,
					tfa[z],             // double cextratimeexpo,
					cextrapredcorr,     // bool cextrapredcorr,
					cextracorrfactor,   // double cextracorrfactor,
					verbose,            // bool verbose1,
					false,              // bool verbose2,
					false,              // bool realsize
					&resi,              // double *presi
					NULL                // std::vector<std::vector<int> > *tciaa
				);

				mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f\n",
					cextrasrangex,cextrasorder,cextratrange,cextratorder,cextradistexpo,tfa[z],tfa2[z]);
			}

			if ((tfa2[0] < tfa2[1]) && (tfa2[0] < btv))
				tflast = tfa[0];
			else if ((tfa2[1] < tfa2[0]) && (tfa2[1] < btv))
				tflast = tfa[1];
		}

	
		tfa[0] = 4.5;
		tfa[1] = 6.0;

		for (z=0;z<2;z++) {

			tfa2[z] = CompressCubeBenchmark(
				corder,             // int corder,
				eps,                // int eps,
				csigni,             // int csigni,
				csplit,             // int csplit,
				cblock,             // int cblock,
				ctables,            // int ctables,
				optctables,         // bool optctables,
				cpreopt,            // bool cpreopt,
				cmaxiter,           // int cmaxiter,
				cmaxchunk,          // int cmaxchunk,
				hilbert,            // bool hilbert,
				nbhfac,             // double nbhfac,
				ccoderun,           // bool ccoderun,
				cbw,                // bool cbw,
				cmtf,               // bool cmtf,
				usecextra,          // bool usecextra,
				cextrasrangex,      // int cextrasrangex,
				cextrasrangey,      // int cextrasrangey,
				cextrasrangez,      // int cextrasrangez,
				cextratrange,       // int cextratrange,
				cextrasorder,       // int cextrasorder,
				cextratorder,       // int cextratorder,
				cextraoffsetx,      // int cextraoffsetx,
				cextraoffsety,      // int cextraoffsety,
				cextraoffsetz,      // int cextraoffsetz,
				cextracrosss,       // bool cextracrosss,
				cextracrosst,       // bool cextracrosst,
				cextrawrap,         // bool cextrawrap,
				cextracrossranges,  // bool cextracrossranges,
				cextracrossranget,  // bool cextracrossranget,
				tfa[z],             // double cextradistexpo,
				cextratimeexpo,     // double cextratimeexpo,
				cextrapredcorr,     // bool cextrapredcorr,
				cextracorrfactor,   // double cextracorrfactor,
				verbose,            // bool verbose1,
				false,              // bool verbose2,
				false,              // bool realsize
				&resi,              // double *presi
				NULL                // std::vector<std::vector<int> > *tciaa
			);

			mprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f\n",
				cextrasrangex,cextrasorder,cextratrange,cextratorder,tfa[z],cextratimeexpo,tfa2[z]);
		}

		if ((tfa2[0] < tfa2[1]) && (tfa2[0] < btv))
			cextradistexpo = tfa[0];
		else if ((tfa2[1] < tfa2[0]) && (tfa2[1] < btv))
			cextradistexpo = tfa[1];

		cextratimeexpo = tflast;

		goto _end;
	}
	/********************* End of Separate Code Block for Level 1 ******************************************/

	for (z=0;z<3;z++)
		tfa[z] = 1.0e30;

	for (z=0;z<3;z++) {

		if (z+1 > steps)
			break;

		tfa[z] = CompressCubeBenchmark(
			corder,             // int corder,
			eps,                // int eps,
			csigni,             // int csigni,
			csplit,             // int csplit,
			cblock,             // int cblock,
			ctables,            // int ctables,
			optctables,         // bool optctables,
			cpreopt,            // bool cpreopt,
			cmaxiter,           // int cmaxiter,
			cmaxchunk,          // int cmaxchunk,
			hilbert,            // bool hilbert,
			nbhfac,             // double nbhfac,
			ccoderun,           // bool ccoderun,
			cbw,                // bool cbw,
			cmtf,               // bool cmtf,
			usecextra,          // bool usecextra,
			1,                  // int cextrasrangex,
			1,                  // int cextrasrangey,
			1,                  // int cextrasrangez,
			z+1,                // int cextratrange,
			0,                  // int cextrasorder,
			MAX(z-1,0),         // int cextratorder,
			0,                  // int cextraoffsetx,
			0,                  // int cextraoffsety,
			0,                  // int cextraoffsetz,
			cextracrosss,       // bool cextracrosss,
			cextracrosst,       // bool cextracrosst,
			cextrawrap,         // bool cextrawrap,
			cextracrossranges,  // bool cextracrossranges,
			cextracrossranget,  // bool cextracrossranget,
			cextradistexpo,     // double cextradistexpo,
			cextratimeexpo,     // double cextratimeexpo,
			cextrapredcorr,     // bool cextrapredcorr,
			cextracorrfactor,   // double cextracorrfactor,
			verbose,            // bool verbose1,
			false,              // bool verbose2,
			realsize,           // bool realsize
			&resi,              // double *presi
			NULL                // std::vector<std::vector<int> > *tciaa
		);

		if (realsize)
			buf.sprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f (%10.3f kiB)",
				1,0,z+1,MAX(z-1,0),cextradistexpo,cextratimeexpo,resi,tfa[z]);
		else
			buf.sprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f",
				1,0,z+1,MAX(z-1,0),cextradistexpo,cextratimeexpo,tfa[z]);

		mprintf("%s",(const char*)buf);

		if (tfa[z] < btv) {
			btv = tfa[z];
			bto = MAX(z-1,0);
			btr = z+1;
			bsrx = 1;
			bsry = 1;
			bsrz = 1;
			bso = 0;
			bsofx = 0;
			bsofy = 0;
			bsofz = 0;
			mprintf("  <---");
		}

		mprintf("\n");

		rlist.push_back(std::pair<double,std::string>(tfa[z],(const char*)buf));

		rlist2.push_back(std::pair<double,VARPARAMS>(tfa[z],VARPARAMS(1,1,1,0,0,0,0,z+1,MAX(z-1,0),cextradistexpo,cextratimeexpo)));
	}

	if ((tfa[2]+0.001 < tfa[0]) && (tfa[2]+0.001 < tfa[1])) {
		mprintf("        ---> Time-continuous case.\n");
		ca = 0;
	} else if ((tfa[1]+0.001 < tfa[0]) && (tfa[1]+0.001 < tfa[2])) {
		mprintf("        ---> Semi-time-continuous case.\n");
		ca = 1;
	} else {
		mprintf("        ---> Time-discontinuous case.\n");
		ca = 2;
	}

	if (level == 4) {

		mprintf("        SRX  SRY  SRZ  OFX  OFY  OFZ  SO  TR  TO  DistExp  TimeExp        Residuum\n");

		bi = 0;

		for (zsrx=1;zsrx<10;zsrx++) {

			for (zsry=1;zsry<10;zsry++) {

				for (zsrz=1;zsrz<10;zsrz++) {

					if (MAX3(zsrx,zsry,zsrz) - MIN3(zsrx,zsry,zsrz) > 2)
						continue;

					for (zsofx=0;zsofx<zsrx;zsofx++) {

						for (zsofy=0;zsofy<zsry;zsofy++) {

							for (zsofz=0;zsofz<zsrz;zsofz++) {

								if (MAX3(zsrx,zsry,zsrz) <= 3) {
									if (MAX3(abs(zsofx-(zsrx/2)),abs(zsofy-(zsry/2)),abs(zsofz-(zsrz/2))) > 0)
										continue;
								} else if (MAX3(zsrx,zsry,zsrz) <= 5) {
									if (MAX3(abs(zsofx-(zsrx/2)),abs(zsofy-(zsry/2)),abs(zsofz-(zsrz/2))) > 1)
										continue;
								} else {
									if (MAX3(abs(zsofx-(zsrx/2)),abs(zsofy-(zsry/2)),abs(zsofz-(zsrz/2))) > 2)
										continue;
								}

								for (zso=0;zso<MIN(MAX3(zsrx,zsry,zsrz),9);zso++) {

									if (bi > 1)
										mprintf("\n");

									bi = 0;

									if (((zsrx >= 5) || (zsry >= 5) || (zsrz >= 5)) && (zso == 0))
										continue;

									if (((zsrx >= 7) || (zsry >= 7) || (zsrz >= 7)) && (zso < 2))
										continue;

									if (((zsrx == 9) || (zsry == 9) || (zsrz == 9)) && (zso < 3))
										continue;

									for (z=1;z<MIN(steps+1,7);z++) { // trange

										for (z2=z-2;z2>=-1;z2--) { // torder

											if ((z > 1) && (z2 == -1))
												continue;
											if ((z > 3) && (z2 == 0))
												continue;

											// The three cases from above
											if ((zsrx == 1) && (zsry == 1) && (zsrz == 1) && (z == 1) && (z2 == -1))
												continue;
											if ((zsrx == 1) && (zsry == 1) && (zsrz == 1) && (z == 2) && (z2 == 0))
												continue;
											if ((zsrx == 1) && (zsry == 1) && (zsrz == 1) && (z == 3) && (z2 == 1))
												continue;

											if (ca == 0) {

												if ((z2 < 0) || ((z2 < 1) && (z > 3)))
													continue;

											} else if (ca == 1) {

												if ((z2 < 0) || (z2 > 1))
													continue;

											} else {

												if (z2 > -1)
													continue;
											}

											tflast = tf;

											tf = CompressCubeBenchmark(
												corder,             // int corder,
												eps,                // int eps,
												csigni,             // int csigni,
												csplit,             // int csplit,
												cblock,             // int cblock,
												ctables,            // int ctables,
												optctables,         // bool optctables,
												cpreopt,            // bool cpreopt,
												cmaxiter,           // int cmaxiter,
												cmaxchunk,          // int cmaxchunk,
												hilbert,            // bool hilbert,
												nbhfac,             // double nbhfac,
												ccoderun,           // bool ccoderun,
												cbw,                // bool cbw,
												cmtf,               // bool cmtf,
												usecextra,          // bool usecextra,
												zsrx,               // int cextrasrangex,
												zsry,               // int cextrasrangey,
												zsrz,               // int cextrasrangez,
												z,                  // int cextratrange,
												zso,                // int cextrasorder,
												z2,                 // int cextratorder,
												zsofx,              // int cextraoffsetx,
												zsofy,              // int cextraoffsety,
												zsofz,              // int cextraoffsetz,
												cextracrosss,       // bool cextracrosss,
												cextracrosst,       // bool cextracrosst,
												cextrawrap,         // bool cextrawrap,
												cextracrossranges,  // bool cextracrossranges,
												cextracrossranget,  // bool cextracrossranget,
												cextradistexpo,     // double cextradistexpo,
												cextratimeexpo,     // double cextratimeexpo,
												cextrapredcorr,     // bool cextrapredcorr,
												cextracorrfactor,   // double cextracorrfactor,
												verbose,            // bool verbose1,
												false,              // bool verbose2,
												realsize,           // bool realsize
												&resi,              // double *presi
												NULL                // std::vector<std::vector<int> > *tciaa
											);

											if (realsize)
												buf.sprintf("        %3d  %3d  %3d  %3d  %3d  %3d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f (%10.3f kiB)",
													zsrx,zsry,zsrz,zsofx,zsofy,zsofz,zso,z,z2,cextradistexpo,cextratimeexpo,resi,tf);
											else
												buf.sprintf("        %3d  %3d  %3d  %3d  %3d  %3d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f",
													zsrx,zsry,zsrz,zsofx,zsofy,zsofz,zso,z,z2,cextradistexpo,cextratimeexpo,tf);

											mprintf("%s",(const char*)buf);

											bi++;

											rlist.push_back(std::pair<double,std::string>(tf,(const char*)buf));

											rlist2.push_back(std::pair<double,VARPARAMS>(tf,VARPARAMS(zsrx,zsry,zsrz,zso,zsofx,zsofy,zsofz,z,z2,cextradistexpo,cextratimeexpo)));

											if (tf < btv) {
												btv = tf;
												bto = z2;
												btr = z;
												bsrx = zsrx;
												bsry = zsry;
												bsrz = zsrz;
												bsofx = zsofx;
												bsofy = zsofy;
												bsofz = zsofz;
												bso = zso;
												mprintf("  <---");
											}

											mprintf("\n");

											if ((z2 < z-2) && (tflast < tf))
												break;
										}
									}
								}
							}
						}
					}
				}
			}
		}


	} else {


		bi = 0;

		for (zsr=1;zsr<10;zsr+=2) {

			if ((zsr == 9) && (level < 3))
				break;

			for (zso=0;zso<MIN(zsr,9);zso++) {

				if (bi > 1)
					mprintf("\n");

				bi = 0;

				if ((zsr >= 5) && (zso == 0))
					continue;

				if ((zsr >= 7) && (zso < 2))
					continue;

				if ((zsr == 9) && (zso < 3))
					continue;

				for (z=1;z<MIN(steps+1,7);z++) { // trange

					if ((level != 3) && (z > 2)) {
						if (zso > 4)
							continue;
						if ((zsr == 5) && (zso == 4))
							continue;
					}

					for (z2=z-2;z2>=-1;z2--) { // torder

						if ((z > 1) && (z2 == -1))
							continue;
						if ((z > 3) && (z2 == 0))
							continue;

						// The three cases from above
						if ((zsr == 1) && (z == 1) && (z2 == -1))
							continue;
						if ((zsr == 1) && (z == 2) && (z2 == 0))
							continue;
						if ((zsr == 1) && (z == 3) && (z2 == 1))
							continue;

						if (ca == 0) {

							if ((z2 < 0) || ((z2 < 1) && (z > 3)))
								continue;

						} else if (ca == 1) {

							if ((z2 < 0) || (z2 > 1))
								continue;

						} else {

							if (z2 > -1)
								continue;
						}

						tflast = tf;

						tf = CompressCubeBenchmark(
							corder,             // int corder,
							eps,                // int eps,
							csigni,             // int csigni,
							csplit,             // int csplit,
							cblock,             // int cblock,
							ctables,            // int ctables,
							optctables,         // bool optctables,
							cpreopt,            // bool cpreopt,
							cmaxiter,           // int cmaxiter,
							cmaxchunk,          // int cmaxchunk,
							hilbert,            // bool hilbert,
							nbhfac,             // double nbhfac,
							ccoderun,           // bool ccoderun,
							cbw,                // bool cbw,
							cmtf,               // bool cmtf,
							usecextra,          // bool usecextra,
							zsr,                // int cextrasrangex,
							zsr,                // int cextrasrangey,
							zsr,                // int cextrasrangez,
							z,                  // int cextratrange,
							zso,                // int cextrasorder,
							z2,                 // int cextratorder,
							zsr/2,              // int cextraoffsetx,
							zsr/2,              // int cextraoffsety,
							zsr/2,              // int cextraoffsetz,
							cextracrosss,       // bool cextracrosss,
							cextracrosst,       // bool cextracrosst,
							cextrawrap,         // bool cextrawrap,
							cextracrossranges,  // bool cextracrossranges,
							cextracrossranget,  // bool cextracrossranget,
							cextradistexpo,     // double cextradistexpo,
							cextratimeexpo,     // double cextratimeexpo,
							cextrapredcorr,     // bool cextrapredcorr,
							cextracorrfactor,   // double cextracorrfactor,
							verbose,            // bool verbose1,
							false,              // bool verbose2,
							realsize,           // bool realsize
							&resi,              // double *presi
							NULL                // std::vector<std::vector<int> > *tciaa
						);

						if (realsize)
							buf.sprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f (%10.3f kiB)",
								zsr,zso,z,z2,cextradistexpo,cextratimeexpo,resi,tf);
						else
							buf.sprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f",
								zsr,zso,z,z2,cextradistexpo,cextratimeexpo,tf);

						mprintf("%s",(const char*)buf);

						bi++;

						rlist.push_back(std::pair<double,std::string>(tf,(const char*)buf));

						rlist2.push_back(std::pair<double,VARPARAMS>(tf,VARPARAMS(zsr,zsr,zsr,zso,zsr/2,zsr/2,zsr/2,z,z2,cextradistexpo,cextratimeexpo)));

						if (tf < btv) {
							btv = tf;
							bto = z2;
							btr = z;
							bsrx = zsr;
							bsry = zsr;
							bsrz = zsr;
							bsofx = zsr/2;
							bsofy = zsr/2;
							bsofz = zsr/2;
							bso = zso;
							mprintf("  <---");
						}

						mprintf("\n");

						if ((z2 < z-2) && (tflast < tf))
							break;
					}
				}
			}
		}
	}

	std::sort(rlist.begin(),rlist.end());

/*	FILE *a;
	a = OpenFileWrite("opticube_list.txt",true);
	mfprintf(a,"        sr  so  tr  to  distexp  timeexp       residuum\n");
	for (z=0;z<(int)rlist.size();z++)
		mfprintf(a,"%s\n",rlist[z].second.c_str());
	fclose(a);*/

	if (level == 2) {

		mprintf("        Evaluating best 10:\n");

		std::sort(rlist2.begin(),rlist2.end());

		for (z=0;z<10;z++)
			rlist3.push_back(rlist2[z]);

		int tia[4];

		for (z=0;z<4;z++) tia[z] = 0;

		for (z=0;z<(int)rlist3.size();z++)
			tia[rlist3[z].second.srangex/2]++;

		// Make sure that at least 2 sets from each srange are included
		for (z=0;z<4;z++) {
			if ((tia[z] < 1) || ((z > 0) && (tia[z] < 2))) {
				for (z2=10;z2<(int)rlist2.size();z2++) {
					if ((rlist2[z2].second.srangex/2) == z) {
						rlist3.push_back(rlist2[z2]);
						tia[z]++;
						if ((tia[z] >= 2) || ((z == 0) && (tia[z] == 1)))
							break;
					}
				}
			}
		}

		// Crop the total number back to 10
		if (rlist3.size() > 10) {
			for (z=(int)rlist3.size()-1;z>=0;z--) {
				if ((tia[rlist3[z].second.srangex/2] > 2) || ((rlist3[z].second.srangex == 1) && (tia[rlist3[z].second.srangex/2] > 1))) {
					tia[rlist3[z].second.srangex/2]--;
					rlist3.erase(rlist3.begin()+z);
				}
				if (rlist3.size() == 10)
					break;
			}
		}

		std::sort(rlist3.begin(),rlist3.end());

		btr = -1;
		btv = 1.0e30;
		for (z=0;z<(int)rlist3.size();z++) {

			tf = CompressCubeBenchmark(
				corder,                    // int corder,
				eps,                       // int eps,
				csigni,                    // int csigni,
				csplit,                    // int csplit,
				cblock,                    // int cblock,
				ctables,                   // int ctables,
				optctables,                // bool optctables,
				cpreopt,                   // bool cpreopt,
				cmaxiter,                  // int cmaxiter,
				cmaxchunk,                 // int cmaxchunk,
				hilbert,                   // bool hilbert,
				nbhfac,                    // double nbhfac,
				ccoderun,                  // bool ccoderun,
				cbw,                       // bool cbw,
				cmtf,                      // bool cmtf,
				usecextra,                 // bool usecextra,
				rlist3[z].second.srangex,  // int cextrasrangex,
				rlist3[z].second.srangey,  // int cextrasrangey,
				rlist3[z].second.srangez,  // int cextrasrangez,
				rlist3[z].second.trange,   // int cextratrange,
				rlist3[z].second.sorder,   // int cextrasorder,
				rlist3[z].second.torder,   // int cextratorder,
				rlist3[z].second.offsetx,  // int cextraoffsetx,
				rlist3[z].second.offsety,  // int cextraoffsety,
				rlist3[z].second.offsetz,  // int cextraoffsetz,
				cextracrosss,              // bool cextracrosss,
				cextracrosst,              // bool cextracrosst,
				cextrawrap,                // bool cextrawrap,
				cextracrossranges,         // bool cextracrossranges,
				cextracrossranget,         // bool cextracrossranget,
				rlist3[z].second.distexp,  // double cextradistexpo,
				rlist3[z].second.timeexp,  // double cextratimeexpo,
				cextrapredcorr,            // bool cextrapredcorr,
				cextracorrfactor,          // double cextracorrfactor,
				verbose,                   // bool verbose1,
				false,                     // bool verbose2,
				true,                      // bool realsize
				&resi,                     // double *presi
				NULL                       // std::vector<std::vector<int> > *tciaa
			);

			buf.sprintf("        %2d  %2d  %2d  %2d   %6.3f   %6.3f  %14.4f (%10.3f kiB)",
				rlist3[z].second.srangex,rlist3[z].second.sorder,rlist3[z].second.trange,
				rlist3[z].second.torder,rlist3[z].second.distexp,rlist3[z].second.timeexp,resi,tf);

			mprintf("%s",(const char*)buf);

			if (tf < btv) {
				btv = tf;
				btr = z;
				mprintf("  <---");
			}

			mprintf("\n");
		}

		cextrasrangex = rlist3[btr].second.srangex;
		cextrasrangey = rlist3[btr].second.srangey;
		cextrasrangez = rlist3[btr].second.srangez;
		cextrasorder = rlist3[btr].second.sorder;
		cextratrange = rlist3[btr].second.trange;
		cextratorder = rlist3[btr].second.torder;
		cextraoffsetx = rlist3[btr].second.offsetx;
		cextraoffsety = rlist3[btr].second.offsety;
		cextraoffsetz = rlist3[btr].second.offsetz;

	} else {

		cextrasrangex = bsrx;
		cextrasrangey = bsry;
		cextrasrangez = bsrz;
		cextrasorder = bso;
		cextratrange = btr;
		cextratorder = bto;
		cextraoffsetx = bsofx;
		cextraoffsety = bsofy;
		cextraoffsetz = bsofz;
	}

	if (level >= 2) {

		gc_iOptCubeOrder = corder;
		gc_iOptCubeEps = eps;
		gc_iOptCubeSigni = csigni;
		gc_bOptCubeHilbert = hilbert;
		gc_fOptCubeNbhFac = nbhfac;
		gc_bOptCubeUseExtra = usecextra;
		gc_iOptCubeSRangeX = cextrasrangex;
		gc_iOptCubeSRangeY = cextrasrangey;
		gc_iOptCubeSRangeZ = cextrasrangez;
		gc_iOptCubeTRange = cextratrange;
		gc_iOptCubeSOrder = cextrasorder;
		gc_iOptCubeTOrder = cextratorder;
		gc_iOptCubeOffsetX = cextraoffsetx;
		gc_iOptCubeOffsetY = cextraoffsety;
		gc_iOptCubeOffsetZ = cextraoffsetz;
		gc_bOptCubeCrossS = cextracrosss;
		gc_bOptCubeCrossT = cextracrosst;
		gc_bOptCubeWrap = cextrawrap;
		gc_bOptCubeCrossRangeS = cextracrossranges;
		gc_bOptCubeCrossRangeT = cextracrossranget;
		gc_fOptCubeDistExpo = cextradistexpo;
		gc_fOptCubeTimeExpo = cextratimeexpo;
		gc_bOptCubePredCorr = cextrapredcorr;
		gc_fOptCubeCorrFactor = cextracorrfactor;

		gc_iOptCubeSplit = csplit;
		gc_iOptCubeBlock = cblock;
		gc_iOptCubeTables = ctables;
		gc_bOptCubeOptTables = optctables;
		gc_bOptCubePreOpt = cpreopt;
		gc_iOptCubeMaxIter = cmaxiter;
		gc_iOptCubeMaxChunk = cmaxchunk;
		gc_bOptCubeCoderun = ccoderun;
		gc_bOptCubeBW = cbw;
		gc_bOptCubeMTF = cmtf;
		gc_bOptCubeRealSize = realsize;

		if (cextratorder+2 != cextratrange) {

			mprintf("        Optimizing time exponent:\n");

			cextratimeexpo = GoldenSectionSearch(4.0, (level>=3)?0.02:0.2, &CCCEngine::OptimizeCube_ObjectiveTExpo);

			if (level >= 3)
 				cextratimeexpo = floor(cextratimeexpo*1000.0+0.5)/1000.0;
			else
				cextratimeexpo = floor(cextratimeexpo*100.0+0.5)/100.0;

		} else
			cextratimeexpo = 0;

		gc_fOptCubeTimeExpo = cextratimeexpo;

		mprintf("        Optimizing distance exponent:\n");

		cextradistexpo = GoldenSectionSearch(8.0, (level>=3)?0.02:0.2, &CCCEngine::OptimizeCube_ObjectiveSExpo);

		if (level >= 3)
			cextradistexpo = floor(cextradistexpo*1000.0+0.5)/1000.0;
		else
			cextradistexpo = floor(cextradistexpo*100.0+0.5)/100.0;
	}

	if (level >= 2) {

		mprintf("        Optimizing Compressor settings:\n");

		tciaa.clear();

		tf = CompressCubeBenchmark(
			corder,             // int corder,
			eps,                // int eps,
			csigni,             // int csigni,
			csplit,             // int csplit,
			cblock,             // int cblock,
			ctables,            // int ctables,
			optctables,         // bool optctables,
			cpreopt,            // bool cpreopt,
			cmaxiter,           // int cmaxiter,
			cmaxchunk,          // int cmaxchunk,
			hilbert,            // bool hilbert,
			nbhfac,             // double nbhfac,
			ccoderun,           // bool ccoderun,
			cbw,                // bool cbw,
			cmtf,               // bool cmtf,
			usecextra,          // bool usecextra,
			cextrasrangex,      // int cextrasrangex,
			cextrasrangey,      // int cextrasrangey,
			cextrasrangez,      // int cextrasrangez,
			cextratrange,       // int cextratrange,
			cextrasorder,       // int cextrasorder,
			cextratorder,       // int cextratorder,
			cextraoffsetx,      // int cextraoffsetx,
			cextraoffsety,      // int cextraoffsety,
			cextraoffsetz,      // int cextraoffsetz,
			cextracrosss,       // bool cextracrosss,
			cextracrosst,       // bool cextracrosst,
			cextrawrap,         // bool cextrawrap,
			cextracrossranges,  // bool cextracrossranges,
			cextracrossranget,  // bool cextracrossranget,
			cextradistexpo,     // double cextradistexpo,
			cextratimeexpo,     // double cextratimeexpo,
			cextrapredcorr,     // bool cextrapredcorr,
			cextracorrfactor,   // double cextracorrfactor,
			verbose,            // bool verbose1,
			false,              // bool verbose2,
			true,               // bool realsize
			NULL,               // double *presi
			&tciaa              // std::vector<std::vector<int> > *tciaa
		);

		mprintf("        TC  BL   BW  MTF  Frame Size [kiB]\n");

		if (level >= 4) {

			iatc.push_back(1);
			iatc.push_back(2);
			iatc.push_back(3);
			iatc.push_back(4);
			iatc.push_back(6);
			iatc.push_back(8);
			iatc.push_back(10);
			iatc.push_back(12);
			iatc.push_back(15);
			iatc.push_back(18);
			iatc.push_back(21);
			iatc.push_back(24);

			iabl.push_back(10);
			iabl.push_back(12);
			iabl.push_back(14);
			iabl.push_back(16);
			iabl.push_back(20);
			iabl.push_back(24);
			iabl.push_back(28);
			iabl.push_back(32);
			iabl.push_back(36);
			iabl.push_back(40);
			iabl.push_back(48);
			iabl.push_back(56);
			iabl.push_back(64);

			btv = 1.0e30;
			btr = -1;

			for (z=0;z<(int)iatc.size();z++) {

				for (z2=0;z2<(int)iabl.size();z2++) {

					tf = 0;
					for (z3=0;z3<(int)tciaa.size();z3++) {

						bs.Clear();
						tie = new CIntegerEngine();
						tie->Compress(tciaa[z3],&bs,cbw,cmtf,ccoderun,iabl[z2],iatc[z],false,false,false,cmaxiter,cmaxchunk,verbose);
						delete tie;
						tf += bs.GetByteLength();
					}

					mprintf("        %2d  %2d  %3s  %3s  %10.4f",iatc[z],iabl[z2],cbw?"yes":"no",cmtf?"yes":"no",tf/tciaa.size()/1024.0);
					if (tf < btv) {
						btv = tf;
						ctables = iatc[z];
						cblock = iabl[z2];
						mprintf("  <---");
					}
					mprintf("\n");

					if (iatc[z] == 1)
						break; // One table: Block length does not matter
				}
			}

		} else {
			
			if (level >= 3) {

				ti = 11;
				tia[0] = 28;
				tia[1] = 24;
				tia[2] = 20;
				tia[3] = 16;
				tia[4] = 12;
				tia[5] = 8;
				tia[6] = 6;
				tia[7] = 4;
				tia[8] = 3;
				tia[9] = 2;
				tia[10] = 1;
			} else {
				ti = 4;
				tia[0] = 10;
				tia[1] = 6;
				tia[2] = 3;
				tia[3] = 1;
			}

			btv = 1.0e30;
			btr = -1;
			for (z=0;z<ti;z++) {

				tfa[z] = 0;
				for (z2=0;z2<(int)tciaa.size();z2++) {

					bs.Clear();
					tie = new CIntegerEngine();
					tie->Compress(tciaa[z2],&bs,cbw,cmtf,ccoderun,cblock,tia[z],false,false,false,cmaxiter,cmaxchunk,verbose);
					delete tie;
					tfa[z] += bs.GetByteLength();
				}

				mprintf("        %2d  %2d  %3s  %3s  %10.3f",tia[z],cblock,cbw?"yes":"no",cmtf?"yes":"no",tfa[z]/1024.0/tciaa.size());
				if (tfa[z] < btv) {
					btv = tfa[z];
					btr = z;
					mprintf("  <---");
				}
				mprintf("\n");

				if ((z > 0) && (tfa[z] > tflast))
					break;

				tflast = tfa[z];
			}
			ctables = tia[btr];

			if (ctables != 1) {

				mprintf("\n");

				if (level >= 3) {
					ti = 10;
					tia[0] = 10;
					tia[1] = 12;
					tia[2] = 14;
					tia[3] = 16;
					tia[4] = 20;
					tia[5] = 24;
					tia[6] = 28;
					tia[7] = 32;
					tia[8] = 36;
					tia[9] = 40;
				} else {
					ti = 4;
					tia[0] = 15;
					tia[1] = 20;
					tia[2] = 30;
					tia[3] = 40;
				}

				btv = 1.0e30;
				btr = -1;
				for (z=0;z<ti;z++) {

					tfa[z] = 0;
					for (z2=0;z2<(int)tciaa.size();z2++) {

						bs.Clear();
						tie = new CIntegerEngine();
						tie->Compress(tciaa[z2],&bs,cbw,cmtf,ccoderun,tia[z],ctables,false,false,false,cmaxiter,cmaxchunk,verbose);
						delete tie;
						tfa[z] += bs.GetByteLength();
					}

					mprintf("        %2d  %2d  %3s  %3s  %10.3f",ctables,tia[z],cbw?"yes":"no",cmtf?"yes":"no",tfa[z]/1024.0/tciaa.size());
					if (tfa[z] < btv) {
						btv = tfa[z];
						btr = z;
						mprintf("  <---");
					}
					mprintf("\n");

					if ((z > 0) && (tfa[z] > tflast))
						break;

					tflast = tfa[z];
				}
				cblock = tia[btr];
			}
		}

		if (level >= 3) {

			for (z=0;z<3;z++) {

				switch(z) {
					case 0: cbw=true; cmtf=true; break;
					case 1: cbw=true; cmtf=false; break;
					case 2: cbw=false; cmtf=false; break;
				}
				tfa[z] = 0;
				for (z2=0;z2<(int)tciaa.size();z2++) {

					bs.Clear();
					tie = new CIntegerEngine();
					tie->Compress(tciaa[z2],&bs,cbw,cmtf,ccoderun,cblock,ctables,false,false,false,cmaxiter,cmaxchunk,verbose);
					delete tie;
					tfa[z] += bs.GetByteLength();
				}

				mprintf("        %2d  %2d  %3s  %3s  %10.3f\n",ctables,cblock,cbw?"yes":"no",cmtf?"yes":"no",tfa[z]/1024.0/tciaa.size());
			}
			fac = 1.0;
			if ((tfa[0] <= tfa[1]) && (tfa[0] <= tfa[2]/fac)) {
				cbw = true;
				cmtf = true;
			} else if ((tfa[1] <= tfa[0]) && (tfa[1] <= tfa[2]/fac)) {
				cbw = true;
				cmtf = false;
			} else {
				cbw = false;
				cmtf = false;
			}
		}
	}
_end:

	FormatTime((unsigned long)time(NULL)-t0,&buf);
	mprintf("        Optimization took %s\n",(const char*)buf);

/*	if (gc_fResiCorrFrame != NULL) {
		fclose(gc_fResiCorrFrame);
		gc_fResiCorrFrame = NULL;
	}

	if (gc_fResiCorrTraj != NULL) {
		fclose(gc_fResiCorrTraj);
		gc_fResiCorrTraj = NULL;
	}*/

	mprintf(WHITE,"    <<< Volumetric Trajectory Parameter Optimization Done <<<\n");

	return true;
}


bool CCCEngine::DecompressCube(const char *inp, const char *outp, const char *readref, int steps, int stride, bool verbose) {

	UNUSED(stride);

	CCubeFrame *cfr, *cfr2;
	int z, i, al, cl, ty, ver;
	CBitSet bsat, bscu, bshe;
	FILE *b, *fref;
	CBarbecubeFile bf;

	mprintf("Opening archive file \"%s\" ...\n",inp);
	if (!bf.OpenRead(inp)) {
		mprintf("Error: Could not open file for reading.\n");
		return false;
	}

	if (outp != NULL) {
		mprintf("Opening output cube file \"%s\" ...\n",outp);
		b = fopen(outp,"wb");
		if (b == NULL) {
			mprintf("Error: Could not open file for writing.\n");
			return false;
		}
	} else {
		b = NULL;
		mprintf("No output file specified; no output will be written.\n");
	}

	fref = NULL;
	if (readref != NULL) {
		mprintf("Opening reference cube file \"%s\" ...\n",readref);
		fref = fopen(readref,"rb");
		if (fref == NULL) {
			mprintf("Error: Could not open file for reading.\n");
			return false;
		}
	}

	m_oaOutputCubeBuf.resize(7);
	for (z=0;z<7;z++)
		m_oaOutputCubeBuf[z] = NULL;
	m_iOutputCubeBufPos = 0;

	m_oaOutputAtomBuf.resize(11);
	for (z=0;z<11;z++)
		m_oaOutputAtomBuf[z] = NULL;
	m_iOutputAtomBufPos = 0;

	mprintf("Starting process...\n");

	i = 0;
	while (true) {

		if (verbose)
			mprintf("\n\n");

		if (!bf.ReadFrame())
			break;

		ty = bf.GetFrameType();
		ver = bf.GetFrameTypeVersion();

		if ((ty == BARBECUBE_FRAMETYPE_IDX) || (ty == BARBECUBE_FRAMETYPE_COMPIDX))
			continue;

		if ((ty == BARBECUBE_FRAMETYPE_COMPCUBE) || (ty == BARBECUBE_FRAMETYPE_COMPCUBESTART)) {
			if (ver > 1) {
				mprintf("Error: Unexpected frame type version (%d v%d).\n",ty,ver);
				return false;
			}
		} else {
			mprintf("Error: Unexpected frame type (%d v%d).\n",ty,ver);
			return false;
		}

		bshe.Clear();
		bshe.m_iaData.assign(bf.GetFramePayload()->begin(),bf.GetFramePayload()->begin()+8);

		mprintf("    Step %d ...\n",i+1);

		al = bshe.ReadBitsInteger(32);
		cl = bshe.ReadBitsInteger(32);

		if (verbose)
			mprintf("Atom block %d bytes, Cube block %d bytes.\n",al,cl);

		cfr = new CCubeFrame();
		PushOutputCubeFrame(cfr);

		cfr->m_pAtoms = new CAtomSet();
		PushOutputAtomFrame(cfr->m_pAtoms);

		bsat.Clear();
		bsat.m_iaData.assign(bf.GetFramePayload()->begin()+8,bf.GetFramePayload()->begin()+8+al);

		bscu.Clear();
		bscu.m_iaData.assign(bf.GetFramePayload()->begin()+8+al,bf.GetFramePayload()->begin()+8+al+cl);

		if (!DecompressAtomFrame(&bsat,ver,verbose,false,false))
			return false;

		if (!DecompressCubeFrame(&bscu,ver,verbose,false,false))
			return false;

		if (readref != NULL) {

			mprintf("      Comparing to reference...\n");

			cfr2 = new CCubeFrame();

			if (!cfr2->ReadFrame(fref,cfr->m_iEps,cfr->m_iSigni,cfr->m_pAtoms->m_iSigni,verbose))
				break;

			for (z=0;z<cfr->m_iResXYZ;z++)
				if (!ExpMantisEqual(cfr->m_iaExpo[z],cfr->m_iaMantis[z],cfr2->m_iaExpo[z],cfr2->m_iaMantis[z])) {
					mprintf("        Error %7d: %.10G vs %.10G\n",z,cfr->m_faBin[z],cfr2->m_faBin[z]);
					break;
				}

			delete cfr2;

			if (verbose)
				mprintf("Done.\n");
		}

		mprintf("      %.3f KiB compressed data unpacked.\n",(al+cl)/1024.0);

		if (outp != NULL)
			cfr->WriteFrame(b,verbose);

		if (verbose)
			mprintf("Done.\n");

		i++;
		if (i == steps)
			break;
	}

	bf.Close();

	if (outp != NULL)
		fclose(b);

	if (readref != NULL)
		fclose(fref);

	mprintf("Finished decompressing the cube file.\n\n");

	return true;
}


bool CCCEngine::PrepareDecompressCube(bool verbose) {

	int z;

	if (verbose)
		mprintf(">>> CCCEngine::PrepareDecompressCube() >>>\n");

	for (z=0;z<(int)m_oaOutputCubeBuf.size();z++)
		if (m_oaOutputCubeBuf[z] != NULL)
			delete m_oaOutputCubeBuf[z];

	for (z=0;z<(int)m_oaOutputAtomBuf.size();z++)
		if (m_oaOutputAtomBuf[z] != NULL)
			delete m_oaOutputAtomBuf[z];

	m_oaOutputCubeBuf.resize(10);
	for (z=0;z<7;z++)
		m_oaOutputCubeBuf[z] = NULL;
	m_iOutputCubeBufPos = 0;

	m_oaOutputAtomBuf.resize(10);
	for (z=0;z<7;z++)
		m_oaOutputAtomBuf[z] = NULL;
	m_iOutputAtomBufPos = 0;

	if (verbose)
		mprintf("<<< CCCEngine::PrepareDecompressCube() <<<\n");

	return true;
}


bool CCCEngine::DecompressCubeStep(const std::vector<unsigned char> *inp, int ver, bool verbose, bool skipvol) {

	CCubeFrame *cfr;
	int al, cl;
	CBitSet bsat, bscu, bshe;

	if (verbose)
		mprintf(">>> CCCEngine::DecompressCubeStep() >>>\n");

	bshe.Clear();
	bshe.m_iaData.assign(inp->begin(),inp->begin()+8);

	al = bshe.ReadBitsInteger(32);
	cl = bshe.ReadBitsInteger(32);

	cfr = new CCubeFrame();
	PushOutputCubeFrame(cfr);

	cfr->m_pAtoms = new CAtomSet();
	PushOutputAtomFrame(cfr->m_pAtoms);

	bsat.Clear();
	bsat.m_iaData.assign(inp->begin()+8,inp->begin()+8+al);

	bscu.Clear();
	bscu.m_iaData.assign(inp->begin()+8+al,inp->begin()+8+al+cl);

	if (!DecompressAtomFrame(&bsat,ver,verbose,false,false))
		return false;

	if (!skipvol)
		if (!DecompressCubeFrame(&bscu,ver,verbose,false,false))
			return false;

	if (verbose)
		mprintf("<<< CCCEngine::DecompressCubeStep() <<<\n");

	return true;
}


bool CCCEngine::CompressXYZ(
		const char *inp,
		const char *outp,
		const char *ref,
		int start,
		int steps,
		int stride,
		int order,
		bool optorder,
		int signi,
		int split,
		int block,
		int tables,
		bool opttables,
		bool coderun,
		bool bw,
		bool mtf,
		bool preopt,
		int maxiter,
		bool sortatom,
		bool useextra,
		int extratrange,
		int extratorder,
		double extratimeexpo,
		bool comment,
		bool compare,
		int maxchunk,
		int optimize,
		int optsteps,
		bool onlyopt,
		bool verbose
	) {

	const CAtomSet *as;
	CAtomSet *as2;
	int z, z2, i, o, lasize, morder, atc;
	long insize;
	double tb, tb2, tf, tf2;
	CBitSet bsat, bstemp;
	bool err, to;
	CBarbecubeFile bf;


	mprintf("Opening XYZ file \"%s\" ...\n",inp);

	m_pReadCacheXYZ = new CCCReadXYZCache();
	m_pReadCacheXYZ->SetReadParameters(signi);
	if (!m_pReadCacheXYZ->FileOpenRead(inp,ref)) {
		mprintf("Error: Could not open file for reading.\n");
		return false;
	}

	if (useextra && (extratrange > order))
		order = extratrange;

	if (optimize != 0) {

		if (order+1 > optsteps)
			m_pReadCacheXYZ->SetHistoryDepth(order+1);
		else
			m_pReadCacheXYZ->SetHistoryDepth(optsteps);

		mprintf("\n");
		m_pReadCacheXYZ->CacheSteps(optsteps,true);
		mprintf("\n");

		if (m_iOptIncludeFirst == -1) {
			if ((m_pReadCacheXYZ->GetCachedSteps() < optsteps) || (optsteps < 10)) {
				mprintf("        Low step count, switching on -optstart.\n\n");
				m_iOptIncludeFirst = 1;
			} else {
				mprintf("        Large step count, switching off -optstart.\n\n");
				m_iOptIncludeFirst = 0;
			}
		}

		m_pReadCacheXYZ->SetStartLock();

		if (!OptimizeXYZParameters(
				optimize,       // int     level,
				order,          // int    &order, 
				signi,          // int     signi, 
				split,          // int    &split, 
				block,          // int    &block, 
				tables,         // int    &tables, 
				opttables,      // bool   &opttables, 
				coderun,        // bool   &coderun, 
				bw,             // bool   &bw, 
				mtf,            // bool   &mtf,
				preopt,         // bool   &preopt,
				maxiter,        // int    &maxiter,
				sortatom,       // bool   &sortatom,
				useextra,       // bool   &useextra,
				extratrange,    // int    &extratrange,
				extratorder,    // int    &extratorder,
				extratimeexpo,  // double &extratimeexpo,
				comment,        // bool    comment, 
				maxchunk,       // int    &maxchunk, 
				verbose,        // bool    verbose
				false           // bool    cubepart
		)) {
			eprintf("Error: Parameter optimization failed.\n");
			delete m_pReadCacheXYZ;
			m_pReadCacheXYZ = NULL;
			return false;
		}

		mprintf("\n    The optimal parameters for this trajectory are:\n");
		mprintf("     ");
		if (useextra) {
			if (extratrange != extratorder+2) {
				if (optimize >= 3)
					mprintf(" -extra yes -extrange %d -extorder %d -extimeexpo %.3f",extratrange,extratorder,extratimeexpo);
				else
					mprintf(" -extra yes -extrange %d -extorder %d -extimeexpo %.2f",extratrange,extratorder,extratimeexpo);
			} else
				mprintf(" -extra yes -extrange %d -extorder %d",extratrange,extratorder);
		} else
			mprintf(" -extra no -order %d",order);

		if (optimize >= 2) {
			mprintf(" -tables %d",tables);
			if (tables != 1)
				mprintf(" -block %d",block);
		}

		if (optimize >= 3) {

			if (bw)
				mprintf(" -bw yes");
			if (mtf)
				mprintf(" -mtf yes");
		}

		mprintf("\n\n");

		if (onlyopt) {
			m_pReadCacheXYZ->CloseFile();
			delete m_pReadCacheXYZ;
			m_pReadCacheXYZ = NULL;
			return true;
		}

		m_pReadCacheXYZ->RewindReadPos();

		m_pReadCacheXYZ->LiftStartLock();

	} else {

		m_pReadCacheXYZ->SetHistoryDepth(order+1);
	}

	gc_oStatistics.m_lSize = 0;
	gc_oStatistics.m_lOverhead = 0;
	gc_oStatistics.m_lAlphabet = 0;
	gc_oStatistics.m_lHuffmanTables = 0;
	gc_oStatistics.m_lTableSwitch = 0;
	gc_oStatistics.m_lHuffmanData = 0;

	if (outp != NULL) {
		mprintf("Opening compressed output file \"%s\" ...\n",outp);
		if (!bf.OpenWriteAppend(outp)) {
			mprintf("Error: Could not open file for writing.\n");
			m_pReadCacheXYZ->CloseFile();
			delete m_pReadCacheXYZ;
			m_pReadCacheXYZ = NULL;
			return false;
		}
	}

	m_oaOutputAtomBuf.resize(order+1);
	for (z=0;z<order+1;z++)
		m_oaOutputAtomBuf[z] = NULL;
	m_iOutputAtomBufPos = 0;

	mprintf("Starting process...\n");

	if (start != 0) {
		mprintf("Fast-forwarding to step %d ...\n",start+1);
		for (z=0;z<start;z++)
			if (!m_pReadCacheXYZ->SkipOneStep()) {
				delete m_pReadCacheXYZ;
				m_pReadCacheXYZ = NULL;
				return false;
			}
	}

	morder = order;

	i = 0;
	tb = 0;
	tb2 = 0;
	tf = 0;
	tf2 = 0;
	to = false;

	if (optorder)
		lasize = 1000000000;
	else
		lasize = -1;

	err = false;

	while (true) {

		if (verbose)
			mprintf("\n\n");

		mprintf("    Step %d ...\n",i+1);

		as = m_pReadCacheXYZ->GetNextFrame();

		if (i == 0)
			atc = (int)as->m_oaAtoms.size();

		if (as == NULL) {
			mprintf("      Reached end of input trajectory.\n");
			break;
		}

		for (z=0;z<stride-1;z++) {
			if (verbose)
				mprintf("Skipping frame...\n");
			if (!m_pReadCacheXYZ->SkipOneStep()) {
				mprintf("      Reached end of input trajectory.\n");
				break;
			}
		}

		if (i == 0) {
			if (useextra) {
				mprintf("      Initializing Extrapolator (%d/%d)...\n",extratrange,extratorder);
				m_pExtrapolatorXYZ = new CExtrapolator();
				m_pExtrapolatorXYZ->InitializeXYZ(extratrange,extratorder,extratimeexpo,verbose,false);
			}
		}

		if (m_pExtrapolatorXYZ != NULL)
			m_pExtrapolatorXYZ->PushAtomFrame(as);

		if (i < order)
			o = i;
		else
			o = order;

_aagain:
		bsat.Clear();
		PushStatistics();

		CompressAtomFrame(
			&bsat,
			o,
			signi,
			split,
			block,
			tables,
			opttables,
			coderun,
			bw,
			mtf,
			preopt&&(i>=2),
			maxiter,
			sortatom,
			(i==0),
			comment,
			maxchunk,
			verbose
		);

		if (useextra)
			mprintf("      Output size %9.3f KiB.\n",bsat.GetByteLength()/1024.0);
		else
			mprintf("      Order %d, output size %9.3f KiB.\n",o,bsat.GetByteLength()/1024.0);

		if (lasize >= 0) { // Only happens if optorder == true

			if (!useextra) {

				if (to) {
					to = false;
					if (bsat.GetByteLength() <= bstemp.GetByteLength()) {
						mprintf("    Size is indeed smaller with lower order. Limiting atom order to %d.\n",o);
						PopDiffStatistics();
						order = o;
						lasize = -1;
					} else {
						mprintf("    Size was smaller with higher order. Not limiting.\n");
						PopStatistics();
						bsat = CBitSet(bstemp);
						lasize = bsat.GetByteLength();
					}
				} else {
					if ((o != 0) && ((double)bsat.GetByteLength() >= (double)lasize*0.97)) {
						mprintf("    Size did not decrease any further with order %d. Trying order %d...\n",o,o-1);
						bstemp = CBitSet(bsat);
						to = true;
						o--;
						goto _aagain;
					} else if (o == order)
						lasize = -1;
					else
						lasize = bsat.GetByteLength();
				}
			}
		}

		PopIgnoreStatistics();

		tb += bsat.GetByteLength();
		tf++;

		if ((bsat.GetLength()%8) != 0)
			gc_oStatistics.m_lOverhead += 8 - (bsat.GetLength()%8);

		if (i >= morder) {
			tb2 += bsat.GetByteLength();
			tf2++;
		}

		if (compare) {

			as2 = new CAtomSet();
			PushOutputAtomFrame(as2);

			DecompressAtomFrame(&bsat,1,verbose,false,true);

			if (verbose)
				mprintf("Comparing input and output...\n");

			for (z=0;z<(int)as2->m_oaAtoms.size();z++)
				for (z2=0;z2<3;z2++)
					if (as->m_oaAtoms[z]->m_iCoord[z2] != as2->m_oaAtoms[z]->m_iCoord[z2]) {
						eprintf("        Error %d[%d]: %.6f != %.6f\n",z,z2,as->m_oaAtoms[z]->m_fCoord[z2],as2->m_oaAtoms[z]->m_fCoord[z2]);
						err = true;
						goto _end;
					}
		
			if (verbose)
				mprintf("Done.\n");
		}

		if (outp != NULL) {
			bf.CreateShortFrame((i==0)?BARBECUBE_FRAMETYPE_COMPTRAJSTART:BARBECUBE_FRAMETYPE_COMPTRAJ,1,i+1);
			bf.PushPayload(bsat.m_iaData);
			bf.FinalizeFrame();
		}

		i++;
		if (i == steps)
			break;
	}
_end:

	if (outp != NULL) {
		bf.WriteIndexFrame(true);
		bf.Close();
	}

	insize = m_pReadCacheXYZ->ftell();

	m_pReadCacheXYZ->CloseFile();

	delete m_pReadCacheXYZ;
	m_pReadCacheXYZ = NULL;

	if (m_pExtrapolatorXYZ != NULL) {
		delete m_pExtrapolatorXYZ;
		m_pExtrapolatorXYZ = NULL;
	}

	if (err) {
		mprintf("Errors occurred while compressing the XYZ file.\n");
		return false;
	} else {
		mprintf("Finished compressing the XYZ file.\n");
		mprintf("%10.3f MiB (%12s Bytes) overhead.\n",double(gc_oStatistics.m_lOverhead)/1024.0/1024.0/8.0,(gc_oStatistics.m_lOverhead>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) alphabet data.\n",double(gc_oStatistics.m_lAlphabet)/1024.0/1024.0/8.0,(gc_oStatistics.m_lAlphabet>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) Huffman tables.\n",double(gc_oStatistics.m_lHuffmanTables)/1024.0/1024.0/8.0,(gc_oStatistics.m_lHuffmanTables>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) table switching.\n",double(gc_oStatistics.m_lTableSwitch)/1024.0/1024.0/8.0,(gc_oStatistics.m_lTableSwitch>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) payload data.\n",double(gc_oStatistics.m_lHuffmanData)/1024.0/1024.0/8.0,(gc_oStatistics.m_lHuffmanData>>3).string());
		mprintf("%10.3f MiB (%12s Bytes) in total.\n",double(gc_oStatistics.m_lOverhead+gc_oStatistics.m_lAlphabet+gc_oStatistics.m_lHuffmanTables+gc_oStatistics.m_lTableSwitch+gc_oStatistics.m_lHuffmanData)/1024.0/1024.0/8.0,((gc_oStatistics.m_lOverhead+gc_oStatistics.m_lAlphabet+gc_oStatistics.m_lHuffmanTables+gc_oStatistics.m_lTableSwitch+gc_oStatistics.m_lHuffmanData)>>3).string());
		if (tf > 0) {
			mprintf("\n  * Totals:\n");
			mprintf("      %9.3f KiB per frame on average.\n",tb/1024.0/tf);
			mprintf("      %9.3f bits per coodinate on average.\n",tb/tf/atc/3.0*8.0);
			mprintf("      Compression ratio of %.3f : 1\n",(double)insize/tb);
		}
		if (tf2 > 0) {
			mprintf("\n  * Starting from step %d:\n",morder+1);
			mprintf("      %9.3f KiB per frame on average.\n",tb2/1024.0/tf2);
			mprintf("      %9.3f bits per coodinate on average.\n",tb2/tf2/atc/3.0*8.0);
			mprintf("      Compression ratio of %.3f : 1\n",((double)insize/tf*tf2)/tb2);
		}
		mprintf("\n");
	}

	return true;
}


double CCCEngine::CompressXYZBenchmark(
		int order, 
		int signi, 
		int split, 
		int block, 
		int tables, 
		bool opttables, 
		bool coderun, 
		bool bw, 
		bool mtf,
		bool preopt,
		int maxiter,
		bool sortatom,
		bool useextra,
		int extratrange,
		int extratorder,
		double extratimeexpo,
		bool comment, 
		int maxchunk, 
		bool verbose1,
		bool verbose2,
		bool realsize,
		double *presi,
		std::vector<std::vector<int> > *tciaa
	) {

	const CAtomSet *as;
	int i, o;
	double tb, tf, resi, tresi;
	CBitSet bsat;


	resi = 0;

	if (useextra && (extratrange > order))
		order = extratrange;

	if (verbose1)
		mprintf("Starting process...\n");

	i = 0;
	tb = 0;
	tf = 0;

	if (m_pReadCacheCube != NULL)
		m_pReadCacheCube->RewindReadPos();
	else
		m_pReadCacheXYZ->RewindReadPos();

	while (true) {

		if (verbose2)
			mprintf("\n\n");

		if (verbose1)
			mprintf("    Step %d ...\n",i+1);

		if (m_pReadCacheCube != NULL)
			as = m_pReadCacheCube->GetNextFrame()->m_pAtoms;
		else
			as = m_pReadCacheXYZ->GetNextFrame();

		if (as == NULL)
			break;

		if (i == 0) {

			if (!realsize)
				BuildIdentityAtomSort(as);

			if (useextra) {
				if (verbose1)
					mprintf("      Initializing Extrapolator (%d|%d)...\n",extratrange,extratorder);
				m_pExtrapolatorXYZ = new CExtrapolator();
				m_pExtrapolatorXYZ->InitializeXYZ(extratrange,extratorder,extratimeexpo,verbose2,!verbose1);
			}
		}

		if (m_pExtrapolatorXYZ != NULL)
			m_pExtrapolatorXYZ->PushAtomFrame(as);

		if (i < order)
			o = i;
		else
			o = order;

		if ((i+1 >= extratrange) || (m_iOptIncludeFirst > 0)) {

			bsat.Clear();

			CompressAtomFrame(
				&bsat,
				o,
				signi,
				split,
				block,
				tables,
				opttables,
				coderun,
				bw,
				mtf,
				preopt&&(i>=2),
				maxiter,
				sortatom,
				(i==0),
				comment,
				maxchunk,
				verbose2,
				!realsize,
				&tresi,
				tciaa
			);

			resi += tresi;

			tf++;

			if (realsize) {
				tb += bsat.GetByteLength();
/*				if (gc_fResiCorrFrame != NULL) {
					mfprintf(gc_fResiCorrFrame,"%d;  %.3f\n", bsat.GetByteLength(), mypow( tresi / ((as->m_oaAtoms.size() + 1) * 3.0), 1.0/m_fEntropyExpoXYZ ) );
					fflush(gc_fResiCorrFrame);
				}*/
			}
		}

		i++;

		if (m_pReadCacheCube != NULL) {
			if (m_pReadCacheCube->GetCachedSteps() == 0)
				break;
		} else {
			if (m_pReadCacheXYZ->GetCachedSteps() == 0)
				break;
		}
	}

	if (m_pExtrapolatorXYZ != NULL) {
		delete m_pExtrapolatorXYZ;
		m_pExtrapolatorXYZ = NULL;
	}

	resi = sqrt( resi / (tf * (as->m_oaAtoms.size() + 1) * 3.0) );
	//resi = mypow( resi / (tf * (as->m_oaAtoms.size() + 1) * 3.0), 1.0/m_fEntropyExpoXYZ );

	if (presi != NULL)
		*presi = resi;

	if (realsize) {
/*		if (gc_fResiCorrTraj != NULL) {
			mfprintf(gc_fResiCorrTraj,"%.1f;  %.4f\n",tb / tf,resi);
			fflush(gc_fResiCorrTraj);
		}*/
		return tb / tf;
	} else
		return resi;
}


double CCCEngine::GoldenSectionSearch(double maxl, double eps, double (CCCEngine::*fobj)(double)) {

	double a, c, fa, fc;


	a = 0;
	fa = (this->*fobj)(a);

	c = maxl;
	fc = (this->*fobj)(c);

	return REC_GoldenSectionSearch(a,c,c,fa,fc,fc,fobj,eps,0);
}


double CCCEngine::REC_GoldenSectionSearch(double a, double b, double c, double fa, double fb, double fc, double (CCCEngine::*fobj)(double), double eps, int depth) {

	double x, fx;


	if ((c - b) > (b - a))
		x = b + (2 - (1 + sqrt(5)) / 2) * (c - b);
	else
		x = b - (2 - (1 + sqrt(5)) / 2) * (b - a);

	if (fabs(c - a) < eps)
		return (c + a) / 2.0; 

	fx = (this->*fobj)(x);

	if (fx < fb) {
		if ((c - b) > (b - a))
			return REC_GoldenSectionSearch(b, x, c, fb, fx, fc, fobj, eps, depth+1);
		else
			return REC_GoldenSectionSearch(a, x, b, fa, fx, fb, fobj, eps, depth+1);
	} else {
		if ((c - b) > (b - a))
			return REC_GoldenSectionSearch(a, b, x, fa, fb, fx, fobj, eps, depth+1);
		else
			return REC_GoldenSectionSearch(x, b, c, fx, fb, fc, fobj, eps, depth+1);
	}
}


double CCCEngine::OptimizeXYZ_ObjectiveTExpo(double texpo) {

	double entropy, resi;

	entropy = CompressXYZBenchmark(
		gc_iOptXYZOrder,     // int order, 
		gc_iOptXYZSigni,     // int signi, 
		gc_iOptXYZSplit,     // int split, 
		gc_iOptXYZBlock,     // int block, 
		gc_iOptXYZTables,    // int tables, 
		gc_iOptXYZOptTables, // bool opttables, 
		gc_bOptXYZCoderun,   // bool coderun, 
		gc_bOptXYZBW,        // bool bw, 
		gc_bOptXYZMTF,       // bool mtf,
		gc_bOptXYZPreOpt,    // bool preopt,
		gc_iOptXYZMaxIter,   // int maxiter,
		gc_bOptXYZSortAtom,  // bool sortatom,
		gc_bOptXYZUseExtra,  // bool useextra,
		gc_iOptXYZExTRange,  // int extratrange,
		gc_iOptXYZExTOrder,  // int extratorder,
		texpo,               // double extratimeexpo,
		gc_bOptXYZComment,   // bool comment, 
		gc_iOptXYZMaxChunk,  // int maxchunk, 
		false,               // bool verbose1,
		false,               // bool verbose2,
		gc_bOptXYZRealSize,  // bool realsize
		&resi,               // double *presi
		NULL                 // std::vector<std::vector<int> > *tciaa
	);

	if (gc_bOptXYZRealSize)
		mprintf("        %5d  %5d  %8.3f  %14.5f (%10.4f kiB)\n",gc_iOptXYZExTRange,gc_iOptXYZExTOrder,texpo,resi,entropy/1024.0);
	else
		mprintf("        %5d  %5d  %8.3f  %14.5f\n",gc_iOptXYZExTRange,gc_iOptXYZExTOrder,texpo,entropy);

	return entropy;
}


bool CCCEngine::OptimizeXYZParameters(
		int     level,
		int    &order, 
		int     signi, 
		int    &split, 
		int    &block, 
		int    &tables, 
		bool   &opttables, 
		bool   &coderun, 
		bool   &bw, 
		bool   &mtf,
		bool   &preopt,
		int    &maxiter,
		bool   &sortatom,
		bool   &useextra,
		int    &extratrange,
		int    &extratorder,
		double &extratimeexpo,
		bool    comment, 
		int    &maxchunk, 
		bool    verbose,
		bool    cubepart
	) {

	int z, z2, z3, smr, smo, steps, btr;
	int tia[4];
	double smv, tf, tflast, resi, btv, tfa[4], fac;
	bool realsize;
	unsigned long t0;
	CxString buf;
	std::vector<std::pair<double,std::string> > rlist;
	std::vector<std::vector<int> > tciaa;
	std::vector<int> iabl, iatc;
	CBitSet bs;
	CIntegerEngine *tie;


	UNUSED(cubepart);
	tflast = 0;

	mprintf(WHITE,"    >>> Position Trajectory Parameter Optimization >>>\n");

/*	if (level == 3) {
		gc_fResiCorrFrame = OpenFileWrite("corrframexyz.csv",true);
		gc_fResiCorrTraj = OpenFileWrite("corrtrajxyz.csv",true);
	}*/

	t0 = (unsigned long)time(NULL);

	if (level >= 3)
		realsize = true;
	else
		realsize = false;

	if (m_pReadCacheCube != NULL) {
		m_pReadCacheCube->RewindReadPos();
		steps = m_pReadCacheCube->GetCachedSteps();
	} else {
		m_pReadCacheXYZ->RewindReadPos();
		steps = m_pReadCacheXYZ->GetCachedSteps();
	}

	mprintf("        Optimization level %d\n",level);
	if ((level == 1) && !cubepart)
		mprintf("          Please note: Level 1 and 2 are identical for position trajectories.\n");
	//mprintf("        Entropy Exponent %.3f\n",m_fEntropyExpoXYZ);
	mprintf("        Have %d cached steps.\n",steps);

	mprintf("        Range  Order  Exponent        Residuum\n");
	smv = 1.0e30;
	tf = 0;
	smr = -1;
	smo = -1;
	useextra = true;
	//extratimeexpo = 1.0;

	for (z=1;z<MIN(steps+1,10);z++) { // trange

		for (z2=z-2;z2>=-1;z2--) { // torder

			if ((z > 1) && (z2 == -1))
				continue;

			tflast = tf;

			tf = CompressXYZBenchmark(
				order,          // int order, 
				signi,          // int signi, 
				split,          // int split, 
				block,          // int block, 
				tables,         // int tables, 
				opttables,      // bool opttables, 
				coderun,        // bool coderun, 
				bw,             // bool bw, 
				mtf,            // bool mtf,
				preopt,         // bool preopt,
				maxiter,        // int maxiter,
				sortatom,       // bool sortatom,
				useextra,       // bool useextra,
				z,              // int extratrange,
				z2,             // int extratorder,
				extratimeexpo,  // double extratimeexpo,
				comment,        // bool comment, 
				maxchunk,       // int maxchunk, 
				verbose,        // bool verbose1,
				false,          // bool verbose2,
				realsize,       // bool realsize
				&resi,          // double *presi
				NULL            // std::vector<std::vector<int> > *tciaa
			);

			if (realsize)
				buf.sprintf("        %5d  %5d  %8.3f  %14.5f (%10.4f kiB)",z,z2,extratimeexpo,resi,tf/1024.0);
			else
				buf.sprintf("        %5d  %5d  %8.3f  %14.5f",z,z2,extratimeexpo,tf);

			mprintf("%s",(const char*)buf);

			rlist.push_back(std::pair<double,std::string>(tf,(const char*)buf));

			if (tf < smv) {
				mprintf("  <---");
				smv = tf;
				smo = z2;
				smr = z;
			}

			mprintf("\n");

			if ((z2 < z-2) && (tflast < tf))
				break;
		}
	}

	std::sort(rlist.begin(),rlist.end());

/*	FILE *a;
	a = OpenFileWrite("optixyz_list.txt",true);
	mfprintf(a,"        Range  Order  Exponent       Entropy\n");
	for (z=0;z<(int)rlist.size();z++)
		mfprintf(a,"%s\n",rlist[z].second.c_str());
	fclose(a);*/

	extratrange = smr;
	extratorder = smo;

	if (extratorder+2 != extratrange) {

		gc_iOptXYZOrder = order;
		gc_iOptXYZSigni = signi;
		gc_bOptXYZUseExtra = useextra;
		gc_iOptXYZExTRange = extratrange;
		gc_iOptXYZExTOrder = extratorder;

		gc_iOptXYZSplit = split;
		gc_iOptXYZBlock = block;
		gc_iOptXYZTables = tables;
		gc_iOptXYZOptTables = opttables;
		gc_bOptXYZCoderun = coderun;
		gc_bOptXYZBW = bw;
		gc_bOptXYZMTF = mtf;
		gc_bOptXYZPreOpt = preopt;
		gc_iOptXYZMaxIter = maxiter;
		gc_bOptXYZSortAtom = sortatom;
		gc_bOptXYZComment = comment;
		gc_iOptXYZMaxChunk = maxchunk;
		gc_bOptXYZRealSize = realsize;

		mprintf("\n");

		extratimeexpo = GoldenSectionSearch(7.0, (level>=3)?0.02:0.1, &CCCEngine::OptimizeXYZ_ObjectiveTExpo);

		if (level >= 3)
			extratimeexpo = floor(extratimeexpo*1000.0+0.5)/1000.0;
		else
			extratimeexpo = floor(extratimeexpo*100.0+0.5)/100.0;

	} else
		extratimeexpo = 0;

	if (level >= 2) {

		mprintf("        Optimizing Compressor settings:\n");

		tciaa.clear();

		tf = CompressXYZBenchmark(
			order,          // int order, 
			signi,          // int signi, 
			split,          // int split, 
			block,          // int block, 
			tables,         // int tables, 
			opttables,      // bool opttables, 
			coderun,        // bool coderun, 
			bw,             // bool bw, 
			mtf,            // bool mtf,
			preopt,         // bool preopt,
			maxiter,        // int maxiter,
			sortatom,       // bool sortatom,
			useextra,       // bool useextra,
			extratrange,    // int extratrange,
			extratorder,    // int extratorder,
			extratimeexpo,  // double extratimeexpo,
			comment,        // bool comment, 
			maxchunk,       // int maxchunk, 
			verbose,        // bool verbose1,
			false,          // bool verbose2,
			true,           // bool realsize
			NULL,           // double *presi
			&tciaa          // std::vector<std::vector<int> > *tciaa
		);

		mprintf("        TC   BL    BW   MTF   Frame Size [kiB]\n");

		if (level >= 4) {

			iatc.push_back(1);
			iatc.push_back(2);
			iatc.push_back(3);
			iatc.push_back(4);
			iatc.push_back(6);
			iatc.push_back(8);
			iatc.push_back(10);
			iatc.push_back(12);
		/*	iatc.push_back(15);
			iatc.push_back(18);
			iatc.push_back(21);
			iatc.push_back(24);*/

			iabl.push_back(10);
			iabl.push_back(12);
			iabl.push_back(14);
			iabl.push_back(16);
			iabl.push_back(20);
			iabl.push_back(24);
			iabl.push_back(28);
			iabl.push_back(32);
			iabl.push_back(36);
			iabl.push_back(40);
			iabl.push_back(48);
			iabl.push_back(56);
			iabl.push_back(64);

			btv = 1.0e30;
			btr = -1;

			for (z=0;z<(int)iatc.size();z++) {

				for (z2=0;z2<(int)iabl.size();z2++) {

					tf = 0;
					for (z3=0;z3<(int)tciaa.size();z3++) {

						bs.Clear();
						tie = new CIntegerEngine();
						tie->Compress(tciaa[z3],&bs,bw,mtf,coderun,iabl[z2],iatc[z],false,false,false,maxiter,maxchunk,verbose);
						delete tie;
						tf += bs.GetByteLength();
					}

					mprintf("        %2d   %2d   %3s   %3s   %10.4f",iatc[z],iabl[z2],bw?"yes":"no",mtf?"yes":"no",tf/tciaa.size()/1024.0);
					if (tf < btv) {
						btv = tf;
						tables = iatc[z];
						block = iabl[z2];
						mprintf("  <---");
					}
					mprintf("\n");

					if (iatc[z] == 1)
						break; // One table: Block length does not matter
				}
			}

		} else {

			tia[0] = 1;
			tia[1] = 3;
			tia[2] = 6;
			tia[3] = 12;

			btv = 1.0e30;
			btr = -1;
			for (z=0;z<4;z++) {

				tfa[z] = 0;
				for (z2=0;z2<(int)tciaa.size();z2++) {

					bs.Clear();
					tie = new CIntegerEngine();
					tie->Compress(tciaa[z2],&bs,bw,mtf,coderun,block,tia[z],false,false,false,maxiter,maxchunk,verbose);
					delete tie;
					tfa[z] += bs.GetByteLength();
				}

				mprintf("        %2d   %2d   %3s   %3s   %10.4f",tia[z],block,bw?"yes":"no",mtf?"yes":"no",tfa[z]/tciaa.size()/1024.0);
				if (tfa[z] < btv) {
					btv = tfa[z];
					btr = z;
					mprintf("  <---");
				}
				mprintf("\n");

				if ((z > 0) && (tfa[z] > tflast))
					break;

				tflast = tfa[z];
			}
			tables = tia[btr];

			if (tables != 1) {

				tia[0] = 12;
				tia[1] = 16;
				tia[2] = 24;
				tia[3] = 32;

				btv = 1.0e30;
				btr = -1;
				for (z=0;z<4;z++) {

					tfa[z] = 0;
					for (z2=0;z2<(int)tciaa.size();z2++) {

						bs.Clear();
						tie = new CIntegerEngine();
						tie->Compress(tciaa[z2],&bs,bw,mtf,coderun,tia[z],tables,false,false,false,maxiter,maxchunk,verbose);
						delete tie;
						tfa[z] += bs.GetByteLength();
					}

					mprintf("        %2d   %2d   %3s   %3s   %10.4f",tables,tia[z],bw?"yes":"no",mtf?"yes":"no",tfa[z]/tciaa.size()/1024.0);
					if (tfa[z] < btv) {
						btv = tfa[z];
						btr = z;
						mprintf("  <---");
					}
					mprintf("\n");

					if ((z > 0) && (tfa[z] > tflast))
						break;

					tflast = tfa[z];
				}
				block = tia[btr];
			}
		}

		if (level >= 3) {

			for (z=0;z<3;z++) {

				switch(z) {
					case 0: bw=true; mtf=true; break;
					case 1: bw=true; mtf=false; break;
					case 2: bw=false; mtf=false; break;
				}
				tfa[z] = 0;
				for (z2=0;z2<(int)tciaa.size();z2++) {

					bs.Clear();
					tie = new CIntegerEngine();
					tie->Compress(tciaa[z2],&bs,bw,mtf,coderun,block,tables,false,false,false,maxiter,maxchunk,verbose);
					delete tie;
					tfa[z] += bs.GetByteLength();
				}

				mprintf("        %2d   %2d   %3s   %3s   %10.4f\n",tables,block,bw?"yes":"no",mtf?"yes":"no",tfa[z]/tciaa.size()/1024.0);
			}
			fac = 1.0;
			if ((tfa[0] <= tfa[1]) && (tfa[0] <= tfa[2]/fac)) {
				bw = true;
				mtf = true;
			} else if ((tfa[1] <= tfa[0]) && (tfa[1] <= tfa[2]/fac)) {
				bw = true;
				mtf = false;
			} else {
				bw = false;
				mtf = false;
			}
		}
	}
	
	FormatTime((unsigned long)time(NULL)-t0,&buf);
	mprintf("        Optimization took %s\n",(const char*)buf);

/*	if (gc_fResiCorrFrame != NULL) {
		fclose(gc_fResiCorrFrame);
		gc_fResiCorrFrame = NULL;
	}

	if (gc_fResiCorrTraj != NULL) {
		fclose(gc_fResiCorrTraj);
		gc_fResiCorrTraj = NULL;
	}*/

	mprintf(WHITE,"    <<< Position Trajectory Parameter Optimization Done <<<\n");

	return true;
}


bool CCCEngine::DecompressXYZ(const char *inp, const char *outp, const char *readref, int steps, int stride, bool verbose) {

	UNUSED(stride);

	CAtomSet *as, *as2;
	int z, z2, i, ty, ver;
	CBitSet bsat;
	FILE *b, *fref;
	CBarbecubeFile bf;


	mprintf("Opening archive file \"%s\" ...\n",inp);
	if (!bf.OpenRead(inp)) {
		mprintf("Error: Could not open file for reading.\n");
		return false;
	}

	mprintf("Opening output XYZ file \"%s\" ...\n",outp);
	b = fopen(outp,"wb");
	if (b == NULL) {
		mprintf("Error: Could not open file for writing.\n");
		return false;
	}

	fref = NULL;
	if (readref != NULL) {
		mprintf("Opening reference XYZ file \"%s\" ...\n",readref);
		fref = fopen(readref,"rb");
		if (fref == NULL) {
			mprintf("Error: Could not open file for reading.\n");
			fclose(b);
			return false;
		}
	}

	m_oaOutputAtomBuf.resize(11);
	for (z=0;z<11;z++)
		m_oaOutputAtomBuf[z] = NULL;
	m_iOutputAtomBufPos = 0;

	mprintf("Starting process...\n");

	i = 0;
	while (true) {

		if (verbose)
			mprintf("\n\n");

		if (!bf.ReadFrame())
			break;

		ty = bf.GetFrameType();
		ver = bf.GetFrameTypeVersion();

		if ((ty == BARBECUBE_FRAMETYPE_IDX) || (ty == BARBECUBE_FRAMETYPE_COMPIDX))
			continue;

		if (((ty != BARBECUBE_FRAMETYPE_COMPTRAJ) && (ty != BARBECUBE_FRAMETYPE_COMPTRAJSTART)) || (ver > 1)) {
			mprintf("Error: Unexpected frame type (%d v%d).\n",ty,ver);
			return false;
		}

		mprintf("    Step %d ...\n",i+1);

		if (verbose)
			mprintf("Atom block %lu bytes.\n",bf.GetFramePayload()->size());

		as = new CAtomSet();
		PushOutputAtomFrame(as);

		bsat.Clear();
		bsat.m_iaData.assign(bf.GetFramePayload()->begin(),bf.GetFramePayload()->end());

		DecompressAtomFrame(&bsat,ver,verbose,false,false);

		if (readref != NULL) {

			mprintf("      Comparing to reference...\n");

			as2 = new CAtomSet();

			if (!as2->ReadXYZ(fref,as->m_iSigni,NULL))
				break;

			for (z=0;z<(int)as2->m_oaAtoms.size();z++) {
				for (z2=0;z2<3;z2++)
					if (as->m_oaAtoms[z]->m_iCoord[z2] != as2->m_oaAtoms[z]->m_iCoord[z2])
						mprintf("        Error %d[%d]: %.6f != %.6f\n",z,z2,as->m_oaAtoms[z]->m_fCoord[z2],as2->m_oaAtoms[z]->m_fCoord[z2]);
			}

			delete as2;

			if (verbose)
				mprintf("Done.\n");
		}

		mprintf("      %.3f KiB compressed data unpacked.\n",bf.GetFramePayload()->size()/1024.0);

		as->WriteXYZ(b,as->m_iSigni);

		if (verbose)
			mprintf("Done.\n");

		i++;
		if (i == steps)
			break;
	}

	bf.Close();

	fclose(b);

	if (readref != NULL)
		fclose(fref);

	mprintf("Finished decompressing the XYZ file.\n\n");

	return true;
}


void CCCEngine::PushOutputCubeFrame(CCubeFrame *cfr) {

	m_iOutputCubeBufPos++;
	if (m_iOutputCubeBufPos == (int)m_oaOutputCubeBuf.size())
		m_iOutputCubeBufPos = 0;

	if (m_oaOutputCubeBuf[m_iOutputCubeBufPos] != NULL)
		delete m_oaOutputCubeBuf[m_iOutputCubeBufPos];
	m_oaOutputCubeBuf[m_iOutputCubeBufPos] = cfr;
}


void CCCEngine::PushOutput2CubeFrame(CCubeFrame *cfr) {

	m_iOutput2CubeBufPos++;
	if (m_iOutput2CubeBufPos == (int)m_oaOutput2CubeBuf.size())
		m_iOutput2CubeBufPos = 0;

	if (m_oaOutput2CubeBuf[m_iOutput2CubeBufPos] != NULL)
		delete m_oaOutput2CubeBuf[m_iOutput2CubeBufPos];
	m_oaOutput2CubeBuf[m_iOutput2CubeBufPos] = cfr;
}


CCubeFrame* CCCEngine::GetOutputCubeFrame(int depth) {

	int ti;

	if (depth >= (int)m_oaOutputCubeBuf.size()) {
		eprintf("CCCEngine::GetOutputCubeFrame(): Out of bounds (%d/%d).\n",depth,(int)m_oaOutputCubeBuf.size());
		abort();
	}

	ti = m_iOutputCubeBufPos - depth;
	if (ti < 0)
		ti += (int)m_oaOutputCubeBuf.size();

	return m_oaOutputCubeBuf[ti];
}


CCubeFrame* CCCEngine::GetOutput2CubeFrame(int depth) {

	int ti;

	if (depth >= (int)m_oaOutput2CubeBuf.size()) {
		eprintf("CCCEngine::GetOutput2CubeFrame(): Out of bounds (%d/%d).\n",depth,(int)m_oaOutput2CubeBuf.size());
		abort();
	}

	ti = m_iOutput2CubeBufPos - depth;
	if (ti < 0)
		ti += (int)m_oaOutput2CubeBuf.size();

	return m_oaOutput2CubeBuf[ti];
}


void CCCEngine::PushOutputAtomFrame(CAtomSet *cfr) {

	m_iOutputAtomBufPos++;
	if (m_iOutputAtomBufPos == (int)m_oaOutputAtomBuf.size())
		m_iOutputAtomBufPos = 0;

	if (m_oaOutputAtomBuf[m_iOutputAtomBufPos] != NULL)
		delete m_oaOutputAtomBuf[m_iOutputAtomBufPos];
	m_oaOutputAtomBuf[m_iOutputAtomBufPos] = cfr;
}


void CCCEngine::PushOutput2AtomFrame(CAtomSet *cfr) {

	m_iOutput2AtomBufPos++;
	if (m_iOutput2AtomBufPos == (int)m_oaOutput2AtomBuf.size())
		m_iOutput2AtomBufPos = 0;

	if (m_oaOutput2AtomBuf[m_iOutput2AtomBufPos] != NULL)
		delete m_oaOutput2AtomBuf[m_iOutput2AtomBufPos];
	m_oaOutput2AtomBuf[m_iOutput2AtomBufPos] = cfr;
}


CAtomSet* CCCEngine::GetOutputAtomFrame(int depth) {

	int ti;

	if (depth >= (int)m_oaOutputAtomBuf.size()) {
		eprintf("CCCEngine::GetOutputAtomFrame(): Out of bounds (%d/%d).\n",depth,(int)m_oaOutputAtomBuf.size());
		abort();
	}

	ti = m_iOutputAtomBufPos - depth;
	if (ti < 0)
		ti += (int)m_oaOutputAtomBuf.size();

	return m_oaOutputAtomBuf[ti];
}


CAtomSet* CCCEngine::GetOutput2AtomFrame(int depth) {

	int ti;

	if (depth >= (int)m_oaOutput2AtomBuf.size()) {
		eprintf("CCCEngine::GetOutput2AtomFrame(): Out of bounds (%d/%d).\n",depth,(int)m_oaOutput2AtomBuf.size());
		abort();
	}

	ti = m_iOutput2AtomBufPos - depth;
	if (ti < 0)
		ti += (int)m_oaOutput2AtomBuf.size();

	return m_oaOutput2AtomBuf[ti];
}


bool SORT_AtomOrd(int i1, int i2) {

	return gc_iaAtomOrd[i1] > gc_iaAtomOrd[i2];
}


void CCCEngine::BuildAtomSort(const CAtomSet *as) {

	int z;

//	if (m_bAtomSortBuilt)
//		return;

	m_iaAtomSort.resize(as->m_oaAtoms.size());
	gc_iaAtomOrd.resize(as->m_oaAtoms.size());
	for (z=0;z<(int)m_iaAtomSort.size();z++) {
		m_iaAtomSort[z] = z;
		gc_iaAtomOrd[z] = as->m_oaAtoms[z]->m_iOrd;
	}

	std::stable_sort(m_iaAtomSort.begin(),m_iaAtomSort.end(),SORT_AtomOrd);

//	m_bAtomSortBuilt = true;

/*	mprintf("### AtomSort:\n");
	for (z=0;z<(int)m_iaAtomSort.size();z++)
		mprintf("  %2d) Ord=%2d, Pos=%2d\n",z+1,gc_iaAtomOrd[m_iaAtomSort[z]],m_iaAtomSort[z]+1);*/
}


void CCCEngine::BuildIdentityAtomSort(const CAtomSet *as) {

	int z;

	m_iaAtomSort.resize(as->m_oaAtoms.size());
	gc_iaAtomOrd.resize(as->m_oaAtoms.size());
	for (z=0;z<(int)m_iaAtomSort.size();z++) {
		m_iaAtomSort[z] = z;
		gc_iaAtomOrd[z] = as->m_oaAtoms[z]->m_iOrd;
	}
}


bool CCCEngine::MergeBQB(const char *outfile, const std::vector<const char*> &infile, bool verbose) {

	int z, i, zi, zo;
	CBarbecubeFile *pout;
	std::vector<CBarbecubeFile*> pin;

	SetBarbecubeVerbose(verbose);

	mprintf("\n");
	mprintf(WHITE,"    *****************************\n");
	mprintf(WHITE,"    ***   Merging BQB Files   ***\n");
	mprintf(WHITE,"    *****************************\n\n");

	mprintf("    Will merge the input files\n");
	for (z=0;z<(int)infile.size();z++)
		mprintf("      [%d] %s\n",z+1,infile[z]);
	mprintf("    into the output file %s.\n\n",outfile);

	mprintf("    Opening input files...\n");
	pin.resize(infile.size());
	for (z=0;z<(int)infile.size();z++) {
		mprintf("      [%d] %s... ",z+1,infile[z]);
		pin[z] = new CBarbecubeFile();
		if (!pin[z]->OpenRead(infile[z])) {
			eprintf("Error.\n");
			return false;
		}
		if ((i = pin[z]->GetTotalFrameCount()) != -1)
			mprintf("Ok, %d frames (index).",i);
		else
			mprintf("Ok, no index.");
		mprintf("\n");
	}

	mprintf("\n    Opening output file...\n");
	pout = new CBarbecubeFile();
	if (!pout->OpenWriteReplace(outfile)) {
		eprintf("Error.\n");
		return false;
	}

	zo = 0;
	for (z=0;z<(int)pin.size();z++) {

		mprintf("    Processing input file %d.\n",z+1);
		zi = 0;

		while (true) {
			if (!pin[z]->ReadFrame()) {
				mprintf("    Reached end of input file %d after reading %d frames.\n",z+1,zi);
				break;
			}
			zi++;
			mprintf("      Input file %d frame %6d: Type %2d.%d, Index %6d, Size %7.2f KiB",z+1,zi,pin[z]->GetFrameType(),pin[z]->GetFrameTypeVersion(),pin[z]->GetFrameIndex(),pin[z]->GetFramePayload()->size()/1024.0);
			if ((pin[z]->GetFrameType() == 2) || (pin[z]->GetFrameType() == 3)) {
				mprintf(" --> Skipping index frame.\n");
				continue;
			}
			mprintf(" --> Output frame %6d.\n",zo);
			pout->CreateShortFrame(pin[z]->GetFrameType(),pin[z]->GetFrameTypeVersion(),pin[z]->GetFrameIndex());
			pout->PushPayload(*pin[z]->GetFramePayload());
			pout->FinalizeFrame();
			zo++;
		}
	}

	mprintf("    Merged %d frames from %lu input files into output file.\n\n",zo,infile.size());

	mprintf("    Writing new index frame for output file...\n");
	pout->WriteIndexFrame(true);
	mprintf("    All done.\n\n");
	pout->Close();
	delete pout;
	for (z=0;z<(int)pin.size();z++) {
		pin[z]->Close();
		delete pin[z];
	}

	return true;
}


/*bool CCCEngine::SplitBQB(const char *infile, const char *outbase, int splitlength, int steps, bool verbose) {

	int z, z2, i, k, ofc, ofc2, al, cl;
	int ao, co, aorder, corder;
	int lcsize, lasize, ty, ver;
	bool err, to;
	CBarbecubeFile bqin, *bqout;
	CCubeFrame *cfr, *cfr2;
	CBitSet bshe, bsat, bscu, bstemp;
	CxString buf;


	lcsize = 1000000000; // To avoid "use of uninitialized variable" warning
	lasize = 1000000000;
	ofc = 0;
	ofc2 = 0;

	mprintf("\n");
	mprintf(WHITE,"    ******************************\n");
	mprintf(WHITE,"    ***   Splitting BQB File   ***\n");
	mprintf(WHITE,"    ******************************\n\n");

	SetBarbecubeVerbose(verbose);

	if (!bqin.OpenRead(infile)) {
		eprintf("CCCEngine::SplitBQB(): Error: Could not open BQB file \"%s\" for reading.\n",infile);
		return false;
	}

	m_oaOutputCubeBuf.resize(9);
	m_oaOutput2CubeBuf.resize(9);
	m_oaInputCubeBuf.resize(9);
	m_oaInputAtomBuf.resize(9);
	m_oaOutputAtomBuf.resize(9);
	m_oaOutput2AtomBuf.resize(9);
	for (z=0;z<9;z++) {
		m_oaInputCubeBuf[z] = NULL;
		m_oaOutputCubeBuf[z] = NULL;
		m_oaOutput2CubeBuf[z] = NULL;
		m_oaInputAtomBuf[z] = NULL;
		m_oaOutputAtomBuf[z] = NULL;
		m_oaOutput2AtomBuf[z] = NULL;
	}
	m_iInputCubeBufPos = 0;
	m_iOutputCubeBufPos = 0;
	m_iOutput2CubeBufPos = 0;
	m_iInputAtomBufPos = 0;
	m_iOutputAtomBufPos = 0;
	m_iOutput2AtomBufPos = 0;

	i = 0;
	k = 0;
	aorder = 8;
	corder = 8;
	bqout = NULL;
	to = false;

	mprintf("\n");
	while (true) {

		mprintf("    Reading input frame %d...\n",i+1);
		if (!bqin.ReadFrame()) {
			mprintf("Could not read further frame from input file.\n");
			break;
		}

		ty = bqin.GetFrameType();
		ver = bqin.GetFrameTypeVersion();

		if ((ty == 2) || (ty == 3)) {
			mprintf("      Skipping index frame.\n");
			goto _skip;
		}
		if ((ty != 8) && (ty != 9)) {
			eprintf("CCCEngine::SplitBQB(): Error: Splitting only implemented for compressed cube frames yet.\n");
			return false;
		}

		if (bqout == NULL) {
			buf.sprintf("%s%03d.bqb",outbase,k+1);
			mprintf("Creating next output file \"%s\".\n",(const char*)buf);
			bqout = new CBarbecubeFile();
			if (!bqout->OpenWriteReplace((const char*)buf)) {
				eprintf("CCCEngine::SplitBQB(): Error: Could not open BQB file \"%s\" for writing.\n",(const char*)buf);
				return false;
			}
			lcsize = 1000000000;
			lasize = 1000000000;
			aorder = 8;
			corder = 8;
			ofc = 0;
			ofc2 = 0;
		}

		mprintf("      Decompressing...\n");

		bshe.Clear();
		bshe.m_iaData.assign(bqin.GetFramePayload()->begin(),bqin.GetFramePayload()->begin()+8);

		al = bshe.ReadBitsInteger(32);
		cl = bshe.ReadBitsInteger(32);

		cfr = new CCubeFrame();
		PushOutputCubeFrame(cfr);
		PushInputCubeFrame_NoDelete(cfr);

		cfr->m_pAtoms = new CAtomSet();
		PushOutputAtomFrame(cfr->m_pAtoms);
		PushInputAtomFrame_NoDelete(cfr->m_pAtoms);

		bsat.Clear();
		bsat.m_iaData.assign(bqin.GetFramePayload()->begin()+8,bqin.GetFramePayload()->begin()+8+al);

		bscu.Clear();
		bscu.m_iaData.assign(bqin.GetFramePayload()->begin()+8+al,bqin.GetFramePayload()->begin()+8+al+cl);

		if (!DecompressAtomFrame(&bsat,ver,verbose,false))
			return false;

		if (!DecompressCubeFrame(&bscu,ver,verbose,false))
			return false;

		if (ofc >= 9) {

			mprintf("      Pass-through writing output file %d, frame %d...\n",k+1,ofc+1);

			bqout->CreateShortFrame((ofc==0)?BARBECUBE_FRAMETYPE_COMPCUBESTART:BARBECUBE_FRAMETYPE_COMPCUBE,0,i+1);
			bqout->PushPayload(*bqin.GetFramePayload());
			bqout->FinalizeFrame();

		} else {

			mprintf("      Re-compressing...\n");

_again:
			if (ofc2 < corder)
				co = ofc2;
			else if (ofc < corder)
				co = ofc;
			else
				co = corder;

			if (ofc2 < aorder)
				ao = ofc2;
			else if (ofc < aorder)
				ao = ofc;
			else
				ao = aorder;

_aagain:
			bsat.Clear();
			CompressAtomFrame(
				&bsat,
				ao,
				6,         // aprecision
				31,        // asplit
				40,        // ablock
				4,         // atables
				false,     // optatables
				true,      // acoderun
				true,      // abw
				true,      // amtf
				(ofc2>=2), // apreopt
				10,        // amaxiter
				true,      // asortatom
				(ofc==0),  // atominfo
				true,      // keepcomment
				0,         // amaxchunk
				verbose
				);

			mprintf("        Atoms: Order %d, output size %9.3f KiB.\n",ao,bsat.GetByteLength()/1024.0);

			if (lasize >= 0) {
				if (to) {
					to = false;
					if (bsat.GetByteLength() <= bstemp.GetByteLength()) {
						mprintf("        Size is indeed smaller with lower order. Limiting atom order to %d.\n",ao);
						aorder = ao;
						lasize = -1;
					} else {
						mprintf("        Size was smaller with higher order. Not limiting.\n");
						bsat = CBitSet(bstemp);
						lasize = bsat.GetByteLength();
					}
				} else {
					if ((ao != 0) && ((double)bsat.GetByteLength() >= (double)lasize*0.97)) {
						mprintf("        Size did not decrease any further with order %d. Trying order %d...\n",ao,ao-1);
						bstemp = CBitSet(bsat);
						to = true;
						ao--;
						goto _aagain;
					} else if (ao == aorder)
						lasize = -1;
					else
						lasize = bsat.GetByteLength();
				}
			}

_cagain:
			bscu.Clear();
			CompressCubeFrame(
				&bscu,
				co,
				12,       // ceps
				5,        // csigni
				31,       // csplit
				40,       // cblock
				6,        // ctables
				false,    // copttables
				true,     // chilbert
				1.075,    // nbhfac
				true,     // ccoderun
				false,    // cbw
				false,    // cmtf
				false,    // cpreopt
				10,       // cmaxiter
				(ofc==0), // atominfo
				0,        // cmaxchunk
				verbose
				);

			mprintf("        Cube:  Order %d, output size %9.3f KiB.\n",co,bscu.GetByteLength()/1024.0);

			if (lcsize >= 0) {
				if (to) {
					to = false;
					if (bscu.GetByteLength() <= bstemp.GetByteLength()) {
						mprintf("        Size is indeed smaller with lower order. Limiting cube order to %d.\n",co);
						corder = co;
						lcsize = -1;
					} else {
						mprintf("        Size was smaller with higher order. Not limiting.\n");
						bscu = CBitSet(bstemp);
						lcsize = bscu.GetByteLength();
					}
				} else {
					if ((co != 0) && ((double)bscu.GetByteLength() >= (double)lcsize*0.97)) {
						mprintf("        Size did not decrease any further with order %d. Trying order %d...\n",co,co-1);
						bstemp = CBitSet(bscu);
						to = true;
						co--;
						goto _cagain;
					} else if (co == corder)
						lcsize = -1;
					else
						lcsize = bscu.GetByteLength();
				}
			}

			mprintf("        Comparing input and output...\n");

			cfr2 = new CCubeFrame();
			PushOutput2CubeFrame(cfr2);
			cfr2->m_pAtoms = new CAtomSet();
			PushOutput2AtomFrame(cfr2->m_pAtoms);

			DecompressAtomFrame(&bsat,1,verbose,true);

			DecompressCubeFrame(&bscu,1,verbose,true);

			err = false;
			for (z=0;z<(int)cfr2->m_pAtoms->m_oaAtoms.size();z++)
				for (z2=0;z2<3;z2++)
					if (cfr->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2] != cfr2->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2]) {
						eprintf("        Error in atom coordinate %d[%d]: %.6f (%ld) != %.6f (%ld)\n",z,z2,cfr->m_pAtoms->m_oaAtoms[z]->m_fCoord[z2],cfr->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2],cfr2->m_pAtoms->m_oaAtoms[z]->m_fCoord[z2],cfr2->m_pAtoms->m_oaAtoms[z]->m_iCoord[z2]);
						err = true;
					}

			for (z=0;z<cfr->m_iResXYZ;z++)
				if (!ExpMantisEqual(cfr->m_iaExpo[z],cfr->m_iaMantis[z],cfr2->m_iaExpo[z],cfr2->m_iaMantis[z])) {
					eprintf("        Error in volumetric data element %7d: %.10G vs %.10G\n",z,cfr->m_faBin[z],cfr2->m_faBin[z]);
					err = true;
				}

			if (err) {
				if (ofc2 != 0) {
					eprintf("Errors occured. Compressing frame again with order zero.\n");
					err = false;
					ofc2 = 0;
					goto _again;
				} else {
					eprintf("Errors occured despite of order zero. Aborting.\n");
					return false;
				}
			}

			bshe.Clear();
			bshe.WriteBits(bsat.GetByteLength(),32);
			bshe.WriteBits(bscu.GetByteLength(),32);

			mprintf("      Writing output file %d, frame %d...\n",k+1,ofc+1);

			bqout->CreateShortFrame((ofc2==0)?BARBECUBE_FRAMETYPE_COMPCUBESTART:BARBECUBE_FRAMETYPE_COMPCUBE,0,i+1);
			bqout->PushPayload(bshe.m_iaData);
			bqout->PushPayload(bsat.m_iaData);
			bqout->PushPayload(bscu.m_iaData);
			bqout->FinalizeFrame();
		}

_skip:
		i++;
		ofc++;
		ofc2++;

		if ((i % splitlength) == 0) {
			mprintf("Closing output file \"%s\".\n",(const char*)buf);
			bqout->WriteIndexFrame(true);
			bqout->Close();
			delete bqout;
			bqout = NULL;
			k++;
		}

		if (i == steps) {
			mprintf("Reached step limit.\n\n");
			break;
		}
	}

	if (bqout != NULL) {
		mprintf("Closing output file \"%s\".\n\n",(const char*)buf);
		bqout->WriteIndexFrame(true);
		bqout->Close();
		delete bqout;
		bqout = NULL;
		k++;
	}

	mprintf("Processed %d frames, wrote %d output files.\n\n",i,k);

	bqin.Close();

	// Those are duplicates of the output buffers and deleted there
	m_oaInputAtomBuf.clear();
	m_oaInputCubeBuf.clear();

	mprintf("All done. Leaving.\n");

	return true;
}*/


void CCCEngine::ExportExtrapolatorSettings(CBitSet *bs, bool volumetric, bool verbose) {

	UNUSED(verbose);

	if (volumetric) {

		// Use Volumetric Extrapolator?
		if (m_pExtrapolator == NULL) {
			bs->WriteBit(0);
			return;
		}
		bs->WriteBit(1);

		// ResX
		if ((m_pExtrapolator->m_iRes[0] < 1) || (m_pExtrapolator->m_iRes[0] > 1023)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.ResX out of range (%d, allowed 1..1023).\n",m_pExtrapolator->m_iRes[0]);
			abort();
		}
		bs->WriteBits(m_pExtrapolator->m_iRes[0],10);

		// ResY
		if ((m_pExtrapolator->m_iRes[1] < 1) || (m_pExtrapolator->m_iRes[1] > 1023)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.ResY out of range (%d, allowed 1..1023).\n",m_pExtrapolator->m_iRes[1]);
			abort();
		}
		bs->WriteBits(m_pExtrapolator->m_iRes[1],10);

		// ResZ
		if ((m_pExtrapolator->m_iRes[2] < 1) || (m_pExtrapolator->m_iRes[2] > 1023)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.ResZ out of range (%d, allowed 1..1023).\n",m_pExtrapolator->m_iRes[2]);
			abort();
		}
		bs->WriteBits(m_pExtrapolator->m_iRes[2],10);

		// PredCorr
		if (m_pExtrapolatorCorr != NULL)
			bs->WriteBit(1);
		else
			bs->WriteBit(0);

		if (m_pExtrapolatorCorr != NULL) {

			// SRangeX
			if ((m_pExtrapolatorCorr->m_iSRange[0] < 1) || (m_pExtrapolatorCorr->m_iSRange[0] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SRangeX out of range (%d, allowed 1..15).\n",m_pExtrapolatorCorr->m_iSRange[0]);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSRange[0],4);

			// SRangeY
			if ((m_pExtrapolatorCorr->m_iSRange[1] < 1) || (m_pExtrapolatorCorr->m_iSRange[1] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SRangeY out of range (%d, allowed 1..15).\n",m_pExtrapolatorCorr->m_iSRange[1]);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSRange[1],4);

			// SRangeZ
			if ((m_pExtrapolatorCorr->m_iSRange[2] < 1) || (m_pExtrapolatorCorr->m_iSRange[2] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SRangeZ out of range (%d, allowed 1..15).\n",m_pExtrapolatorCorr->m_iSRange[2]);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSRange[2],4);

			// SOrder
			if ((m_pExtrapolatorCorr->m_iSOrder < 0) || (m_pExtrapolatorCorr->m_iSOrder > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOrder out of range (%d, allowed 0..15).\n",m_pExtrapolatorCorr->m_iSOrder);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSOrder,4);

			// OffsetX
			if ((m_pExtrapolatorCorr->m_iSOffset[0] < 0) || (m_pExtrapolatorCorr->m_iSOffset[0] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOffsetX out of range (%d, allowed 0..15).\n",m_pExtrapolatorCorr->m_iSOffset[0]);
				abort();
			}
			if (m_pExtrapolatorCorr->m_iSOffset[0] >= m_pExtrapolatorCorr->m_iSRange[0]) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOffsetX (%d) >= SRangeX (%d.\n",m_pExtrapolatorCorr->m_iSOffset[0],m_pExtrapolatorCorr->m_iSRange[0]);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSOffset[0],4);

			// OffsetY
			if ((m_pExtrapolatorCorr->m_iSOffset[1] < 0) || (m_pExtrapolatorCorr->m_iSOffset[1] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOffsetY out of range (%d, allowed 0..15).\n",m_pExtrapolatorCorr->m_iSOffset[1]);
				abort();
			}
			if (m_pExtrapolatorCorr->m_iSOffset[1] >= m_pExtrapolatorCorr->m_iSRange[1]) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOffsetY (%d) >= SRangeY (%d.\n",m_pExtrapolatorCorr->m_iSOffset[1],m_pExtrapolatorCorr->m_iSRange[1]);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSOffset[1],4);

			// OffsetZ
			if ((m_pExtrapolatorCorr->m_iSOffset[2] < 0) || (m_pExtrapolatorCorr->m_iSOffset[2] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOffsetZ out of range (%d, allowed 0..15).\n",m_pExtrapolatorCorr->m_iSOffset[2]);
				abort();
			}
			if (m_pExtrapolatorCorr->m_iSOffset[2] >= m_pExtrapolatorCorr->m_iSRange[2]) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.SOffsetZ (%d) >= SRangeZ (%d.\n",m_pExtrapolatorCorr->m_iSOffset[2],m_pExtrapolatorCorr->m_iSRange[2]);
				abort();
			}
			bs->WriteBits(m_pExtrapolatorCorr->m_iSOffset[2],4);

			// CrossS
			if (m_pExtrapolatorCorr->m_bCrossS)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			// CrossRangeS
			if (m_pExtrapolatorCorr->m_bCrossRangeS)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			// Wrap
			if (m_pExtrapolatorCorr->m_bWrap)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			// DistExpo
			if ((m_pExtrapolatorCorr->m_fDistExpo < 0) || (m_pExtrapolatorCorr->m_fDistExpo > 16.0)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.DistExpo out of range (%.3f, allowed 0..16).\n",m_pExtrapolatorCorr->m_fDistExpo);
				abort();
			}
			bs->WriteBits((unsigned int)floor(m_pExtrapolatorCorr->m_fDistExpo*1000.0+0.5),14);

			// CorrFactor
			if ((m_pExtrapolatorCorr->m_fCorrFactor < 0) || (m_pExtrapolatorCorr->m_fCorrFactor > 2.0)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorCorr.CorrFactor out of range (%.3f, allowed 0..2).\n",m_pExtrapolatorCorr->m_fCorrFactor);
				abort();
			}
			bs->WriteBits((unsigned int)floor(m_pExtrapolatorCorr->m_fCorrFactor*1000.0+0.5),11);

		} else {

			// SRangeX
			if ((m_pExtrapolator->m_iSRange[0] < 1) || (m_pExtrapolator->m_iSRange[0] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SRangeX out of range (%d, allowed 1..15).\n",m_pExtrapolator->m_iSRange[0]);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSRange[0],4);

			// SRangeY
			if ((m_pExtrapolator->m_iSRange[1] < 1) || (m_pExtrapolator->m_iSRange[1] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SRangeY out of range (%d, allowed 1..15).\n",m_pExtrapolator->m_iSRange[1]);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSRange[1],4);

			// SRangeZ
			if ((m_pExtrapolator->m_iSRange[2] < 1) || (m_pExtrapolator->m_iSRange[2] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SRangeZ out of range (%d, allowed 1..15).\n",m_pExtrapolator->m_iSRange[2]);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSRange[2],4);

			// SOrder
			if ((m_pExtrapolator->m_iSOrder < 0) || (m_pExtrapolator->m_iSOrder > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOrder out of range (%d, allowed 0..15).\n",m_pExtrapolator->m_iSOrder);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSOrder,4);

			// OffsetX
			if ((m_pExtrapolator->m_iSOffset[0] < 0) || (m_pExtrapolator->m_iSOffset[0] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOffsetX out of range (%d, allowed 0..15).\n",m_pExtrapolator->m_iSOffset[0]);
				abort();
			}
			if (m_pExtrapolator->m_iSOffset[0] >= m_pExtrapolator->m_iSRange[0]) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOffsetX (%d) >= SRangeX (%d.\n",m_pExtrapolator->m_iSOffset[0],m_pExtrapolator->m_iSRange[0]);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSOffset[0],4);

			// OffsetY
			if ((m_pExtrapolator->m_iSOffset[1] < 0) || (m_pExtrapolator->m_iSOffset[1] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOffsetY out of range (%d, allowed 0..15).\n",m_pExtrapolator->m_iSOffset[1]);
				abort();
			}
			if (m_pExtrapolator->m_iSOffset[1] >= m_pExtrapolator->m_iSRange[1]) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOffsetY (%d) >= SRangeY (%d.\n",m_pExtrapolator->m_iSOffset[1],m_pExtrapolator->m_iSRange[1]);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSOffset[1],4);

			// OffsetZ
			if ((m_pExtrapolator->m_iSOffset[2] < 0) || (m_pExtrapolator->m_iSOffset[2] > 15)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOffsetZ out of range (%d, allowed 0..15).\n",m_pExtrapolator->m_iSOffset[2]);
				abort();
			}
			if (m_pExtrapolator->m_iSOffset[2] >= m_pExtrapolator->m_iSRange[2]) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.SOffsetZ (%d) >= SRangeZ (%d.\n",m_pExtrapolator->m_iSOffset[2],m_pExtrapolator->m_iSRange[2]);
				abort();
			}
			bs->WriteBits(m_pExtrapolator->m_iSOffset[2],4);

			// CrossS
			if (m_pExtrapolator->m_bCrossS)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			// CrossRangeS
			if (m_pExtrapolator->m_bCrossRangeS)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			// Wrap
			if (m_pExtrapolator->m_bWrap)
				bs->WriteBit(1);
			else
				bs->WriteBit(0);

			// DistExpo
			if ((m_pExtrapolator->m_fDistExpo < 0) || (m_pExtrapolator->m_fDistExpo > 16.0)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.DistExpo out of range (%.3f, allowed 0..16).\n",m_pExtrapolator->m_fDistExpo);
				abort();
			}
			bs->WriteBits((unsigned int)floor(m_pExtrapolator->m_fDistExpo*1000.0+0.5),14);

			// CorrFactor
			if ((m_pExtrapolator->m_fCorrFactor < 0) || (m_pExtrapolator->m_fCorrFactor > 2.0)) {
				eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.CorrFactor out of range (%.3f, allowed 0..2).\n",m_pExtrapolator->m_fCorrFactor);
				abort();
			}
			bs->WriteBits((unsigned int)floor(m_pExtrapolator->m_fCorrFactor*1000.0+0.5),11);
		}

		// TRange
		if ((m_pExtrapolator->m_iTRange < 0) || (m_pExtrapolator->m_iTRange > 15)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.TRange out of range (%d, allowed 0..15).\n",m_pExtrapolator->m_iTRange);
			abort();
		}
		bs->WriteBits(m_pExtrapolator->m_iTRange,4);

		// TOrder
		if ((m_pExtrapolator->m_iTOrder < 0) || (m_pExtrapolator->m_iTOrder > 15)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.TOrder out of range (%d, allowed 0..15).\n",m_pExtrapolator->m_iTOrder);
			abort();
		}
		bs->WriteBits(m_pExtrapolator->m_iTOrder,4);

		// CrossT
		if (m_pExtrapolator->m_bCrossT)
			bs->WriteBit(1);
		else
			bs->WriteBit(0);

		// CrossRangeT
		if (m_pExtrapolator->m_bCrossRangeT)
			bs->WriteBit(1);
		else
			bs->WriteBit(0);

		// TimeExpo
		if ((m_pExtrapolator->m_fTimeExpo < 0) || (m_pExtrapolator->m_fTimeExpo > 16.0)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolator.TimeExpo out of range (%.3f, allowed 0..16).\n",m_pExtrapolator->m_fTimeExpo);
			abort();
		}
		bs->WriteBits((unsigned int)floor(m_pExtrapolator->m_fTimeExpo*1000.0+0.5),14);

		// Reserved
		bs->WriteBit(0);

	} else { // Position

		// Use Position Extrapolator?
		if (m_pExtrapolatorXYZ == NULL) {
			bs->WriteBit(0);
			return;
		}
		bs->WriteBit(1);

		// TRange
		if ((m_pExtrapolatorXYZ->m_iTRange < 0) || (m_pExtrapolatorXYZ->m_iTRange > 15)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorXYZ.TRange out of range (%d, allowed 0..15).\n",m_pExtrapolatorXYZ->m_iTRange);
			abort();
		}
		bs->WriteBits(m_pExtrapolatorXYZ->m_iTRange,4);

		// TOrder
		if ((m_pExtrapolatorXYZ->m_iTOrder < 0) || (m_pExtrapolatorXYZ->m_iTOrder > 15)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorXYZ.TOrder out of range (%d, allowed 0..15).\n",m_pExtrapolatorXYZ->m_iTOrder);
			abort();
		}
		bs->WriteBits(m_pExtrapolatorXYZ->m_iTOrder,4);

		// TimeExpo
		if ((m_pExtrapolatorXYZ->m_fTimeExpo < 0) || (m_pExtrapolatorXYZ->m_fTimeExpo > 16.0)) {
			eprintf("CCCEngine::ExportExtrapolatorSettings(): Error: m_pExtrapolatorXYZ.TimeExpo out of range (%.3f, allowed 0..16).\n",m_pExtrapolatorXYZ->m_fTimeExpo);
			abort();
		}
		bs->WriteBits((unsigned int)floor(m_pExtrapolatorXYZ->m_fTimeExpo*1000.0+0.5),14);

		// Reserved
		bs->WriteBit(0);
	}
}


void CCCEngine::ImportExtrapolatorSettings(CBitSet *bs, bool volumetric, bool verbose) {

	int srangex, srangey, srangez, trange, sorder, torder, offsetx, offsety, offsetz, postrange, postorder, resx, resy, resz;
	bool crosss, crosst, wrap, crossranges, crossranget;
	double distexpo, timeexpo, corrfactor, postimeexpo;
	bool predcorr;


	if (volumetric) {

		if (!bs->ReadBit()) {
			if (m_pExtrapolator != NULL) {
				eprintf("CCCEngine::ImportExtrapolatorSettings(): Warning: No volumetric extrapolator specified in input frame, but m_pExtrapolator != NULL.\n");
				delete m_pExtrapolator;
				m_pExtrapolator = NULL;
			}
			if (m_pExtrapolatorCorr != NULL) {
				eprintf("CCCEngine::ImportExtrapolatorSettings(): Warning: No volumetric extrapolator specified in input frame, but m_pExtrapolatorCorr != NULL.\n");
				delete m_pExtrapolatorCorr;
				m_pExtrapolatorCorr = NULL;
			}
			return;
		}

		// ResX
		resx = bs->ReadBitsInteger(10);

		// ResY
		resy = bs->ReadBitsInteger(10);

		// ResZ
		resz = bs->ReadBitsInteger(10);

		// PredCorr
		predcorr = bs->ReadBit();

		// SRangeX
		srangex = bs->ReadBitsInteger(4);

		// SRangeY
		srangey = bs->ReadBitsInteger(4);

		// SRangeZ
		srangez = bs->ReadBitsInteger(4);

		// SOrder
		sorder = ((int)bs->ReadBitsInteger(4));

		// OffsetX
		offsetx = bs->ReadBitsInteger(4);

		// OffsetY
		offsety = bs->ReadBitsInteger(4);

		// OffsetZ
		offsetz = bs->ReadBitsInteger(4);

		// CrossS
		crosss = bs->ReadBit();

		// CrossRangeS
		crossranges = bs->ReadBit();

		// Wrap
		wrap = bs->ReadBit();

		// DistExpo
		distexpo = bs->ReadBitsInteger(14) / 1000.0;

		// CorrFactor
		corrfactor = bs->ReadBitsInteger(11) / 1000.0;

		// TRange
		trange = bs->ReadBitsInteger(4);

		// TOrder
		torder = ((int)bs->ReadBitsInteger(4));

		// CrossT
		crosst = bs->ReadBit();

		// CrossRangeT
		crossranget = bs->ReadBit();

		// TimeExpo
		timeexpo = bs->ReadBitsInteger(14) / 1000.0;

		// Reserved
		if (bs->ReadBit()) {
			eprintf("CCCEngine::ImportExtrapolatorSettings(): Error: Reserved bit after volumetric extrapolator is 1.\n");
			mprintf("Either the BQB file is damaged, or was written with a more recent software version.\n\n");
			abort();
		}


		if (m_pExtrapolator != NULL) {

			if (predcorr) {

				if (m_pExtrapolatorCorr == NULL)
					goto _volcheckfail;

				// SRangeX
				if (m_pExtrapolatorCorr->m_iSRange[0] != srangex)
					goto _volcheckfail;

				// SRangeY
				if (m_pExtrapolatorCorr->m_iSRange[1] != srangey)
					goto _volcheckfail;

				// SRangeZ
				if (m_pExtrapolatorCorr->m_iSRange[2] != srangez)
					goto _volcheckfail;

				// SOrder
				if (m_pExtrapolatorCorr->m_iSOrder != sorder)
					goto _volcheckfail;

				// OffsetX
				if (m_pExtrapolatorCorr->m_iSOffset[0] != offsetx)
					goto _volcheckfail;

				// OffsetY
				if (m_pExtrapolatorCorr->m_iSOffset[1] != offsety)
					goto _volcheckfail;

				// OffsetZ
				if (m_pExtrapolatorCorr->m_iSOffset[2] != offsetz)
					goto _volcheckfail;

				// CrossS
				if (m_pExtrapolatorCorr->m_bCrossS != crosss)
					goto _volcheckfail;

				// CrossRangeS
				if (m_pExtrapolatorCorr->m_bCrossRangeS != crossranges)
					goto _volcheckfail;

				// Wrap
				if (m_pExtrapolatorCorr->m_bWrap != wrap)
					goto _volcheckfail;

				// DistExpo
				if (fabs(m_pExtrapolatorCorr->m_fDistExpo-distexpo) > 1.0e-6)
					goto _volcheckfail;

				// CorrFactor
				if (fabs(m_pExtrapolatorCorr->m_fCorrFactor-corrfactor) > 1.0e-6)
					goto _volcheckfail;

			} else {

				if (m_pExtrapolatorCorr != NULL)
					goto _volcheckfail;

				// SRangeX
				if (m_pExtrapolator->m_iSRange[0] != srangex)
					goto _volcheckfail;

				// SRangeY
				if (m_pExtrapolator->m_iSRange[1] != srangey)
					goto _volcheckfail;

				// SRangeZ
				if (m_pExtrapolator->m_iSRange[2] != srangez)
					goto _volcheckfail;

				// SOrder
				if (m_pExtrapolator->m_iSOrder != sorder)
					goto _volcheckfail;

				// OffsetX
				if (m_pExtrapolator->m_iSOffset[0] != offsetx)
					goto _volcheckfail;

				// OffsetY
				if (m_pExtrapolator->m_iSOffset[1] != offsety)
					goto _volcheckfail;

				// OffsetZ
				if (m_pExtrapolator->m_iSOffset[2] != offsetz)
					goto _volcheckfail;

				// CrossS
				if (m_pExtrapolator->m_bCrossS != crosss)
					goto _volcheckfail;

				// CrossRangeS
				if (m_pExtrapolator->m_bCrossRangeS != crossranges)
					goto _volcheckfail;

				// Wrap
				if (m_pExtrapolator->m_bWrap != wrap)
					goto _volcheckfail;

				// DistExpo
				if (fabs(m_pExtrapolator->m_fDistExpo-distexpo) > 1.0e-6)
					goto _volcheckfail;

				// CorrFactor
				if (fabs(m_pExtrapolator->m_fCorrFactor-corrfactor) > 1.0e-6)
					goto _volcheckfail;
			}

			// ResX
			if (m_pExtrapolator->m_iRes[0] != resx)
				goto _volcheckfail;

			// ResY
			if (m_pExtrapolator->m_iRes[1] != resy)
				goto _volcheckfail;

			// ResZ
			if (m_pExtrapolator->m_iRes[2] != resz)
				goto _volcheckfail;

			// TRange
			if (m_pExtrapolator->m_iTRange != trange)
				goto _volcheckfail;

			// TOrder
			if (m_pExtrapolator->m_iTOrder != torder)
				goto _volcheckfail;

			// CrossT
			if (m_pExtrapolator->m_bCrossT != crosst)
				goto _volcheckfail;

			// CrossRangeT
			if (m_pExtrapolator->m_bCrossRangeT != crossranget)
				goto _volcheckfail;

			// TimeExpo
			if (fabs(m_pExtrapolator->m_fTimeExpo-timeexpo) > 1.0e-6)
				goto _volcheckfail;

			return;

_volcheckfail:
			eprintf("CCCEngine::ImportExtrapolatorSettings(): Warning: Reloading volumetric extrapolator with different settings.\n");
			delete m_pExtrapolator;
			m_pExtrapolator = NULL;
		}

		if (m_pExtrapolatorCorr != NULL) {
			delete m_pExtrapolatorCorr;
			m_pExtrapolatorCorr = NULL;
		}

		if (predcorr) {

			mprintf("      Initializing Volumetric Predictor Extrapolator (%d|%d)...\n",trange,torder);
			m_pExtrapolator = new CExtrapolator();
			m_pExtrapolator->Initialize(
				resx,     // resx,
				resy,     // resy,
				resz,     // resz,
				1,        // srangex,
				1,        // srangey,
				1,        // srangez,
				trange,   // trange,
				0,        // sorder,
				torder,   // torder,
				0,        // offsetx,
				0,        // offsety,
				0,        // offsetz,
				false,    // crosss,
				false,    // crosst,
				false,    // wrap,
				false,    // crossranges,
				false,    // crossranget
				0,        // distexpo
				timeexpo, // timeexpo
				1.0,      // corrfactor
				verbose,
				false
			);

			if ((srangex == srangey) && (srangex == srangez))
				mprintf("      Initializing Volumetric Corrector Extrapolator (%d/%d)...\n",srangex,sorder);
			else
				mprintf("      Initializing Volumetric Corrector Extrapolator (%d|%d|%d/%d)...\n",srangex,srangey,srangez,sorder);
			m_pExtrapolatorCorr = new CExtrapolator();
			m_pExtrapolatorCorr->Initialize(
				resx,        // resx,
				resy,        // resy,
				resz,        // resz,
				srangex,     // srangex,
				srangey,     // srangey,
				srangez,     // srangez,
				1,           // trange,
				sorder,      // sorder,
				0,           // torder,
				offsetx,     // offsetx,
				offsety,     // offsety,
				offsetz,     // offsetz,
				crosss,      // crosss,
				false,       // crosst,
				wrap,        // wrap,
				crossranges, // crossranges,
				false,       // crossranget
				distexpo,    // distexpo
				0,           // timeexpo
				corrfactor,  // corrfactor
				verbose,
				false
			);

		} else {

			if ((srangex == srangey) && (srangex == srangez))
				mprintf("      Initializing Volumetric Extrapolator (%d/%d;%d/%d)...\n",srangex,sorder,trange,torder);
			else
				mprintf("      Initializing Volumetric Extrapolator (%d|%d|%d/%d;%d/%d)...\n",srangex,srangey,srangez,sorder,trange,torder);
			m_pExtrapolator = new CExtrapolator();
			m_pExtrapolator->Initialize(
				resx,        // resx,
				resy,        // resy,
				resz,        // resz,
				srangex,     // srangex,
				srangey,     // srangey,
				srangez,     // srangez,
				trange,      // trange,
				sorder,      // sorder,
				torder,      // torder,
				offsetx,     // offsetx,
				offsety,     // offsety,
				offsetz,     // offsetz,
				crosss,      // crosss,
				crosst,      // crosst,
				wrap,        // wrap,
				crossranges, // crossranges,
				crossranget, // crossranget
				distexpo,    // distexpo
				timeexpo,    // timeexpo
				1.0,         // corrfactor
				verbose,
				false
			);
		}
	
	} else {

		// Use Position Extrapolator?
		if (!bs->ReadBit()) {
			if (m_pExtrapolatorXYZ != NULL) {
				eprintf("CCCEngine::ImportExtrapolatorSettings(): Warning: No position extrapolator specified in input frame, but m_pExtrapolatorXYZ != NULL.\n");
				delete m_pExtrapolatorXYZ;
				m_pExtrapolatorXYZ = NULL;
			}
			return;
		}

		// TRange
		postrange = bs->ReadBitsInteger(4);

		// TOrder
		postorder = ((int)bs->ReadBitsInteger(4));

		// TimeExpo
		postimeexpo = bs->ReadBitsInteger(14) / 1000.0;

		// Reserved
		if (bs->ReadBit()) {
			eprintf("CCCEngine::ImportExtrapolatorSettings(): Error: Reserved bit after position extrapolator is 1.\n");
			mprintf("Either the BQB file is damaged, or was written with a more recent software version.\n\n");
			abort();
		}

		if (m_pExtrapolatorXYZ != NULL) {

			// TRange
			if (m_pExtrapolatorXYZ->m_iTRange != postrange)
				goto _poscheckfail;

			// TOrder
			if (m_pExtrapolatorXYZ->m_iTOrder != postorder)
				goto _poscheckfail;

			// TimeExpo
			if (fabs(m_pExtrapolatorXYZ->m_fTimeExpo-postimeexpo) > 1.0e-6)
				goto _poscheckfail;

			return;

_poscheckfail:
			eprintf("CCCEngine::ImportExtrapolatorSettings(): Warning: Reloading position extrapolator with different settings.\n");
			delete m_pExtrapolatorXYZ;
			m_pExtrapolatorXYZ = NULL;
		}

		mprintf("      Initializing Extrapolator (%d|%d)...\n",postrange,postorder);
		m_pExtrapolatorXYZ = new CExtrapolator();
		m_pExtrapolatorXYZ->InitializeXYZ(postrange,postorder,postimeexpo,verbose,false);
	}
}




