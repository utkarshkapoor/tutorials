/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


// This must always be the first include directive
#include "config.h"

#include "cc_crc32.h"


/* This code is taken from   http://www.networkdls.com/Software/View/CRC32   */


CCRC32::CCRC32() {

	//0x04C11DB7 is the official polynomial used by PKZip, WinZip and Ethernet.
	unsigned int iPolynomial = 0x04C11DB7;

	memset(&this->iTable, 0, sizeof(this->iTable));

	// 256 values representing ASCII character codes.
	for(int iCodes = 0; iCodes <= 0xFF; iCodes++) {
		this->iTable[iCodes] = this->Reflect(iCodes, 8) << 24;

		for(int iPos = 0; iPos < 8; iPos++) {
			this->iTable[iCodes] = (this->iTable[iCodes] << 1)
				^ ((this->iTable[iCodes] & (1 << 31)) ? iPolynomial : 0);
		}

		this->iTable[iCodes] = this->Reflect(this->iTable[iCodes], 32);
	}
}


unsigned int CCRC32::Reflect(unsigned int iReflect, const char cChar) const {

	unsigned int iValue = 0;

	// Swap bit 0 for bit 7, bit 1 For bit 6, etc....
	for(int iPos = 1; iPos < (cChar + 1); iPos++) {
		if(iReflect & 1)
			iValue |= (1 << (cChar - iPos));
		iReflect >>= 1;
	}

	return iValue;
}


unsigned int CCRC32::ComputeCRC32(const std::vector<unsigned char> &data, int from, int to) const {

	unsigned int crc;
	int z;

	crc = 0xffffffff; // Initialize the CRC.

	if (to == -1)
		to = (int)data.size()-1;

	for (z=from;z<=to;z++)
		crc = (crc >> 8) ^ this->iTable[(crc & 0xFF) ^ data[z]];

	crc ^= 0xffffffff; // Finalize the CRC.

	return crc;
}


unsigned int CCRC32::ComputeCRC32_Begin(const std::vector<unsigned char> &data, int from, int to) const {

	unsigned int crc;
	int z;

	crc = 0xffffffff; // Initialize the CRC.

	if (to == -1)
		to = (int)data.size()-1;

	for (z=from;z<=to;z++)
		crc = (crc >> 8) ^ this->iTable[(crc & 0xFF) ^ data[z]];

	return crc;
}


unsigned int CCRC32::ComputeCRC32_Continue(const std::vector<unsigned char> &data, unsigned int last, int from, int to) const {

	unsigned int crc;
	int z;

	crc = last; // Initialize the CRC.

	if (to == -1)
		to = (int)data.size()-1;

	for (z=from;z<=to;z++)
		crc = (crc >> 8) ^ this->iTable[(crc & 0xFF) ^ data[z]];

	return crc;
}


unsigned int CCRC32::ComputeCRC32_Finish(unsigned int last) const {

	return last ^ 0xffffffff; // Finalize the CRC.
}


