/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_BITSET_H
#define CC_BITSET_H


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include "tools.h"
#include <vector>


class CBitSet {
public:

	CBitSet()
		: m_iExcessBits(0), m_iReadPosBytes(0), m_iReadPosExcess(0) { }


	CBitSet(const CBitSet &set)
		: m_iExcessBits(set.m_iExcessBits), m_iReadPosBytes(set.m_iReadPosBytes), m_iReadPosExcess(set.m_iReadPosExcess) {
		m_iaData.assign(set.m_iaData.begin(),set.m_iaData.end());
	}


	explicit CBitSet(CBitSet *set)
		: m_iExcessBits(set->m_iExcessBits) {
		m_iaData.assign(set->m_iaData.begin(),set->m_iaData.end());
	}


	int GetLength() const {
		if (m_iExcessBits == 0)
			return (int)(8*m_iaData.size());
		else
			return (int)(8*(m_iaData.size()-1) + m_iExcessBits);
	}


	int GetByteLength() const {
		return (int)m_iaData.size();
	}


	void Dump() const;
	void DumpPlain() const;
	void ExportToFile(FILE *a) const;
	bool ImportFromFile(FILE *a, int bytes);


	void Clear() {
		m_iExcessBits = 0;
		m_iReadPosBytes = 0;
		m_iReadPosExcess = 0;
		m_iaData.clear();
	}


	void WriteBit(unsigned char i) {
		if (m_iExcessBits == 0)
			m_iaData.push_back(i);
		else
			m_iaData[m_iaData.size()-1] |= i<<m_iExcessBits;

		m_iExcessBits++;

		if (m_iExcessBits == 8)
			m_iExcessBits = 0;
	}


	void WriteBits(unsigned long i, int bits) {
		if ((int)mylog2(i+1) > bits) {
			eprintf("CBitSet::WriteBits(): Error: %lu does not fit into %d bits.\n",i,bits);
			abort();
		}
		for (int z=0;z<bits;z++)
			WriteBit((i&(1<<z))!=0?1:0);
	}


	void WriteSignedBits(long i, int bits) {
		unsigned long ul;
		ul = iabs(i);
		if ((int)mylog2(ul+1) > bits-1) {
			eprintf("CBitSet::WriteSignedBits(): Error: %ld does not fit into %d bits.\n",i,bits);
			abort();
		}
		for (int z=0;z<bits-1;z++)
			WriteBit((ul&(1<<z))!=0?1:0);
		if (i >= 0)
			WriteBit(0);
		else
			WriteBit(1);
	}


	void WriteBitsFloat(float f) {
		unsigned char *uc;
		int z;
		uc = reinterpret_cast<unsigned char*>(&f);
		for (z=0;z<4;z++)
			WriteBits(uc[z],8);
	}


	void WriteBitsDouble(double f) {
		unsigned char *uc;
		int z;
		uc = reinterpret_cast<unsigned char*>(&f);
		for (z=0;z<8;z++)
			WriteBits(uc[z],8);
	}


	float ReadBitsFloat() {
		unsigned char uc[4];
		char *p;

		int z;
		for (z=0;z<4;z++)
			uc[z] = (unsigned char)ReadBitsInteger(8);
		p = (char*)uc; // This is to comply with strict-aliasing rules (char* may alias to anything)
		return *reinterpret_cast<float*>(p);
	}


	double ReadBitsDouble() {
		unsigned char uc[8];
		int z;
		char *p;

		for (z=0;z<8;z++)
			uc[z] = (unsigned char)ReadBitsInteger(8);
		p = (char*)uc; // This is to comply with strict-aliasing rules (char* may alias to anything)
		return *reinterpret_cast<double*>(p);
	}


	void WriteBits(CBitSet *set) {
		int z, z2;
		if (set->m_iExcessBits == 0) {
			for (z=0;z<(int)set->m_iaData.size();z++)
				for (z2=0;z2<8;z2++)
					WriteBit((set->m_iaData[z]&(1<<z2))!=0?1:0);
		} else {
			for (z=0;z<(int)set->m_iaData.size()-1;z++)
				for (z2=0;z2<8;z2++)
					WriteBit((set->m_iaData[z]&(1<<z2))!=0?1:0);
			for (z2=0;z2<set->m_iExcessBits;z2++)
				WriteBit((set->m_iaData[set->m_iaData.size()-1]&(1<<z2))!=0?1:0);
		}
	}


	void WriteProgressiveUnsignedChar(unsigned char i) {
		if (i < 4) {
			WriteBit(0);
			WriteBits(i,2);
		} else if (i < 16) {
			WriteBit(1);
			WriteBit(0);
			WriteBits(i,4);
		} else if (i < 64) {
			WriteBit(1);
			WriteBit(1);
			WriteBit(0);
			WriteBits(i,6);
		} else {
			WriteBit(1);
			WriteBit(1);
			WriteBit(1);
			WriteBit(0);
			WriteBits(i,8);
		}
	}


	unsigned char ReadProgressiveUnsignedChar() {
		int z = 1;
		while (ReadBit())
			z++;
		return (unsigned char)ReadBitsInteger(z*2);
	}


	void WriteProgressiveUnsignedInt(unsigned int i) {
		if (i < 256) {
			WriteBit(0);
			WriteBits(i,8);
		} else if (i < 65536) {
			WriteBit(1);
			WriteBit(0);
			WriteBits(i,16);
		} else if (i < 16777216) {
			WriteBit(1);
			WriteBit(1);
			WriteBit(0);
			WriteBits(i,24);
		} else {
			WriteBit(1);
			WriteBit(1);
			WriteBit(1);
			WriteBit(0);
			WriteBits(i,32);
		}
	}


	unsigned int ReadProgressiveUnsignedInt() {
		int z = 1;
		while (ReadBit())
			z++;
		return ReadBitsInteger(z*8);
	}


	bool ReadBit() {
		bool b;
		if (m_iReadPosBytes >= (int)m_iaData.size()) {
			eprintf("CBitSet::ReadBit(): Error: End of BitSet reached.\n");
			abort();
		}
		b = (m_iaData[m_iReadPosBytes] & (1<<m_iReadPosExcess)) != 0;
		m_iReadPosExcess++;
		if (m_iReadPosExcess == 8) {
			m_iReadPosExcess = 0;
			m_iReadPosBytes++;
		}
		return b;
	}


	unsigned long ReadBitsInteger(int bits) {
		unsigned long u = 0;
		for (int z=0;z<bits;z++)
			if (ReadBit())
				u |= 1<<z;
		return u;
	}


	long ReadBitsSignedInteger(int bits) {
		unsigned long u = 0;
		for (int z=0;z<bits-1;z++)
			if (ReadBit())
				u |= 1<<z;
		if (ReadBit())
			return -((long)u);
		else
			return (long)u;
	}


	int GetReadPos() {
		return m_iReadPosBytes*8 + m_iReadPosExcess;
	}


	std::vector<unsigned char> m_iaData;
	int m_iExcessBits;
	int m_iReadPosBytes;
	int m_iReadPosExcess;
};


#endif


