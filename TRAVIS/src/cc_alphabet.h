/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_ALPHABET_H
#define CC_ALPHABET_H


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <vector>
#include "cc_bitset.h"


class CAlphabetEntry {
public:

	CAlphabetEntry()
		: m_iSymbol(0), m_iFrequency(0), m_iIndex(0) { }

	int m_iSymbol;
	int m_iFrequency;
	int m_iIndex;
};


class CAlphabet {
public:
	~CAlphabet() {
		for (int z=0;z<(int)m_oaAlphabet.size();z++)
			delete m_oaAlphabet[z];
	}

	void BuildAlphabet(const std::vector<int> &inp, bool verbose);
	void Export(CBitSet *bs, bool huffman, bool chr, bool verbose) const;
	void Import(CBitSet *bs, bool chr, bool verbose);
	int FindIndex(int symbol) const;
	void RecalcFrequencies(const std::vector<int> &ia);

	std::vector<CAlphabetEntry*> m_oaAlphabet;
	std::vector<CAlphabetEntry*> m_oaAlphabetSortFreq;
	std::vector<int> m_iaIndices;
};


#endif

