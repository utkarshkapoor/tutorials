/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_CUBEFRAME_H
#define CC_CUBEFRAME_H


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <vector>
#include <string>
#include <string.h>


class CCCAtom {
public:

	CCCAtom()
		: m_iOrd(0) { }


	int m_iOrd;
	std::string m_sLabel;
	double m_fCoord[3];
	double m_fRelCoord[3];
	long m_iCoord[3];
	long m_iRelCoord[3];
};


class CAtomSet {
public:

	CAtomSet()
		: m_bOrd(false), m_bLabels(false), m_sComment(NULL) {
		for (int z=0;z<3;z++) {
			m_faCOM[z] = 0;
			m_iaCOM[z] = 0;
		}
	}

	~CAtomSet() {
		for (int z=0;z<(int)m_oaAtoms.size();z++)
			delete m_oaAtoms[z];
		if (m_sComment != NULL)
			delete[] m_sComment;
	}


	bool ReadXYZ(FILE *a, int signi, FILE *ref);
	void WriteXYZ(FILE *a, int signi);
	bool SkipXYZ(FILE *a);


	bool m_bOrd;
	bool m_bLabels;
	int m_iSigni;
	double m_faCOM[3];
	int m_iaCOM[3];
	std::vector<CCCAtom*> m_oaAtoms;
	char *m_sComment;
};


class CCubeFrame {
public:
	CCubeFrame()
		: m_iResXY(0), m_iResYZ(0), m_iResXYZ(0), m_pAtoms(NULL) {
		for (int z=0;z<3;z++) {
			m_iRes[z] = 0;
			m_fMinVal[z] = 0;
			m_fMaxVal[z] = 0;
			m_fCenter[z] = 0;
			m_fStrideA[z] = 0;
			m_fStrideB[z] = 0;
			m_fStrideC[z] = 0;
			m_iCenter[z] = 0;
			m_iStrideA[z] = 0;
			m_iStrideB[z] = 0;
			m_iStrideC[z] = 0;
		}
	}

	~CCubeFrame() {
//		if (m_pAtoms != NULL)
//			delete m_pAtoms;
	}

	bool ReadFrame(FILE *a, int eps, int csigni, int asigni, bool verbose=false);
	bool SkipFrame(FILE *a, bool verbose=false) const;

	void WriteFrame(FILE *a, bool verbose=false);
	void WriteFrame_Double(FILE *a, bool verbose=false);

	int m_iEps;
	int m_iSigni;
	std::vector<double> m_faBin;
	std::vector<int> m_iaMantis;
	std::vector<char> m_iaExpo;
	int m_iRes[3];
	int m_iResXY;
	int m_iResYZ;
	int m_iResXYZ;
	double m_fMinVal[3];
	double m_fMaxVal[3];
	double m_fCenter[3];
	double m_fStrideA[3];
	double m_fStrideB[3];
	double m_fStrideC[3];
	long m_iCenter[3];
	long m_iStrideA[3];
	long m_iStrideB[3];
	long m_iStrideC[3];
	CAtomSet *m_pAtoms;
};


#endif

