/*****************************************************************************
    TRAVIS - Trajectory Analyzer and Visualizer
    http://www.travis-analyzer.de/

    Copyright (c) 2009-2018 Martin Brehm
                  2012-2018 Martin Thomas

    This file written by Martin Brehm.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/


#ifndef CC_HUFFTREE_H
#define CC_HUFFTREE_H


// This must always be the first include directive
#include "config.h"

#include "cc_tools.h"
#include <vector>
#include "cc_bitset.h"


class CHuffmanSymbol {
public:

	CHuffmanSymbol() 
		: m_iSymbol(0), m_pBitString(NULL) { }

	~CHuffmanSymbol() {
		for (int z=0;z<(int)m_oaChildren.size();z++)
			delete m_oaChildren[z];
		if (m_pBitString != NULL)
			delete m_pBitString;
	}

	void REC_BuildBitstrings(int depth);

	int m_iSymbol;
	int m_iDepth;
	int m_iFrequency;
	bool m_bVirtual;
	std::vector<CHuffmanSymbol*> m_oaChildren;
	CBitSet *m_pBitString;
};


class CHuffmanEstimator {

public:
	void Init(int alphasize);
	unsigned int EstimateBitLength(const std::vector<int> &ia);
	unsigned int EstimateBitLength(const std::vector<int> &ia, int i2);
	unsigned int EstimateBitLength(const std::vector<int> &ia, const std::vector<int> &ia2);
	unsigned int EstimateBitLength(const std::vector<int> &ia, const std::vector<int> &ia2, const std::vector<int> &ia3);
	void BuildBitLengthTable(const std::vector<int> &ia);
	std::vector<int> m_iaLength;

private:
	std::vector<unsigned int> m_iaTempFrequencies;
	std::vector<int> m_iaTempHeap;
	std::vector<int> m_iaTempParent;
	int m_iAlphaSize;
};


class CHuffmanTree {
public:

	CHuffmanTree()
		: m_pTree(NULL), m_iMaxBitLength(0) { }


	~CHuffmanTree() {
		if (m_pTree != NULL)
			delete m_pTree;
	}


	int DecodeSymbol(CBitSet *bs) const {
		CHuffmanSymbol *hs = m_pTree;
//		printf("********\n");
		while (hs->m_oaChildren.size() == 2)
			if (bs->ReadBit()) {
//				printf("A\n");
				hs = hs->m_oaChildren[1];
			} else {
//				printf("B\n");
				hs = hs->m_oaChildren[0];
			}
		return hs->m_iSymbol;
	}


	void Init(int alphasize);
	void BuildTree(bool canonical=false, bool showsymbols=false);
	void BuildPrelimTree();

	void REC_BuildBitstringLengths(int depth, CHuffmanSymbol *sym);

	int ExportTree(CBitSet *bs, bool chr, bool verbose);
	int REC_ExportTree(CBitSet *bs, CHuffmanSymbol *sym, int bits);

	void ImportTree(CBitSet *bs, bool chr, bool verbose);
	int REC_ImportTree(CBitSet *bs, CHuffmanSymbol *sym, int bits);

	void REC_PushCanonicalSymbol(CHuffmanSymbol *sym, CHuffmanSymbol *ts, unsigned long b, int depth);

	bool m_bCanonical;
	CHuffmanSymbol *m_pTree;
	int m_iMaxBitLength;
	std::vector<int> m_iaFrequencies;
	std::vector<int> m_iaLengths;
	std::vector<CBitSet*> m_oaBitStrings;
	std::vector<CHuffmanSymbol*> m_oaSymbols;
	std::vector<CHuffmanSymbol*> m_oaTempSymbols;
};



#endif


